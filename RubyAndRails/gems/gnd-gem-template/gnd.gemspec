Gem::Specification.new do |s|
  s.name               = "gnd"
  s.version            = "0.0.1"
  s.default_executable = "gnd"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Timothy Clark"]
  s.date = %q{2010-01-01}
  s.description = %q{A simple template gem}
  s.email = %q{graphicnetdesign@gmail.com}
  s.files = ["Rakefile", "lib/gnd.rb", "lib/hola/translator.rb", "bin/gnd"]
  s.test_files = ["test/test_gnd.rb"]
  s.homepage = %q{http://rubygems.org/gems/gnd}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.6.2}
  s.summary = %q{GND!}

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end

