class Person

  # Class variable
  @@count = 0

  # Instance variable
  attr_reader :age

  # Class method
  def self.total_count
    @@cont
    puts "Total number of person objects #{ @@count }"
  end

  # Class method
  def self.increment_count
    @@count += 1
  end

  def initialize(age)
    # Instance variable initialized
    @age = age
  end
end