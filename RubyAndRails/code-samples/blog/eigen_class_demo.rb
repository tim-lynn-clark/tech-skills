class Person

  # Class variable
  @@count = 0

  # Instance variable
  attr_reader :age

  # Class method
  def self.total_count
    @@cont
    puts "Total number of person objects #{ @@count }"
  end

  # Class method
  def self.increment_count
    @@count += 1
  end

  def initialize(age)
    # Instance variable initialized
    @age = age
  end
end

# Shows all class methods attached to the Person class'
# eigenclass (singleton class, metaclass)
puts "Person Class ID: #{Person}"
puts '<<-- Person Class: Class Inheritance -->>'
puts Person.class
puts '<<-- Person Class: Meta Class -->'
puts Person.singleton_class
puts '<<-- Person Class: Singleton Methods -->>'
puts Person.singleton_methods

# Notice that the 'tim' object's singleton class object actually
# has an object id, meaning it is a real object and its type is'Person'
tim = Person.new(10)
bob = Person.new(15)

puts ''
puts "Tim Object ID: #{tim}"
puts '<<-- Tim Object: Class Inheritance -->>'
puts tim.class
puts '<<-- Tim Object: Meta Class -->'
puts tim.singleton_class
puts '<<-- Tim Object: Inherited Singleton Methods -->>'
puts tim.singleton_class.singleton_methods
puts '<<-- Tim Object: Object Specific Singleton Methods -->>'
puts tim.singleton_methods

# Notice the 'bob' object's singleton class object has a different
# object id compared to the 'tim' object
puts ''
puts "Bob Object ID: #{bob}"
puts '<<-- Bob Object: Class Inheritance -->>'
puts bob.class
puts '<<-- Bob Object: Meta Class -->'
puts bob.singleton_class
puts '<<-- Bob Object: Inherited Singleton Methods -->>'
puts bob.singleton_class.singleton_methods
puts '<<-- Bob Object: Object Specific Singleton Methods -->>'
puts bob.singleton_class.singleton_methods

# Adding a singleton method to only the 'bob' object
def bob.double_age
  @age * 2
end

puts ''
puts "Tim Object ID: #{tim}"
puts "Tim Object Singleton ID: #{tim.singleton_class}"
puts '<<-- Tim Object: Inherited Singleton Methods -->>'
puts tim.singleton_class.singleton_methods
puts '<<-- Tim Object: Object Specific Singleton Methods -->>'
puts tim.singleton_methods
#puts tim.double_age # Throws an error being that the method iq was not added to the class but to the object

puts ''
puts "Bob Object ID: #{bob}"
puts "Bob Object Singleton ID: #{bob.singleton_class}"
puts '<<-- Bob Object: Inherited Singleton Methods -->>'
puts bob.singleton_class.singleton_methods
puts '<<-- Bob Object: Object Specific Singleton Methods -->>'
puts bob.singleton_methods
puts bob.double_age
# The question is, to what object is the singleton method 'iq' attached to?
# The object ids for both the 'bob' object as well as the 'eigenclass' are identical...