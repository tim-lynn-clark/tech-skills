names = %w(Bob, Sue, Aron, Joseph) # equal to ['Bob', 'Sue', 'Aron', 'Joseph']

# Block style 1
names.each do |name|
  puts "Hello #{name}"
end

# Block style 2
names.each {|name| puts "Hello #{name}"}

# Reusable Blocks

# Single-line lambda
say_hello = -> (name) {puts "Hello #{name}"}
names.each(&say_hello)

# Multi-line lambda
say_hello_again = lambda do |name|
  puts "Hello #{name}"
end
names.each(&say_hello_again)

# Proc
say_hello_yet_again = Proc.new do |name|
  puts "Hello #{name}"
end
names.each(&say_hello_yet_again)