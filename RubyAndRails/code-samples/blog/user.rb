
class User
  attr_accessor :first_name # creates both an accessor and a mutator
  attr_writer :last_name    # creates only a mutator
  attr_reader :last_name    # creates only an accessor

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    # Do some other object setup here
  end
end

tim = User.new('Tim', 'Clark')
