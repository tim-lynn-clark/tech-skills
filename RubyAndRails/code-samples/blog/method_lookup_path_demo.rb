# Question: Describe Ruby's method lookup path.
# Reason:   Competency with Ruby syntax / competency with object-oriented programming paradigms
# Answer:   Because Ruby, unlike Java and C#, allows for ‘mixins’ as well as ‘per-object behaviors’,
#           Ruby’s method lookup path is not linear along the inheritance chain. Ruby’s method
#           lookup path requires five steps:
#     1. Methods defined in the object’s singleton class (eigenclass, metaclass)
#     2. Modules mixed into the singleton class in reverse order of inclusion
#     3. Methods defined by the object’s class Modules included into the object’s class in reverse order of inclusion
#     4. Methods defined by the object’s superclass


module Wolf
  def howl
    "- Wolf Mixin: Howling at the moon\n" + super
  end
end

module Hound
  def howl
    "- Hound Mixin: Barking at a treed raccoon\n" + super
  end
end

module Mini
  def howl
    "- Mini Mixin: Yelping from the safety of my owner's purse\n" + super
  end
end

module AnkleBiter
  def howl
    "- AnkleBiter Mixin: Yelping as I attack what could certainly crush me\n" + super
  end
end

class Canine
  def howl
    "- Canine Class: Woof Woof\n"
  end
end

class Dog < Canine
  include Wolf
  include Hound

  def howl
    "- Dog Class: Bark Bark\n" + super
  end
end

kioshi = Dog.new
kioshi.extend(Mini)
kioshi.extend(AnkleBiter)

def kioshi.howl
  "- Kioshi (instance object specific behavior): Super Bark (earth rumbling)\n" + super
end

puts kioshi.howl