# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "binary_gap"
  spec.version       = '1.0'
  spec.authors       = ["Tim Clark"]
  spec.email         = ["graphicnetdesign@gmail.com"]
  spec.summary       = %q{Binary Gap Algorithm}
  spec.description   = %q{A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at both ends in the binary representation of N. For example, the number 9 has binary representation 1001 and contains a binary gap of length 2. The number 529 has binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3. The number 20 has binary representation 10100 and contains one binary gap of length 1. The number 15 has binary representation 1111 and has no binary gaps.}
  spec.homepage      = "http://memorytin.com/"
  spec.license       = "MIT"

  spec.files         = ['lib/binary_gap.rb']
  spec.executables   = ['bin/binary_gap']
  spec.test_files    = ['tests/test_binary_gap.rb']
  spec.require_paths = ["lib"]
end