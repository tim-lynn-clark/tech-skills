#http://test-unit.github.io/test-unit/en/Test/Unit/Assertions.html

require "./lib/binary_gap"
require "test/unit"

class TestBinaryGap < Test::Unit::TestCase

  def test_binary_gap_9
    gap_length = BinaryGap.find_gaps 9
    assert_equal(1, gap_length.length)
    assert_equal(2, gap_length[0])
  end

  def test_binary_gap_529
    gap_length = BinaryGap.find_gaps 529
    assert_equal(2, gap_length.length)
    assert_equal(4, gap_length[0])
    assert_equal(3, gap_length[1])
  end

  def test_binary_gap_20
    gap_length = BinaryGap.find_gaps 20
    assert_equal(1, gap_length.length)
    assert_equal(1, gap_length[0])
  end

  def test_binary_gap_15
    gap_length = BinaryGap.find_gaps 15
    assert_equal(0, gap_length.length)
  end

end