class BinaryGap

  def self.find_gaps(num)
    num_binary = num.to_s 2
    puts num_binary

    counting_active = false
    gap_list = []
    current_gap_index = -1
    current_gap_count = 0


    num_binary.chars.each do |value|

      value = value.to_i

      if value == 1
        # =1 reset for counting
        if counting_active && current_gap_count > 0
          # store current_gap_count at current_gap_index in gap array
          # increment current_gap_index
          current_gap_index +=1
          gap_list[current_gap_index] = current_gap_count
          # reset current_gap_count and
          current_gap_count = 0
        else
          # set counting active
          counting_active = true
        end
      elsif value == 0
        # if next=0 add to count
        if counting_active
          # increment current_gap_count
          current_gap_count += 1
        end
      end

    end

    gap_list
  end

end