
if ARGV.length == 0
  puts 'You must provide a file path to be read.'
  exit 1
end

filename = ARGV.first

txt = open(filename)

puts "Here are the contents of #{filename}..."

# Read entire file and print
# print txt.read

# Read file line-by-line and print each line
txt.each_with_index do |line, index|
  puts "Line #{index}: #{line}"
end