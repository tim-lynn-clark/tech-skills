Feature: Priority Queue API
  Definition of the Priority Queue's API

  Scenario: Count should be accessible and default to empty
    Given I have a Priority Queue
    When I call the Count property
    Then I should receive back a Count of 0

  Scenario: Allows a new item to be Inserted
    Given I have a Priority Queue
    When I add a new item 'Task001' 1 'This is the first task'
    When I call the Count property
    Then I should receive back a Count of 1

  Scenario: New items inserted should be organized based on Priority
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    Then I should get a Peek at 'Task001' 5 'This is the first task'

  Scenario: Should not matter how deep in the heap an item is inserted, the queue should remain prioritized
    Given I have a Priority Queue
    When I add a new item 'Task002' 6 'This is the second task'
    When I add a new item 'Task003' 2 'This is the second task'
    When I add a new item 'Task004' 1 'This is the second task'
    When I add a new item 'Task005' 1 'This is the second task'
    When I add a new item 'Task006' 3 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    When I add a new item 'Task007' 3 'This is the second task'
    When I add a new item 'Task008' 1 'This is the second task'
    When I add a new item 'Task009' 2 'This is the second task'
    Then I should get a Peek at 'Task002' 6 'This is the second task'
    
  Scenario: Allows a Peek at the highest priority item without removing it
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    Then I should get a Peek at 'Task001' 5 'This is the first task'
    When I call the Count property
    Then I should receive back a Count of 2

  Scenario: Returns Top (highest priority item) and removes it
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    Then I should get a Top of 'Task001' 5 'This is the first task'
    When I call the Count property
    Then I should receive back a Count of 1

  Scenario: Removes an element
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    When I Remove the item 'Task002'
    When I call the Count property
    Then I should receive back a Count of 1
    Then I should get a Top of 'Task001' 5 'This is the first task'

  Scenario: When an element is removed the queue should reprioritize
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    When I Remove the item 'Task001'
    When I call the Count property
    Then I should receive back a Count of 1
    Then I should get a Top of 'Task002' 2 'This is the second task'

  Scenario: Elements should allow their Value to be updated
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    When I Update the item 'Task001' 10 'This is the first task UPDATED'
    When I call the Count property
    Then I should receive back a Count of 2
    Then I should get a Top of 'Task001' 10 'This is the first task UPDATED'

  Scenario: Elements should allow their Priority to be updated and the queue reprioritized
    Given I have a Priority Queue
    When I add a new item 'Task002' 2 'This is the second task'
    When I add a new item 'Task001' 5 'This is the first task'
    When I Update the item 'Task001' 1 'This is the first task UPDATED'
    When I call the Count property
    Then I should receive back a Count of 2
    Then I should get a Top of 'Task002' 2 'This is the second task'