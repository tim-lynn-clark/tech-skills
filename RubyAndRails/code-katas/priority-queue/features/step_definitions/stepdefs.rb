require_relative '../../priority_queue'

Given('I have a Priority Queue') do
  @queue = PriorityQueue.new
end

When('I call the Count property') do
  @actual_count = @queue.count
end

Then('I should receive back a Count of {int}') do |expected_answer|
  expect(@actual_count).to eq(expected_answer)
end

When('I add a new item {string} {int} {string}') do |key, priority, value|
  @queue.insert(key, priority, value)
end

Then('I should get a Peek at {string} {int} {string}') do |key, priority, value|
  top_item = @queue.peek

  expect(top_item.key).to eq(key)
  expect(top_item.priority).to eq(priority)
  expect(top_item.value).to eq(value)
end

Then('I should get a Top of {string} {int} {string}') do |key, priority, value|
  top_item = @queue.top

  expect(top_item.key).to eq(key)
  expect(top_item.priority).to eq(priority)
  expect(top_item.value).to eq(value)
end

When('I Remove the item {string}') do |key|
  @queue.remove key
end

When('I Update the item {string} {int} {string}') do |key, priority, value|
  @queue.update(key, priority, value)
end