# API
#   top() ->  PriorityItem
#   peek() -> PriorityItem
#   insert(key, priority, value)
#   remove(key)
#   update(key, priority, value)
#
# Contract
#   The top element returned by the queue is always the element with the highest
#   priority currently stored in the queue
#
# Data Model
#   An array whose elements are the items stored in the heap
#   A 'max-heap' will be used, we assume a higher priority is the larger number
#
# Invariants
#   Every element has two children. For the element at position i,
#   its children are located at indicies (2*i)+1 and 2*(i+1)
#
#   Every element has higher priority than its children


class PriorityQueue

  def initialize
    # Determines how many heap branches are to be made
    # out of the storage array
    @branching_factor = 2
    @queue = []
    @priority_item_struct = Struct.new(:key, :priority, :value)
  end

  def count
    @queue.length
  end

  def insert(key, priority, value)
    @queue.push @priority_item_struct.new(key, priority, value)
    bubble_up
    @queue
  end

  def peek
    @queue[0]
  end

  def top
    # If queue is empty return nil
    return nil if @queue.empty?

    # Obtain reference to last node in queue
    # Return that node if queue is now empty
    last_node = @queue.pop
    return last_node if @queue.empty?

    # Get a reference to first node of array
    # Place last node in its place and push down
    # is most likely has the lowest priority and
    # needs to move back down the array
    top_node = @queue[0]
    @queue[0] = last_node
    push_down

    # Return top node
    top_node
  end

  def remove(key)
    results = find_node key
    index = results[0]
    node = results[1]

    @queue.delete_at index

    # Return node for convenience
    node
  end

  def update(key, priority, value)
    results = find_node key
    return nil unless results

    index = results[0]
    node = results[1]

    # Update value if necessary
    node.value = value unless node.value == value

    # If priority has not changed, there is no need to sort
    return if node.priority == priority

    # If priority has changed, record the node's current priority
    # and assign the new priority to it
    old_priority = node.priority
    node.priority = priority

    # Check old and new priority to see if we need to
    # bubble up or push down
    push_down(index) if priority < old_priority
    bubble_up(index) if priority > old_priority

    # Return node for convenience
    node
  end

  private

  def bubble_up(index = (@queue.length - 1))
    # Start from the node at the given index
    # (last element in the heap array by default)
    # Store a reference to the current node
    current_node = @queue[index]
    # Start parent index at current node's index
    parent_index = index

    # Check to see if the current node is at the root of the heap
    # if so, no need to bubble up as it has no parent
    while parent_index.positive?
      # Store index as current node's index
      current_index = parent_index
      # Compute the index of the current node's parent within the heap
      parent_index = get_parent_index(parent_index)

      # If parent node's priority is NOT less than that of current node
      # we break out of the iteration as we do not need to move the current node
      unless @queue[parent_index].priority < @queue[current_index].priority
        break
      end

      # If parent node's priority is less than that of current node
      # move parent node into current node's location
      @queue[current_index] = @queue[parent_index]

      # move current node up to parent node's location
      @queue[parent_index] = current_node
    end
  end

  def push_down(index = 0)
    # Start from the node at the given index
    # (first element in the heap array by default)
    # Store a reference to the current node
    current_node = @queue[index]
    current_index = index

    # See if current node is a leaf in the heap
    # Leaves have no children and therefore cannot be pushed down
    leaf_index = first_leaf_index
    while current_index < leaf_index
      # Locate a child of the current node with the highest priority
      # and store a reference to it
      priority_child = highest_priority_child(current_index)

      # discontinue if there are no priority children
      break if priority_child.nil?

      # Obtain the index and a reference to the highest priority child node
      child_index = priority_child[0]
      child = priority_child[1]

      # If the parent node's priority is higher than that of
      # its highest priority child, there is no need to move it
      break unless child.priority > current_node.priority

      # If the child node has a higher priority than that of
      # its parent, swap them around
      @queue[current_index] = @queue[child_index]
      current_index = child_index
      # move current node down to child node's location
      @queue[current_index] = current_node
    end
  end

  def get_parent_index(index)
    (index - 1) / @branching_factor
  end

  def first_leaf_index
    (@queue.length - 2) / @branching_factor + 1
  end

  def highest_priority_child(index)
    # Calculate the indexes of all children of the index provided
    # based on the branching factor given to the heap
    indices = []
    @branching_factor.times do |i|
      child_index = @branching_factor * index + i
      indices.push child_index
    end

    # Obtain a reference to each child node with their associated index
    nodes = []
    indices.each do |child_index|
      nodes.push [child_index, @queue[child_index]]
    end

    # Iterate over the children to find the one with the highest priority
    highest_priority_node = nil
    nodes.each do |node|
      if highest_priority_node.nil?
        highest_priority_node = node
        next
      end

      if highest_priority_node[1].priority < node[1].priority
        highest_priority_node = node
      end
    end

    # Return the highest priority child found
    highest_priority_node
  end

  def find_node(key)
    # TODO: Improvement might be made by storing the key and the index in a hash
    # This will affect the methods:
    # - insert
    # - remove
    # - top
    @queue.each_index do |index|
      node = @queue[index]
      return index, node if node.key == key
    end

    nil
  end

end