require_relative '../lib/madlibs'

RSpec.describe Madlibs::Story do
  it "includes a :programming key" do
    expect(Madlibs::Story.stories).to include(:programming)
  end

  it "'programming' key should work" do
    story = Madlibs::Story.new "programming"
    expect(story.story_text).to start_with("Our favorite").and end_with(".")
  end

  it "'none' key should cause the story to be nil" do
    expect(Madlibs::Story.new("none").story_text).to be_nil
  end

  it "possesses at least one 'word identifier'" do
    story = Madlibs::Story.new "programming"
    expect(story.word_identifiers.length).to be > 0
  end
end