require_relative '../lib/madlibs'

RSpec.describe Madlibs::Game do
  it "version is #{Madlibs::VERSION}" do
    expect(Madlibs::Game.version).to eql("Version #{Madlibs::VERSION}")
  end

  it "should start with 'programming'" do
    expect(Madlibs::Game.stories).to start_with("Name").and end_with("\n")
  end
end