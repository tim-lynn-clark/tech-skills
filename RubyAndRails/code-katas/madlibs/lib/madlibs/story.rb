require 'active_support/inflector'

module Madlibs
  class Story

    # Class properties and behaviors
    def self.stories
      @@stories
    end

    @@stories = {
        programming: "Our favorite language is ((gem_one)). We think ((gem_one)) is better than ((gem_two)).",
        food: "I had a ((adjective)) sandwich for lunch today. It dripped allover my ((body_part)) and ((noun))."
    }

    # Instances properties and behaviors
    attr_reader :story_identifier
    attr_reader :story_text
    attr_reader :story_text_updated
    attr_reader :word_identifiers
    attr_reader :word_substitutions

    def initialize(story_identifier)
      @identifier_regex = /\(\((\w*)\)\)/
      @story_identifier = story_identifier.parameterize.underscore.to_sym
      @story_text = Story.stories[@story_identifier]
      @story_text_updated = ""
      @word_substitutions = nil
      parse_word_identifiers
    end

    def create(word_substitutions)
      @word_substitutions = word_substitutions
      substitute_words
    end

    private
    def parse_word_identifiers
      @word_identifiers = []

      unless @story_text
        return
      end

      @story_text.scan(@identifier_regex) do |identifier|
        unless @word_identifiers.include? identifier[0]
          @word_identifiers.push identifier[0]
        end
      end
      @word_identifiers
    end

    def substitute_words
      @story_text_updated = @story_text.clone
      @word_substitutions.each_pair do |key, value|
        word_regex = /\(\(#{key}\)\)/
        @story_text_updated = @story_text_updated.gsub(word_regex, value)
      end
    end
  end
end