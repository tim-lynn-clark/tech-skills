require_relative '../../lib/madlibs/version'
require_relative '../../lib/madlibs/story'

module Madlibs
  class Game
    def self.version
      "Version #{Madlibs::VERSION}"
    end

    def self.stories
      story_list = ""
      Madlibs::Story.stories.each_pair do |key, value|
        story_list += "Name: #{key}, Story: #{value} \n"
      end
      story_list
    end

    def initialize(story)
      @story = Madlibs::Story.new(story)

      unless @story.story_text
        puts "The story name you provided does not exist."
        puts "Please select one of the following stories:"
        puts Game.stories
        return
      end

      puts "The story you have selected to manipulate is: #{@story.story_identifier}"
    end

    def play
      get_word_replacements
    end

    private
    def get_word_replacements
      word_replacements = {}
      @story.word_identifiers.each do |word|
        puts "Provide one: " + word
        word_replacements[word] = STDIN.gets.chomp
      end

      @story.create word_replacements
      puts @story.story_text_updated
    end
  end
end