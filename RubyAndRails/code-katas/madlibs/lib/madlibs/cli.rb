require 'thor'
require_relative '../madlibs'

module Madlibs
  class CLI < Thor
    desc "version", "Current Madlibs application version"
    method_option :aliases => "-v"
    def version
      puts Madlibs::Game.version
    end

    desc "stories", "Lists the available MadLib stories you can choose from"
    def stories
      puts Madlibs::Game.stories
    end

    desc "play STORY", "Begins playing the game with the selected story"
    method_options :aliases => "-p"
    def play(story)

      game = Madlibs::Game.new story
      game.play

    end
  end
end