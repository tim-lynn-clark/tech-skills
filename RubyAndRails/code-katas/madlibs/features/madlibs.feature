Feature: Game
  In order to play game
  As a CLI
  I want to

  Scenario: Madlibs version
    When I run `madlibs version`
    Then the output should contain "Version"

  Scenario: Story list
    When I run `madlibs stories`
    Then the output should contain "programming"

  Scenario: Play game bad story name
    When I run `madlibs play none`
    Then the output should contain "The story name you provided does not exist"

  Scenario: Play game good story name
    When I run `madlibs play programming`
    Then the output should contain "The story you have selected to play with is"