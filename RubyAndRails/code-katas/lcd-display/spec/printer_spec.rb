require_relative '../lib/lcd_display'

RSpec.describe LcdDisplay::Printer do
  it "prints 0 Size 1" do
    printer = LcdDisplay::Printer.new('0', 1)
    number_text = printer.print
    puts number_text
    expect(number_text).to include(' - ').and include('| |').and include('   ')
  end

  it "prints 0 Size 2" do
    printer = LcdDisplay::Printer.new('0', 2)
    number_text = printer.print
    puts number_text
    expect(number_text).to include(' -- ').and include('|  |').and include('    ')
  end

  it "prints 0 Size 3" do
    printer = LcdDisplay::Printer.new('0', 3)
    number_text = printer.print
    puts number_text
    expect(number_text).to include(' --- ').and include('|   |').and include('     ')
  end

  it "prints 1 Size 1" do
    printer = LcdDisplay::Printer.new('1', 1)
    number_text = printer.print
    puts number_text
    expect(number_text).to include('   ').and include('  |').and include('   ')
  end

  it "prints 1 Size 2" do
    printer = LcdDisplay::Printer.new('1', 2)
    number_text = printer.print
    puts number_text
    expect(number_text).to include('    ').and include('   |').and include('    ')
  end

  it "prints 1 Size 3" do
    printer = LcdDisplay::Printer.new('1', 3)
    number_text = printer.print
    puts number_text
    expect(number_text).to include('    ').and include('   |').and include('    ')
  end

  it "prints 2 Size 2" do
    printer = LcdDisplay::Printer.new('2', 2)
    number_text = printer.print
    puts number_text
    expect(number_text).to include(' -- ').and include('   |').and include('|   ')
  end

  it "prints 3 Size 2" do
    printer = LcdDisplay::Printer.new('3', 2)
    number_text = printer.print
    puts number_text
    expect(number_text).to include(' -- ').and include('   |').and include('   |')
  end
end