module LcdDisplay
  VERSION = "1.0.0"

  # Final version of the LCD Printer is a combination of various elegant solutions
  # found online from some very awesome developers
  # My original solution was not even close to this elegant
  class Printer
    SEPARATOR = ' '
    VERTICAL = 2
    HORIZONTAL = 3

    def initialize(number, size = 2)
      @size = size.to_i
      @number = number
      @digit_parts ={
          horY:  ' ' + '-' * @size + ' ',             # Horizontal Dash       ' - '
          horN:  ' ' + ' ' * @size + ' ',             # Horizontal Space      '   '
          verDD: ['|' + ' ' * @size + '|'] * @size ,  # Vertical Double Dash  '| |'
          verRD: [' ' + ' ' * @size + '|'] * @size ,  # Vertical Right Dash   '  |'
          verLD: ['|' + ' ' * @size + ' '] * @size ,  # Vertical Left Dash    '|  '
      }
      @coded_numbers = [
          [@digit_parts[:horY], @digit_parts[:verDD], @digit_parts[:horN], @digit_parts[:verDD], @digit_parts[:horY]], # 0
          [@digit_parts[:horN], @digit_parts[:verRD], @digit_parts[:horN], @digit_parts[:verRD], @digit_parts[:horN]], # 1
          [@digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horY], @digit_parts[:verLD], @digit_parts[:horY]], # 2
          [@digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horY]], # 3
          [@digit_parts[:horN], @digit_parts[:verDD], @digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horN]], # 4
          [@digit_parts[:horY], @digit_parts[:verLD], @digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horY]], # 5
          [@digit_parts[:horY], @digit_parts[:verLD], @digit_parts[:horY], @digit_parts[:verDD], @digit_parts[:horY]], # 6
          [@digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horN], @digit_parts[:verRD], @digit_parts[:horN]], # 7
          [@digit_parts[:horY], @digit_parts[:verDD], @digit_parts[:horY], @digit_parts[:verDD], @digit_parts[:horY]], # 8
          [@digit_parts[:horY], @digit_parts[:verDD], @digit_parts[:horY], @digit_parts[:verRD], @digit_parts[:horY]], # 9
      ]
    end

    def print
      number_text = ""
      (0...(VERTICAL * @size + HORIZONTAL)).each do |i|
        number_text += @number.chars.collect { |number| @coded_numbers[number.to_i].flatten[i] }.join SEPARATOR
        number_text += "\n"
      end
      number_text
    end

  end
end