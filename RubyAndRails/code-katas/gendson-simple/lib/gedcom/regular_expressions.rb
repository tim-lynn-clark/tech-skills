module Gendson
  module Gedcom
    module RegularExpressions
      # Used to parse each line of a GedCom file as they are read from the file
      # Locates three groups:
      # 1. The depth within the current tree
      # 2. The GedCom 'tag' identifying the data type of the line
      # 3. The data
      PARSE_LINES = /^(\d+)\s+(\w+|@\w+\d+@)\s*(.*)/

      # Used to parse out GedCom IDs located in the groups pulled by the PARSE_LINE regex
      # Locates two groups:
      # 1. Character designating the type of ID (I-Individual, F-Family, SUBM-Submitter)
      # 2. Actual numeric value of the ID
      PARSE_IDS = /@(\D+\d+)@/
    end
  end
end