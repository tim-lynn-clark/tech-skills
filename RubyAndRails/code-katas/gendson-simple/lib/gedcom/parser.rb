require 'json'
require_relative '../../lib/gedcom/regular_expressions'

module Gendson
  module Gedcom
    class Parser
      def initialize(gedcom_file)
        @gedcom_file = gedcom_file
        @gendson = {}
      end

      def start
        root_node = nil
        previous_node = nil

        File.readlines(@gedcom_file).each do |line|
          node = parse_line line

          next unless node

          case node[:depth].to_i
          when 0
            @gendson[node[:tag]] = {}
            @gendson[node[:tag]][:value] = node[:value] unless node[:value].empty?
            root_node = @gendson[node[:tag]]
          when 1
            root_node[node[:tag]] = {}
            root_node[node[:tag]][:value] = node[:value] unless node[:value].empty?
            # root_node[node[:tag]].push node
            previous_node = root_node[node[:tag]]
          when 2
            previous_node[node[:tag]] = {} unless previous_node[node[:tag]]
            if previous_node[node[:tag]][:value] && !node[:value].empty?
              value = previous_node[node[:tag]][:value] + node[:value]
            else
              value = node[:value]
            end
            previous_node[node[:tag]][:value] = value
          end
        end

        puts @gendson.to_json
      end

      private

      def parse_line(line)
        matches = Gendson::Gedcom::RegularExpressions::PARSE_LINES.match(line)

        if matches
          # original_line = matches[0]
          depth = matches[1]
          value_one = matches[2]
          value_two = matches[3]

          determine_tag_order value_one, value_two, depth
        end
      end

      def determine_tag_order(value_one, value_two, depth)
        value_matches = Gendson::Gedcom::RegularExpressions::PARSE_IDS.match(value_one)

        if value_matches
          tag = value_two
          value = value_matches[1]
        else
          value_matches = Gendson::Gedcom::RegularExpressions::PARSE_IDS.match(value_two)
          if value_matches
            tag = value_one
            value = value_matches[1]
          else
            tag = value_one
            value = value_two
          end

        end

        {
            depth: depth,
            tag: tag,
            value: value
        }
      end

      def add_individual

      end

      def add_family

      end
    end
  end
end
