require_relative '../../lib/gedcom/tags/tags'

module Gendson
  module Gedcom
    class TagFactory
      def tag_created?
        @tag_created
      end

      attr_reader :tag
      attr_reader :message

      def initialize
        @tag_created  = true
        @tag          = nil
        @message      = ''
      end

      def build(raw_node)
        @raw_node = raw_node
        identify_node_type
      end

      private
      def identify_node_type
        unless @raw_node[0][:depth] == 0
          @message = 'Raw node did not contain a root tag of depth 0.'
          return
        end

        case @raw_node[0][:tag]
          when 'HEAD'
            @tag = Gendson::Gedcom::Tags::Header.new(@raw_node)
          when 'SUBM'
            @tag = Gendson::Gedcom::Tags::Submitter.new(@raw_node)
          when 'INDI'
            @tag = Gendson::Gedcom::Tags::Individual.new(@raw_node)
          when 'FAM'
            @tag = Gendson::Gedcom::Tags::Family.new(@raw_node)
          when 'SOUR'
            @tag = Gendson::Gedcom::Tags::Source.new(@raw_node)
          else
            @tag_created = false
            @message = 'Raw node provided did not have an identifiable root node tag.'
        end
      end
    end
  end
end
