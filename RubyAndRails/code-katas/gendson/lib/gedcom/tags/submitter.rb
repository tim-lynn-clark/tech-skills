require_relative '../../../lib/gedcom/tags/tag'
require_relative '../../../lib/gedcom/tags/address'

module Gendson
  module Gedcom
    module Tags
      class Submitter < Gendson::Gedcom::Tags::Tag
        # SUBM: SUBMITTER - An individual or organization who contributes genealogical data to a file or transfers it to someone else.
        #
        # Tags found as children of the SUBM tag
        # NAME: NAME - A word or combination of words used to help identify an individual, title, or other item. More than one NAME line should be used for people who were known by multiple names.
        # ADDR: ADDRESS - The contemporary place, usually required for postal purposes, of an individual, a submitter of information, a repository, a business, a school, or a company.
        #     CONC: CONCATENATION - An indicator that additional data belongs to the superior value. The information from the CONC value is to be connected to the value of the superior preceding line without a space and without a carriage return and/or new line character. Values that are split for a CONC tag must always be split at a non­space. If the value is split on a space the space will be lost when concatenation takes place. This is because of the treatment that spaces get as a GEDCOM delimiter, many GEDCOM values are trimmed of trailing spaces and some systems look for the first non­space starting after the tag to determine the beginning of the value.
        # PHON: PHONE - A unique number assigned to access a specific telephone.
        # RIN: REC_ID_NUMBER - A number assigned to a record by an originating automated system that can be used by a receiving system to report results pertaining to that record.

        attr_reader   :tag, :id, :name, :address, :phone, :record_id_number, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag                = 'SUBM'
          @id                 = raw_node[0][:value]
          @name               = ''
          @address            = ''
          @phone              = ''
          @record_id_number   = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node

          sorter          = Gendson::Gedcom::NodeSorter.new(1, @raw_node)
          @related_nodes  = sorter.related_node_sets

          @related_nodes.each do |node|
            process_node node
          end
        end

        def as_hash
          {
              ID:   @id,
              NAME: @name,
              RIN:  @record_id_number,
              PHON: @phone,
              ADDR: @address ? @address.as_hash : nil
          }
        end

        private

        def process_node(node)
          case node[0][:tag]
            when 'NAME'
              @name               = node[0][:value]
            when 'ADDR'
              @address            = Gendson::Gedcom::Tags::Address.new(node)
            when 'PHON'
              @phone              = node[0][:value]
            when 'RIN'
              @record_id_number   = node[0][:value]
            else
              @unprocessed_tags.push node[0][:value]
              unknown_tag_warning @tag, node[0]
          end
        end
      end
    end
  end
end