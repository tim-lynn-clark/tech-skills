require_relative '../../../lib/gedcom/tags/tag'

module Gendson
  module Gedcom
    module Tags
      class Gedcom < Gendson::Gedcom::Tags::Tag
        # GEDC: GEDCOM - Information about the use of GEDCOM in a transmission.
        #
        # Tags found as children of GEDC tags
        # VERS: VERSION - Indicates which version of a product, item, or publication is being used or referenced.
        # FORM: FORMAT - An assigned name given to a consistent format in which information can be conveyed.

        attr_reader :tag, :version, :format, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag      = 'GEDC'
          @version  = ''
          @format   = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node

          process_raw_node
        end

        def as_hash
          {
              VERS: @version,
              FORM: @format
          }
        end

        private

        def process_raw_node
          @raw_node.each do |subnode|
            case subnode[:tag]
              when 'VERS'
                @version  = subnode[:value]
              when 'FORM'
                @format   = subnode[:value]
              else
                @unprocessed_tags.push subnode[:value]
                unknown_tag_warning @tag, subnode
            end
          end
        end
      end
    end
  end
end
