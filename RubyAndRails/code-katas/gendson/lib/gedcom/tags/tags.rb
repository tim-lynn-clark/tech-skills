require_relative '../../../lib/gedcom/tags/header'
require_relative '../../../lib/gedcom/tags/gedcom'
require_relative '../../../lib/gedcom/tags/note'
require_relative '../../../lib/gedcom/tags/submitter'
require_relative '../../../lib/gedcom/tags/individual'
require_relative '../../../lib/gedcom/tags/family'
require_relative '../../../lib/gedcom/tags/source'

module Gendson
  module Gedcom
    module Tags
      # Used as a library reference to pull in all tag classes
      # used in the TagFactory
    end
  end
end
