require_relative '../../../lib/gedcom/tags/tag'

module Gendson
  module Gedcom
    module Tags
      class LifeEvent < Gendson::Gedcom::Tags::Tag
        # BURI: BURIAL - The event of the proper disposing of the mortal remains of a deceased person.
        #
        # Tags found as children of life events (birth, death, burial)
        # DATE: DATE - The time of an event in a calendar place.
        # PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # AGE: AGE - The age of the individual at the time an event occurred, or the age listed in the document.

        attr_reader :tag, :date, :place, :age, :unprocessed_tags

        def initialize(event_tag, raw_node)
          # Strings
          @tag    = event_tag
          @date   = ''
          @place  = ''
          @age    = ""
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node

          process_raw_node
        end

        def as_hash
          {
              DATE: @date,
              PLAC: @place,
              AGE:  @age
          }
        end

        private

        def process_raw_node
          @raw_node.each do |subnode|
            case subnode[:tag]
              when 'DATE'
                @date   = subnode[:value]
              when 'PLAC'
                @place  = subnode[:value]
              when 'AGE'
                @age    = subnode[:value]
              else
                @unprocessed_tags.push subnode[:value]
                unknown_tag_warning @tag, subnode
            end
          end
        end
      end
    end
  end
end
