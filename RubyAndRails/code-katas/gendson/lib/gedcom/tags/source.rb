require_relative '../../../lib/gedcom/tags/tag'

module Gendson
  module Gedcom
    module Tags
      class Source < Gendson::Gedcom::Tags::Tag
        attr_reader :tag, :id, :page, :text, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag              = 'SOUR'
          @id               = raw_node[0][:value] ? raw_node[0][:value] : ''
          @page             = ''
          @text             = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node

          process_raw_node
        end

        def as_hash
          {
              ID:   @id,
              PAGE: @page,
              TEXT: @text
          }
        end

        private

        def process_raw_node
          @raw_node.each do |subnode|
            case subnode[:tag]
              when 'PAGE'
                @page   = subnode[:value]
              when 'TEXT'
                @text  = subnode[:value]
              else
                @unprocessed_tags.push subnode[:value]
                unknown_tag_warning @tag, subnode
            end
          end
        end
      end
    end
  end
end
