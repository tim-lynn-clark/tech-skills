require_relative '../../../lib/gedcom/tags/tag'
require_relative '../../../lib/gedcom/tags/life_event'

module Gendson
  module Gedcom
    module Tags
      class Individual  < Gendson::Gedcom::Tags::Tag
        # INDI: INDIVIDUAL - A person
        #
        # Tags found as children of the INDI tag
        # NAME: NAME - A word or combination of words used to help identify an individual, title, or other item. More than one NAME line should be used for people who were known by multiple names.
        # TITL: TITLE - A description of a specific writing or other work, such as the title of a book when used in a source context, or a formal designation used by an individual in connection with positions of royalty or other social status, such as Grand Duke.
        # SEX: SEX - Indicates the sex of an individual­­male or female.
        # CHR: CHRISTENING - The religious event (not LDS) of baptizing and/or naming a child.
        #     DATE: DATE - The time of an event in a calendar format.
        #     PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # BIRT: BIRTH - The event of entering into life.
        #     DATE: DATE - The time of an event in a calendar format.
        #     PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # DEAT: DEATH - The event when mortal life terminates.
        #     DATE: DATE - The time of an event in a calendar format.
        #     PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # BURI: BURIAL - The event of the proper disposing of the mortal remains of a deceased person.
        #     DATE: DATE - The time of an event in a calendar format.
        #     PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # FAMS: FAMILY_SPOUSE - Identifies the family in which an individual appears as a spouse.
        # FAMC: FAMILY_CHILD - Identifies the family in which an individual appears as a child.
        # RIN: REC_ID_NUMBER - A number assigned to a record by an originating automated system that can be used by a receiving system to report results pertaining to that record.
        # NOTE: NOTE - Additional information provided by the submitter for understanding the enclosing data.
        #     CONC: CONCATENATION - An indicator that additional data belongs to the superior value. The information from the CONC value is to be connected to the value of the superior preceding line without a space and without a carriage return and/or new line character. Values that are split for a CONC tag must always be split at a non­space. If the value is split on a space the space will be lost when concatenation takes place. This is because of the treatment that spaces get as a GEDCOM delimiter, many GEDCOM values are trimmed of trailing spaces and some systems look for the first non­space starting after the tag to determine the beginning of the value.

        attr_reader   :tag, :id, :name, :title, :sex, :family_spouse, :family_child,
                      :record_id_number, :birth, :death, :burial, :christening, :note, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag              = 'INDI'
          @id               = raw_node[0][:value]
          @name             = ''
          @title            = ''
          @sex              = ''
          @family_spouse    = ''
          @family_child     = ''
          @record_id_number = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node
          @birth        = nil
          @death        = nil
          @burial       = nil
          @christening  = nil
          @note         = nil

          sorter          = Gendson::Gedcom::NodeSorter.new(1, @raw_node)
          @related_nodes  = sorter.related_node_sets

          @related_nodes.each do |node|
            process_node node
          end
        end

        def as_hash
          {
              ID:   @id,
              NAME: @name,
              TITL: @title,
              SEX:  @sex,
              FAMS: @family_spouse,
              FAMC: @family_child,
              RIN:  @record_id_number,
              BIRT: @birth ? @birth.as_hash : nil,
              DEAT: @death ? @death.as_hash : nil,
              BURI: @burial ? @burial.as_hash : nil,
              CHR:  @christening ? @christening.as_hash : nil,
              NOTE: @note ? @note.as_hash : nil
          }
        end

        private

        def process_node(node)
          case node[0][:tag]
            when 'NAME'
              @name               = node[0][:value]
            when 'TITL'
              @title              = node[0][:value]
            when 'SEX'
              @sex                = node[0][:value]
            when 'FAMS'
              @family_spouse      = node[0][:value]
            when 'FAMC'
              @family_child       = node[0][:value]
            when 'RIN'
              @record_id_number   = node[0][:value]
            when 'BIRT'
              @birth  = Gendson::Gedcom::Tags::LifeEvent.new('BIRT', node)
            when 'DEAT'
              @death  = Gendson::Gedcom::Tags::LifeEvent.new('DEAT', node)
            when 'BURI'
              @burial = Gendson::Gedcom::Tags::LifeEvent.new('BURI', node)
            when 'CHR'
              @burial = Gendson::Gedcom::Tags::LifeEvent.new('CHR', node)
            when 'NOTE'
              @note   = Gendson::Gedcom::Tags::Note.new(node)
            else
              @unprocessed_tags.push node[0][:value]
              unknown_tag_warning @tag, node[0]
          end
        end
      end
    end
  end
end
