require_relative '../../../lib/gedcom/tags/tag'
require_relative '../../../lib/gedcom/node_sorter'
require_relative '../../../lib/gedcom/tags/gedcom'
require_relative '../../../lib/gedcom/tags/note'

module Gendson
  module Gedcom
    module Tags
      class Header < Gendson::Gedcom::Tags::Tag
        # HEAD: HEADER - Identifies information pertaining to an entire GEDCOM transmission.
        #
        # Tags found as children of HEAD tags
        # SOUR: SOURCE - The initial or original material from which information was obtained.
        # DEST: DESTINATION - A system receiving data.
        # DATE: DATE - The time of an event in a calendar format.
        # FILE: FILE - An information storage place that is ordered and arranged for preservation and reference.
        # CHAR: CHARACTER - An indicator of the character set used in writing this automated information.
        # GEDC: GEDCOM - Information about the use of GEDCOM in a transmission.
        #     VERS: VERSION - Indicates which version of a product, item, or publication is being used or referenced.
        #     FORM: FORMAT - An assigned name given to a consistent format in which information can be conveyed.
        # SUBM: SUBMITTER - An individual or organization who contributes genealogical data to a file or transfers it to someone else.
        # NOTE: NOTE - Additional information provided by the submitter for understanding the enclosing data.
        #     CONC: CONCATENATION - An indicator that additional data belongs to the superior value. The information from the CONC value is to be connected to the value of the superior preceding line without a space and without a carriage return and/or new line character. Values that are split for a CONC tag must always be split at a non­space. If the value is split on a space the space will be lost when concatenation takes place. This is because of the treatment that spaces get as a GEDCOM delimiter, many GEDCOM values are trimmed of trailing spaces and some systems look for the first non­space starting after the tag to determine the beginning of the value.

        attr_reader   :tag, :source, :destination, :date, :file,
                      :character, :submitter, :gedcom, :note, :message, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag          = 'HEAD'
          @source       = ''
          @destination  = ''
          @date         = ''
          @file         = ''
          @character    = ''
          @submitter    = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node     = raw_node
          @gedcom       = nil
          @note         = nil

          sorter          = Gendson::Gedcom::NodeSorter.new(1, @raw_node)
          @related_nodes  = sorter.related_node_sets

          @related_nodes.each do |node|
            process_node node
          end
        end

        def as_hash
          {
              SOUR: @source,
              DEST: @destination,
              DATE: @date,
              FILE: @file,
              CHAR: @character,
              SUBM: @submitter,
              GEDC: @gedcom ? @gedcom.as_hash : nil,
              NOTE: @note ? @note.as_hash : nil
          }
        end

        private

        def process_node(node)
          case node[0][:tag]
            when 'SOUR'
              @source       = node[0][:value]
            when 'DEST'
              @destination  = node[0][:value]
            when 'DATE'
              @date         = node[0][:value]
            when 'FILE'
              @file         = node[0][:value]
            when 'CHAR'
              @character    = node[0][:value]
            when 'SUBM'
              @submitter    = node[0][:value]
            when 'GEDC'
              @gedcom       = Gendson::Gedcom::Tags::Gedcom.new(node)
            when 'NOTE'
              @note         = Gendson::Gedcom::Tags::Note.new(node)
            else
              @unprocessed_tags.push node[0][:value]
              unknown_tag_warning @tag, node[0]
          end
        end
      end
    end
  end
end
