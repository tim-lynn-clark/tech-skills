require_relative '../../../lib/gedcom/tags/tag'
require_relative '../../../lib/gedcom/tags/life_event'

module Gendson
  module Gedcom
    module Tags
      class Family < Gendson::Gedcom::Tags::Tag
        # FAM: FAMILY - Identifies a legal, common law, or other customary relationship of man and woman and their children, if any, or a family created by virtue of the marriage of a child to its biological father and mother.
        #
        # Tags found as children of the INDI tag
        # HUSB: HUSBAND - An individual in the family role of a married man or father.
        # WIFE: WIFE - An individual in the role as a mother and/or married woman.
        # CHIL: CHILD - The natural, adopted, or sealed (LDS) child of a father and a mother.
        # MARR: MARRIAGE - A legal, common­law, or customary event of creating a family unit of a man and a woman as husband and wife.
        #     DATE: DATE - The time of an event in a calendar format.
        #     PLAC: PLACE - A jurisdictional name to identify the place or location of an event.
        # RIN: REC_ID_NUMBER - A number assigned to a record by an originating automated system that can be used by a receiving system to report results pertaining to that record.
        # DIV: DIVORCE - An event of dissolving a marriage through civil action.
        # SOUR: SOURCE - The initial or original material from which information was obtained.
        #     PAGE: PAGE - A number or description to identify where information can be found in a referenced work.

        attr_reader   :tag, :id, :husband, :wife, :child, :record_id_number, :marriage, :divorce, :source, :unprocessed_tags

        def initialize(raw_node)
          # Strings
          @tag              = 'FAM'
          @id               = raw_node[0][:value]
          @husband          = ''
          @wife             = ''
          @child            = []
          @record_id_number = ''
          @divorce          = ''
          @unprocessed_tags = []

          # Nodes
          @raw_node    = raw_node
          @marriage    = nil
          @source      = nil

          sorter          = Gendson::Gedcom::NodeSorter.new(1, @raw_node)
          @related_nodes  = sorter.related_node_sets

          @related_nodes.each do |node|
            process_node node
          end
        end

        def as_hash
          {
              ID:   @id,
              HUSB: @husband,
              WIFE: @wife,
              CHIL: @child,
              RIN:  @record_id_number,
              DIV:  @divorce,
              MARR: @marriage ? @marriage.as_hash : nil,
              SOUR: @source ? @source.as_hash : nil
          }
        end

        private

        def process_node(node)
          case node[0][:tag]
            when 'HUSB'
              @husband            = node[0][:value]
            when 'WIFE'
              @wife               = node[0][:value]
            when 'CHIL'
              @child.push           node[0][:value]
            when 'RIN'
              @record_id_number   = node[0][:value]
            when 'MARR'
              @marriage           = Gendson::Gedcom::Tags::LifeEvent.new('MARR', node)
            when 'DIV'
              @divorce            = node[0][:value]
            when 'SOUR'
              @source             = Gendson::Gedcom::Tags::Source.new(node)
            else
              @unprocessed_tags.push node[0][:value]
              unknown_tag_warning @tag, node[0]
          end
        end
      end
    end
  end
end
