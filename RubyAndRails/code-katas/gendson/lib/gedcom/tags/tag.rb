module Gendson
  module Gedcom
    module Tags
      class Tag
        def unknown_tag_warning(origin_tag, tag)
          unless tag[:tag] == origin_tag
            puts "Unprocessed: Origin-#{origin_tag} Tag-#{tag[:tag]}, Value-#{tag[:value]}"
          end
        end
      end
    end
  end
end
