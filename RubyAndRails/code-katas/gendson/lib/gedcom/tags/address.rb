require_relative '../../../lib/gedcom/tags/tag'

module Gendson
  module Gedcom
    module Tags
      class Address < Gendson::Gedcom::Tags::Tag
        # ADDR: ADDRESS - The contemporary place, usually required for postal purposes, of an individual, a submitter of information, a repository, a business, a school, or a company.
        #     CONC: CONCATENATION - An indicator that additional data belongs to the superior value. The information from the CONC value is to be connected to the value of the superior preceding line without a space and without a carriage return and/or new line character. Values that are split for a CONC tag must always be split at a non­space. If the value is split on a space the space will be lost when concatenation takes place. This is because of the treatment that spaces get as a GEDCOM delimiter, many GEDCOM values are trimmed of trailing spaces and some systems look for the first non­space starting after the tag to determine the beginning of the value.

        attr_reader :tag, :text

        def initialize(raw_node)
          # Strings
          @tag        = 'ADDR'
          @text       = ''

          # Nodes
          @raw_node   = raw_node

          process_raw_node
        end

        def as_hash
          {
              TEXT: @text
          }
        end

        private

        def process_raw_node
          @raw_node.each do |subnode|
            @text += subnode[:value]
          end
        end
      end
    end
  end
end
