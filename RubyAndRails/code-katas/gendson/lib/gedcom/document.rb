module Gendson
  module Gedcom
    class Document
      attr_reader :header, :submitter, :individuals, :families, :unprocessed_tags

      def initialize(gendson_objects)
        @gendson_objects = gendson_objects
        @header = nil
        @submitter = nil
        @individuals = []
        @families = []
        @sources = []
        @unprocessed_tags = []

        process_gendson_objects
      end

      def as_hash
        {
            HEAD: @header ? @header.as_hash : nil,
            SUBM: @submitter ? @submitter.as_hash : nil,
            INDI: @individuals,
            FAM:  @families,
            SOUR: @sources
        }
      end

      private

      def process_gendson_objects
        @gendson_objects.each do |gendson|
          case gendson.tag
            when 'HEAD'
              @header = gendson
            when 'SUBM'
              @submitter = gendson
            when 'INDI'
              @individuals.push gendson.as_hash
            when 'FAM'
              @families.push gendson.as_hash
            when 'SOUR'
              @sources.push gendson.as_hash
            else
              @unprocessed_tags.push gendson
          end
        end
      end

    end
  end
end
