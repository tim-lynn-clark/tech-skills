require_relative '../../lib/gedcom/regular_expressions'
require_relative '../../lib/gedcom/tag_factory'
require_relative '../../lib/gedcom/node_sorter'
require_relative '../../lib/gedcom/document'

module Gendson
  module Gedcom
    class Parser
      def initialize(gedcom_file)
        @gedcom_file = gedcom_file
        @gedcom_raw_nodes = []
        @gendson_objects = []
      end

      def start
        File.readlines(@gedcom_file).each do |line|
          node = parse_line line
          next unless node
          @gedcom_raw_nodes.push node
        end

        sorter = Gendson::Gedcom::NodeSorter.new(0, @gedcom_raw_nodes)
        @gedcom_raw_nodes = sorter.related_node_sets
        process_raw_nodes
        Gendson::Gedcom::Document.new(@gendson_objects)
      end

      private

      def process_raw_nodes
        @gedcom_raw_nodes.each do |raw_node|
          factory = Gendson::Gedcom::TagFactory.new
          factory.build raw_node
          @gendson_objects.push factory.tag if factory.tag_created?
        end
      end

      def parse_line(line)
        matches = Gendson::Gedcom::RegularExpressions::PARSE_LINES.match(line)

        if matches
          # original_line = matches[0]
          depth = matches[1].to_i
          value_one = matches[2]
          value_two = matches[3]

          determine_tag_order value_one, value_two, depth
        end
      end

      def determine_tag_order(value_one, value_two, depth)
        value_matches = Gendson::Gedcom::RegularExpressions::PARSE_IDS.match(value_one)

        if value_matches
          tag = value_two
          value = value_matches[1]
        else
          value_matches = Gendson::Gedcom::RegularExpressions::PARSE_IDS.match(value_two)
          if value_matches
            tag = value_one
            value = value_matches[1]
          else
            tag = value_one
            value = value_two
          end

        end

        {
            depth: depth,
            tag: tag,
            value: value
        }
      end
    end
  end
end
