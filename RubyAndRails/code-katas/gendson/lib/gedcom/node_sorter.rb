module Gendson
  module Gedcom
    class NodeSorter

      attr_reader :related_node_sets

      def initialize(depth, nodes)
        @depth = depth
        @nodes = nodes
        @related_node_sets = []
        @current_node_group = []

        nodes.each do |node|
          gather_related_nodes node
        end
      end
      
      private

      def gather_related_nodes(node)
        if node[:depth] == @depth
          @current_node_group = []
          @related_node_sets.push @current_node_group
        end

        @current_node_group.push node
      end
    end
  end
end
