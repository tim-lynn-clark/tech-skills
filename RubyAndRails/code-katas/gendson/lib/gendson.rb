require_relative '../lib/gedcom/regular_expressions'
require_relative '../lib/gedcom/parser'

module Gendson
  VERSION = "1.0.0"
end