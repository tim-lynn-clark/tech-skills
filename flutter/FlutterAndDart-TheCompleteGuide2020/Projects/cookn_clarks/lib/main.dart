import 'package:cookn_clarks/data/dummy_data.dart';
import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/views/router.dart';
import 'package:flutter/material.dart';
import 'package:cookn_clarks/themes/base.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'glutenFree' : false,
    'vegetarian' : false,
    'vegan' : false,
    'lactoseFree' : false,
  };

  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  // Handlers
  void handleFiltersSet(Map<String, bool> filterData) {

    List<Meal> filteredMeals = DUMMY_MEALS.where((meal) {
      if(_filters['glutenFree'] && !meal.isGlutenFree) {
        return false;
      }

      if(_filters['vegetarian'] && !meal.isVegetarian) {
        return false;
      }

      if(_filters['vegan'] && !meal.isVegan) {
        return false;
      }

      if(_filters['lactoseFree'] && !meal.isLactoseFree) {
        return false;
      }

      return true;
    }).toList();

    print(filterData);
    print(filteredMeals);

    setState(() {
      _filters = filterData;
      _availableMeals = filteredMeals;
    });
  }

  void toggleFavoriteMeal(Meal meal) {
    final existingIndex = _favoriteMeals.indexWhere((iMeal) {
      return iMeal.id == meal.id;
    });

    if(existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMeals.add(meal);
      });
    }
  }

  bool isFavoriteMeal(String mealID) {
    return _favoriteMeals.any((element) => element.id == mealID);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Cook'n Clarks",
      theme: baseTheme(),
      // Note: https://medium.com/flutter-community/clean-navigation-in-flutter-using-generated-routes-891bd6e000df
      onGenerateRoute: Router.generateRouteWithHandlers(
        eventHandlers: {
          'handleFiltersSet': handleFiltersSet,
          'toggleFavoriteMeal': toggleFavoriteMeal
        },
        data: {
          'filters': _filters,
          'availableMeals': _availableMeals,
          'favoriteMeals': _favoriteMeals,
        },
        utilities: {
          'isFavoriteMeal': isFavoriteMeal,
        },
      ),
    );
  }
}

