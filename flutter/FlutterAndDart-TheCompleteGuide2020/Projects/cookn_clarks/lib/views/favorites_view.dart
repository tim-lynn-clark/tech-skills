import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/widgets/meal_item.dart';
import 'package:flutter/material.dart';

class FavoritesView extends StatefulWidget {
  static const routePath = '/favorites-view';

  List<Meal> favoriteMeals;

  FavoritesView({this.favoriteMeals});

  @override
  _FavoritesViewState createState() => _FavoritesViewState();
}

class _FavoritesViewState extends State<FavoritesView> {
  @override
  Widget build(BuildContext context) {
    if(widget.favoriteMeals.isEmpty) {
      return Center(
        child: Text('You do not have any favorite meals yet. Try adding some!'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (context, index) {
          return MealItem(
            key: Key(widget.favoriteMeals[index].id),
            meal: widget.favoriteMeals[index],
          );
        },
        itemCount: widget.favoriteMeals.length,
      );
    }
  }
}