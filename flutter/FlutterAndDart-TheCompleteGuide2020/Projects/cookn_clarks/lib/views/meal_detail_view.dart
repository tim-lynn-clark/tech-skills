import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/widgets/meal_ingredients.dart';
import 'package:cookn_clarks/widgets/meal_steps.dart';
import 'package:flutter/material.dart';

class MealDetailView extends StatelessWidget {
  // NOTE: good practice to take the route name from the main.dart file where the route
  // map was created and add it as a static constant variable on your view widget class.
  // This class variable can then be used when navigating to this screen from another.
  // This avoids typing errors when navigating.
  static const routePath = '/meal-detail-view';

  final Meal meal;
  final Function toggleFavoriteMeal;
  final Function isFavoriteMeal;
  const MealDetailView({this.meal, this.toggleFavoriteMeal, this.isFavoriteMeal});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
      ),
      body: ListView(
        children: [
          Container(
            height: 300,
            width: double.infinity,
            child: Image.network(
              meal.imageUrl,
              height: 250,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          MealIngredients(meal),
          MealSteps(meal)
        ],
      ),
      // // NOTE: using a floating button to delete an item
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.delete),
      //   onPressed: () {
      //     // NOTE: using the pop method of the navigator to remove the top most screen from the stack
      //     // NOTE: the pop method also allows data to be returned to the next screen in the stack
      //     // NOTE: see meal item to see how to access the returned data
      //     Navigator.of(context).pop(meal.id);
      //   },
      // ),
      // NOTE: using a floating button to favorite an item
      floatingActionButton: FloatingActionButton(
        child: Icon(
          isFavoriteMeal(meal.id)
              ? Icons.star
              : Icons.star_border
        ),
        onPressed: () {
          toggleFavoriteMeal(meal);
        },
      ),
    );
  }
}
