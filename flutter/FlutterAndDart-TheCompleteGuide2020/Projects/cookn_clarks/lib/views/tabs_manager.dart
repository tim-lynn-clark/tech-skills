import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/views/category_view.dart';
import 'package:cookn_clarks/views/favorites_view.dart';
import 'package:cookn_clarks/views/main_drawer_view.dart';
import 'package:flutter/material.dart';

class TabsManager extends StatefulWidget {
  // NOTE: good practice to take the route name from the main.dart file where the route
  // map was created and add it as a static constant variable on your view widget class.
  // This class variable can then be used when navigating to this screen from another.
  // This avoids typing errors when navigating.
  static const routePath = '/';

  List<Meal> favoriteMeals;
  TabsManager({this.favoriteMeals});

  @override
  _TabsManagerState createState() => _TabsManagerState();
}

class _TabsManagerState extends State<TabsManager> {

  // NOTE: used to change tabs when using the bottom navigation bar
  List<Map<String, Object>> _tabs;
  int _selectedTabIndex = 0;

  void _selectTabPage(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  void initState() {
    _tabs = [
      { 'title': 'Categories', 'tab': CategoryView() },
      { 'title': 'Your Favorites', 'tab': FavoritesView(favoriteMeals: widget.favoriteMeals) }
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      // // NOTE: can be used to set the selected tab
      // // could have it be user configured
      // initialIndex: 1,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_tabs[_selectedTabIndex]['title']),
          // Note: used when you want tabs in the AppBar
          // bottom: TabBar(
          //   tabs: [
          //     Tab(
          //       icon: Icon(Icons.category),
          //       text: 'Categories'
          //     ),
          //     Tab(
          //         icon: Icon(Icons.star),
          //         text: 'Favorites'
          //     )
          //   ],
          // ),
        ),
        // NOTE: body for use when tabs are displayed in the AppBar (see above)
        // body: TabBarView(
        //   children: [
        //     CategoryView(),
        //     FavoritesView()
        //   ],
        // ),
        // NOTE: Adding a hamburger menu and slide-out screen
        drawer: MainDrawerView(),
        body: _tabs[_selectedTabIndex]['tab'],
        // NOTE: Used when you want tabs at the bottom of the screen
        bottomNavigationBar: BottomNavigationBar(
          onTap: _selectTabPage,
          backgroundColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.white,
          selectedItemColor: Theme.of(context).accentColor,
          currentIndex: _selectedTabIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.category,
              ),
              title: Text(
                'Categories',
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.star,
              ),
              title: Text(
                'Favorites',
              )
            )
          ]
        ),
      ),
    );
  }
}