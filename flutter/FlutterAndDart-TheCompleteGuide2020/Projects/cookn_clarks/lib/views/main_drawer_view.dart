import 'package:cookn_clarks/views/filtered_view.dart';
import 'package:cookn_clarks/views/tabs_manager.dart';
import 'package:cookn_clarks/widgets/drawer_item.dart';
import 'package:flutter/material.dart';

class MainDrawerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // NOTE: The drawer widget creates the slide in menu that appears whe
    // the hamburger menu is tapped.
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: const EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Cooking it Up!',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 30,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          SizedBox(height: 20,),
          DrawerItem(
            'Meals',
            Icon(
              Icons.restaurant,
              size: 26,
            ),
            // NOTE: Push replacement named will replace the current screen on the stack
            // with the one requested. This will stop screens from piling up when using
            // a drawer widget to add slide in menus.
            () => Navigator.pushReplacementNamed(context, TabsManager.routePath),
          ),
          DrawerItem(
            'Filters',
            Icon(
              Icons.search,
              size: 26,
            ),
            // NOTE: Push replacement named will replace the current screen on the stack
            // with the one requested. This will stop screens from piling up when using
            // a drawer widget to add slide in menus.
            () => Navigator.pushReplacementNamed(context, FilteredView.routePath),
          ),
        ],
      ),
    );
  }
}
