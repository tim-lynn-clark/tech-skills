import 'package:cookn_clarks/models/category.dart';
import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/views/category_meals_view.dart';
import 'package:cookn_clarks/views/category_view.dart';
import 'package:cookn_clarks/views/favorites_view.dart';
import 'package:cookn_clarks/views/filtered_view.dart';
import 'package:cookn_clarks/views/meal_detail_view.dart';
import 'package:cookn_clarks/views/tabs_manager.dart';
import 'package:flutter/material.dart';

// Note: https://medium.com/flutter-community/clean-navigation-in-flutter-using-generated-routes-891bd6e000df

class Router {
  // NOTE: Modified dynamic route generating function to accept a map of named event handlers and map of named data
  // NOTE: Named map of event handlers is to allow child widgets to communicate events and pass data back to the main widget
  // NOTE: Named map of data allows the main widget to pass data down to child widgets
  static Function generateRouteWithHandlers({
    Map<String, Function> eventHandlers,
    Map<String, Object> data,
    Map<String, Object> utilities}) {

    return (RouteSettings settings) {
      switch (settings.name) {
        case TabsManager.routePath: return MaterialPageRoute(
            builder: (context) => TabsManager(favoriteMeals: data['favoriteMeals'])
        );
        case CategoryView.routePath: return MaterialPageRoute(
            builder: (context) => CategoryView()
        );
        case CategoryMealsView.routePath:
          var category = settings.arguments as Category;
          return MaterialPageRoute(builder: (context) => CategoryMealsView(
              category: category,
              availableMeals: data['availableMeals'],
            )
          );
        case MealDetailView.routePath:
          var meal = settings.arguments as Meal;
          return MaterialPageRoute(builder: (context) => MealDetailView(
              meal: meal,
              toggleFavoriteMeal: eventHandlers['toggleFavoriteMeal'],
              isFavoriteMeal: utilities['isFavoriteMeal'],
            )
          );
        case FilteredView.routePath: return MaterialPageRoute(
          builder: (context) => FilteredView(
              filters: data['filters'] ,
              handleFiltersSet: eventHandlers['handleFiltersSet'],
          )
        );
        case FavoritesView.routePath: return MaterialPageRoute(
            builder: (context) => FavoritesView()
        );
        default: return MaterialPageRoute(
            builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text('Misdirection...'),
              ),
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            )
        );
      }
    };
  }
}