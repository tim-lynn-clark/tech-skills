import 'package:cookn_clarks/widgets/category_item.dart';
import 'package:flutter/material.dart';
import 'package:cookn_clarks/data/dummy_data.dart';

class CategoryView extends StatelessWidget {
  // NOTE: good practice to take the route name from the main.dart file where the route
  // map was created and add it as a static constant variable on your view widget class.
  // This class variable can then be used when navigating to this screen from another.
  // This avoids typing errors when navigating.
  static const routePath = '/category-view';

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_CATEGORIES
          .map((category) => CategoryItem(
          category
      )).toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3/2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
