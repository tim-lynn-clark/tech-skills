import 'package:cookn_clarks/models/category.dart';
import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/widgets/meal_item.dart';
import 'package:flutter/material.dart';

class CategoryMealsView extends StatefulWidget {
  // NOTE: good practice to take the route name from the main.dart file where the route
  // map was created and add it as a static constant variable on your view widget class.
  // This class variable can then be used when navigating to this screen from another.
  // This avoids typing errors when navigating.
  static const routePath = '/categories-meals-view';

  final Category category;
  final List<Meal> availableMeals;
  CategoryMealsView({this.category, this.availableMeals});

  @override
  _CategoryMealsViewState createState() => _CategoryMealsViewState();
}

class _CategoryMealsViewState extends State<CategoryMealsView> {
  List<Meal> _categoryMeals;

  @override
  void initState() {
    _categoryMeals = widget.availableMeals.where((meal) {
      return meal.categories.contains(widget.category.id);
    }).toList();

    super.initState();
  }

  // void _removeMeal(String mealId) {
  //   setState(() {
  //     _categoryMeals.removeWhere((meal) => meal.id == mealId);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    // NOTE: OLD Implementation Accessing the arguments passed via the Navigator as a result of
    // a call for a named route to load this view (screen/page).
    // final Category _category = ModalRoute.of(context).settings.arguments as Category;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category.title),
      ),
      body: Center(
        child: ListView.builder(
            itemBuilder: (context, index) {
              return MealItem(
                key: Key(_categoryMeals[index].id),
                meal: _categoryMeals[index],
              );
            },
          itemCount: _categoryMeals.length,
        ),
      ),
    );
  }
}
