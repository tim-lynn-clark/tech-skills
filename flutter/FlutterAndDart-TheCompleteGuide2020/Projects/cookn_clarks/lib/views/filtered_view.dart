import 'package:cookn_clarks/views/main_drawer_view.dart';
import 'package:flutter/material.dart';

class FilteredView extends StatefulWidget {
  static const routePath = '/filtered-view';
  final Map<String, bool> filters;
  final Function handleFiltersSet;
  const FilteredView({this.filters, this.handleFiltersSet});

  @override
  _FilteredViewState createState() => _FilteredViewState();
}

class _FilteredViewState extends State<FilteredView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Favorites'),
        // NOTE: used to pass the set filters back to the main screen
        // so they can be passed down to other screens that need to know
        // about them.
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () => widget.handleFiltersSet(widget.filters),
          )
        ],
      ),
      drawer: MainDrawerView(),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Text(
                'Refine your meal selection',
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                SwitchListTile(
                  title: const Text('Gluten-free'),
                  subtitle: const Text('Restrict to gluten-free meals.'),
                  value: widget.filters['glutenFree'],
                  onChanged: (newValue) {
                    setState(() {
                      widget.filters['glutenFree'] = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: const Text('Vegetarian'),
                  subtitle: const Text('Restrict to vegetarian meals.'),
                  value: widget.filters['vegetarian'],
                  onChanged: (newValue) {
                    setState(() {
                      widget.filters['vegetarian'] = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: const Text('Vegan'),
                  subtitle: const Text('Restrict to vegan meals.'),
                  value: widget.filters['vegan'],
                  onChanged: (newValue) {
                    setState(() {
                      widget.filters['vegan'] = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: const Text('Lactose-free'),
                  subtitle: const Text('Restrict to lactose-free meals.'),
                  value: widget.filters['lactoseFree'],
                  onChanged: (newValue) {
                    setState(() {
                      widget.filters['lactoseFree'] = newValue;
                    });
                  },
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}
