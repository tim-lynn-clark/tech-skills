import 'package:flutter/material.dart';

ThemeData baseTheme() {
  return ThemeData(
    primarySwatch: Colors.pink,
    accentColor: Colors.yellow,
    canvasColor: const Color.fromRGBO(255, 254, 229, 1),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'Raleway',
    textTheme: ThemeData.light().textTheme.copyWith(
      bodyText1: TextStyle(
        color: const Color.fromRGBO(20, 51, 51, 1)
      ),
      bodyText2: TextStyle(
        color: const Color.fromRGBO(20, 51, 51, 1)
      ),
      headline1: TextStyle(
        color: Colors.black87,
        fontSize: 20,
        fontFamily: 'RobotoCondensed',
        fontWeight: FontWeight.bold
      )
    )
  );
}