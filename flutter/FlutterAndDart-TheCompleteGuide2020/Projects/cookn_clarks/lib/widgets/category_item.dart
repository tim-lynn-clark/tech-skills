import 'package:cookn_clarks/models/category.dart';
import 'package:cookn_clarks/views/category_meals_view.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  final Category _category;
  const CategoryItem(this._category);

  // NOTE: Navigating between view widgets is done using a Navigator, the context,
  // and pushing onto the page stack a Material Page Route
  void _selectCategory(BuildContext context) {
    // // Note: using manual navigation using material page routes can quickly become
    // // hard to manage as the number of view increase in your application
    // // at that point you can use named routes. Setup a routes table in the main.dart file
    // Navigator.of(context).push(MaterialPageRoute(
    //   builder: (_) {
    //     return CategoryMealsView(_category);
    //   }
    // ));

    // Note: using named page routes found in the main.dart file where the app was first setup
    // Note: using named routes allows data to be passed to the new view via arguments
    // Navigator.of(context).pushNamed(
    //   CategoryMealsView.routePath,
    //   arguments: _category
    // );
    Navigator.pushNamed(context, CategoryMealsView.routePath, arguments: _category);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () => _selectCategory(context),
        splashColor: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Text(
            _category.title,
            style: Theme.of(context).textTheme.headline1,
          ),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    _category.color.withOpacity(0.7),
                    _category.color
                  ],
                  begin: Alignment.bottomLeft,
                  end: Alignment.bottomRight
              ),
              borderRadius: BorderRadius.circular(15)
          ),
        ),
      ),
    );
  }
}
