import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final String _title;
  final Icon _icon;
  final Function _tapHandler;

  const DrawerItem(this._title, this._icon, this._tapHandler);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: _icon,
      title: Text(
        _title,
        style: TextStyle(
            fontFamily: 'RobotoCondensed',
            fontSize: 24,
            fontWeight: FontWeight.bold
        ),
      ),
      onTap: () => _tapHandler(),
    );
  }
}
