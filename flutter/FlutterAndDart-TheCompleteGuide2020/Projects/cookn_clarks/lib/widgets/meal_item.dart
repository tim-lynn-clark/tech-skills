import 'package:cookn_clarks/models/meal.dart';
import 'package:cookn_clarks/views/meal_detail_view.dart';
import 'package:flutter/material.dart';

class MealItem extends StatelessWidget {
  final Meal meal;
  const MealItem({
    Key key,
    @required this.meal,
  }) : super(key: key);

  // Handlers
  void mealSelectedHandler(BuildContext context) {
    Navigator.pushNamed(
      context,
      MealDetailView.routePath,
      arguments: meal
    );
    // // NOTE: a future.promise is returned when navigator pushNamed is called
    // // This allows a handle to be setup to capture data returned by the screen
    // // that was pushed to the screen stack by this screen.
    // // see MealDetailView where a floating button was used to demonstrate deleting
    // .then((mealId) {
    //   if(mealId != null) {
    //     removeItemHandler(mealId);
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => mealSelectedHandler(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(15),
                      topRight: const Radius.circular(15)),
                  child: Image.network(
                    meal.imageUrl,
                    height: 250,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 20,
                    ),
                    child: Text(
                        meal.title,
                      style: TextStyle(
                        fontSize: 26,
                        color: Colors.white,
                      ),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(Icons.schedule,),
                      SizedBox(width: 6,),
                      Text('${meal.duration} min.')
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.work,),
                      SizedBox(width: 6,),
                      Text('${meal.complexityTitle}')
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.attach_money,),
                      SizedBox(width: 6,),
                      Text('${meal.affordabilityTitle}')
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
