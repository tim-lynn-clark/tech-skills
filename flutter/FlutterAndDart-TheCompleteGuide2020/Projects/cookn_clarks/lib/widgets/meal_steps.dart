import 'package:cookn_clarks/models/meal.dart';
import 'package:flutter/material.dart';

class MealSteps extends StatelessWidget {
  final Meal _meal;
  MealSteps(this._meal);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            'Steps',
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(10)
          ),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          height: 200,
          width: 300,
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      child: Text('# ${index+1}.'),
                    ),
                    title: Text('${_meal.steps[index]}'),
                  ),
                  Divider()
                ],
              );
            },
            itemCount: _meal.steps.length,
          ),
        ),
      ],
    );
  }
}
