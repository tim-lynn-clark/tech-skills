import 'package:cookn_clarks/models/meal.dart';
import 'package:flutter/material.dart';

class MealIngredients extends StatelessWidget {
  final Meal _meal;
  MealIngredients(this._meal);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            'Ingredients',
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(10)
          ),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          height: 200,
          width: 300,
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Card(
                color: Theme.of(context).accentColor,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 10
                  ),
                  child: Text(_meal.ingredients[index]),
                ),
              );
            },
            itemCount: _meal.ingredients.length,
          ),
        ),
      ],
    );
  }
}
