import 'package:flutter/material.dart';
import 'package:personal_expense_tracker/models/transaction.dart';
import 'package:personal_expense_tracker/widgets/transaction_form.dart';
import 'package:personal_expense_tracker/widgets/transaction_list.dart';

class TransactionManager extends StatefulWidget {
  @override
  _TransactionManagerState createState() => _TransactionManagerState();
}

class _TransactionManagerState extends State<TransactionManager> {
  final List<Transaction> _transactions = [
    Transaction(id: 1, title: 'Super Shoes', amount: 79.99, date: DateTime.now()),
    Transaction(id: 1, title: 'Mega Hat', amount: 24.99, date: DateTime.now()),
    Transaction(id: 1, title: "Rock'n Romper", amount: 58.99, date: DateTime.now()),
  ];

  // Handlers
  void _newTransactionHandler(String title, double amount) {
    final transaction = Transaction(
      title: title,
      amount: amount,
      date: DateTime.now(),
      id: DateTime.now().millisecondsSinceEpoch
    );

    setState(() {
      _transactions.add(transaction);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TransactionForm(_newTransactionHandler),
        TransactionList(_transactions)
      ],
    );
  }
}