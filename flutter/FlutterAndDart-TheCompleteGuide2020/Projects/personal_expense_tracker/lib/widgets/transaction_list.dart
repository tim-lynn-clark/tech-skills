import 'package:flutter/material.dart';
import 'package:personal_expense_tracker/models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> _transactions;
  final Function _deleteTransactionHandler;

  TransactionList(this._transactions, this._deleteTransactionHandler);

  Widget showList(BuildContext context) {
    Widget screen;
    if(_transactions.isEmpty){
      return LayoutBuilder( builder: (context, constraints) {
          return Column(
            children: <Widget>[
              Text(
                'No transactions exist yet.',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: constraints.maxHeight * 0.6,
                child: Image.asset(
                  'assets/images/waiting.png',
                  fit: BoxFit.cover,
                ),
              ),
            ],
          );
        }
      );
    } else {
      screen = ListView.builder(
        itemBuilder: (context, index) {
          // return TransactionRow(_transactions[index]);
          return Card(
            elevation: 5,
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
            child: ListTile(
              leading: CircleAvatar(
                  radius: 30,
                  child: Padding(
                      // NOTE: PERFORMANCE: This EdgeInsets instance can be marked as final to improve performance
                      padding: const EdgeInsets.all(6),
                      child: FittedBox(
                          child: Text('\$${_transactions[index].amount}')
                      )
                  )
              ),
              title: Text(
                '${_transactions[index].title}',
                style: Theme.of(context).textTheme.headline6,
              ),
              subtitle: Text(
                '${_transactions[index].dateFormatted}',
                style: Theme.of(context).textTheme.bodyText2,
              ),
              // NOTE: Using MediaQuery to determine if additional text can be added due to extra screen space
              trailing: MediaQuery.of(context).size.width > 360
              ? FlatButton.icon(
                  icon: Icon(
                    Icons.delete
                  ),
                  // NOTE: PERFORMANCE: This text widget can be marked as final to improve performance
                  // because the string being set is a string literal and is constant.
                  label: const Text('Delete'),
                  color: Theme.of(context).errorColor,
                  onPressed: () => _deleteTransactionHandler(_transactions[index].id),
                )
              : IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Theme.of(context).errorColor
                ),
                onPressed: () => _deleteTransactionHandler(_transactions[index].id),
              ),
            ),
          );
        },
        itemCount: _transactions.length,
      );
    }

    return screen;
  }

  @override
  Widget build(BuildContext context) {
    return showList(context);
  }
}