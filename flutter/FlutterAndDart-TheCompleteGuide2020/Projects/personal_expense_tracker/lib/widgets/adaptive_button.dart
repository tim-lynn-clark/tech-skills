//NOTE: Provides detection of the platform the application is running on
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

// NOTE: Creating a custom widget to handle adaptive UI changes based on
// platform is a really good idea
class AdaptiveFlatButton extends StatelessWidget {
  final String _text;
  final Function _pressHandler;

  //NOTE: PERFORMANCE: setting a constructor to const improve performance as it signifies
  // that each instance of the class as instantiated will not change as the arguments are
  // assigned to final properties within the object. Flutter will then not create new instances
  // when re-rendering the widget tree.
  const AdaptiveFlatButton(this._text, this._pressHandler);

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? CupertinoButton(
      child: Text(
          _text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          )
      ),
      onPressed: _pressHandler,
    )
        : FlatButton(
      textColor: Theme.of(context).primaryColor,
      child: Text(
          _text,
          style: TextStyle(
              fontWeight: FontWeight.bold
          )
      ),
      onPressed: _pressHandler,
    );
  }
}
