import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expense_tracker/widgets/adaptive_button.dart';

class TransactionForm extends StatefulWidget {
  final Function _newTransactionHandler;

  // Constructors
  TransactionForm(this._newTransactionHandler);

  @override
  _TransactionFormState createState() => _TransactionFormState();
}

class _TransactionFormState extends State<TransactionForm> {
  // NOTE: When using TextEditingControllers to monitor text fields,
  // the widget needs to be StatefulWidgets
  final _titleController = TextEditingController();
  final _amountController = TextEditingController();
  DateTime _selectedDate;

  // Event Handlers
  void _formComplete() {
    final enteredTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    if(enteredTitle.isEmpty || enteredAmount <= 0 || _selectedDate == null) {
      return;
    }

    widget._newTransactionHandler(
      enteredTitle,
      enteredAmount,
      _selectedDate
    );

    _titleController.clear();
    _amountController.clear();
    _selectedDate = null;
  }

  void _showDatePicker() {
    showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year),
        lastDate: DateTime.now()
    ).then((pickedDate) {
      if(pickedDate == null){
        return;
      }

      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    //NOTE: Allows the modal window contents to be scrolled now that it's container
    // height has been adjusted to respect the soft keyboard.
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          // NOTE: adding padding for the soft keyboard
          padding: EdgeInsets.only(
            top:10,
            left:10,
            right:10,
            // NOTE: ViewInsets provides information on anything lapping into the
            // view, allowing us to modify a widget to respect the soft keyboard
            bottom: MediaQuery.of(context).viewInsets.bottom + 10
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              TextField(
                // NOTE: PERFORMANCE: This InputDecoration instance can be marked as final to improve performance
                decoration: const InputDecoration(
                  labelText: 'Title',
                ),
                controller: _titleController,
              ),
              TextField(
                // NOTE: PERFORMANCE: This InputDecoration instance can be marked as final to improve performance
                decoration: const InputDecoration(
                  labelText: 'Amount',
                ),
                controller: _amountController,
                keyboardType: TextInputType.number,
                onSubmitted: (_) => _formComplete(),
              ),
              Container(
                height: 70,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          _selectedDate == null ?
                          'No date selected.' :
                          'Date: ${DateFormat('MM/dd/yyyy').format(_selectedDate)}'
                      ),
                    ),
                    AdaptiveFlatButton('Choose Date', _showDatePicker)
                  ],
                ),
              ),
              RaisedButton(
                // NOTE: PERFORMANCE: This text widget instance can be marked as final to improve performance
                child: const Text('Add Transaction',
                  style: TextStyle(
                    color: Colors.white
                  ),
                ),
                onPressed: _formComplete,
              )
            ],
          )
        )
      ),
    );
  }
}