import 'package:flutter/material.dart';
import 'package:personal_expense_tracker/models/transaction.dart';

class TransactionRow extends StatelessWidget {
  Transaction _transaction;

  TransactionRow(this._transaction);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 15
            ),
            decoration: BoxDecoration(
              border: Border.all(
                // NOTE: Theme.of(context) allows you to access theme data
                // set in the overall app theme for use in your widgets.
                // This allows your widgets to be restyled at the app level
                color: Theme.of(context).primaryColorDark,
                width: 2
              )
            ),
            // NOTE: PERFORMANCE: This EdgeInsets instance can be marked as final to improve performance
            padding: const EdgeInsets.all(10),
            child: Text(
              _transaction.amountFormatted,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Theme.of(context).primaryColorDark
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  _transaction.title,
                style: Theme.of(context).textTheme.headline4,
              ),
              Text(
                _transaction.dateFormatted,
                style: Theme.of(context).textTheme.bodyText2,
              )
          ],)
        ],
      )
    );
  }
}