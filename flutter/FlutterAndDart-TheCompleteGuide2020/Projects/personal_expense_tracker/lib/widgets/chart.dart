import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expense_tracker/models/transaction.dart';
import 'package:personal_expense_tracker/widgets/chart_bar.dart';

class Chart extends StatelessWidget {
  final List<Transaction> _recentTransactions;
  const Chart(this._recentTransactions);

  List<Map<String, Object>> get groupedTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index),);
      double totalSum = 0.0;
      for(var i = 0; i<_recentTransactions.length; i++) {
        var current = _recentTransactions[i];
        if(current.date.day == weekDay.day &&
            current.date.month == weekDay.month &&
            current.date.year == weekDay.year) {
          totalSum += current.amount;
        }
      }

      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum};
    }).reversed.toList();
  }

  double get totalSpending {
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum += item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 6,
      // NOTE: PERFORMANCE: This EdgeInsets instance can be marked as final to improve performance
      margin: const EdgeInsets.all(20),
      child: Padding(
        // NOTE: PERFORMANCE: This EdgeInsets instance can be marked as final to improve performance
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: groupedTransactionValues.map((data) {
            return Flexible(
              fit: FlexFit.tight,
              child: ChartBar(
                data['day'],
                data['amount'],
                totalSpending == 0.0 ? 0.0 : (data['amount'] as double) / totalSpending
              )
            );
          }).toList(),
        ),
      )
    );
  }
}
