import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPercOfTotal;

  // NOTE: To improve flutter app performance, you can set constructors to be
  // constants if all of the arguments it takes in are assigned to final variables
  // within the class. This makes every instance of this class immutable as its
  // data does not change after creation.
  const ChartBar(this.label, this.spendingAmount, this.spendingPercOfTotal);

  @override
  Widget build(BuildContext context) {
    // NOTE: LayoutBuilder can be used to gather information about the constraining partent
    // widget and use that information to dynamically calculate the sizes of widgets
    // used within the current widget.
    return LayoutBuilder(
      builder: (context, constraint) {
        return Column(
          children: [
            Container(
              height: constraint.maxHeight * 0.15,
              child: FittedBox(
                // NOTE: this text widget cannot be marked as const to improve performance
                // due to the fact that the text is set dynamically through string interpolation
                child: Text('\$${spendingAmount.toStringAsFixed(0)}'),
              ),
            ),
            SizedBox(height: constraint.maxHeight * 0.05,),
            Container(
              height: constraint.maxHeight * 0.6,
              width: 10,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey,
                          width: 1.0
                      ),
                      color: Color.fromRGBO(220, 220, 220, 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  FractionallySizedBox(
                    heightFactor: spendingPercOfTotal,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: constraint.maxHeight * 0.05,),
            Container(
              height: constraint.maxHeight * 0.15,
              child: FittedBox(
                  child: Text(label)
              )
            ),
          ],
        );
      }
    );
  }
}
