//NOTE: Provides detection of the platform the application is running on
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personal_expense_tracker/models/transaction.dart';
import 'package:personal_expense_tracker/widgets/chart.dart';
import 'package:personal_expense_tracker/widgets/transaction_form.dart';
import 'package:personal_expense_tracker/widgets/transaction_list.dart';

class HomePage extends StatefulWidget {

  // NOTE: Test property to show you can access the properties and methods
  // of the actual material widget from the state object below when needed
  final int testAccessProperty = 333;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _showChart = false;

  // NOTE: In the object that maintains the state of a stateful widget
  // you can access the actual widget instance and it's properties
  // and methods through the use of the 'widget' variable
  void accessWidgetPropertiesExample() {
    int testProperty = widget.testAccessProperty;
    print(testProperty);
  }


  final List<Transaction> _transactions = [
    Transaction(id: 1, title: 'Super Shoes', amount: 79.99, date: DateTime.now()),
    Transaction(id: 2, title: 'Mega Hat', amount: 24.99, date: DateTime.now()),
    Transaction(id: 3, title: "Rock'n Romper", amount: 58.99, date: DateTime.now()),
  ];

  // Getter/Setters
  List<Transaction> get _recentTransactions {
    return _transactions.where((element) {
      return element.date.isAfter(
        DateTime.now().subtract(
            Duration(days: 7)
        )
      );
    }).toList();
  }

  // Methods

  // NOTE: Calculate the size of the screen space remaining for custom widgets after status bar and app bar have been removed
  double _calculateAvailableScreenSpace(var appBar) {
    return MediaQuery.of(context).size.height - appBar.preferredSize.height - MediaQuery.of(context).padding.top;
  }

  // Handlers
  void _newTransactionHandler(String title, double amount, DateTime date) {
    final transaction = Transaction(
        title: title,
        amount: amount,
        date: date,
        id: DateTime.now().millisecondsSinceEpoch
    );

    setState(() {
      _transactions.add(transaction);
    });

    // Closes the modal dialog containing
    // the new transaction form
    Navigator.of(context).pop();
  }

  void _deleteTransactionHandler(int id) {
    setState(() {
      _transactions.removeWhere((element) => element.id == id);
    });
  }

  void _showTransactionForm(BuildContext context) {
    // NOTE: creates a new modal dialog containing a custom widget
    showModalBottomSheet(
      context: context,
      builder: (bContext) {
        return GestureDetector(
          onTap: (){},
          child: TransactionForm(_newTransactionHandler),
          behavior: HitTestBehavior.opaque,
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {

    // NOTE: can use MediaQuery to find out the device orientation and adapt
    // the UI to best fit
    final isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;

    // NOTE: Store AppBar in variable in order to access its size information
    // when calculating the size of other controls
    // NOTE: Determining which navigation widget to use based on platform
    final PreferredSizeWidget appBar = Platform.isIOS
      ? CupertinoNavigationBar(
          // NOTE: PERFORMANCE: This text widget instance can be marked as final to improve performance
          middle: const Text('Spend-o-track-it'),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                child: Icon(
                  CupertinoIcons.add,
                ),
                onTap: () => this._showTransactionForm(context),
              )
            ],
          )
          )
        : AppBar(
            // NOTE: PERFORMANCE: This text widget instance can be marked as final to improve performance
            title: const Text('Spend-o-track-it'),
            actions: <Widget>[
              // NOTE: creating a top menu bar button
              IconButton(
                icon: Icon(
                  Icons.add,
                ),
                onPressed: () => this._showTransactionForm(context),
              )
            ],
          );

   final  Container transactionListWidget = Container(
      height: _calculateAvailableScreenSpace(appBar) * .7,
      child: TransactionList(_transactions, _deleteTransactionHandler),
    );

    // NOTE: Widget tree moved out of the return statement in order to have
    // access to it after determining which platform-specific scaffold to use
    // to setup the application UI
    final SafeArea widgetTree = SafeArea(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // NOTE: New feature of flutter, if statements in a list
            // allows for conditional rendering of widgets
            if(isLandscape) Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Show Chart',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  //NOTE: a Switch widget is a good option for allowing users to
                  // select what it is they want to see on the screen when there is
                  // not enough screen to show everything (moving form portrait to landscape mode
                  // NOTE: the adaptive constructor on the Switch will automatically
                  // change the look and feel of the switch based on the device's OS
                  // so the controls look like the native controls for that platform
                  Switch.adaptive(
                    value: _showChart,
                    activeColor: Theme.of(context).accentColor,
                    onChanged: (val) {
                      setState(() {
                        _showChart = val;
                      });
                    },
                  ),
                ]
            ),
            if(!isLandscape) Container(
              height: _calculateAvailableScreenSpace(appBar) * .3,
              child: Chart(_recentTransactions),
            ),
            if(!isLandscape) transactionListWidget,
            if(isLandscape) _showChart ? Container(
              height: _calculateAvailableScreenSpace(appBar) * .7,
              child: Chart(_recentTransactions),
            )
                : transactionListWidget
          ],
        ),
      ),
    );

    // NOTE: Determining which page scaffold to use based on platform
    return Platform.isIOS
        ? CupertinoPageScaffold(
            navigationBar: appBar,
            child: widgetTree,
          )
        : Scaffold(
          appBar: appBar,
          body: widgetTree,
          // NOTE: Create a floating action button at the bottom of the screen
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          // NOTE: Floating action buttons is more of an Android UI pattern, if you
          // would rather have a more iOS feeling app, you can omit the button leaving
          // only the plus button in the app bar.
          floatingActionButton: Platform.isIOS
              ? Container()
              : FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () => _showTransactionForm(context),
              ),

        );
  }
}