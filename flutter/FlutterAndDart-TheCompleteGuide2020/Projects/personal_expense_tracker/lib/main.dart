import 'package:flutter/material.dart';
// NOTE: needed when restricting device orientations
// import 'package:flutter/services.dart';
import 'package:personal_expense_tracker/themes/purple.dart';
import 'package:personal_expense_tracker/views/home_page.dart';

void main() {
  // NOTE: To restrict device orientations
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations([
  //   DeviceOrientation.portraitUp,
  //   DeviceOrientation.portraitDown
  // ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Spend-o-track-it',
      theme: purpleTheme(),
      home: HomePage()
    );
  }
}
