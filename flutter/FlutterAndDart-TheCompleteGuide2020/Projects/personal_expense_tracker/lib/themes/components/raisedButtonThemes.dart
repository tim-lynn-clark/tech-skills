import 'package:flutter/material.dart';

ButtonThemeData orangeButton(){
  return ButtonThemeData(
    buttonColor: Colors.orange,
    splashColor: Colors.deepOrangeAccent,
    highlightColor: Colors.deepOrange,
  );
}

ButtonThemeData purpleButton(){
  return ButtonThemeData(
    buttonColor: Colors.purple,
    splashColor: Colors.purpleAccent,
    highlightColor: Colors.deepPurple,
    textTheme: ButtonTextTheme.primary
  );
}