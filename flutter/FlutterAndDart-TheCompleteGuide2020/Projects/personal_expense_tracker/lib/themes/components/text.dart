import 'package:flutter/material.dart';

TextTheme regularTextTheme() {
  return TextTheme(
      headline4: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.black87
      ),
      bodyText1: TextStyle(
          fontSize: 16,
          color: Colors.black87
      ),
      bodyText2: TextStyle(
          fontSize: 16,
          color: Colors.grey
      )

  );
}