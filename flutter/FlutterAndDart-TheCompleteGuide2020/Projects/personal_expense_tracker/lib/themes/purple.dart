import 'package:flutter/material.dart';
import 'package:personal_expense_tracker/themes/components/raisedButtonThemes.dart';
import 'package:personal_expense_tracker/themes/components/text.dart';

ThemeData purpleTheme() {
  return ThemeData(
    // Light/Dark Mode)
    brightness: Brightness.light,

    // App bar theme
    appBarTheme: AppBarTheme(
      textTheme: ThemeData.light().textTheme.copyWith(
        headline6: TextStyle(
          fontFamily: 'OpenSans',
          fontSize: 20,
          fontWeight: FontWeight.bold,
        )
      )
    ),

    // Color properties
    primarySwatch: Colors.purple,
    accentColor: Colors.orange,
    errorColor: Colors.redAccent,

    // Text properties
    fontFamily: 'Quicksand',
    textTheme: regularTextTheme(),

    // App-wide structural properties
    visualDensity: VisualDensity.adaptivePlatformDensity,

    // Widget-specific themes
    buttonTheme: purpleButton(),

  );
}