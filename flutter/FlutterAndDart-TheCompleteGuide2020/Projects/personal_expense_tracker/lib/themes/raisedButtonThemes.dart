import 'package:flutter/material.dart';

orangeButton(){
  return ButtonThemeData(
    buttonColor: Colors.orange,
    splashColor: Colors.deepOrangeAccent,
    highlightColor: Colors.deepOrange,
  );
}

purpleButton(){
  return ButtonThemeData(
    buttonColor: Colors.purple,
    splashColor: Colors.purpleAccent,
    highlightColor: Colors.deepPurple,
  );
}