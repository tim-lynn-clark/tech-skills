import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';


class Transaction {
  final int id;
  final String title;
  final double amount;
  final DateTime date;

  // NOTE: @required is a Flutter-specific language feature,
  // does not work in plain old Dart
  Transaction({
    @required this.id,
    @required this.title,
    @required this.amount,
    @required this.date
  });

  String get amountFormatted => "\$${amount.toStringAsFixed(2)}";
  String get dateFormatted => "${DateFormat('MM/dd/yyyy').format(date)}";
}