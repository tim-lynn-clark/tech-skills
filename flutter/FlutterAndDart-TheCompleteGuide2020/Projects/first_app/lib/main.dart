import 'package:flutter/material.dart';
import 'package:first_app/widgets/my_app.dart';

void main() {
  // Flutter takes an instance of the primary application widget
  // and renders it in the application
  runApp(MyApp());
}

