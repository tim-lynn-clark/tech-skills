import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  int _finalScore;
  Function _replayHandler;

  Result({int finalScore, Function replayHandler}) {
    _finalScore = finalScore;
    _replayHandler = replayHandler;
  }

  String get resultPhrase {
    String resultText = 'Score: ' + _finalScore.toString() + ' - ';
    if(_finalScore <= 8) {
      resultText += 'You are a strange one...';
    }
    else if(_finalScore <= 12) {
      resultText += 'You are a little odd...';
    }
    else if(_finalScore <= 16) {
      resultText += 'Not bad at all!';
    }
    else {
      resultText += 'You are amazing!';
    }
    return resultText;
  }

  MaterialColor get textColor {
    MaterialColor textColor;
    if(_finalScore <= 8) {
      textColor = Colors.red;
    }
    else if(_finalScore <= 12) {
      textColor = Colors.deepOrange;
    }
    else if(_finalScore <= 16) {
      textColor = Colors.blue;
    }
    else {
      textColor = Colors.green;
    }
    return textColor;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: textColor
            ),
            textAlign: TextAlign.center,
          ),
          RaisedButton(
            child: Text('Replay',
              style: TextStyle(
                color: Colors.white
              ),
            ),
            color: Colors.blue,
            onPressed: () => _replayHandler(),
          )
        ],
      )
    );
  }
}