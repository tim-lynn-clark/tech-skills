import 'package:flutter/material.dart';
import 'package:first_app/widgets/question.dart';
import 'package:first_app/widgets/answer_button.dart';

class Quiz extends StatelessWidget {
  Function _answerSelectHandler;
  Map _question;

  Quiz({@required Map question, @required Function answerSelectHandler}){
    _question = question;
    _answerSelectHandler = answerSelectHandler;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(_question['questionText']),
        // Utilizing spread operator to unwrap the returned list
        // into a list of AnswerButton widgets
        ..._question['answers'].map((answer) {
          return AnswerButton(answer: answer, selectHandler: _answerSelectHandler);
        }).toList()
      ],
    );
  }
}