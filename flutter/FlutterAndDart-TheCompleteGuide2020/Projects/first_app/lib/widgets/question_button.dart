import 'package:flutter/material.dart';

class QuestionButton extends StatelessWidget {
  // Making the internal property immutable as it is a Stateless Widget
  String _questionTitleText;
  int _index;
  Function _selectHandler;

  // Dart short form constructor
  QuestionButton({ String titleText, int index, Function selectHandler}){
    this._questionTitleText = titleText;
    this._index = index;
    this._selectHandler = selectHandler;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.all(10.0),
        child: RaisedButton(
            child: Text(_questionTitleText),
            color: Colors.blue,
            onPressed: () => _selectHandler(_index),
        )
    );
  }

}