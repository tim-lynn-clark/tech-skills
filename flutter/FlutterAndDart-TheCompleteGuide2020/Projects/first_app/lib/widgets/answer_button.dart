import 'package:flutter/material.dart';

class AnswerButton extends StatelessWidget {
  // Making the internal property immutable as it is a Stateless Widget
  Map _answer;
  Function _selectHandler;

  // Dart short form constructor
  AnswerButton({ Map answer, Function selectHandler}){
    this._answer = answer;
    this._selectHandler = selectHandler;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.all(10.0),
        child: RaisedButton(
            child: Text(_answer['text']),
            color: Colors.blue,
            onPressed: () => _selectHandler(_answer['score']),
        )
    );
  }

}