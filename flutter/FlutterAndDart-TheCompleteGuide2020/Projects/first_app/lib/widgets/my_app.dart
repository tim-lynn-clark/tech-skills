import 'package:first_app/screens/quiz.dart';
import 'package:first_app/screens/result.dart';
import 'package:flutter/material.dart';


// Primary Application Widget
// Stateful widget is a state container wrapper around
// a widget's functionality and internal state
class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

// State for the Primary Application Widget
// Note: Adding an underscore to an identifier makes it private to the package (file)
class _MyAppState extends State<MyApp> {
  // App State (Data)
  // Note: Adding an underscore to an identifier makes it private to the class
  int _selectedQuestionIndex = 0;
  int _totalScore = 0;
  // Const can be on either side of the assignment operator
  final _questions = const <Map>[
    {
      'questionText': 'What is your favorite color?',
      'answers': [
        {'text': 'Blue', 'score': 1},
        {'text': 'Green', 'score': 5},
        {'text': 'Red', 'score': 10},
        {'text': 'Orange', 'score': 15}
      ]
    },
    {
      'questionText': 'What is your favorite animal?',
      'answers': [
        {'text': 'Dog', 'score': 15},
        {'text': 'Horse', 'score': 5},
        {'text': 'Lion', 'score': 10},
        {'text': 'Monkey', 'score': 1}
      ]
    },
    {
      'questionText': 'What is your favorite car brand?',
      'answers': [
        {'text': 'Dodge', 'score': 10},
        {'text': 'GMC', 'score': 5},
        {'text': 'Ford', 'score': 1},
        {'text': 'Jeep', 'score': 15}
      ]
    }
  ];


  // Event Handlers
    void _answerSelectHandler(int score) {
    if(_selectedQuestionIndex < _questions.length){
      setState(() {
        _selectedQuestionIndex += 1;
        _totalScore += score;
      });
    }
  }

  void _replay() {
    setState(() {
      _selectedQuestionIndex = 0;
      _totalScore = 0;
    });
  }

  // Methods
  Widget determineScreen() {
    Widget screen;
    if(_selectedQuestionIndex < _questions.length){
      screen = Quiz(question: _questions[_selectedQuestionIndex], answerSelectHandler: _answerSelectHandler);
    } else {
      screen = Result(finalScore: _totalScore, replayHandler: _replay);
    }
    return screen;
  }

  @override
  Widget build(BuildContext context) {

    // UI Widget Hierarchy
    return MaterialApp(
      // Scaffold widget gives you a basic phone screen
      // with basic styling
      home: Scaffold(
        // AppBar widget provides a top men bar
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: determineScreen()
    ));
  }
}