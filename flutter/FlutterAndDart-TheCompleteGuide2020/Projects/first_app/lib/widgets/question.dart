import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  // Making the internal property immutable as it is a Stateless Widget
  final String _questionText;

  // Dart short form constructor
  Question(this._questionText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10.0),
      child: Text(_questionText,
        style: TextStyle(fontSize: 28,),
        textAlign: TextAlign.center,
    ));
  }

}