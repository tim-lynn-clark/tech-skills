import 'package:flutter/foundation.dart';
import 'package:shop_it_up/models/cart_item.dart';
import 'package:shop_it_up/models/order_item.dart';

// NOTE: Provides application-wide order state using the ChangeNotifier
class OrdersProvider with ChangeNotifier {
  List<OrderItem> _orders = [];

  List<OrderItem> get orders {
    return [..._orders];
  }

  void addOrder(List<CartItem> cartItems, double total) {
    _orders.insert(0, OrderItem(
      id: DateTime.now().millisecond.toString(),
      amount: total,
      date: DateTime.now(),
      products: cartItems
    ));

    notifyListeners();
  }
}