import 'package:flutter/material.dart';
import 'package:shop_it_up/models/cart_item.dart';
import 'package:shop_it_up/models/product.dart';

// NOTE: Provides application-wide shopping cart state using the ChangeNotifier
class CartProvider with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }

  double get totalAmount {
    double total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.product.price * cartItem.quantity;
    });
    return total;
  }

  void addItem({Product product}) {
    if(_items.containsKey(product.id)){
      _items.update(product.id, (existingCartItem) {
        existingCartItem.quantity++;
        return existingCartItem;
      });
    } else {
      _items.putIfAbsent(
        product.id,
        () {
          return CartItem(
              id: product.id,
              product: product,
              quantity: 1
          );
        }
      );
    }
    notifyListeners();
  }

  void removeItem({String productId}) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleItem({String productId}) {
    if(!_items.containsKey(productId)) {
      return;
    }

    if(_items[productId].quantity > 1) {
      _items.update(productId, (existingItem) {
        existingItem.quantity--;
        return existingItem;
      });
    } else {
      _items.remove(productId);
    }

    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}