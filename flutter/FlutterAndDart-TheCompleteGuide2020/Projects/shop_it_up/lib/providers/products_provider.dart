import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:shop_it_up/utilities/api/ApiRoutes.dart';

// NOTE: ChangeNotifier is a mixin module adding Pub/Sub abilities to a dart class
class ProductsProvider with ChangeNotifier {

  List<Product> _products = [
    // Product(
    //   id: 'p1',
    //   title: 'Red Shirt',
    //   description: 'A red shirt - it is pretty red!',
    //   price: 29.99,
    //   imageUrl:
    //   'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    // ),
    // Product(
    //   id: 'p2',
    //   title: 'Trousers',
    //   description: 'A nice pair of trousers.',
    //   price: 59.99,
    //   imageUrl:
    //   'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    // ),
    // Product(
    //   id: 'p3',
    //   title: 'Yellow Scarf',
    //   description: 'Warm and cozy - exactly what you need for the winter.',
    //   price: 19.99,
    //   imageUrl:
    //   'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    // ),
    // Product(
    //   id: 'p4',
    //   title: 'A Pan',
    //   description: 'Prepare any meal you want.',
    //   price: 49.99,
    //   imageUrl:
    //   'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    // ),
  ];

  // NOTE: Access to product list is controlled through a getter
  // NOTE: instead of returning a reference to the original list, we create a copy
  // NOTE: the list copy is important as we do not want the original list to be updated without notification sent out regarding the change
  List<Product> get products {
    return [..._products];
  }

  List<Product> get favoriteProducts {
    return _products.where((product) => product.isFavorite).toList();
  }

  // NOTE: The data in the state provider should be changed through methods so that all listening widgets can be notified a change has happened to what they find important
  Future<void> addProduct(Product newProduct) {

    // NOTE: Using Firebase and the HTTP package to send new products into a real-time database
    // NOTE: HTTP methods return futures/promises so resolve with the network request completes and returns
    // NOTE: Return is using the automatically returned future of the post method to send to the caller, so the caller knows when the future resolves
    // NOTE: in this instance the Post method returns and the then is called, once it resolves, it returns a promise. This promise can then be sent tot he caller to let the caller know the then has completed work.
    // NOTE: extremely useful when showing spinners to the user so they know work is being done, see ProductFormScreen where this addProduct methods called
    return http.post(ApiRoutes().products.base,
      body: newProduct.toJSON()
    )
    .then((response) {

      final Product newProductWithId = Product(
        id: json.decode(response.body)['name'],
        title: newProduct.title,
        description: newProduct.description,
        price: newProduct.price,
        imageUrl: newProduct.imageUrl,
        isFavorite: newProduct.isFavorite,
      );

      print(newProductWithId.id);
      print(newProductWithId.title);
      print(newProductWithId.description);
      print(newProductWithId.price);
      print(newProductWithId.imageUrl);
      print(newProductWithId.isFavorite);

      _products.add(newProductWithId);

      // NOTE: Here we ask for all listening widgets to be notified the data they care about has changed
      notifyListeners();
    })
    // NOTE: Handling errors
    .catchError((error){
      print(error);
      // NOTE: throwing the error in order to get it to the product form screen so it can be handled there
      throw error;
    });
  }

  Future<void> updateProduct(Product updatedProduct) async {
    int index = _products.indexWhere((product) => product.id == updatedProduct.id);
    if(index > -1) {
      try {
        await http.patch(
            ApiRoutes().products.baseWithId(updatedProduct.id),
            body: updatedProduct.toJSON()
        );
        _products[index] = updatedProduct;
        notifyListeners();
      } catch(error) {
        print(error);
        throw(error);
      }
    }
  }

  void deleteProduct(String id) {
    int index = _products.indexWhere((product) => product.id == id);
    if(index > -1) {
      // NOTE: Optimistic deleting
      // NOTE: Reference to deleted product, just in case remote delete fails
      Product removedProduct = _products[index];
      _products.removeAt(index);

      http.delete(ApiRoutes().products.baseWithId(id)).then((response) {

        if(response.statusCode >= 400) {
          
        }

        // NOTE: Remove reference to product on successful remote delete
        removedProduct = null;
      }).catchError((_) {
        // NOTE: Add removed product back if remote delete fails
        _products.insert(index, removedProduct);
      }).whenComplete(() => notifyListeners());
    }
  }

  //NOTE: Using async/await to get products
  Future<void> fetchAndSetProducts() async {
    try {
      final response = await http.get(ApiRoutes().products.base);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProducts = [];
      extractedData.forEach((key, value) {
        loadedProducts.add(Product(
          id: key,
          title: value['title'],
          description: value['description'],
          price: value['price'],
          isFavorite: value['isFavorite'],
          imageUrl: value['imageUrl'],
        ));
      });
      _products = loadedProducts;
      notifyListeners();

    } catch(error) {
      throw(error);
    }
  }
}