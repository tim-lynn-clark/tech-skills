import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// NOTE: In this application we are following the immutable data model pattern and our Product data model has all of
// NOTE: Its properties set to final so they cannot be changed after instantiation of the object. This is to make
// NOTE: all data models thread safe for better performance within a mobile application
// https://engineering.linkedin.com/blog/2016/04/keeping-immutable-models-consistent
// https://medium.com/pinterest-engineering/immutable-models-and-data-consistency-in-our-ios-app-d10e248bfef8

// NOTE: Core data models within an application can also be used as change providers
// NOTE: this is done by adding the ChangeNotifier mixin and in every method that changes the model, calling notifyListeners
class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void toggleIsFavorite() {
    isFavorite = !isFavorite;
    // NOTE: Notifying all widgets listening to this provider that the underlying data has been modified
    // NOTE: this will cause each listening widget to execute a rebuild
    notifyListeners();
  }

  String toJSON() {
    // NOTE: Using the Dart Convert package to convert a map into a JSON string
    return json.encode({
      'id': id,
      'title': title,
      'description': description,
      'price': price,
      'imageUrl': imageUrl,
      'isFavorite': isFavorite,
    });
  }
}