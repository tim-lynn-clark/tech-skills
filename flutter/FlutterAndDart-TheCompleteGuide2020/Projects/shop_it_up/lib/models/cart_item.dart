import 'package:shop_it_up/models/product.dart';

// NOTE: In this application we are following the immutable data model pattern and our Product data model has all of
// NOTE: Its properties set to final so they cannot be changed after instantiation of the object. This is to make
// NOTE: all data models thread safe for better performance within a mobile application
// https://engineering.linkedin.com/blog/2016/04/keeping-immutable-models-consistent
// https://medium.com/pinterest-engineering/immutable-models-and-data-consistency-in-our-ios-app-d10e248bfef8
class CartItem {
  final String id;
  final Product product;
  int quantity;

  CartItem({this.id, this.product, this.quantity});
}