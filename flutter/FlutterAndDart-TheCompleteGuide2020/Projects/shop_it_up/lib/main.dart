import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/providers/cart_provider.dart';
import 'package:shop_it_up/providers/orders_provider.dart';
import 'package:shop_it_up/providers/products_provider.dart';
import 'package:shop_it_up/ui/themes/primary_theme.dart';
import 'package:shop_it_up/ui/views/cart_display_screen.dart';
import 'package:shop_it_up/ui/views/orders_list_screen.dart';
import 'package:shop_it_up/ui/views/product_details_screen.dart';
import 'package:shop_it_up/ui/views/product_form_screen.dart';
import 'package:shop_it_up/ui/views/product_list_screen.dart';
import 'package:shop_it_up/ui/views/user_products_list_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // NOTE: Data provided via a provider must be made available at the highest widget level the data will be needed
    // NOTE: for this application both the Product and Product Details screens need to know about products
    // NOTE: Using the ChangeNotifierProvider supplied by the Provider package, can be used to supply a single provider (state object) to a widget tree hierarchy
    // return ChangeNotifierProvider(
    // NOTE: The MultiProvider allows multiply providers (state objects) to be supplied to a widget tree
    // NOTE: It requires you to pass one or more ChangeNotifierProviders to its providers argument
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => ProductsProvider()
        ),
        ChangeNotifierProvider(
          create: (ctx) => CartProvider()
        ),
        ChangeNotifierProvider(
            create: (ctx) => OrdersProvider()
        ),
      ],
      // NOTE: The notifier provider requires that you provide an instance of your state management class
      // NOTE: The instance of the state management class will then be provided to the wrapped widget and its child tree
      // NOTE: PERFORMANCE: With this, only child widgets that are actively listening for data changes will be rebuilt in the UI, this is in contrast to the entire widget tree being rebuilt if the state was managed at the root of the app manually
      // NOTE: Create is used when using the ChangeNotifierProvider and having only a single provider (state object)
      // create: (context) => ProductsProvider(),
      child: MaterialApp(
        title: 'Shop-It-Up!',
        theme: PrimaryTheme.buildTheme(),
        home: ProductListScreen(),
        // NOTE: Named routes for easy screen navigation
        routes: {
          ProductDetailsScreen.screenRoute: (ctx) => ProductDetailsScreen(),
          CartDisplayScreen.screenRoute: (ctx) => CartDisplayScreen(),
          OrdersListScreen.screenRoute: (ctx) => OrdersListScreen(),
          UserProductsListScreen.screenRoute: (ctx) => UserProductsListScreen(),
          ProductFormScreen.screenRoute: (ctx) => ProductFormScreen()
        },
      ),
    );
  }
}
