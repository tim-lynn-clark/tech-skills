class ProductRoutes {
  final String _baseRoute = 'https://shop-it-up-demo.firebaseio.com/products';
  final String _json = '.json';

  get base {
    return '$_baseRoute$_json';
  }

  String baseWithId(id) {
    return '$_baseRoute/$id$_json';
  }
}