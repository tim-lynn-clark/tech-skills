import 'package:flutter/material.dart';

class PrimaryTheme {
  static ThemeData buildTheme() {
    return ThemeData(
      primarySwatch: Colors.blue,
      accentColor: Colors.deepOrange,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      fontFamily: 'Lato',
    );
  }
}