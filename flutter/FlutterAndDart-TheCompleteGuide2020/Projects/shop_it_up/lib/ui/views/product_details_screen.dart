import 'package:flutter/material.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:shop_it_up/models/test_products.dart';

class ProductDetailsScreen extends StatelessWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/product-details-screen';

  @override
  Widget build(BuildContext context) {

    // NOTE: Pulling the arguments from the screen navigation that occurred making this new screen
    final product = ModalRoute.of(context).settings.arguments as Product;

    return Scaffold(
      appBar: AppBar(
        title: Text(product.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                product.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 10,),
            Text(
              '\$${product.price}',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 20,
              ),
            ),
            SizedBox(height: 10,),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              child: Text(
                '${product.description}',
                textAlign: TextAlign.center,
                softWrap: true,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 15,
                ),
              ),
            ),
          ]
        ),
      )
    );
  }
}
