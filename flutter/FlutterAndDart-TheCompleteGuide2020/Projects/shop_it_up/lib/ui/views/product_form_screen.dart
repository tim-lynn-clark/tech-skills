import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:shop_it_up/providers/products_provider.dart';

class ProductFormScreen extends StatefulWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/products-form-screen';

  ProductFormScreen({Key key}) : super(key: key);

  @override
  _ProductFormScreenState createState() => _ProductFormScreenState();
}

class _ProductFormScreenState extends State<ProductFormScreen> {
  // NOTE: Using a FocusNode to place the screen focus on a specific input element in a form
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  // NOTE: Normally a form will maintain the state of all input nodes within it, but when you need to know
  // NOTE: the value of a filed has changed within the form (for the image preview) you will still need
  // NOTE: to manually add a TextEditingController and assign it to the input who's value you want while in the form
  final _imageUrlController = TextEditingController();

  // NOTE: To make the image preview update not only when the keyboard is closed, but when the input field loses focus
  // NOTE: we can use a FocusNode assigned to the imageURL input field.
  final _imageFocusNode = FocusNode();

  // NOTE: GlobalKeys are rarely used except with widgets containing forms as they provide access to the form state
  // NOTE: from outside of the form itself. This is extremely useful for accessing the data gathered via a form and
  // NOTE: storing that data.
  final _productForm = GlobalKey<FormState>();

  // NOTE: Used by the addProduct method to determine if a spinner should be displayed
  // NOTE: while a new product is being saved to the Firebase database.
  bool _isLoading = false;

  // NOTE: Variable to store the product created when save product is called.
  Product _currentProduct = Product(
    id: null,
    title: '',
    price: 0.0,
    description: '',
    imageUrl: '',
  );

  @override
  void initState() {
    // NOTE: manually adding a listener to the focus node, so we are alerted when the image url input loses focus and we can updated the image preview
    _imageFocusNode.addListener(_handleUpdateImagePreview);
    super.initState();
  }

  bool _isInit = true;
  @override
  void didChangeDependencies() {
    // NOTE: We do not want to run this code every time the build method is called, which will happen within didChangeDependencies
    if(_isInit) {
      // NOTE: Pull the product that was passed as an argument to the form
      final passedProduct = ModalRoute.of(context).settings.arguments as Product;
      if(passedProduct != null) {
        _currentProduct = passedProduct;
        // NOTE: When manually using a TextEditingController and not leaving it up to the From to generate them
        // NOTE: behind the scenes as in the case of the image url, you must set its initial value using the controller,
        // NOTE: you are not allowed to do it using the initialValue property of the text field as is done with the title, price, and description text fields in the form below
        // NOTE: see https://alvinalexander.com/flutter/how-to-supply-initial-value-textformfield/
        _imageUrlController.text = _currentProduct.imageUrl;
      }
      _isInit = false;
    }

    super.didChangeDependencies();
  }


  // Validation functions
  String _validateImageUrl(value) {
    if(value.isEmpty){
      return 'Please provide a image URL.';
    }
    if(!value.startsWith('http://') && !value.startsWith('https://')){
      return 'Please provide a valid URL.';
    }
    if(!value.endsWith('.png') && !value.endsWith('.jpg') && !value.endsWith('.jpeg')){
      return 'Please provide a valid image (png, jpg, jpeg).';
    }

    return '';
  }

  // NOTE: Event handler added to the image focus node, so we know when that input field no longer has focus and we update the state so the image preview will be updated
  void _handleUpdateImagePreview() {
    if(!_imageFocusNode.hasFocus) {
      // NOTE: Stop preview update if URL validation fail
      if(_validateImageUrl(_imageUrlController.text).isNotEmpty) {
        print(_imageUrlController.text);
        return;
      }

      // NOTE: The controller is actively monitoring the input field on its own and storing the value
      // NOTE: so although the widget's state needs to be updated, we do not need to pass anything to the setState method
      // NOTE: as we are not manually changing the value in the controller, we just need the new value in it to be recorded
      setState(() {});
    }
  }

  // NOTE: Handling form submission
  void _saveProduct() async {

    // NOTE: Form validation can be kicked off manually by calling validate on the form's GlobalKey
    if(!_productForm.currentState.validate()) {
      return;
    }

    // NOTE: Saves the current state of the form by triggering a onSaved method on each input widget
    // NOTE: assigned to the form and pulling out the current values stored in those input widgets and providing them
    // NOTE: as values to an anonymous function.
    // NOTE: These values can then be stored in another data structure for persistence purposes.
    _productForm.currentState.save();

    // NOTE: at the beginning of the product creation we indicate a loading spinner should be displayed
    setState(() {
      _isLoading = true;
    });

    if(_currentProduct.id != null) {
      // Editing existing product as it has an id
      await Provider.of<ProductsProvider>(context, listen: false).updateProduct(_currentProduct);

      // NOTE: Remove the spinner
      setState(() {
        _isLoading = false;
      });

      // NOTE: remove and destroy this screen and move back to the previous one
      Navigator.of(context).pop();
    } else {
      // NOTE: Save product through the state container
      // NOTE: addProduct saves product to Firebase database and returns a Future to let us know when the save was completed
      Provider.of<ProductsProvider>(context, listen: false).addProduct(_currentProduct)
          // NOTE: Handling save errors
          .catchError((error){
            // NOTE: ShowDialog returns a future allowing our .then below to still be executed on a failure as well as on a success
            return showDialog<Null>(
                context: context,
                builder: (ctx) => AlertDialog(
                  title: const Text('An error has occurred!'),
                  content: Text('Something went wrong'),
                  actions: [
                    FlatButton(
                      child: const Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                )
            );
          })
          .then((_){
            // NOTE: Remove the spinner
            setState(() {
              _isLoading = false;
            });

            // NOTE: remove and destroy this screen and move back to the previous one
            Navigator.of(context).pop();
          });
    }
  }

  // NOTE: FocusNodes, TextEditingControllers, and FocusNode Listeners used manually must be disposed of when widget dies or else they remain and consume memory
  // NOTE: Use the dispose method of the widget to clean them up.
  @override
  void dispose() {
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageFocusNode.removeListener(_handleUpdateImagePreview);
    _imageFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Product'),
        actions: [
          // NOTE: Adding a save button to the top menu
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _saveProduct,
          )
        ],
      ),
      // NOTE: forms within flutter automatically handle:
      // NOTE: - creating TextEditingControllers for all controls added, values of the inputs are provided when the form is submitted (no access to the behind the scene input controllers the form creates)
      // NOTE: - field validation
      body: _isLoading
      ? Center(
        child: CircularProgressIndicator(),
      )
      : Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          // NOTE: Setting the GlobalKey to the form so its data can be accessed outside of the form
          // NOTE: extremely useful in form submission handlers.
          key: _productForm,
          child: SingleChildScrollView(
            child: Column(
              children: [
                // NOTE: Instead of using TextField, which would require manually using TextEditingControllers to maintain state
                // NOTE: we use TextFormFields instead, which work together with the From widget to maintain the state of the fields in the form
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Title',
                  ),
                  initialValue: _currentProduct.title,
                  // NOTE: textInputAction controls the look and action of the enter key on the soft-keyboard
                  // NOTE: When using a custom action in place of the enter key, you have to manage the onPress of that button manually
                  // NOTE: in this instance we want the 'next' button on the keyboard to move to the next input field
                  textInputAction: TextInputAction.next,
                  // NOTE: Text fields can have validation added to them by providing an anonymous function that validates
                  // NOTE: the current input in the field. Returning 'null' means there are no issues, returning a 'String' containing an error message means validation was not successful.
                  validator: (value) {
                    if(value.isEmpty){
                      return 'Please provide a title.';
                    }
                    return null;
                  },
                  // NOTE: Fired when the enter key or action button is clicked on the keyboard
                  // NOTE: takes an anonymous function that accepts the value entered in this input
                  onFieldSubmitted: (value) {
                    // NOTE: FocusScope uses the current context to locate the form in which this input exists
                    // NOTE: then using the requestFocus method, the screen focus can be passed from this input to the one attached to the passed in FocusNode
                    FocusScope.of(context).requestFocus(_priceFocusNode);
                  },
                  // NOTE: This function is automatically called when the form.save method is called on the form's GlobalKey.currentState
                  onSaved: (value) {
                    // NOTE: Because we are using an immutable data model pattern, whenever we update the current product
                    // NOTE: we have to create a new instance and transfer existing values while setting new ones
                    _currentProduct = Product(
                      // NOTE: Updated value set
                      title: value,
                      // NOTE: Copying existing values
                      id: _currentProduct.id,
                      price: _currentProduct.price,
                      description: _currentProduct.description,
                      imageUrl: _currentProduct.imageUrl,
                    );
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Price',
                  ),
                  initialValue: _currentProduct.price.toString(),
                  // NOTE: This will change the soft-keyboard type to a number pad
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  validator: (value) {
                    if(value.isEmpty) {
                      return 'Please provide a price.';
                    }
                    if(double.tryParse(value) == null) {
                      return 'Please enter a valid dollar value.';
                    }
                    if(double.parse(value) <= 0) {
                      return 'Please enter a value greater than 0.';
                    }
                    return null;
                  },
                  // NOTE: Assigns a FocusNode (instantiated above) to this input field
                  focusNode: _priceFocusNode,
                  onFieldSubmitted: (value) {
                    FocusScope.of(context).requestFocus(_descriptionFocusNode);
                  },
                  onSaved: (value) {
                    _currentProduct = Product(
                      // NOTE: Updated value set
                      price: double.parse(value),
                      // NOTE: Copying existing values
                      id: _currentProduct.id,
                      title: _currentProduct.title,
                      description: _currentProduct.description,
                      imageUrl: _currentProduct.imageUrl,
                    );
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Description',
                  ),
                  initialValue: _currentProduct.description,
                  // NOTE: for inputs accepting long text, adjust the maxLines property
                  maxLines: 3,
                  // NOTE: change the keyboard to one supporting multiline text for better user experience
                  keyboardType: TextInputType.multiline,
                  focusNode: _descriptionFocusNode,
                  validator: (value) {
                    if(value.isEmpty){
                      return 'Please provide a description.';
                    }
                    if(value.length < 10){
                      return 'Should be at least 10 characters long.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _currentProduct = Product(
                      // NOTE: Updated value set
                      description: value,
                      // NOTE: Copying existing values
                      id: _currentProduct.id,
                      title: _currentProduct.title,
                      price: _currentProduct.price,
                      imageUrl: _currentProduct.imageUrl,
                    );
                  },
                ),
                // NOTE: Setting up an image preview within the form
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      margin: const EdgeInsets.only(top: 8, right: 10,),
                      decoration: BoxDecoration(border: Border.all(
                        width: 1,
                        color: Colors.grey,
                      )),
                      child: _imageUrlController.text.isEmpty
                          ? Text('enter a url')
                          : FittedBox(
                        //TODO: should as some validation to see if it a valid URL before trying to load it
                        child: Image.network(
                            _imageUrlController.text,
                          fit: BoxFit.cover
                        ),
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Image URL',
                        ),
                        // NOTE: See notes in didChangeDependencies as to why initialValue will not work here due to the
                        // NOTE: manual use of a TextEditingController
                        // initialValue: _currentProduct.imageUrl,
                        // NOTE: sets the soft-keyboard to one that best supports the input of URLs
                        keyboardType: TextInputType.url,
                        // NOTE: Changes the enter key on the keyboard to a done key
                        textInputAction: TextInputAction.done,
                        // NOTE: assigning a manual controller allows us to get the current value of this input
                        // NOTE: prior to the submission of the form.
                        controller: _imageUrlController,
                        // NOTE: adding this focus node will allow us to set the image preview when the input loses focus
                        focusNode: _imageFocusNode,
                        validator: (value) {
                          String error = _validateImageUrl(value);
                          if(error.isNotEmpty) {
                            return error;
                          }
                          return null;
                        },
                        // NOTE: due to the use of a InputEditingController to capture the value of this input
                        // NOTE: we need to make sure the value is stored in the state via that controller
                        onEditingComplete: () {
                          // NOTE: The controller is actively monitoring the input field on its own and storing the value
                          // NOTE: so although the widget's state needs to be updated, we do not need to pass anything to the setState method
                          // NOTE: as we are not manually changing the value in the controller, we just need the new value in it to be recorded
                          setState(() {});
                        },
                        // NOTE: Saving the form when the soft keyboard enter/done is tapped
                        onFieldSubmitted: (value) {
                          _saveProduct();
                        },
                        onSaved: (value) {
                        _currentProduct = Product(
                          // NOTE: Updated value set
                          imageUrl: value,
                          // NOTE: Copying existing values
                          id: _currentProduct.id,
                          title: _currentProduct.title,
                          price: _currentProduct.price,
                          description: _currentProduct.description,
                        );
                      },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


// NOTE: Image fix code
// Expanded(
// child: TextFormField(
// decoration: InputDecoration(labelText: 'Image URL'),
// keyboardType: TextInputType.url,
// textInputAction: TextInputAction.done,
// controller: _imageUrlController,
// onEditingComplete: () {
// setState(() {});
// },
// ),