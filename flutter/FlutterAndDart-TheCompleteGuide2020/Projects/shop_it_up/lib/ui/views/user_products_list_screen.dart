import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/providers/products_provider.dart';
import 'package:shop_it_up/ui/views/product_form_screen.dart';
import 'package:shop_it_up/ui/widgets/app_drawer.dart';
import 'package:shop_it_up/ui/widgets/user_product_list_item.dart';

class UserProductsListScreen extends StatelessWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/user-products-list-screen';

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<ProductsProvider>(context, listen: false).fetchAndSetProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(ProductFormScreen.screenRoute);
            },
          )
        ]
      ),
      drawer: AppDrawer(),
      body: Consumer<ProductsProvider>(
        builder: (context, productsProvider, child) {
          // NOTE: Using a refresh indicator widget to implement pull-down refresh for this screen
          return RefreshIndicator(
            onRefresh: () {
              return _refreshProducts(context);
            },
            child: Padding(
              padding: EdgeInsets.all(8),
              child: ListView.builder(
                itemCount: productsProvider.products.length,
                itemBuilder: (ctx, index) {
                  return Column(
                    children: [
                      UserProductListItem(
                        product: productsProvider.products[index],
                      ),
                      Divider()
                    ]
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
