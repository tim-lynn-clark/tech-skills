import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/providers/cart_provider.dart';
import 'package:shop_it_up/providers/products_provider.dart';
import 'package:shop_it_up/ui/views/cart_display_screen.dart';
import 'package:shop_it_up/ui/widgets/app_drawer.dart';
import 'package:shop_it_up/ui/widgets/badge.dart';
import 'package:shop_it_up/ui/widgets/products_grid.dart';

class ProductListScreen extends StatefulWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/product-list-screen';

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

enum FilterOptions {
  favorites,
  all
}

class _ProductListScreenState extends State<ProductListScreen> {
  bool _showOnlyFavorites = false;

  // NOTE: flag variable used in didChangeDependencies lifecycle method in order to make sure init code runs only once.
  bool _isInit = true;
  bool _isLoading = false;
  // NOTE: using the didChangeDependencies instead of initState in order to get access to the context and the data providers held therein
  @override
  void didChangeDependencies() {

    if(_isInit) {
      setState(() {
        _isLoading = true;
      });

      //NOTE: Load product data required by this screen
      Provider.of<ProductsProvider>(context).fetchAndSetProducts()
        .then((_) {
          setState(() {
            _isLoading = false;
          });
        }
      );
      _isInit = false;
    }

    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
        actions: [
          // NOTE: creates a vertical three dot menu item in the app bar that contains menu items
          PopupMenuButton(
            onSelected: (FilterOptions option) {
              setState(() {
                if(option == FilterOptions.favorites) {
                  _showOnlyFavorites = true;
                } else {
                  _showOnlyFavorites = false;
                }
              });
            },
            icon: Icon(
              Icons.more_vert,
            ),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text('Only Favorites'),
                value: FilterOptions.favorites,
              ),
              PopupMenuItem(
                child: Text('All'),
                value: FilterOptions.all,
              ),
            ],
          ),
          // NOTE: Using the consumer wrapper widget to provide state change notifications to just the badge widget
          Consumer<CartProvider>(
            builder: (context, cartProvider, child) => Badge(
              child: child,
              value: cartProvider.itemCount.toString(),
            ),
            // NOTE: Using the option child argument to pass in a portion of the consumer wrapped widget that does not need to be redrawn when state changes
            // NOTE: you can see that the icon will never change, only the value of the text displayed over the icon
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
              ),
              onPressed: () {
                Navigator.of(context).pushNamed(CartDisplayScreen.screenRoute);
              },
            ),
          )
        ],
      ),
      drawer: AppDrawer(),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
          )
          : ProductsGrid(showOnlyFavorites: _showOnlyFavorites),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
