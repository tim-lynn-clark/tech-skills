import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/providers/cart_provider.dart';
import 'package:shop_it_up/providers/orders_provider.dart';
import 'package:shop_it_up/ui/widgets/cart_item_display.dart';

class CartDisplayScreen extends StatelessWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/cart-display-screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cart'),
      ),
      body: Consumer<CartProvider>(
      builder: (context, cartProvider, child) =>  Column(
        children: [
          Card(
            margin: const EdgeInsets.all(15),
            child: Padding(
              padding: const EdgeInsets.all(8),
              // NOTE: Using a consumer and the CartProvider to get access to the cart state object
              // NOTE: this will only updated the necessary sub-tree of this widget's tree.
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Total',
                      style: TextStyle(fontSize: 20),
                    ),
                    // NOTE: Alignment: Spacer will take up all available space between two widgets pushing them to each side
                    // NOTE: really helpful when trying to get spacing right between controls on the screen
                    Spacer(),
                    Chip(
                      label: Text(
                        '\$${cartProvider.totalAmount}',
                        style: TextStyle(
                          color: Theme.of(context).primaryTextTheme.headline1.color,
                        ),
                      ),
                      backgroundColor: Theme.of(context).accentColor,
                    ),
                    FlatButton(
                      child: Text('ORDER NOW!'),
                      onPressed: () {
                        // NOTE: Using app state to get access to orders state provider to add a new order
                        // NOTE: since we are accessing just to add a new order, we do not need to listen to state changes
                        // NOTE: because we do not want to listen, we set the listen argument to false to stop widget rebuilds
                        Provider.of<OrdersProvider>(context, listen: false).addOrder(
                          cartProvider.items.values.toList(),
                          cartProvider.totalAmount
                        );

                        // NOTE: now we clear the cart provider's state now that it has transitioned to the orders provider's state
                        cartProvider.clear();
                      },
                      textColor: Theme.of(context).primaryColor,
                    ),
                  ],
                ),
            ),
          ),
          SizedBox(height: 10,),
          Expanded(
            child: ListView.builder(
              itemCount: cartProvider.items.length,
              itemBuilder: (ctx, index) {
                return CartItemDisplay(cartItem: cartProvider.items.values.toList()[index]);
              },
            ),
          )
        ],
      ),
      ),
    );
  }
}
