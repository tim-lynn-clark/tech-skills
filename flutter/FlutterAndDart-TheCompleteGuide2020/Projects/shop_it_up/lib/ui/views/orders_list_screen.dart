import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/providers/orders_provider.dart';
import 'package:shop_it_up/ui/widgets/app_drawer.dart';
import 'package:shop_it_up/ui/widgets/order_item_display.dart';

class OrdersListScreen extends StatelessWidget {
  // NOTE: consistent key for this screen to be used when navigating screens
  static const String screenRoute = '/orders-list-screen';

  @override
  Widget build(BuildContext context) {
    // NOTE: Accessing Order Provider State in order to disply order history
    final OrdersProvider ordersProvider = Provider.of<OrdersProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Orders'),
      ),
      drawer: AppDrawer(),
      body: ListView.builder(
        itemCount: ordersProvider.orders.length,
        itemBuilder: (ctx, index) {
          return OrderItemDisplay(ordersProvider.orders[index]);
        },
      ),
    );
  }
}
