import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:shop_it_up/providers/products_provider.dart';
import 'package:shop_it_up/ui/views/product_form_screen.dart';

class UserProductListItem extends StatelessWidget {
  final Product product;
  const UserProductListItem({this.product});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(product.title),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(
          product.imageUrl
        ),
      ),
      trailing: Container(
        width: 100,
        child: Row(
          children: [
            IconButton(
              icon: Icon(Icons.edit),
              color: Theme.of(context).primaryColor,
              onPressed: () {
                Navigator.of(context).pushNamed(
                  ProductFormScreen.screenRoute,
                  arguments: product
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.delete),
              color: Theme.of(context).errorColor,
              onPressed: () {
                Provider.of<ProductsProvider>(context, listen: false).deleteProduct(product.id);
              },
            ),
          ]
        ),
      ),
    );
  }
}
