import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/models/cart_item.dart';
import 'package:shop_it_up/providers/cart_provider.dart';

class CartItemDisplay extends StatelessWidget {
  final CartItem cartItem;
  const CartItemDisplay({this.cartItem});

  @override
  Widget build(BuildContext context) {

    // NOTE: Dismissible provides the ability to swipe a widget off the screen and capture that event
    return Dismissible(
      // NOTE: Dismissible requires a unique key in order to handle the removal of a widget from the list in which it is contained
      key: ValueKey(cartItem.product.id),
      // NOTE: Dismissible allows you to set a background that will be shown as the dismiss animation kicks off.
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(right: 20),
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 4)
      ),
      // NOTE: Allows restricting the direction the swipe can happen
      direction: DismissDirection.endToStart,
      // NOTE: With most destructive actions, you will want to verify the user actually wants to perform the action
      confirmDismiss: (direction) {
        // NOTE: confirmDismiss requires that a future/promise is provided as its return value
        // NOTE: awesome thing is, showDialog returns a future in order to handle the clicking of the dialog actions buttons
        return showDialog(
          context: context,
          builder: (ctx) {
            return AlertDialog(
              title: const Text('Are you sure'),
              content: const Text('Do you want to remove the item from the cart?'),
              actions: [
                FlatButton(
                  child: const Text('No'),
                  onPressed: () {
                    // NOTE: we can use the built in functionality inherent in the Navigator to return data to the caller
                    // NOTE: the pop function that removes the current screen, in this case this dialog, you can pass data as an optional argument
                    Navigator.of(ctx).pop(false);
                  },
                ),
                FlatButton(
                  child: const Text('Yes'),
                  onPressed: () {
                    // NOTE: we can use the built in functionality inherent in the Navigator to return data to the caller
                    // NOTE: the pop function that removes the current screen, in this case this dialog, you can pass data as an optional argument
                    Navigator.of(ctx).pop(true);
                  },
                )
              ],
            );
          }
        );
      },
      // NOTE: Capturing the dismiss action and the direction and handling the delete
      onDismissed: (direction) {
        Provider.of<CartProvider>(context, listen: false).removeItem(productId: cartItem.product.id);
      },
      child: Card(
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(
              child: Padding(
                padding: const EdgeInsets.all(5),
                // NOTE: Wrapping a Text widget in a FittedBox will assure that the text fits within its containing widget
                child: FittedBox(
                    child: Text('\$${cartItem.product.price}'),
                ),
              ),
            ),
            title: Text(cartItem.product.title),
            subtitle: Text('Total: \$${cartItem.product.price * cartItem.quantity }'),
            trailing: Text('x ${cartItem.quantity}'),
          ),
        ),
      ),
    );
  }
}
