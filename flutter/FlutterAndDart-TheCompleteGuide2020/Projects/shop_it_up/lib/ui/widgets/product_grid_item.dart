import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:shop_it_up/providers/cart_provider.dart';
import 'package:shop_it_up/ui/views/product_details_screen.dart';

class ProductGridItem extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    // NOTE: Pulling the product from the product provider that was created in the products grid widget
    // NOTE: calling without listen set to false will trigger and entire widget tree rebuild, which can be expensive
    // NOTE: by calling with listen set to false, it will load once, which the widget is first built and no subsequent rebuilds will take place when the data in the provider changes
    // NOTE: this means this widget would never rebuild due to data changes in the provider. This is useful when only a part of this widget's tree needs to be updated due to provider data changes
    // NOTE: to updated only subtree within this widget we can wrap the leading widget with the Consumer widget (see below)
    Product productProvider = Provider.of<Product>(context, listen: false);
    CartProvider cartProvider = Provider.of<CartProvider>(context, listen: false);

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () {
            // NOTE: Navigator used to navigate to new screens named in app routes
            Navigator.of(context).pushNamed(
              ProductDetailsScreen.screenRoute,
              // NOTE: arguments used to pass data from this widget to the new screen
              arguments: productProvider
            );
          },
          child: Image.network(
            productProvider.imageUrl,
            fit: BoxFit.cover,
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          // NOTE: using the consumer container widget provides the ability to update only a portion of the widget tree when provider data is updated
          // NOTE: only the IconButton will be updated as the isFavorite status of a product is manipulated. This is very useful in widgets with very large trees
          leading: Consumer<Product>(
            builder: (ctx, product, child) => IconButton(
              icon: product.isFavorite ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
              color: Theme.of(context).accentColor,
              // NOTE: we are now calling the toggleIsFavorite method on the product model which is also a change provider
              // NOTE: when toggleIsFavorite is called, the method updates the isFavorite property on the product and the notifyListeners method is called
              // NOTE: since this widget listens to the product provider for changes, it will be updated as the product is updated
              onPressed: () => product.toggleIsFavorite(),
            ),
          ),
          title: Text(
            productProvider.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            icon: Icon(Icons.shopping_cart),
            color: Theme.of(context).accentColor,
            onPressed: () {
              cartProvider.addItem(product: productProvider);
              // NOTE: When using scaffold in this manner we are asking flutter to look up the widget tree hierarchy
              // NOTE: and find us the closest scaffold within the context. In this case it would be the scaffold in the ProductListScreen
              // NOTE: Using a reference to the nearest widget with a scaffold, usually the current screen on which this widget is on, you can do things such as:

              // NOTE: Automatically open the drawer, if the scaffold has one
              //Scaffold.of(context).openDrawer();

              // NOTE: If your method will allow snackbars to be rendered in quick succession
              // NOTE: you can automatically dismiss the current snackbar so the next one can be shown.
              // NOTE: in this case quickly adding items will cause the snackbars to pile up
              Scaffold.of(context).hideCurrentSnackBar();
              // NOTE: Automatically open the snackbar, a modal dialog with action buttons
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(
                  'Add item to cart.',
                ),
                duration: Duration(seconds: 2),
                action: SnackBarAction(
                  label: 'UNDO',
                  onPressed: () {
                    cartProvider.removeSingleItem(productId: productProvider.id);
                  },
                )
              ));

              
            },
          ),
        )
      ),
    );
  }
}
