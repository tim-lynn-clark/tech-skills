import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_it_up/models/product.dart';
import 'package:shop_it_up/providers/products_provider.dart';
import 'package:shop_it_up/ui/widgets/product_grid_item.dart';

class ProductsGrid extends StatelessWidget {
  bool showOnlyFavorites = false;

  ProductsGrid({this.showOnlyFavorites});

  @override
  Widget build(BuildContext context) {

    // NOTE: Using the provider package, we can now pull the products list from the Products state object
    // NOTE: the products state object was set as the state object in main.dart using the ChangeNotifierProvider around the app
    // NOTE: By setting the generic type on the .of call, we can specify which state object we are looking for within the widget hierarchy
    final ProductsProvider productsProvider = Provider.of<ProductsProvider>(context);
    final List<Product> products = showOnlyFavorites ? productsProvider.favoriteProducts : productsProvider.products;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      itemBuilder: (ctx, index) {
        // NOTE: Wrapping the contents of the GridView itemBuilder means that for each product
        // NOTE: a new product provider will be created that is associated with each product
        // NOTE: that exists in the list.
        // NOTE: this means we do not need to pass the actual product to the ProductGridItem as it can now
        // NOTE: pull its product from the associated product provider that was created one level up in the
        // NOTE: widget hierarchy. It also means that it can subscribe to changes within the product provider allowing it to see when isFavorite changes
        // return ChangeNotifierProvider(
        //   create: (c) => products[index],
        //   child: ProductGridItem()
        // );
        // NOTE: The .value constructor is to be used when you are creating a single provider per object in a list or grid
        // NOTE: this is because lists and grids will recycle the visually rendered components as they leave the screen due to scrolling
        // NOTE: meaning they will take the next data object and tie it to a reused component UI, which means the UI will be out of date when it scrolls back into view
        // NOTE: the .value constructor stops the reuse issue and forces the UI to be recreated when it is tied to a provider object
        return ChangeNotifierProvider.value(
            value: products[index],
            child: ProductGridItem()
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}
