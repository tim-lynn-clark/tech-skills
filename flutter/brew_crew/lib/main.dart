import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/ui/screens/wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  //NOTE: Verify the widget is ready to go before initializing Firebase for this app.
  WidgetsFlutterBinding.ensureInitialized();
  //NOTE: https://stackoverflow.com/questions/63492211/no-firebase-app-default-has-been-created-call-firebase-initializeapp-in
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //NOTE: Using a StreamProvider to listen to our AuthService user stream,
    //NOTE: which is monitoring for authentication changes.
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        title: 'Brew Crew',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Wrapper(),
      ),
    );
  }
}