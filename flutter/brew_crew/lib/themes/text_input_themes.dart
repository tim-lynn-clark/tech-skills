import 'package:flutter/material.dart';

//NOTE: Separating out the text input decoration code for reuse across application forms
//NOTE: the decorator can be modified by using the copyWith method and altering just the
//NOTE: properties that need to be tweaked in a specific form
const textInputDecorationPinkBorder = InputDecoration(
  labelText: 'CHANGE ME USING COPY WITH',
  fillColor: Colors.white,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white,
      width: 2.0,
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.pink,
      width: 2.0,
    ),
  ),
);