import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/themes/text_input_themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SignIn extends StatefulWidget {
  final Function selectionHandler;
  const SignIn({this.selectionHandler});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool _isLoading = false;
  String _error = '';

  final AuthService _auth = AuthService();

  final _signInForm = GlobalKey<FormState>();
  final FocusNode _passwordFocusNode = FocusNode();

  var _currentUserAuth = {
    'email': '',
    'password': ''
  };

  void _signUserIn() async {
    if(!_signInForm.currentState.validate()) {
      return;
    }

    _signInForm.currentState.save();

    setState(() {
      _isLoading = true;
    });

    //NOTE: Using register with email and password function created in custom auth service
    dynamic result = await _auth.signInWithEmailAndPassword(
      email: _currentUserAuth['email'],
      password: _currentUserAuth['password'],
    );

    if(result == null) {
      setState(() {
        _error = 'Please supply a valid email and/or password.';
      });
    }

    if(this.mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    _passwordFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: const Text('Sign into Brew Crew'),
        actions: [
          FlatButton.icon(
            icon: Icon(
              Icons.person,
            ),
            label: const Text('Register'),
            onPressed: () {
              widget.selectionHandler();
            },
          )
        ],
      ),
      body:
      _isLoading
      ? Center(
        child: SpinKitCubeGrid(
          color: Colors.pink,
          size: 50.0,
        ),
      )
      : Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //NOTE: Button to demonstrate how to do anonymous login
              // RaisedButton(
              //   child: const Text('Sign in anon'),
              //   //NOTE: Widget event handlers can be set to 'async' allowing await to be used within them
              //   onPressed: () async {
              //     //NOTE: using the AuthService to sign the anonymous user into the application
              //     dynamic result = await _auth.signInAnon();
              //
              //     if(result == null) {
              //       //TODO: Handle errors gracefully
              //       print('error signing in');
              //     } else {
              //       print('signed in');
              //       print(result.uid);
              //     }
              //   },
              // ),
              Form(
                key: _signInForm,
                child: Column(
                  children: [
                    SizedBox(height: 20.0),
                    TextFormField(
                      autofocus: true,
                      //NOTE: Storing the input decoration in a file under themes
                      //NOTE: using the copyWith method to alter the labelText attribute of the decorator
                      decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Email'),
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if(value.isEmpty){
                          return 'Please provide an email.';
                        }
                        return null;
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(_passwordFocusNode);
                      },
                      onSaved: (value) {
                        _currentUserAuth['email'] = value;
                      },
                    ),
                    SizedBox(height: 20.0),
                    TextFormField(
                      focusNode: _passwordFocusNode,
                      obscureText: true,
                      //NOTE: Storing the input decoration in a file under themes
                      //NOTE: using the copyWith method to alter the labelText attribute of the decorator
                      decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Password'),
                      textInputAction: TextInputAction.done,
                      validator: (value) {
                        if(value.isEmpty){
                          return 'Please provide a password.';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _currentUserAuth['password'] = value;
                      },
                    ),
                    SizedBox(height: 20.0),
                    RaisedButton(
                      color: Colors.pink[400],
                      child: const Text(
                        'Sign in',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _signUserIn();
                      },
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      _error,
                      style: TextStyle(color: Colors.red, fontSize: 14.0),
                    )
                  ],
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}