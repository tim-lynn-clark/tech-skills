import 'package:brew_crew/models/brew.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/ui/controls/brew_form.dart';
import 'package:brew_crew/ui/controls/brew_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {

  //NOTE: Accessing the Auth Service
  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {


    //NOTE: UI - Bottom Sheet Modal
    //NOTE: Event handler used to show a modal bottom sheet on the screen
    void _showSettingsPanel() {
      showModalBottomSheet(context: context, builder: (context) {
        return Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 60),
          child: BrewForm(),
        );
      });
    }



    //NOTE: Using a Stream Provider to watch the Brews query snapshot from the Database Service
    //NOTE: this allows all consumers to be notified when brews in the query are altered and the UI can be updated
    return StreamProvider<List<Brew>>.value(
      value: DatabaseService().brews,
      child: Scaffold(
        backgroundColor: Colors.brown[50],
          appBar: AppBar(
            title: const Text('Brew Crew'),
            backgroundColor: Colors.brown[400],
            elevation: 0.0,
            actions: [
              FlatButton.icon(
                icon: Icon(Icons.person),
                label: const Text('Logout'),
                //NOTE: Widget event handlers can be set to async allowing await to be used within the function body
                onPressed: () async {
                  //NOTE: Signing a user out via the Auth Service
                  //NOTE: Once this executes, the user within the Auth Service should become null as it is returned from
                  //NOTE: the Firebase Auth service. When this is returned the User stream should notify of a change in authentication
                  //NOTE: and the StreamProvider listening to the User stream from the Auth Service, which is available through the main.dart file
                  //NOTE: The wrapper widget, which is listening to the Stream Provider, will be notified that the user became null and it should switch screens
                  await _auth.signOut();
                },
              ),
              FlatButton.icon(
                icon: Icon(Icons.settings),
                label: const Text('settings'),
                onPressed: () => _showSettingsPanel(),
              )
            ],
          ),
        //NOTE: Using a container with a BoxDecorator to add a background image to a screen
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/coffee_bg.png'),
              fit: BoxFit.cover
            )
          ),
          child: BrewList()
        ),
      ),
    );
  }
}
