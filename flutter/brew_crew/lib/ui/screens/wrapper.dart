import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/ui/screens/authenticate.dart';
import 'package:brew_crew/ui/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//NOTE: Wrapper widget that listens for authentication changes and determines which screen to show
class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //NOTE: Accessing the StreamProvider that is monitoring the AuthService user stream
    //NOTE: and which is being provided in main.dart to the entire app
    final _user = Provider.of<User>(context);

    if(_user == null) {
      return Authenticate();
    }
    return Home();

  }
}
