import 'package:brew_crew/ui/screens/register.dart';
import 'package:brew_crew/ui/screens/sign_in.dart';
import 'package:flutter/material.dart';

class Authenticate extends StatefulWidget {
  Authenticate({Key key}) : super(key: key);

  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool _showSignIn = true;

  void _toggleView() {
    setState(() => _showSignIn = !_showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _showSignIn ? SignIn(selectionHandler: _toggleView) : Register(selectionHandler: _toggleView),
    );
  }
}