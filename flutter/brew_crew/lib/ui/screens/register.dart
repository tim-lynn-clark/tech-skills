import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/themes/text_input_themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Register extends StatefulWidget {
  final Function selectionHandler;
  const Register({this.selectionHandler});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool _isLoading = false;
  String _error = '';

  final AuthService _auth = AuthService();

  final _registrationForm = GlobalKey<FormState>();
  final FocusNode _passwordFocusNode = FocusNode();

  var _currentUserAuth = {
    'email': '',
    'password': ''
  };

  void _registerUser() async {
    if(!_registrationForm.currentState.validate()) {
      return;
    }

    _registrationForm.currentState.save();

    setState(() {
      _isLoading = true;
    });

    //NOTE: Using register with email and password function created in custom auth service
    dynamic result = await _auth.registerWithEmailAndPassword(
      email: _currentUserAuth['email'],
      password: _currentUserAuth['password'],
    );

    if(result == null) {
      setState(() {
        _error = 'Please supply a valid email and/or password.';
      });
    }

    if(this.mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: const Text('Sing up for Brew Crew'),
        actions: [
          FlatButton.icon(
            icon: Icon(
              Icons.person,
            ),
            label: const Text('Sign In'),
            onPressed: () {
              widget.selectionHandler();
            },
          )
        ],
      ),
      body:
      _isLoading
          ? Center(
        child: SpinKitCubeGrid(
          color: Colors.pink,
          size: 50.0,
        ),
      )
          : Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: SingleChildScrollView(
          child: Column(
              children: [
                Form(
                  key: _registrationForm,
                  child: Column(
                    children: [
                      SizedBox(height: 20.0),
                      TextFormField(
                        autofocus: true,
                        //NOTE: Storing the input decoration in a file under themes
                        //NOTE: using the copyWith method to alter the labelText attribute of the decorator
                        decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Email'),
                        textInputAction: TextInputAction.next,
                        validator: (value) {
                          if(value.isEmpty){
                            return 'Please provide an email.';
                          }
                          return null;
                        },
                        onFieldSubmitted: (value) {
                          FocusScope.of(context).requestFocus(_passwordFocusNode);
                        },
                        onSaved: (value) {
                          _currentUserAuth['email'] = value;
                        },
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        focusNode: _passwordFocusNode,
                        obscureText: true,
                        //NOTE: Storing the input decoration in a file under themes
                        //NOTE: using the copyWith method to alter the labelText attribute of the decorator
                        decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Password'),
                        textInputAction: TextInputAction.done,
                        validator: (value) {
                          if(value.isEmpty){
                            return 'Please provide a password.';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _currentUserAuth['password'] = value;
                        },
                      ),
                      SizedBox(height: 20.0),
                      RaisedButton(
                        color: Colors.pink[400],
                        child: const Text(
                          'Register',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          _registerUser();
                        },
                      ),
                      SizedBox(height: 20.0),
                      Text(
                        _error,
                        style: TextStyle(color: Colors.red, fontSize: 14.0),
                      )
                    ],
                  ),
                )
              ]
          ),
        ),
      ),
    );
  }
}