import 'package:brew_crew/models/brew.dart';
import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/themes/text_input_themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import '../../services/database.dart';

class BrewForm extends StatefulWidget {
  BrewForm({Key key}) : super(key: key);

  @override
  _BrewFormState createState() => _BrewFormState();
}

class _BrewFormState extends State<BrewForm> {

  User _user;
  Brew _currentUserBrew;
  final _brewForm = GlobalKey<FormState>();
  final List<String> _sugars = ['0','1','2','3','4'];
  String _error = '';
  bool _isLoading = false;
  bool _isInit = true;

  Map<String, dynamic> _currentBrew = {
    'name': '',
    'sugars': '0',
    'strength': 100,
  };

  final FocusNode _sugarsFocusNode = FocusNode();
  final FocusNode _strengthFocusNode = FocusNode();

  void _savePreferences() async {
    if(!_brewForm.currentState.validate()) {
      return;
    }

    _brewForm.currentState.save();

    setState(() {
      _isLoading = true;
    });

    final DatabaseService _database = DatabaseService(uid: _user.uid);
    dynamic result = await _database.updateUserData(
      name: _currentBrew['name'],
      sugars: _currentBrew['sugars'],
      strength: _currentBrew['strength']
    );

    // if(result == null) {
    //   setState(() {
    //     _error = 'Please make valid selections.';
    //   });
    // }

    if(this.mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void dispose() {
    _sugarsFocusNode.dispose();
    _strengthFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //NOTE: Access the currently logged in user
    _user = Provider.of<User>(context);

    return SingleChildScrollView(
      child: StreamBuilder<Brew>(
        stream: DatabaseService(uid: _user.uid).userBrew,
        builder: (context, snapshot) {

          //NOTE: Verify that the stream has returned data prior to trying to use it
          if(snapshot.hasData){

            //NOTE: Pull the brew for the current user from the stream
            _currentUserBrew = snapshot.data;

            if(_isInit) {

              _currentBrew['name'] = _currentUserBrew.name ?? _currentBrew['name'];
              _currentBrew['sugars'] = _currentUserBrew.sugars ?? _currentBrew['sugars'];
              _currentBrew['strength'] = _currentUserBrew.strength ?? _currentBrew['strength'];

              _isInit = false;
            }

            return
              _isLoading
              ? Center(
                  child: SpinKitCubeGrid(
                    color: Colors.pink,
                    size: 50.0,
                  ),
                )
              : Form(
              key: _brewForm,
              child: Column(
                children: [
                  Text(
                      'Update your brew preferences.',
                      style: TextStyle(fontSize: 18)
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    initialValue: _currentBrew['name'],
                    autofocus: true,
                    //NOTE: Storing the input decoration in a file under themes
                    //NOTE: using the copyWith method to alter the labelText attribute of the decorator
                    decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Name'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if(value.isEmpty){
                        return 'Please provide a name.';
                      }
                      return null;
                    },
                    onFieldSubmitted: (value) {
                      FocusScope.of(context).requestFocus(_sugarsFocusNode);
                    },
                    onSaved: (value) {
                      _currentBrew['name'] = value;
                    },
                  ),
                  SizedBox(height: 20.0),
                  DropdownButtonFormField(
                    value: _currentBrew['sugars'],
                    decoration: textInputDecorationPinkBorder.copyWith(labelText: 'Sugars'),
                    items: _sugars.map((sugar){
                      return DropdownMenuItem(
                        value: sugar,
                        child: Text('$sugar sugars'),
                      );
                    }).toList(),
                    focusNode: _sugarsFocusNode,
                    onChanged: (value) {
                      setState(() {
                        _currentBrew['sugars'] = value;
                      });
                      FocusScope.of(context).requestFocus(_strengthFocusNode);
                    },
                  ),
                  SizedBox(height: 20.0),
                  Slider(
                    value: _currentBrew['strength'].toDouble(),
                    activeColor: Colors.brown[_currentBrew['strength']],
                    min: 100.0,
                    max: 900.0,
                    divisions: 8,
                    onChanged: (value) {
                      setState(() {
                        _currentBrew['strength'] = value.round();
                      });
                    },
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    color: Colors.pink[400],
                    child: const Text(
                      'Update',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      _savePreferences();
                    },
                  ),
                  SizedBox(height: 20.0),
                  Text(
                    _error,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                  )
                ],
              ),
            );
          }

          return Center(
            child: SpinKitCubeGrid(
              color: Colors.pink,
              size: 50.0,
            ),
          );
        }
      ),
    );
  }
}