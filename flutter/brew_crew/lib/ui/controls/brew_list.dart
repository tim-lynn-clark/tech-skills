import 'package:brew_crew/models/brew.dart';
import 'package:brew_crew/ui/controls/brew_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class BrewList extends StatefulWidget {
  BrewList({Key key}) : super(key: key);

  @override
  _BrewListState createState() => _BrewListState();
}

class _BrewListState extends State<BrewList> {

  @override
  Widget build(BuildContext context) {

    //NOTE: Accessing the brews query stream for updated via the Stream Provider from the home screen
    final brews = Provider.of<List<Brew>>(context);

    return brews == null
    ? Center(
      child: SpinKitCubeGrid(
        color: Colors.pink,
        size: 50.0,
      ),
    )
    : ListView.builder(
      itemCount: brews.length,
      itemBuilder: (context, index) {
        return BrewTile(brew: brews[index]);
      },
    );
  }
}