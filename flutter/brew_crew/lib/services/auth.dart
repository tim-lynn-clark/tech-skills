import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart' as FireAuth;

class AuthService {

  //NOTE: Provides access to a singleton instance of the FirebaseAuth object
  final FireAuth.FirebaseAuth _auth = FireAuth.FirebaseAuth.instance;

  User _userFromFirebaseUser(FireAuth.User user) {
    return user != null ? User(uid: user.uid) : null;
  }

  //NOTE: Auth change user stream
  //NOTE: By using a stream object, we can listen for auth changes (sign-in, sign-out)
  //NOTE: and be notified when a change happens so we can respond to those changes
  //NOTE: in this demo, we use this in the wrapper widget
  Stream<User> get user {
    //NOTE: When an auth change happens a user will be returned if signed in, a null will be returned if signed out
    return _auth.authStateChanges()
        //NOTE: Usually returns a Firebase Auth User, using Map, that user can be mapped into an Application-specific user model
        .map((FireAuth.User user) => _userFromFirebaseUser(user));
  }

  //NOTE: Allows anonymous users to sign into the app
  Future<dynamic> signInAnon() async {
    try {
      FireAuth.UserCredential credential = await _auth.signInAnonymously();
      FireAuth.User user = credential.user;
      return _userFromFirebaseUser(user);
    } catch(error) {
      //TODO: SignInAnon - Handle error
      print(error.toString());
      return null;
    }
  }

  //NOTE: Sign in with email and password
  Future signInWithEmailAndPassword({String email, String password}) async {
    try {
      FireAuth.UserCredential credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FireAuth.User user = credential.user;
      return _userFromFirebaseUser(user);
    } catch(error) {
      //TODO: SignInAnon - Handle error
      print(error.toString());
      return null;
    }
  }

  //NOTE: Register with email and password
  Future registerWithEmailAndPassword({String email, String password}) async {
    try {
      FireAuth.UserCredential credential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FireAuth.User user = credential.user;

      // Create a new Brew entry for this newly created user
      dynamic brewEntry = await DatabaseService(uid: user.uid).updateUserData(
        sugars: '0',
        name: '',
        strength: 100
      );

      if(brewEntry == null) {
        //TODO: Handle failure gracefully
      }


      return _userFromFirebaseUser(user);
    } catch(error) {
      //TODO: SignInAnon - Handle error
      print(error.toString());
      return null;
    }
  }

  //NOTE: Sign in a user out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(error) {
      //TODO: SignOut - Handle error
      print(error.toString());
      return null;
    }
  }

}