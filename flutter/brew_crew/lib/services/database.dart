import 'package:brew_crew/models/brew.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {

  final String uid;
  DatabaseService({this.uid});

  // Collection reference
  final CollectionReference brewsCollection = FirebaseFirestore.instance.collection('brews');

  // Get brew stream for listening to brew collection changes
  //NOTE: Creating a stream that can be provided to and watched by a subscriber
  //NOTE: when a change happens to the brews collection, the subscriber will be notified
  //NOTE: a StreamProvider is used to provide this stream to all subscribers through the home screen
  Stream<List<Brew>> get brews {
    return brewsCollection.snapshots().map(_brewListFromSnapshot);
  }
  //NOTE: Transforming a brew snapshot into actual data models for the app
  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      Map<String, dynamic> docData = doc.data();
      return Brew(
        uid: docData['uid'] ?? '',
        name: docData['name'] ?? '',
        sugars: docData['sugars'] ?? '0',
        strength: docData['strength'] ?? 0,
      );
    }).toList();
  }

  // Get user brew stream
  //NOTE: Creating a stream that can be provided to and watched by a subscriber within the application
  //NOTE: when a change happens to the user's brew document, any subscriber interested in the change will be notified.
  //NOTE: a StreamProvider is used to provide this stream to all subscribers through the brew form
  Stream<Brew> get userBrew {
    return brewsCollection.doc(uid).snapshots().map(_brewFromSnapshot);
  }
  //NOTE: Transforming a brew snapshot into an actual brew data model for the app
  Brew _brewFromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> docData = snapshot.data();
    return Brew(
      uid: docData['uid'] ?? '',
      name: docData['name'] ?? '',
      sugars: docData['sugars'] ?? '0',
      strength: docData['strength'] ?? 0,
    );
  }



  Future updateUserData({String sugars, String name, int strength}) async {
    try {
      return await brewsCollection.doc(uid).set({
        'sugars': sugars,
        'name': name,
        'strength': strength
      });
    } catch(error) {
      //TODO: handle errors gracefully
      print(error.toString());
      return null;
    }
  }

}