export {}

/* NOTE: TS has Type Guarding when Unions are used.
* In this situation, let's say there exists the possibility that the combine function
* might receive a string, number, or null due to to some issues that might happen with
* user input. For each type that is defined in the union type, TS will require the developer
* to have code within the combine method that will check for types and handle them individually
* where necessary. This ensures that when the parameters are used within the function, we
* can be assured that each type was checked for and handled appropriately.  */

/* NOTE: Built-in types are easy to guard for using the `typeof` function. */
type CombinePossibilities = number | string | null;

function combine(num1: CombinePossibilities, num2: CombinePossibilities): CombinePossibilities {

    // return num1 + num2; // ERROR: Will not transpile as no type guards have been done

    // NOTE: Guarding for stings
    if(typeof num1 === "string" || typeof num2 === "string") {
        return `${num1} & ${num2}`;
    }

    // NOTE: Guarding for numbers
    if(typeof num1 == "number" && typeof num2 === "number") {
        return num1 + num2;
    }

    return null;
}

console.log(
    combine(5, 6)
);

console.log(
    combine("5", "6")
);

console.log(
    combine("5", 6)
);

console.log(
    combine(5, "6")
);

console.log(
    combine(null, 6)
);

console.log(
    combine(5, null)
);

console.log(
    combine(null, "6")
);

console.log(
    combine("5", null)
);

/* NOTE: For custom types, guarding is a little different. */
class Car {
    drive() {
        console.log("Driving smoothly along");
    }

    burnOut() {
        console.log("Those are some smoke'n tires, literally!")
    }
}
class Truck {
    drive() {
        console.log("Bumps down the rows.");
    }

    loadCargo() {
        console.log("Cargo is being loaded now...");
    }
}

const v1 = new Car();
const v2 = new Truck();

type Vehicle = Car | Truck;

function useVehicle(vehicle: Vehicle) {

    vehicle.drive(); // Works because both expected types have a drive method, no need to guard.
    // vehicle.burnOut(); // ERROR: There is no guarantee that the passed in type will have this method, since it only exists on Car
    // vehicle.loadCargo() // ERROR: There is no guarantee that the passed in type will have this method, since it only exists on Truck

    /* NOTE: A type guard can be performed by first checking to see if the object has the expected property. */
    // Danger is the property is a string and can be typed incorrectly causing a runtime defect.
    if('burnOut' in vehicle) {
        vehicle.burnOut(); // Burnout can now be called since the burnOut property has been proven to exists prior to being called.
    }

    /* NOTE: A type guard can also be performed through the use of the `instanceof` function.  */
    // Better than a property check as it does not rely on string literals.
    if(vehicle instanceof Truck) {
        vehicle.loadCargo(); // loadCargo is now available since the vehicle has been verified to be of type Truck, which defines that method.
    }
}

useVehicle(v1);
useVehicle(v2);
