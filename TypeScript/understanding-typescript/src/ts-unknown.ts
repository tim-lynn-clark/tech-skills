export {}

/* NOTE: Unlike the :any type, which specifies that a variable can hold any data type we ask it to,
* the :unknown type identifies that a variable is intended to hold something unknown at the time
* it is received and assigned to that variable. */

let userFromInput: unknown;

userFromInput = 5;
userFromInput = "This has changed";

let userName: string;

/* NOTE: Unlike the :any type, where the below statement would work, it fails because
* unknown forces the developer to do a type check prior to assigning it. Essentially it forces
* developers to be safe with unknown data received. */

// userName = userFromInput; // ERROR: thrown due to unknown cannot be assigned to type string without a type check

// Works because a type check was performed prior to assignment.
if(typeof userFromInput === 'string') {
    userName = userFromInput;
    console.log(userName);
}


/* NOTE: :unknown is preferred over :any as it enforces TS strong typing through the use of a
* manual type check. */