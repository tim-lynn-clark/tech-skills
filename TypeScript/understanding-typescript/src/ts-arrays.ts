export {}


const person = {
    name: 'Tim',
    age: 20,
    address: {
        street: '123 test street',
        city: 'Testsville',
        zip: '84043'
    },
    hobbies: ['Sports', 'Cooking', 'Camping']
};
/* NOTE: TS will set inferred types on Arrays. In the hobbies array above, the type has been set to
* an array of strings `hobbies string[]`. The inference works on each element of the array see below. */
for(const hobby of person.hobbies) {
    console.log(hobby);
    // Due to the inference, you can use all of the type specific functions without issue
    console.log(hobby.toLowerCase());

    // hobby.map() // ERROR: TS knows this should be a string and not an array and throws an error
}


/* NOTE: TS will infer this array as a mixed type array and make it a `any[]`. Again, this provides native JS-style
* typeless functionality from within TS and should be avoided. */
let mixedArray = ['String', 6.5, true];
mixedArray[1] = 'change to string';

console.log(mixedArray);
