export {}

/* NOTE: TS supports interfaces */

/* NOTE: Can be used for defining how an object must be structured.
* They can define the properties the object should have as well as methods. */
interface Person {
    name: string;
    age: number;
    greet(phrase: string): void;
}

let tim:Person;

tim = {
    name: "Tim",
    age: 20,
    greet(phrase: string) {
        console.log(phrase);
    }
}

tim.greet("Hey there people...");

/* NOTE: Can also be used as contracts on classes. Access modifiers like public, protected, and private
* cannot be used in interfaces, but readonly can. */

/* NOTE: Interfaces can use inheritance. */
interface Named {
    readonly name: string;
}

interface Greetable extends Named {
    greet(phrase: string): void;
}

/* NOTE: Interfaces can have optional properties using the ? operator. */
interface Walkable {
    isWalking: boolean;
    isRunning?: boolean;
}

/* NOTE: Multiple interfaces can be implemented by the same class. */
class Worker implements Greetable, Walkable {
    name: string;
    isWalking: boolean;

    constructor(name: string) {
        this.name = name;
        this.isWalking = true;
    }

    greet(phrase: string): void {
        console.log(phrase + this.name);
    }
}

const berry = new Worker("Berry");
berry.greet("Hello, I am ");


/* NOTE: Interfaces can be dynamic through the use of index properties. This is useful only
* when you need to provide a high-level interface that describes what properties an object must
* provide without specifying the names of those properties. */

/* In this example, we expect an object that implements this interface to have a property
* that has a name that is a string and holds a string. */
interface ErrorContainer {
    [prop: string]: string;
}

const errorBag: ErrorContainer = {} // Valid, as the dynamic interface's properties are not mandatory when using indexed properties

const errorBagTwo: ErrorContainer = {
    // message: 1 // ERROR: fails because the value is not a string as required by the interface
    message: 'The line above will cause a tranpiling error.'
}

console.log(errorBag);
console.log(errorBagTwo);