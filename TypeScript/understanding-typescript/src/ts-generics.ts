/* NOTE: Generics are provided by TS and are not available in JS. */

/* NOTE: First method uses a strongly-typed array of strings
* second method uses the Array class with a generics syntax specifying it will accept strings  */
const names: string[] = [];
const colors: Array<string> = [];

/* NOTE: Promises can be used normally as seen in JS or can be used with generics */
// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if(true) {
//             resolve('This function completed, promise is resolved.');
//         } else {
//             reject('This failed');
//         }
//
//     }, 3000);
// });

// const promiseTwo: Promise<string> = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if(true) {
//             resolve('This function completed, promise is resolved.');
//         } else {
//             reject('This failed');
//         }
//
//     }, 3000);
// });

/* NOTE: Creating your own generic types. */
function merge<T extends object, U extends object>(objectA: T, objectB: U): T & U {
    return Object.assign(objectA, objectB);
}

const merged = merge({name: "Tim"}, {age: 20});
console.log(merged);

// {
//     name: "Tim",
//     age: 20
// }

/* NOTE: Error as the generic types declared within the function definition are constrained to some
* type that extends object. */
// const result = merge<string, string>("Tim", "Clark");
// console.log(result);

/* NOTE: Partial<> generic, really useful when creating objects dynamically due to validation or some step-by-step
* creation process that does not allow the properties to be added up front. */
interface Address {
    street: string;
    city: string;
    state: string;
    zip: string;
}

interface User {
    name: string;
    email: string;
    address: Address;
}

// function createNewUser(name: string, email: string,
//                        street: string, city: string,
//                        state: string, zip: string): User {
//     let newUser = {};
//
//     // Error: every attribute set causes an error because newUser does not
//     // correctly implement the user interface prior to setting attributes
//     // Validate name...
//     newUser.name = name;
//     // Validate email...
//     newUser.email = email;
//     // Validate address...
//
//     let address = {};
//
//     // Error: every attribute set causes an error because address does not
//     // correctly implement the address interface prior to setting attributes
//     // Validate street...
//     address.street = street;
//     // Validate city...
//     address.city = city;
//     // Validate state...
//     address.state = state;
//     // Validate zip...
//     address.zip = zip;
//
//     newUser.address = address;
//
//
//     return newUser;
// }

/* NOTE: Using the Partial<> generic, it allwos you to dynamically add object attributes dynamically
* like in regular JavaScript with out TypeScript errors. Again, this pretty much removes the type safety of
* TypeScript moving your back to vanilla JavaScript and should be avoided where possible. */
function createNewUser(name: string, email: string,
                       street: string, city: string,
                       state: string, zip: string): User {
    let newUser: Partial<User> = {};

    // Validate name...
    newUser.name = name;
    // Validate email...
    newUser.email = email;
    // Validate address...

    let address: Partial<Address> = {};

    // Validate street...
    address.street = street;
    // Validate city...
    address.city = city;
    // Validate state...
    address.state = state;
    // Validate zip...
    address.zip = zip;

    // NOTE: In order to return a Partial<> generic as the type the generic is representing, you have to type cast it
    newUser.address = address as Address;

    // NOTE: In order to return a Partial<> generic as the type the generic is representing, you have to type cast it
    return newUser as User;
}
