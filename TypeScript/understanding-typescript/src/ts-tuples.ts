export {}

/* NOTE: Tuples are fixed-length & fixed-type arrays and are used to strictly control
and array to represent a piece of data with a very specific structure that should not be
violated.
*
* Using Tuples requires that you add an explicit interface to the object using the tuple as
* seen below with the role attribute.  */
const person: {
    name: string;
    age: number;
    role: [number, string];
} = {
    name: 'Tim',
    age: 20,
    role: [1, 'Author']
};

console.log(person);

/* NOTE: Unfortunately, since Tuples are a TS concept and do not translate to JS, the push and
* pop methods of an array still work and can mess up your tuples if not caught BE AWARE. */
// person.role.push('Admin'); // RUNTIME ERROR: this will mess up your tuple since JS allows it.
