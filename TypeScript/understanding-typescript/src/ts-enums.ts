export {}

/* NOTE: TS adds the ability to use Enums, which do not exist in JS */

enum Role {
    ADMIN,
    READ_ONLY,
    AUTHOR
}

const person = {
    name: 'Tim',
    age: 20,
    role: Role.READ_ONLY
}

console.log(person);

/* NOTE: Enums start at 0 for the value by default. The starting number for the increment can be
* adjusted. Now it will begin at 100 and move up from there 101, 102, ... */
enum RoleHigher {
    ADMIN = 100,
    READ_ONLY,
    AUTHOR
}
console.log(RoleHigher);

/* NOTE: Enums are not restricted to auto-incremented integers. */
enum RoleFixed {
    ADMIN = 200,
    READ_ONLY = 205,
    AUTHOR = 210
}
console.log(RoleFixed);

/* NOTE: Enums can be set to other value types, not just numbers. */
enum RoleStrings {
    ADMIN = 'admin',
    READ_ONLY = 'read_only',
    AUTHOR = 'author'
}
console.log(RoleStrings);