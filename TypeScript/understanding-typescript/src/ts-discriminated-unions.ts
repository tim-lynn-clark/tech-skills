/* NOTE: `typeof` and `instanceof` do not work with Interfaces when doing type guards.
* A work around is to provide a common property on each with a unique key that is a string literal on
* which a `switch` can operate. */
interface Bird {
    type: "bird";
    flyingSpeed: number;
}

interface Horse {
    type: "horse";
    runningSpeed: number;
}

type Animal = Bird | Horse;

// NOTE: This function is now guarded by a switch statement.
function moveAnimal(animal: Animal) {
    let speed;
    switch (animal.type) {
        case "bird":
            speed = animal.flyingSpeed;
            break;
        case "horse":
            speed = animal.runningSpeed;
            break;
    }
    return speed;
}