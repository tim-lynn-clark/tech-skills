export {}

/* NOTE: JS classes can be used in TS, only difference is the addition of type information.  */
class Department {
    // // Can set a default value on a property declaration in a class if necessary
    // name: string = 'default value';
    // /* NOTE: TS adds access modifiers to classes. Public is the default. Private must be specified. */
    // public code: number;
    // private employees: string[] = [];
    //
    // constructor(name: string, code: number) {
    //     this.name = name;
    //     this.code = code;
    // }

    /* NOTE: Constructor shorthand for declaring and instantiating class properties (alternative to the verbose
    * method above. */
    constructor(
        // NOTE: Readonly can be used to specify a property's value should not change once set.
        // protected readonly id: string,
        protected id: string,
        public name: string,
        private employees: string[],
        // NOTE: Classes can have optional properties, they have to follow all required properties in order.
        public code?: number,
    ) {}



    // Methods
    describe() {
        console.log('Department: id => ' + this.id + ', name =>' + this.name);
    }

    /* NOTE: TS allows you to tightly bind methods to the class type in which they are defined.
    * This is to combat the accidental passing around of an object's methods within an application
    * where `this` gets reassigned to a context other than an instance of the class to which it belongs. */
    describeAgain(this: Department) {
        console.log('Department: ' + this.name);
    }

    addEmployee(name: string) {
        this.employees.push(name);
    }

    printEmployeeInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

const accounting = new Department(
    "apt0001",
    "Accounting",
    ['Tim', 'Sara'],
    20,
);

// Both are accessible since they are Public properties
console.log(accounting.name);
console.log(accounting.code);
// Employees array is not accessible since it is marked Private
// console.log(accounting.employees); // ERROR: Trying to access a private property of the Accounting object.

/* NOTE: Inheritance */
class ITDepartment extends Department {

    /* NOTE: TS supports getter functions */
    get uid() :string {
        return this.id;
    }

    set uid(value: string) {
        this.id = value;
    }

    constructor(
        // NOTE: Readonly can be used to specify a property's value should not change once set.
        id: string,
        name: string,
        employees: string[],
        code: number,
        private admins: string[]
    ) {
        super(id, name, employees, code);
    }

    describe() {
        console.log('Department: id => ' + this.id + ', name =>' + this.name);
    }

    printAdminInformation() {
        console.log(this.admins.length);
        console.log(this.admins);
    }

    /* NOTE: Static methods and properties are also supported. Example of a static method
     * on the JS Math library: Math.random() */
    static staticClassLevelUtilityMethod(print: string) {
        console.log(print);
    }

    static departmentPrefix = "apt";
}

ITDepartment.staticClassLevelUtilityMethod("This is a static method called on the ITDepartment class not one of its instances.")
console.log(ITDepartment.departmentPrefix);

const it = new ITDepartment(
    "apt0002",
    "Software Development",
    ['Betty', 'Joe'],
    30,
    ['Tim']);

console.log(it.name);
console.log(it.code);
console.log(it.uid);
it.printAdminInformation();