export {}

/* NOTE: As in JS, TS allows functions to be passed into and out of other functions.
* This means they can be both parameters and return types. Being that TS is strongly-typed,
* it needs a type that represents functions. */

function add(n1 :number, n2 :number) :number {
    return n1 + n2;
}

/* NOTE: Functions can of course be referenced with a variable as in JS.
* But, if a type is not specified at initialization, the type is set to any for the variable.
* You can then set it to reference a function, but it could also be changed later on
* in our code to some other type, which may cause issues when you try to execute that type using (). */
let referenceToAdd;
referenceToAdd = add;
console.log(referenceToAdd(5,10)); // Works fine
referenceToAdd = 5;
// referenceToAdd(); // ERROR: because the variable type is any, we run into issues just like in JS

/* NOTE: It would be best to tell TS that we are expecting a function using a function type. */
let typedReferenceToAdd :Function;
typedReferenceToAdd = add;
console.log(typedReferenceToAdd(5,10)); // Works fine
// typedReferenceToAdd = 5; // ERROR: With function set as the type, this causes an error.
typedReferenceToAdd(5,10);

/* NOTE: Creating your own function types that can restrict the type of function that can be referenced or passed,
* based on a specified function signature. */
let customTypedReferenceToAdd: (a: number, b: number) => number;
customTypedReferenceToAdd = add;
console.log(customTypedReferenceToAdd(5,10)); // Works fine
// customTypedReferenceToAdd = 5; // ERROR: With function set as the type, this causes an error.
// customTypedReferenceToAdd(); // ERROR: Throws an error because the function signature defined in the type definition for customTypedReferenceToAdd requires two parameters of type number
customTypedReferenceToAdd(5,10); // succeeds because we provide the correct parameters as defined by the function signature in the type definition.