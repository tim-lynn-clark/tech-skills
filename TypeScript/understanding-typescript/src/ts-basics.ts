export {}

/* NOTE:
* - Parameters are typed using the :type syntax following its identifier.
* - Function return types are defined using :type syntax in the function signature between the parameter list and the
*   `{` opening the function body (block).  */
function add(n1:number, n2:number) :number {
    return n1 + n2;
}

/* NOTE: TS uses type inference, so variables do not need to be strongly typed. They can however, be strongly typed
* if so desired using the same :type syntax used with function parameters.  */
// It is typical TS to allow the types of assigned variables to be inferred by the TS transpiler.
const number1 = 5;
// However, when creating a variable that will have its value assigned later, it is good practice to identify the
// intended type the variable will store.
let number2:number;
// ... assigned later in code
number2 = 2.8;

const result = add(number1, number2);
console.log(result);

/* NOTE: When TS uses type inference to determine the type of a variable at assignment, that inference
* cannot be undone, the variable is strongly-typed from that point forward. */
let myInferredVariable = 'this is now typed as string';
console.log(myInferredVariable);
// myInferredVariable = 1.5; // Trying to assign a float to the variable already inferred as a string will be rejected.

/* NOTE: Dynamically typed variables using :any allow you to have the dynamic nature of regular JS inside of TS.  */
let myAnyVariable:any;
myAnyVariable = 'Setting to a string.';
myAnyVariable = 6.5;
console.log(myAnyVariable);
