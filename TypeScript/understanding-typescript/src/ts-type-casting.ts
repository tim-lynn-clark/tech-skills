/* NOTE: Type casting is supported in TS */

/* NOTE: By default, getElementById returns a generic HTMLElement type or null */
const inputName = document.getElementById('name');

// const nameValue = inputName.value; // ERROR: Cannot use value property as not all HTML elements have a value property

/* NOTE: Works now that it has been cast as an input element rather than just a generic element. */
const nameValue = (inputName as HTMLInputElement).value;