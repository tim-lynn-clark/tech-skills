export {}

/* NOTE: Function return types are added after the parameter list. */
function add(n1 :number, n2 :number) :number {
    return n1 + n2;
}

function printResult(num :number) :void {
    console.log('Result: ' + num);
}

printResult(add(5, 12));


/* NOTE: Function overloads require that the original signature have :any as the type for all of its parameters.  */
function combine(value1: string, value2: string): string;
function combine(value1: number, value2: number): number;
function combine(value1: any, value2: any): any {

    if(typeof value1 === 'number' || typeof value2 === 'number') {
        return value1 + value2;
    }

    if(typeof value1 === 'string' || typeof value2 === 'string') {
        return `${value1} & ${value2}`;
    }
}

console.log(
    combine(1, 3)
)

console.log(
    combine('Tim', 'Clark')
)
