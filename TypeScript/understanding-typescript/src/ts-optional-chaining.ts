/* NOTE: TS allows for optional chaining so you do not have to do null/undefined checking as you
* move through an object's property tree (object with nested objects). */

const someDataFromTheInternet = {
    id: 1,
    date: '11/11/2021',
    data: {
        hash: '2342kjlkefji3j4234kj2kl',
        content: null,
    }
}

console.log(someDataFromTheInternet?.data?.hash);
console.log(someDataFromTheInternet?.data?.content);