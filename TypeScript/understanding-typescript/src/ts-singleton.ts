export {}

/* NOTE: TS supports private constructors, allowing the creation of Singleton Classes. */
class ConfigToRuleThemAll {

    get configuration() {
        return this.configValue;
    }

    private constructor(protected configValue: string) {}

    // NOTE: Singleton setup
    static instance: ConfigToRuleThemAll;
    static getSingleton(): ConfigToRuleThemAll {
        if(ConfigToRuleThemAll.instance) {
            return this.instance;
        }
        this.instance = new ConfigToRuleThemAll("{item:1}");
        return this.instance;
    }
}

// NOTE: The two variables are holding references to the same object.
const configInstance = ConfigToRuleThemAll.getSingleton();
console.log(configInstance);

const sameConfigInstance = ConfigToRuleThemAll.getSingleton();
console.log(sameConfigInstance);