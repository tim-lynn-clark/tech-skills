export {}

 /* NOTE: Union allows multiple types to be specified as acceptable when stored in
 * a variable. The `|` pipe is used to show this. You can union as many types as you
 * need. */

 // Returns the result of addition if both parameters are numbers
 // Returns a concatenated string if one of the parameters is a string
 function combine(input1: number|string, input2: number|string) {
     let result;
     if(typeof input1 === "number" && typeof input2 === "number") {
         result = input1 + input2;
     } else {
         result = input1.toString() + input2.toString();
     }
     return result;
 }

 const combinedAges = combine(20, 25);
 console.log(combinedAges);

 const combinedNames = combine('Tim', 'Clark');
 console.log(combinedNames);