export {}

/* NOTE: TS supports combining Types through intersections to create super type definitions.*/
type Admin = {
    name: string;
    privileges: string[];
};

type Employee = {
    name: string;
}

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
    name: "Tim",
    privileges: ['update-user', 'edit-user'],
}

console.log(e1);
