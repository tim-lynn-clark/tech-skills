export {}

/* NOTE: Literal types are types that represent a literal value.  */

/* NOTE: If you hover over the factor below, you will see that instead of a built-in or
* custom type being inferred as the type, you will see the literal numeric value 6.6
* as the set type for factor. This is because as a defined constant with its value
* set to 6.6, there is no need to infer types as the value 6.6 will "literally" be
* used as the type. */
const factor = 6.6;
console.log(factor);

/* NOTE: Literal types can be used in conjunction with the union operator (the pipe | )
* to specify that a very specific value must be passed as the value of a parameter. */
function SwitchOnLiteral(options: 'ice cream' | 'soda' | 'cake') :string {
    let response:string;
    switch (options) {
        case 'ice cream':
            response = "Here is your ice cream. Enjoy!";
            break;
        case 'soda':
            response = "Refreshing drink coming your way. Enjoy!";
            break;
        case 'cake':
            response = "Everyone loves cake. Enjoy!";
            break;
    }
    return response;
}

//console.log(SwitchOnLiteral('not-valid')); // ERROR: the value passed is not in the list of literal type values specified in the function definition
console.log(SwitchOnLiteral('ice cream'));