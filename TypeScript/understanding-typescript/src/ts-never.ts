export {}

/* NOTE: Specifies that a function will never return a result. */

/* NOTE: Great for functions that kick off an application ending error. */
function generateError(message: string, code: number): never {
    throw {
        message,
        code
    };
}

generateError('An error has occurred!', 500);

/* NOTE: Great for functions that contain an infinite loop, like those that kick off a server
* or some other application that needs to run endlessly. */