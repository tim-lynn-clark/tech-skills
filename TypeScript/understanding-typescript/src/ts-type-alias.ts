/* NOTE: Useful when you have multiple types in a union statement. Instead of
* placing the union in the parameter definition, you can alias the union as a type of its own.
* this allows unions to be reusable throughout the application.  */

type tastyFoods = 'ice cream' | 'soda' | 'cake';

function SwitchOnLiteral(options: tastyFoods) :string {
    let response:string;
    switch (options) {
        case 'ice cream':
            response = "Here is your ice cream. Enjoy!";
            break;
        case 'soda':
            response = "Refreshing drink coming your way. Enjoy!";
            break;
        case 'cake':
            response = "Everyone loves cake. Enjoy!";
            break;
    }
    return response;
}

console.log(SwitchOnLiteral('ice cream'));

/* NOTE: Does not just have to be used with unions, works for objects as well. */

// Instead of the following
// function greet(user: { name: string; age: number }) {
//     console.log('Hi, I am ' + user.name);
// }
//
// function isOlder(user: { name: string; age: number }, checkAge: number) {
//     return checkAge > user.age;
// }
export {}

// You can do this
type User = { name: string; age: number };

function greet(user: User) {
    console.log('Hi, I am ' + user.name);
}

function isOlder(user: User, checkAge: number) {
    return checkAge > user.age;
}

greet({
    name: 'Tim',
    age: 20
});

const result2 = isOlder({
    name: 'Tim',
    age: 20
}, 10);

console.log(result2);