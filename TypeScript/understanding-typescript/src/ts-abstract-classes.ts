export {}

/* NOTE: TS supports abstract classes to define the methods
* a class implementation must have. */
abstract class NoiseMaker {
    abstract beNoisy(): void;
}

class Whistle extends NoiseMaker {
    beNoisy() {
        console.log("Whistlllllllllllllle......");
    }
}

const refsWhistle = new Whistle();
refsWhistle.beNoisy();