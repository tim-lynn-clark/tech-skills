export {}

/* NOTE: Even Objects receive an inferred type from the TS transpiler. TS will create a
* interface for that object behind the scenes that holds each attribute of the object
* and their inferred types. This way we can have type safety on object properties as well.  */
const person = {
    name: 'Tim',
    age: 20,
    address: {
        street: '123 test street',
        city: 'Testsville',
        zip: '84043'
    }
};
/* NOTE: Nested objects will also receive inferred interfaces that are nested*/

console.log(person.name);
//console.log(person.nickname); // ERROR: It can see that the person object does not have a nickname property
//person.name = 5.6; // ERROR: The name property of the person object was inferred to be a string, so it will only accept strings.

/* NOTE: TS allows explicit interfaces to be provided for our custom objects. It is common practice to
* allow TS to handle this through inferred interfaces as it reduces the manual code that needs to be written
* when using objects. */
const developer: {
    name: string;
    age: number;
    role: string;
} = {
    name: 'Tim',
    age: 20,
    role: 'Senior Engineer'
}

console.log(developer);
// developer.name = 7.8; // ERROR: Type violation