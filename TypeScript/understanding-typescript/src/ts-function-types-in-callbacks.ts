export {}

/* NOTE: Function types can be extremely helpful when using callback functions.
* Through the use of a function type, the specific function signature can be provided
* that is required for the callback. This will stop incorrect callback functions
* from being passed as arguments. */

function add(n1: number, n2: number, callback: (num: number) => void ) {
    const result = n1 + n2;
    callback(result);
}

add(5, 10, () => {
    console.log("Does nothing, as nothing was passed to it.");
});