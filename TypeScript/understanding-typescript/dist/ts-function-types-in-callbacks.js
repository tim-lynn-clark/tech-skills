"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function add(n1, n2, callback) {
    var result = n1 + n2;
    callback(result);
}
add(5, 10, function () {
    console.log("Does nothing, as nothing was passed to it.");
});
//# sourceMappingURL=ts-function-types-in-callbacks.js.map