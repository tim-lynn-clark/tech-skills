"use strict";
var _a, _b;
var someDataFromTheInternet = {
    id: 1,
    date: '11/11/2021',
    data: {
        hash: '2342kjlkefji3j4234kj2kl',
        content: null,
    }
};
console.log((_a = someDataFromTheInternet === null || someDataFromTheInternet === void 0 ? void 0 : someDataFromTheInternet.data) === null || _a === void 0 ? void 0 : _a.hash);
console.log((_b = someDataFromTheInternet === null || someDataFromTheInternet === void 0 ? void 0 : someDataFromTheInternet.data) === null || _b === void 0 ? void 0 : _b.content);
//# sourceMappingURL=ts-optional-chaining.js.map