"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function add(n1, n2) {
    return n1 + n2;
}
var referenceToAdd;
referenceToAdd = add;
console.log(referenceToAdd(5, 10));
referenceToAdd = 5;
var typedReferenceToAdd;
typedReferenceToAdd = add;
console.log(typedReferenceToAdd(5, 10));
typedReferenceToAdd(5, 10);
var customTypedReferenceToAdd;
customTypedReferenceToAdd = add;
console.log(customTypedReferenceToAdd(5, 10));
customTypedReferenceToAdd(5, 10);
//# sourceMappingURL=ts-functions-as-types.js.map