"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function SwitchOnLiteral(options) {
    var response;
    switch (options) {
        case 'ice cream':
            response = "Here is your ice cream. Enjoy!";
            break;
        case 'soda':
            response = "Refreshing drink coming your way. Enjoy!";
            break;
        case 'cake':
            response = "Everyone loves cake. Enjoy!";
            break;
    }
    return response;
}
console.log(SwitchOnLiteral('ice cream'));
function greet(user) {
    console.log('Hi, I am ' + user.name);
}
function isOlder(user, checkAge) {
    return checkAge > user.age;
}
greet({
    name: 'Tim',
    age: 20
});
var result2 = isOlder({
    name: 'Tim',
    age: 20
}, 10);
console.log(result2);
//# sourceMappingURL=ts-type-alias.js.map