"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var factor = 6.6;
console.log(factor);
function SwitchOnLiteral(options) {
    var response;
    switch (options) {
        case 'ice cream':
            response = "Here is your ice cream. Enjoy!";
            break;
        case 'soda':
            response = "Refreshing drink coming your way. Enjoy!";
            break;
        case 'cake':
            response = "Everyone loves cake. Enjoy!";
            break;
    }
    return response;
}
console.log(SwitchOnLiteral('ice cream'));
//# sourceMappingURL=ts-literals.js.map