"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function add(n1, n2) {
    return n1 + n2;
}
var number1 = 5;
var number2;
number2 = 2.8;
var result = add(number1, number2);
console.log(result);
var myInferredVariable = 'this is now typed as string';
console.log(myInferredVariable);
var myAnyVariable;
myAnyVariable = 'Setting to a string.';
myAnyVariable = 6.5;
console.log(myAnyVariable);
//# sourceMappingURL=ts-basics.js.map