"use strict";
function moveAnimal(animal) {
    var speed;
    switch (animal.type) {
        case "bird":
            speed = animal.flyingSpeed;
            break;
        case "horse":
            speed = animal.runningSpeed;
            break;
    }
    return speed;
}
//# sourceMappingURL=ts-discriminated-unions.js.map