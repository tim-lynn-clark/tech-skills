"use strict";
var names = [];
var colors = [];
function merge(objectA, objectB) {
    return Object.assign(objectA, objectB);
}
var merged = merge({ name: "Tim" }, { age: 20 });
console.log(merged);
function createNewUser(name, email, street, city, state, zip) {
    var newUser = {};
    newUser.name = name;
    newUser.email = email;
    var address = {};
    address.street = street;
    address.city = city;
    address.state = state;
    address.zip = zip;
    newUser.address = address;
    return newUser;
}
//# sourceMappingURL=ts-generics.js.map