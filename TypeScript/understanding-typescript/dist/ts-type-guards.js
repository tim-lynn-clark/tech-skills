"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function combine(num1, num2) {
    if (typeof num1 === "string" || typeof num2 === "string") {
        return num1 + " & " + num2;
    }
    if (typeof num1 == "number" && typeof num2 === "number") {
        return num1 + num2;
    }
    return null;
}
console.log(combine(5, 6));
console.log(combine("5", "6"));
console.log(combine("5", 6));
console.log(combine(5, "6"));
console.log(combine(null, 6));
console.log(combine(5, null));
console.log(combine(null, "6"));
console.log(combine("5", null));
var Car = (function () {
    function Car() {
    }
    Car.prototype.drive = function () {
        console.log("Driving smoothly along");
    };
    Car.prototype.burnOut = function () {
        console.log("Those are some smoke'n tires, literally!");
    };
    return Car;
}());
var Truck = (function () {
    function Truck() {
    }
    Truck.prototype.drive = function () {
        console.log("Bumps down the rows.");
    };
    Truck.prototype.loadCargo = function () {
        console.log("Cargo is being loaded now...");
    };
    return Truck;
}());
var v1 = new Car();
var v2 = new Truck();
function useVehicle(vehicle) {
    vehicle.drive();
    if ('burnOut' in vehicle) {
        vehicle.burnOut();
    }
    if (vehicle instanceof Truck) {
        vehicle.loadCargo();
    }
}
useVehicle(v1);
useVehicle(v2);
//# sourceMappingURL=ts-type-guards.js.map