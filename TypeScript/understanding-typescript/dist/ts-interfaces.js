"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tim;
tim = {
    name: "Tim",
    age: 20,
    greet: function (phrase) {
        console.log(phrase);
    }
};
tim.greet("Hey there people...");
var Worker = (function () {
    function Worker(name) {
        this.name = name;
        this.isWalking = true;
    }
    Worker.prototype.greet = function (phrase) {
        console.log(phrase + this.name);
    };
    return Worker;
}());
var berry = new Worker("Berry");
berry.greet("Hello, I am ");
var errorBag = {};
var errorBagTwo = {
    message: 'The line above will cause a tranpiling error.'
};
console.log(errorBag);
console.log(errorBagTwo);
//# sourceMappingURL=ts-interfaces.js.map