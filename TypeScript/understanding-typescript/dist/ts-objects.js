"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var person = {
    name: 'Tim',
    age: 20,
    address: {
        street: '123 test street',
        city: 'Testsville',
        zip: '84043'
    }
};
console.log(person.name);
var developer = {
    name: 'Tim',
    age: 20,
    role: 'Senior Engineer'
};
console.log(developer);
//# sourceMappingURL=ts-objects.js.map