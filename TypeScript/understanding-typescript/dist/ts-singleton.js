"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigToRuleThemAll = (function () {
    function ConfigToRuleThemAll(configValue) {
        this.configValue = configValue;
    }
    Object.defineProperty(ConfigToRuleThemAll.prototype, "configuration", {
        get: function () {
            return this.configValue;
        },
        enumerable: false,
        configurable: true
    });
    ConfigToRuleThemAll.getSingleton = function () {
        if (ConfigToRuleThemAll.instance) {
            return this.instance;
        }
        this.instance = new ConfigToRuleThemAll("{item:1}");
        return this.instance;
    };
    return ConfigToRuleThemAll;
}());
var configInstance = ConfigToRuleThemAll.getSingleton();
console.log(configInstance);
var sameConfigInstance = ConfigToRuleThemAll.getSingleton();
console.log(sameConfigInstance);
//# sourceMappingURL=ts-singleton.js.map