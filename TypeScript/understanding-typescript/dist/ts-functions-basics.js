"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function add(n1, n2) {
    return n1 + n2;
}
function printResult(num) {
    console.log('Result: ' + num);
}
printResult(add(5, 12));
function combine(value1, value2) {
    if (typeof value1 === 'number' || typeof value2 === 'number') {
        return value1 + value2;
    }
    if (typeof value1 === 'string' || typeof value2 === 'string') {
        return value1 + " & " + value2;
    }
}
console.log(combine(1, 3));
console.log(combine('Tim', 'Clark'));
//# sourceMappingURL=ts-functions-basics.js.map