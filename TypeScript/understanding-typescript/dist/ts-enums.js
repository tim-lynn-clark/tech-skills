"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
    Role[Role["AUTHOR"] = 2] = "AUTHOR";
})(Role || (Role = {}));
var person = {
    name: 'Tim',
    age: 20,
    role: Role.READ_ONLY
};
console.log(person);
var RoleHigher;
(function (RoleHigher) {
    RoleHigher[RoleHigher["ADMIN"] = 100] = "ADMIN";
    RoleHigher[RoleHigher["READ_ONLY"] = 101] = "READ_ONLY";
    RoleHigher[RoleHigher["AUTHOR"] = 102] = "AUTHOR";
})(RoleHigher || (RoleHigher = {}));
console.log(RoleHigher);
var RoleFixed;
(function (RoleFixed) {
    RoleFixed[RoleFixed["ADMIN"] = 200] = "ADMIN";
    RoleFixed[RoleFixed["READ_ONLY"] = 205] = "READ_ONLY";
    RoleFixed[RoleFixed["AUTHOR"] = 210] = "AUTHOR";
})(RoleFixed || (RoleFixed = {}));
console.log(RoleFixed);
var RoleStrings;
(function (RoleStrings) {
    RoleStrings["ADMIN"] = "admin";
    RoleStrings["READ_ONLY"] = "read_only";
    RoleStrings["AUTHOR"] = "author";
})(RoleStrings || (RoleStrings = {}));
console.log(RoleStrings);
//# sourceMappingURL=ts-enums.js.map