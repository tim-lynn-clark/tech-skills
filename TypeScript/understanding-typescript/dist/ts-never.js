"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function generateError(message, code) {
    throw {
        message: message,
        code: code
    };
}
generateError('An error has occurred!', 500);
//# sourceMappingURL=ts-never.js.map