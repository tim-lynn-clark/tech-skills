"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Department = (function () {
    function Department(id, name, employees, code) {
        this.id = id;
        this.name = name;
        this.employees = employees;
        this.code = code;
    }
    Department.prototype.describe = function () {
        console.log('Department: id => ' + this.id + ', name =>' + this.name);
    };
    Department.prototype.describeAgain = function () {
        console.log('Department: ' + this.name);
    };
    Department.prototype.addEmployee = function (name) {
        this.employees.push(name);
    };
    Department.prototype.printEmployeeInformation = function () {
        console.log(this.employees.length);
        console.log(this.employees);
    };
    return Department;
}());
var accounting = new Department("apt0001", "Accounting", ['Tim', 'Sara'], 20);
console.log(accounting.name);
console.log(accounting.code);
var ITDepartment = (function (_super) {
    __extends(ITDepartment, _super);
    function ITDepartment(id, name, employees, code, admins) {
        var _this = _super.call(this, id, name, employees, code) || this;
        _this.admins = admins;
        return _this;
    }
    Object.defineProperty(ITDepartment.prototype, "uid", {
        get: function () {
            return this.id;
        },
        set: function (value) {
            this.id = value;
        },
        enumerable: false,
        configurable: true
    });
    ITDepartment.prototype.describe = function () {
        console.log('Department: id => ' + this.id + ', name =>' + this.name);
    };
    ITDepartment.prototype.printAdminInformation = function () {
        console.log(this.admins.length);
        console.log(this.admins);
    };
    ITDepartment.staticClassLevelUtilityMethod = function (print) {
        console.log(print);
    };
    ITDepartment.departmentPrefix = "apt";
    return ITDepartment;
}(Department));
ITDepartment.staticClassLevelUtilityMethod("This is a static method called on the ITDepartment class not one of its instances.");
console.log(ITDepartment.departmentPrefix);
var it = new ITDepartment("apt0002", "Software Development", ['Betty', 'Joe'], 30, ['Tim']);
console.log(it.name);
console.log(it.code);
console.log(it.uid);
it.printAdminInformation();
//# sourceMappingURL=ts-classes.js.map