"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var person = {
    name: 'Tim',
    age: 20,
    address: {
        street: '123 test street',
        city: 'Testsville',
        zip: '84043'
    },
    hobbies: ['Sports', 'Cooking', 'Camping']
};
for (var _i = 0, _a = person.hobbies; _i < _a.length; _i++) {
    var hobby = _a[_i];
    console.log(hobby);
    console.log(hobby.toLowerCase());
}
var mixedArray = ['String', 6.5, true];
mixedArray[1] = 'change to string';
console.log(mixedArray);
//# sourceMappingURL=ts-arrays.js.map