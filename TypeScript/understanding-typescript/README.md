# Understanding TypeScript
###### Annotated project that helps identify the features and abilities TypeScript provides on top of standard JavaScript

## TypeScript Features Identified

### Core Types (ts-basics.ts)

- Number (same for JS and TS)
- String (same for JS and TS)
- Boolean (same for JS and TS)
    - "truthy" and "falsey" exist in TS just as they do in JS

### Objects (ts-objects.ts)

- Objects without interfaces receive inferred interfaces from TS in order to guarantee type safety on object properties
- Explicit interfaces can be set on objects.
- Nested objects will also receive inferred interfaces that are nested


    const developer: {
        name: string;
        age: number;
        role: string;
    } = {
        name: 'Tim',
        age: 20,
        role: 'Senior Engineer'
    }


### Arrays (ts-arrays.ts)

- Strongly typed, all values need to be of the same type
- Allows :any to be used, but that removes the whole purpose behind TS as it moves you back to regular JS

### Tuples (ts-tuples.ts)

- Fixed-length & fixed-type array
- Not available in JS, added to TS
- Can still be messed up by the push and pop methods of an array since tuples use the array object for their underlying data structure.


    const person: {
      name: string;
      age: number;
      role: [number, string];
    } = {
      name: 'Tim',
      age: 20,
      role: [1, 'Author']
    };


### Enumerations (ts-enums.ts)

- Named auto-incrementing integers that represent some flag or state you want reusable within your code (think of a list of security roles)
- Can have the auto-incrementing start number set and move from that number up
- Can have all enum values set manually
- Can have enum values that are types other than number


    enum Role {
        ADMIN,
        READ_ONLY,
        AUTHOR
    }


### Union (ts-unions.ts)

- Allows multiple types to be specified as acceptable when stored in a variable
- Uses the | pipe
- As many types as needed can be separated by the pipe

    
    function combine(input1: number|string, input2: number|string) {
      // code goes here...
    }


### Literal (ts-literals.ts)

- Types are types that represent a literal value.
- Constants when defined, will use their value literally as their type
- Used with the union (pipe |) literal types can be used to force the value of a specific parameter to be one out of a specific set. This is useful when you really do not need an enum for use in the wider application.


    const factor = 6.6;

    function SwitchOnLiteral(options: 'ice cream' | 'soda' | 'cake') :string {
      // code goes here...
    }


### Type Aliases (ts-type-alias.ts)

- Useful when you have multiple types in a union statement.
- Instead of placing the union in the parameter definition, you can alias the union as a type of its own.
- Allows unions to be reusable throughout the application.


    type tastyFoods = 'ice cream' | 'soda' | 'cake';

    function SwitchOnLiteral(options: tastyFoods) :string {
      // code goes here...
    }


### Function Basics (ts-functions-basics.ts)

- Parameters are typed using :type syntax 
- Return type can be set using :type syntax
- Return type is inferred based on the return statement
- When no return statement is provided the return type is inferred to be :void
- Overloaded signatures


    function add(n1 :number, n2 :number) :number {
        return n1 + n2;
    } 


### Functions Types (ts-functions-as-types.ts, ts-function-types-in-callback.ts)

- A function signature can be provided as the type of a variable or parameter. the function type or signature defines the parameters with their types and the return with its type.


    let customTypedReferenceToAdd: (a: number, b: number) => number;


### Unknown (ts-unknown.ts)

- Unlike the :any type, which specifies that a variable can hold any data type we ask it to, the :unknown type identifies that a variable is intended to hold something unknown at the time it is received and assigned to that variable.
- Essentially it forces developers to be safe with unknown data received.
- :unknown is preferred over :any as it enforces TS strong typing through the use of a manual type check.


    let userFromInput: unknown;


### Never (ts-never.ts)

- Specifies that a function will never return a result.
- Great for functions that kick off an application ending error.
- Great for functions that contain an infinite loop, like those that kick off a server or some other application that needs to run endlessly.


    function generateError(message: string, code: number): never {
        throw {
            message,
            code
        };
    }


### Classes & Inheritance (ts-classes.ts)

- Blueprints for objects
- Properties and Constructor
- Default property values 
- Access modifiers
- Short hand constructor notation for declaring properties with access modifiers
- Tightly binding class type to class methods


### Abstract Classes (ts-abstract-classes.ts)

- Contracts and partial implementations


### Private Constructor & Singletons (ts-singleton.ts)


### Interfaces (ts-interfaces.ts)

- Contract that objects must implement
- Readonly properties (no access modifiers can be used)
- Optional properties using `?` the question mark
- Classes can implement multiple interfaces
- Interfaces can be dynamic when using `index properties`


### Intersection Types (ts-intersection-types.ts)


### Type Guards (ts-type-guards.ts)


### Generic Types
- Allows the creation of functions that will take many different types
- Allows classes to be created that can use and operate on different types (think of a data store)
- The Partial<> generic type for allowing objects of a strict interface type to be built dynamically (like in vanilla JS)


## NPM Packages Introduced

- `lite-server`: Lightweight web server package that allows you to serve up HTML pages while in development. Support hot reloading as the project files change.
- 
