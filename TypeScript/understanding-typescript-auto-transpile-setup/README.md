# TypeScript Transpiling
###### Learning to setup automated tranpiling of TypeScrip files.

## Options

1. Manually run the transpiler (restricted to a single file)


    tsc app.ts


2. Use the transpiler in watch mode (restricted to a single file)


    tsc app.ts --watch


3. Using a config file for the transpiler to tell it what files to watch and transpile for use. 


## Setting up Full-Project Transpiling (tsconfig.json)

1. Add a Tyspscript configuration file by initializing it as a TS project.


    tsc --init


2. Run the transpiler against all files in the project


    tsc --watch


3. Configure outDir and rootDir


    "outDir":  "./dist"
    "rootDir": "./src"



## NPM Packages Introduced

- `lite-server`: Lightweight web server package that allows you to serve up HTML pages while in development. Support hot reloading as the project files change.
- 
