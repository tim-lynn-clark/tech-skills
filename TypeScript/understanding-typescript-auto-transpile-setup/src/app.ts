let fullName: string;
const firstName = "Tim";
const lastName = "Clark";

fullName = firstName + " " + lastName;

console.log(fullName);


/* NOTE: config options: noUnusedParameters, noUnusedLocals create errors in the transpiling
* if parameters or local variables are defined but never used. */
// function someFunc(message: string, notUsed: string) {
//     let anotherNotUsed = 'test';
//     console.log(message);
// }


/* NOTE: Config option noImplicitReturns will create errors if a function returns in some
* cases but not in others, creating an implicit return. */
// function someOtherFunc(test: boolean) {
//     if(test) {
//         return 1+2;
//     }
// }