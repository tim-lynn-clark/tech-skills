const button = document.querySelector("button");
/* NOTE: the `!` is used to tell TypeScript that the return value will never be null.
* as is used to define the strong type of a return type or parameter of a function.*/
const input1 = document.getElementById("num1")! as HTMLInputElement;
const input2 = document.getElementById("num2")! as HTMLInputElement;

/* NOTE: If not type is supplied, TypeScript will default to the `any` type and an
* implicit :any will be added to each parameter: add(num1:any, num2:any). But to use
* the full power of TypeScript, it is always best to supply the type you are expecting. */
function add(num1:number, num2:number) {
    return num1 + num2;
}

button.addEventListener("click", function() {

    /* NOTE: TypeScript is simply JavaScript with Types added. So casting strings to numbers
    * is done the same. You can use parseInt or simply the unary + operator to treat the string as
    * though it is a number. These of course are simple methods, they do not account for complex
    * situations or check for NaN. */
    console.log(add(+input1.value, +input2.value));
});
