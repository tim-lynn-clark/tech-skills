const button = document.querySelector("button");
const input1 = document.getElementById("num1");
const input2 = document.getElementById("num2");

function add(num1, num2) {
  // Issue here is that if string are passed they are concatinated
  // and are not treated as numbers. This would have to be handled manually in JS
  // return num1 + num2;

  // One option
  if(typeof num1 === "number" && typeof num2 === "number") {
    return num1 + num2;
  }

  // Another option full of all kinds of problems
  // return +num1 + +num2;
}

button.addEventListener("click", function() {
  console.log(add(input1.value, input2.value));
});
