'use strict'

module.exports = {
    name: 'rest-api-demo-mongoose',
    version: '1.0.0',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 3000,
    base_url: process.env.BASE_URL || 'http://localhost:3000',
    db: {
        name: 'rest-api-demo-mongoose',
        uri: 'mongodb://rest-api-demo-mongoose:123qwe123@rest-api-demo-mongoose-shard-00-00-p6alu.mongodb.net:27017,rest-api-demo-mongoose-shard-00-01-p6alu.mongodb.net:27017,rest-api-demo-mongoose-shard-00-02-p6alu.mongodb.net:27017/test?ssl=true&replicaSet=rest-api-demo-mongoose-shard-0&authSource=admin'
    }
};