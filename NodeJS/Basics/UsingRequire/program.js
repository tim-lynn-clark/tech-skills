//NOTE: the 'dice' variable holds a reference to the 'exports' object pulled in when the dice.js file was 'required' into the current file
var dice = require('./dice');
//NOTE: since the 'die' object was added as an attribute of the 'exports' object in the dice.js file, we can grab a reference to it from the 'dice (exports)' object 
var die = dice.die

//NOTE: here we can demonstrate the 'exports' object and the 'die' object by printing them out in the console
console.log(dice)
console.log(dice.die)

//NOTE: We can now use the 'die' object we imported from the dice.js file after we grab a reference to it from the 'exports' object
die.size = 10;
console.log(die.roll());
console.log(die.roll());
console.log(die.roll());
console.log(die.roll());
console.log("Total rolls: " + die.totalRolls);