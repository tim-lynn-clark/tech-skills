var die = {
	size: 6, 
	totalRolls: 0,
	roll: function(){
		var result = Math.ceil(this.size * Math.random());
		this.totalRolls += 1;
		return result;
	}
};

//NOTE: Make variables/functions/objects visible when required into other JS files using the 'require' keyword by adding them to the 'exports' object
exports.die = die;