//https://nodejs.org/api/process.html#process_process_argv
//NOTE: process.argv will always return two array items whether or not any command-line arguements have been passed at runtime
// [0] the execution path of the process, this will be the Node executable's path on the system
// [1] the file path of the JavaScript file that is being executed by Node

console.log(process.argv);
console.log("Number of Parameters: " + process.argv.length);

//NOTE: Accepting and using command-line parameters at application execution
let cost = 9.99
let tax = .07

var count = process.argv[2]

if(count != null && count != undefined && count >= 0){
	var subtotal = count * cost
	var taxation = subtotal * tax
	var total = subtotal + taxation
	console.log("Subtotal: " + subtotal)
	console.log("Tax: " + taxation)
	console.log("Total Cost: " + total)
} else {
	console.log("You must provide a valid number.")
}