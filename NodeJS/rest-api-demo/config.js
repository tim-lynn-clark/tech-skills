'use strict'

module.exports = {
    name: 'rest-api-demo',
    version: '1.0.0',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 3000,
    db: {
        name: 'rest-api-demo',
        uri: 'mongodb+srv://rest-api-demo:123qwe123@rest-api-demo-qfmf2.mongodb.net/rest-api-demo'
    }
};