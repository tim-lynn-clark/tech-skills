const User = require('../models/user');
const convenience = require('../lib/convenience');
const authService = require('../services/authService');

module.exports.create_v100 = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    if(!body.unique_key && !body.email && !body.phone){
        return convenience.ReE(res, 'Please enter an email or phone number to register.');
    } else if(!body.password){
        return convenience.ReE(res, 'Please enter a password to register.');
    }else{
        let err, user;

        [err, user] = await convenience.to(authService.createUser(body));

        if(err) return convenience.ReE(res, err, 422);
        return convenience.ReS(res, {message:'Successfully created new user.', user:user.toWeb(), token:user.getJWT()}, 201);
    }
};

module.exports.get_v100 = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user = req.user;

    return convenience.ReS(res, {user:user.toWeb()});
};

module.exports.update_v100 = async function(req, res){
    let err, user, data;
    user = req.user;
    data = req.body;
    user.set(data);

    [err, user] = await to(user.save());
    if(err){
        console.log(err, user);

        if(err.message.includes('E11000')){
            if(err.message.includes('phone')){
                err = 'This phone number is already in use';
            } else if(err.message.includes('email')){
                err = 'This email address is already in use';
            }else{
                err = 'Duplicate Key Entry';
            }
        }

        return convenience.ReE(res, err);
    }
    return convenience.ReS(res, {message :'Updated User: '+user.email});
};

module.exports.remove_v100 = async function(req, res){
    let user, err;
    user = req.user;

    [err, user] = await convenience.to(user.destroy());
    if(err) return convenience.ReE(res, 'error occured trying to delete user');

    return convenience.ReS(res, {message:'Deleted User'}, 204);
};

module.exports.login_v100 = async function(req, res){
    const body = req.body;
    let err, user;

    [err, user] = await convenience.to(authService.authUser(req.body));
    if(err) return convenience.ReE(res, err, 422);

    return convenience.ReS(res, {token:user.getJWT(), user:user.toWeb()});
};
