const User 			= require('../models/user');
const convenience = require('../lib/convenience');
const validator     = require('validator');

let getUniqueKeyFromBody = function(body){// this is so they can send in 3 options unique_key, email, or phone and it will work
    let unique_key = body.unique_key;
    if(typeof unique_key==='undefined'){
        if(typeof body.email !== 'undefined'){
            unique_key = body.email
        }else if(typeof body.phone !== 'undefined'){
            unique_key = body.phone
        }else{
            unique_key = null;
        }
    }

    return unique_key;
};
module.exports.getUniqueKeyFromBody = getUniqueKeyFromBody;

module.exports.createUser = async function(userInfo){
    let unique_key, auth_info, err;

    auth_info={};
    auth_info.status='create';

    unique_key = getUniqueKeyFromBody(userInfo);
    if(!unique_key) convenience.TE('An email or phone number was not entered.');

    if(validator.isEmail(unique_key)){
        auth_info.method = 'email';
        userInfo.email = unique_key;

        [err, user] = await convenience.to(User.create(userInfo));
        if(err) convenience.TE('user already exists with that email');

        return user;

    }else if(validator.isMobilePhone(unique_key, 'any')){//checks if only phone number was sent
        auth_info.method = 'phone';
        userInfo.phone = unique_key;

        [err, user] = await convenience.to(User.create(userInfo));
        if(err) convenience.TE('user already exists with that phone number');

        return user;
    }else{
        convenience.TE('A valid email or phone number was not entered.');
    }
};

module.exports.authUser = async function(userInfo){//returns token
    let unique_key;
    let auth_info = {};
    auth_info.status = 'login';
    unique_key = getUniqueKeyFromBody(userInfo);

    if(!unique_key) convenience.TE('Please enter an email or phone number to login');


    if(!userInfo.password) convenience.TE('Please enter a password to login');

    let user;
    if(validator.isEmail(unique_key)){
        auth_info.method='email';

        [err, user] = await convenience.to(User.findOne({email:unique_key }));
        if(err) convenience.TE(err.message);

    }else if(validator.isMobilePhone(unique_key, 'any')){//checks if only phone number was sent
        auth_info.method='phone';

        [err, user] = await convenience.to(User.findOne({phone:unique_key }));
        if(err) convenience.TE(err.message);

    }else{
        convenience.TE('A valid email or phone number was not entered');
    }

    if(!user) convenience.TE('Not registered');

    [err, user] = await convenience.to(user.comparePassword(userInfo.password));

    if(err) convenience.TE(err.message);

    return user;

};