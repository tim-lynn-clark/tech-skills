const passport      	= require('passport');
require('./../middleware/passport')(passport);

// User Routes
module.exports = function(server, controllers) {
    server.post({path: "/users", version: "1.0.0"}, controllers.user.create_v100);
    server.post({path: "/users/login", version: "1.0.0"}, controllers.user.login_v100);
    server.get( {path: "/users", version: "1.0.0"}, passport.authenticate('jwt', {session:false}), controllers.user.get_v100);
};
