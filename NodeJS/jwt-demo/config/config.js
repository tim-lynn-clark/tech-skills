// Instantiate environment variables from the .env file in the application root
require('dotenv').config();

// Pull environment variables from process for use in the application
let config = {};

config.app               = process.env.APP               || 'jwt-demo';
config.version           = process.env.VERSION           || '1.0.0';
config.env               = process.env.NODE_ENV          || 'development';
config.port              = process.env.PORT              || '3000';
config.base_url          = process.env.BASE_URL          || 'http://localhost:3000';

config.db_name           = process.env.DB_NAME           || 'jwt-demo';
config.db_uri            = process.env.DB_URI            || 'mongodb://jwt-demo:123qwe123@jwt-demo-shard-00-00-nnlmw.mongodb.net:27017,jwt-demo-shard-00-01-nnlmw.mongodb.net:27017,jwt-demo-shard-00-02-nnlmw.mongodb.net:27017/test?ssl=true&replicaSet=jwt-demo-shard-0&authSource=admin';

config.jwt_encryption    = process.env.JWT_ENCRYPTION    || 'jwt_please_change';
config.jwt_expiration    = process.env.JWT_EXPIRATION    || '10000';

const debug = require('debug')('config');
debug(config);

module.exports = config;