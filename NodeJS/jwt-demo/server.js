// Module Dependencies
const debug             = require('debug')('server');
const fs                = require('fs');
const restify           = require('restify');
const corsMiddleware    = require('restify-cors-middleware');
const mongoose          = require('mongoose');
const passport          = require('passport');
const config            = require('./config/config');

// CORS Allowed Headers
const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['api-token', 'accept', 'accept-version', 'content-type', 'x-csrf-token'],
    exposeHeaders: ['api-token-expiry']
});

// Initialize Server
const server = restify.createServer({
    name: config.app,
    version: config.version,
});

// Middleware
server.use(passport.initialize());
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.jsonBodyParser({ mapParams: true }));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.fullResponse());

// Load all controllers
let controllers = {};
let controllers_path = __dirname + '/controllers';
fs.readdirSync(controllers_path).forEach(function (file) {
    if (file.indexOf('.js') != -1) {
        controllers[file.split('.')[0]] = require(controllers_path + '/' + file)
    }
});

// Start Server, Connect to DB & Require Routes
server.listen(config.port, () => {
    // establish connection to mongodb
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db_uri, { });

    const db = mongoose.connection;

    db.on('error', (err) => {
        console.error(err);
        process.exit(1);
    });

    db.once('open', () => {
        // Load and setup resource routes
        require('./routes/user.routes')(server, controllers);
        debug(`Server is listening on port ${config.port}`);
    });
});