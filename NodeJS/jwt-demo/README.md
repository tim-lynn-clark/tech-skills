# JWT Demo

Node application that demonstrates the use of JavaScript Web Tokens for request authentication in a Restify API. 

## Primary Technologies Used

* [Node.js](https://nodejs.org/en/): Server-side JavaScript run-time environment
* [Restify](https://www.npmjs.com/package/restify): RESTful web API application framework
* [Passport](http://www.passportjs.org/): Unobtrusive authentication middleware for Node
* [Passport JWT](https://www.npmjs.com/package/passport-jwt): JavaScript web token authentication strategy for Passport
* [JsonWebToken](https://www.npmjs.com/package/jsonwebtoken): Client authentication middleware for Node
* [MongoDB Atlas](https://www.mongodb.com/cloud/atlas/): Cloud hosted MongoDB object database as data store
* [Mongoose](http://mongoosejs.com/): Object relational mapper for MongoDB
