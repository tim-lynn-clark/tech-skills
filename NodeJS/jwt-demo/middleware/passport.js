const JwtStrategy   = require('passport-jwt').Strategy;
const ExtractJwt    = require('passport-jwt').ExtractJwt;
const User          = require('../models/user');
const config        = require('../config/config');
const convenience   = require('../lib/convenience');

module.exports = function(passport){
    let opts = {};

    /**
     * This JWT extraction method tells passport to search for the
     * HTTP Authorization header in each request as the location for
     * the bearer token for request authentication.
     */
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.jwt_encryption;

    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
        let err, user;
        [err, user] = await convenience.to(User.findById(jwt_payload.user_id));
        if(err) return done(err, false);
        if(user) {
            return done(null, user);
        }else{
            return done(null, false);
        }
    }));
};