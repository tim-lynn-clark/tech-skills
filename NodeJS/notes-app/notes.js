const fs = require('fs');
const file_name = 'notes.txt';

function addNote(note) {
    fs.appendFileSync(file_name, note + '\n');
}

function getNotes() {
    const file_data = fs.readFileSync(file_name, {encoding: 'utf8'});
    return file_data;
}

function clearNotes() {
    fs.writeFileSync(file_name, '');
}

module.exports = {
    addNote,
    getNotes,
    clearNotes
};
