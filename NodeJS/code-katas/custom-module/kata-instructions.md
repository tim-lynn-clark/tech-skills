Kata Instructions:
 
Create a custom module to write and read to a file named notes.txt
    
Module Requirements:
    
    1. Must utilize Node's File System library to read from and write to the file
    2. Must contain a function for each of the following behaviors: read all, write new, clear all
    3. Must import module functions into primary script
    4. Module functions must be used within the primary script

Bonus Requirements:

    1. Add interaction with the user via the console
