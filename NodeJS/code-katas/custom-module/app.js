const note_manager = require('./note_manager');

// Demonstrate custom module use
note_manager.addNote('Note1: This is a new note.');
note_manager.addNote('Note2: This is a new note.');
note_manager.addNote('Note3: This is a new note.');

let notes = note_manager.getNotes();
console.log('Before Clear:');
console.log(notes);

note_manager.clearNotes();

notes = note_manager.getNotes();
console.log('After Clear:');
console.log(notes);


