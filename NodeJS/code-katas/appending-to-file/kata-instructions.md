Kata Instructions: 

Append a message to a file named notes.txt

    1. Try using options flag on writeFileSync to append to the file
    2. Try using appendFileSync method
