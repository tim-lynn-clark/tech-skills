# Favorite NPM Modules

## Utility Modules

### Nodemon
nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.
https://www.npmjs.com/package/nodemon

### Validator
A library of string validators and sanitizers.
https://www.npmjs.com/package/validator

### Chalk
Terminal string styling done right
https://www.npmjs.com/package/chalk
