<?php
// Single-line comment
# Single-line comment
/*
 * Multi-line comment
 * allows more text
 */
echo "Hello you all from PHP";

# Variables
/*
 * - Prefix $
 * - Start with a letter or underscore
 * - Only letters, numbers, and underscores
 * - Case sensitive
 */

$output = 'Hello everybody!';
echo $output;

# Concatenation
$string1 = "Hello";
$string2 = "World";
echo $string1 . " " . $string2;
echo "$string1 $string2";

# Escaping characters
echo 'They\'re here!';

# Constants
# Case sensitive
define('GREETING', 'Hello you all, this cannot change.');
echo GREETING;