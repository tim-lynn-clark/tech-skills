<?php

# Built-in Data Types
/*
 * Strings
 * Integers
 * Floats
 * Booleans
 * Arrays
 * Objects
 * NULL
 * Resource (reference to a function?)
 */

$output = "Hello to you all";
echo $output;

$num4 = 4;
echo $num4;

$float5 = 5.5;
echo $float5;

$bool = true;
echo $bool;
