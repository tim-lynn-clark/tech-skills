# Webpack: Handling Images

Handling image pre-processing during build process: image resizing, image inclusion in 
JavaScript bundle as raw data or as image in build folder.

### Steps
1. `npm install --save-dev image-webpack-loader url-loader` install image handling loader packages
    1. `image-webpack-loader` takes an image and resizes it
    2. `url-loader` takes and image and based on its size, determines if the image will be included in the JS bundle as Base64 string data or as an actual image in the build folder
    3. `npm install --save-dev file-loader` dependency of `image-webpack-loader`
2. add another object literal to the `webpack.config.js` file under `config.module.rules`
    1. `test: /\.(jpe?g|png|gif|svg)$/,` add test for image file types for the loader to handle
    2. `use: []` add a use array to the rule used to specify the loaders (in right to left order) for the files that meet the test to be passed to
        1. `{}` to the use array, add an object literal in which to specify configuration options to the url-loader
            1. `loader: 'url-loader',` specify the loader this object is encapsulating to which it is passing options to
            2. `options: { limit: 40000 }` specify the options object that will contain the configuration options that will be passed to the loader
3. import images into entry point JS file (root JS of the dependency tree)
4. `publicPath: 'build/'` set in `config.output` of `webpack.config.js`
    1. the public path is used by the url-loader module to build the URL paths to images that have been placed in the build folder and have not been embedded in the JS file as base 64 encoded strings
