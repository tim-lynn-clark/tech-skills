# Demo Music App
Demo app used to learn to apply Webpack to it for build purposes

### Steps

Note: Projects referenced in the following steps are included in the same folder containing this one

1. Add `babel` for JavaScript transpiling (see project 'babel-for-transpiling' for steps)
2. Setup CSS bundling using `css-loader` and `style-loader` (see project 'css-loaders-css-bundle' for steps)
3. Separate out vendor JS libraries from application specific Js libraries
    1. Update entry to be an object literal instead of a single string to a JS file
    2. Add a `bundle` property with the path and name of the desired output file `bundle: './src/index.js',`
    3. Add a `vendor` property and supply it an array of vendor library names `vendor: VENDOR_LIBS`
    4. Create the vendor array `const VENDOR_LIBS = [
                                  'react', 'lodash', 'redux', 'react-redux', 'react-dom',
                                  'faker', 'react-input-range', 'redux-form', 'redux-thunk'
                                ];`
        1. The strings in the array are the exact names of the vendor NPM modules, in the 'dependencies' section of the package.json file, that are to be combined into a single bundle file
    5. Add the `CommonsChunkPlugin` in order to help Webpack identify and remove vendor libraries, included in the vendor bundle, from the main application bundle even though they are specified as dependencies within application specific JS files
    6. Update output section of `webpack.config.js` to dynamically update the generated file names following a provided patter `filename: '[name].js'`
        1. `new webpack.optimize.CommonsChunkPlugin({
                  names: ['vendor', 'manifest']
                })`
4. Add `HtmlWebpackPlugin` for managing the insertion of script tags for the generated bundles as well as custom CSS libraries into an index.html document that is generated off a template index.html file
    1. `new HtmlWebpackPlugin({
              template: 'src/index.html'
            })`
5. Handle cache busting using `chunkhash` provided by Webpack
    1. Update output filename property to use hash in name `filename: '[name].[chunkhash].js'`
6. Add `rimraf` to project and use in `package.json` scripts to handle commands that are different between Windows and OS X
7. Create a new script for cleaning up old files prior to a new build `"clean": "rimraf dist",`
8. Update build script to call the clean command prior to building `"build": "NODE_ENV=production npm run clean && webpack -p",`
