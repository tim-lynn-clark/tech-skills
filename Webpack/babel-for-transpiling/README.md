# Webpack: Babel for Transpiling

Adding Babel to the webpack build process for transpileing ES2015/6/7 
back down to ES5 syntax.

### Steps
1. `npm install --save-dev babel-loader babel-core babel-preset-env` Add Bable as project dependency
    1. babel-loader: teaches babel how to work with webpack
    2. babel-core: knows how to take in code, parse it, and generate output files
    3. babel-preset-env: ruleset for telling Babel exactly what pieces of ES2015/6/7 syntax to look for and how to turn it into ES5 syntax
2. add `module` property to webpack config object in `webpack.config.js` and assign it an object literal
3. add `rules` array property to `module` object
4. add an object literal per loader (module/rule) being used to pre-process files before bundling
    1. define `use: 'babel-loader'` property of rule, this will let webpack know it will be using babel before bundling
    2. define `test: /\.js$/` regular expression to help webpack know which files it should send to the pre-processor
5. `.babelrc` file to hold configuration for the babel loader/module
    1. place object literal at root of the file (JSON syntax)
        1. add `"presets": ["babel-preset-env"]` to let babel know to run the rules contained therein