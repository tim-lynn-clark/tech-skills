# Webpack: Code Splitting

Webpack traditionally bundles all JS into a single `bundle.js` file that is loaded on the first 
page load. Sometimes, it is not optimal to send the entire Js load to a user on first page load, 
it is for these cases that 'code splitting' was created. Webpack can be used to break up the 
bundle into specific separate files that can then be programmatically loaded when needed. 

### Steps
Code splitting is accomplished by simply adding `System.import` calls within your application's
entry point JS file. When Webpack sees a `System.import` call, it will automatically take that 
import and begin the creation of a new `bundle.js` file that begins with a 0 index `0.bundle.js`

    System.import('./image-viewer').then(module => {
        console.log(module);
    })
    
Webpack also adds JS to the bundle files for calling the server to retrieve the additional 
bundled scripts