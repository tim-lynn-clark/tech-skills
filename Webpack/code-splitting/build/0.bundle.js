webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.regularImage = regularImage;
exports.smallImage = smallImage;
exports.bigImage = bigImage;

__webpack_require__(3);

var _bigTest = __webpack_require__(4);

var _bigTest2 = _interopRequireDefault(_bigTest);

var _smallTest = __webpack_require__(5);

var _smallTest2 = _interopRequireDefault(_smallTest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//NOTE: importing component specific images into the JS file so Webpack will hadle them
function regularImage() {
    var image = document.createElement('img');
    image.src = 'http://via.placeholder.com/400x400';
    document.body.appendChild(image);
} //NOTE: importing component specific CSS file into the JS file so Webpack will handle it
function smallImage() {
    //NOTE: Setting base64 encoded string as image element source
    var smallImage = document.createElement('img');
    smallImage.src = _smallTest2.default;
    document.body.appendChild(smallImage);
}

function bigImage() {
    var bigImage = document.createElement('img');
    bigImage.src = _bigTest2.default;
    document.body.appendChild(bigImage);
}

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function () {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for (var i = 0; i < this.length; i++) {
			var item = this[i];
			if (item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function (modules, mediaQuery) {
		if (typeof modules === "string") modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for (var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if (typeof id === "number") alreadyImportedModules[id] = true;
		}
		for (i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if (typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if (mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if (mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)();
// imports


// module
exports.push([module.i, "img {\n    border: 10px solid black;\n}", ""]);

// exports


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "e14b92d131d44acae721df8b18097ccf.jpg";

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QDIRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAPAAAAcgEyAAIAAAAUAAAAgodpAAQAAAABAAAAlgAAAAAAAABIAAAAAQAAAEgAAAABUGl4ZWxtYXRvciAzLjcAADIwMTg6MDY6MTIgMTc6MDY6NjQAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAGSgAwAEAAAAAQAAADgAAAAA/+EJ9Gh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wOk1vZGlmeURhdGU9IjIwMTgtMDYtMTJUMTc6MDY6NjQiIHhtcDpDcmVhdG9yVG9vbD0iUGl4ZWxtYXRvciAzLjciPiA8ZGM6c3ViamVjdD4gPHJkZjpCYWcvPiA8L2RjOnN1YmplY3Q+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw/eHBhY2tldCBlbmQ9InciPz4A/+0AOFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/bAIQACAYGBwYFCAcHBwkJCAoMFA0MCwsMGRITDxQdGh8eHRocHCAkLicgIiwjHBwoNyksMDE0NDQfJzk9ODI8LjM0MgEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8IAEQgAOABkAwEiAAIRAQMRAf/EABwAAAIDAQEBAQAAAAAAAAAAAAMEAgUGAAEHCP/aAAgBAQAAAAC2kXolYr+bqfWsagrpNoUTb9WBk3xgFpRfStfG2WP73flWOyg1odhVx1LL/flha7mbRXYx2/PsYe5zYAQO+1ZAiQ7+dqvofh58rW01b//EABoBAAIDAQEAAAAAAAAAAAAAAAMEAgUGAQf/2gAIAQIQAAAAuyMgXalkDtyP2WYOcrzRfO3TxesLD//EABoBAQEAAgMAAAAAAAAAAAAAAAQDAgYAAQX/2gAIAQMQAAAA8iZUILhsM4cn1jsBzxLKW7yNIZS//8QAMxAAAgEDAwEGBAUEAwAAAAAAAQIDAAQRBRIhEwYiMUFRYRQjcbEWMlKBkRVCgsE0YnL/2gAIAQEAAT8ALsaCM1CB/SukaxiolJNPNtXYtXMmRVjoj3pM0x6duvix8/YVcMlvF0LdOlCPLzb609wd+PKlzIeM0sT486ftXfxWyk28eR3TKRkE0+v6hKDLJeTD2VsCo766Ym5+JlVycl95zitD7YMrLDqkm+InAmxyv1oQiVQ8ZDKwyCDwRXw7Rr7mmgY1ZaOk7dWY/JXx9/YVcq8ihEVUjUYVQeAKl0ySTwyTX4fnbvMhC0ml9MYCEe5WhZQ45m5/8mjJ04D151RA52wxtkHzJpIrW8dIbedmlJJdeAPYDNCwbUrL4ZB0HhDbPDkAeBqJ0IMRf/IHPIrsh2htrKwW21AlYQfkzEZA9VNNPFOgeN1ZW5BU5BqxiS/vhbrMmV5YA5wKulWMCONl6S8BRSLuJBdVX1oSCM7QA/oxOKyv5i232XmjPtHdSSc+/AFFA3PSUewpo3iUvIcZ8Hq0vhbTdYSln8ipwRWj6zPcWbmXcIg5CZplsrV3uJVyXUhlB/Nnz+uasNch0u8S3cJNaSqC4dc8UZ7QwrdWjE2rk9xu6V+lWUFtpGih4eJ7nDPg8qPIV/WL+As6O2ZOcSrmtD1SWHUWhvZWaGY5DNztemikXLrtx6VbrMDuV8Ef9qj6jIXaSLJ8mAr5n6oP5qfSL27GBNbcDABDL91odlNVI7nwz/S4UVpehdobO1ZYdNSQlssTIrDH7HiptHnEqNNFHGx70nzwwz6D0FR6GZUSV7u23ocbEbaMZrTEWO9R7q4REyeY38vXwq+1d5JzcQdSZ/7UHCAfuM1O5lVHe8ZCB+XaSftihJHsWLPXJ8XHc21B2hvIY0imSKVEGN7PhjX4mJkG+0G3zZZh9jUHaTT+ijTztHIwyUALbaHaXSgP+XIf8Gq0kg6qn5/qS7K/+quri2urYLskUqOOmFXdUuoJA4jvYb1Ifyj5qscfxUVnY3Umba+iUNxtMK7vuKuNOurefpwxQTE576AD70V1WHBFsp+ig1b6tdxOFmhA9Rgg0NQeUDMDkewY1ZzxPIco4A8dxwae8t41yU+xo3tnyxXePMAc0b21YlUhLk/24Uf7pLiAIMQkexDVb9lJkQKZ4v4NR9mF8JLhP2U12p0CztXi+c3UYftVlpttEeCc/qB5qLs/aXelWk0AjkuigEiF9pf6VDYw2Xee2jVoyQ25clT6GjHayAF4YwfXHBoJAMr3/YgZFG1jxuKQY9THzV3pdsO89qBu8GCYp+z0MmGidl9CBVzpl1ExCSmVPEgryPTFC6vEypuEQ/pdSCK//8QAIBEAAgMBAAICAwAAAAAAAAAAAQIAAxEEEjETFCEiQf/aAAgBAgEBPwBOhSNETqG5DbLLVQRXLnWiuJ5iVB6xqGU9bAlWg7zWf2H4idS3nyEDQvPlg6mBzJW2nTGqVxjGV8le+4vOv8YxKQPZn11htYyp28Y5Oe5U7bKrnBwGVdNm5s+Zp//EACARAAMAAgEEAwAAAAAAAAAAAAABAgMREgQTFEEhMVH/2gAIAQMBAT8AfTsvpWkLEY8dWypUrUlQzjQ9MuUyun2vhjxdtcRo4naPHkyY9fSGrT3ourRWel6RWavw8iziipWz2ZIRkxyy8UpHFH//2Q=="

/***/ })
]);