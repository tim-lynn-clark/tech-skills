const button = document.createElement('button');
button.innerText = 'Click Me!';
button.onclick = () => {
    //NOTE: System is a global object based on ES2015 module spec
    // that will reach out to the server for the specified module
    // will also pull in all dependencies on which the requested module relies
    System.import('./image-viewer').then(module => {
        console.log(module);
    })
};

document.body.appendChild(button);