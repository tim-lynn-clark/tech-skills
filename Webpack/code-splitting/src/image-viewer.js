//NOTE: importing component specific CSS file into the JS file so Webpack will handle it
import '../styles/image-viewer.css';

//NOTE: importing component specific images into the JS file so Webpack will hadle them
import bigTest from '../assets/big-test.jpg';
import smallTest from '../assets/small-test.jpg';

export function regularImage () {
    const image = document.createElement('img');
    image.src = 'http://via.placeholder.com/400x400';
    document.body.appendChild(image);
}

export function smallImage () {
    //NOTE: Setting base64 encoded string as image element source
    const smallImage = document.createElement('img');
    smallImage.src = smallTest;
    document.body.appendChild(smallImage);
}

export function bigImage () {
    const bigImage = document.createElement('img');
    bigImage.src = bigTest;
    document.body.appendChild(bigImage);
}

