# Demo Music App
Demo app used to learn to apply Webpack Dev Server to a project. The dev 
server acts as an intermediary between the source code as it is being developed 
and the browser testing that development. Instead of running `npm run build` 
after every change to a source file, the dev server will watch all source 
files and rebuild automatically and refresh the browser.

### Steps

1. Add `webpack-dev-server` as an NPM dependency to your project `npm install --save-dev webpack-dev-server`
2. Update `package.json` with a new script that runs the dev server `"serve": "webpack-dev-server"`
3. When running the dev server, webpack builds and saves the files in memory and does not build the `dist` folder
 