
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

//NOTE: Create config object in which all webpack options will be set
const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/
            },
            {
                loader: ExtractTextPlugin.extract({
                    loader: 'css-loader'
                }),
                test: /\.css$/
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css')
    ]
};

//NOTE: Export the config option as webpack expects this file to provide a config
module.exports = config;