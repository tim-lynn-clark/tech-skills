# Webpack: Bundled CSS

Adding 

### Steps
1. `npm install --save-dev extract-text-webpack-plugin@2.0.0-beta.4` This plugin takes a reference to a loader and all text that is returned by the loader is then written to a file
2. `const ExtractTextPlugin = require('extract-text-webpack-plugin');` include the plugin in `webpack.config.js`
3. `loader: ExtractTextPlugin.extract({ loader: 'css-loader' }),` specify using the loader property of the CSS rules object that you will be using the ExtractTextPlugin to watch the `css-loader` for text output
4. `plugins: []` add a plugins attribute with array to the webpack config object
5. `new ExtractTextPlugin('style.css')` add a new ExtractTextPlugin instance to the plugins array and set the name of the file to which it should pipe all text from the loader it is watching
6. 