# Webpack Basics

Walking through the basics of using Webpack to package up project 
JavaScript resources. 

### Steps
1. `npm install --save-dev webpack` install webpack
2. `webpack.config.js` create the webpack config file in the root of the app
3. add basic config object and export it from the file as the module see `webpack.config.js`
4. `entry: './src/index.js'` define the entry property, the first of two required properties that must be defined for webpack to work in a project. This is usually the final JS file up the chain of dependencies, the root JS file, the file that only imports dependencies and is not a dependency for any other files, like `index.js`. Webpack will begin at the entry property file and work its way up creating a dependency tree as it goes.
5. define the output property as an object, the second of the two required properties. This object specifies the location and name of the combined JS based on the dependency tree built by webpack.
    1. `path` must be a fully qualified file path, cannot be a relative path using `./`, using Node's `path` module `path: path.resolve(__dirname, 'build')`
    2. `filename: 'bundle.js'` define the name of the compiled file
6. `"build": "webpack"` add to the `scripts` section of the `package.json` file
7. `npm run build` to kick off webpack and the bundling of all JS in the application