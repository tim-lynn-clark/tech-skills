//NOTE: importing component specific CSS file into the JS file so Webpack will handle it
import '../styles/image-viewer.css';

const image = document.createElement('img');
image.src = 'http://via.placeholder.com/400x400';

document.body.appendChild(image);