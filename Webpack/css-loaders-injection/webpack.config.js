
const path = require('path');

//NOTE: Create config object in which all webpack options will be set
const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/
            },
            {
                use: ['style-loader','css-loader'],
                test: /\.css$/
            }
        ]
    }
};

//NOTE: Export the config option as webpack expects this file to provide a config
module.exports = config;