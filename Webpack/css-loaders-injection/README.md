# Webpack: CSS and Style Loaders

Adding 

### Steps
1. `import '../styles/image-viewer.css'` import CSS files into the associated JS file `/src/image-viewer.js` so it will included in webpack's dependency tree
2. `npm install --save-dev style-loader css-loader` add webpack loaders specific to handling CSS content
    1. `css-loader` knows how to deal with CSS imports 
    2. `style-loader` takes CSS imports and adds them to the HTML document in a style tag
3. in the `webpack.config.js` file, in the `module` section, add a new object literal to the `rules` array to add another loader rule
    1. `use: ['style-loader', 'css-loader']` this syntax allows a test case to pass each file that passes the test to two different loaders one after another, loaders are run from right to left so make sure your order is correct
    2. `test: /\.css$/`