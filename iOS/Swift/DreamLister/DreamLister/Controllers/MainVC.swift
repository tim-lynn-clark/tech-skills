//
//  ViewController.swift
//  DreamLister
//
//  Created by Timothy Clark on 10/4/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit
import CoreData

/* Lesson Objectives
 - Using CoreData
 - NSFetchedResultsController: collects info about results from a query without pulling all the data at one time, lazy loading of data.
 - Using Navigation Controller (Editor, Embed In, Navigation Controller)
 - Extending UIView, adding @IBInspectable attributes to give view round corners and shadow effect
 - Using TableView and TableViewCell
 - Using a SegmentedControl
 - Adding AppDelegate access variable to AppDelegate.swift file, allowing access to iOS created functions
     - let ad = UIApplication.shared.delegate as! AppDelegate // access to delegate
     - let context = ad.persistentContainer.viewContext // access to CoreData context
 - Using UIPickerView
 - CRUD Operations with CoreData
 - Sorting UI data using a Segment control
 */

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!

    //NOTE: used to fetch data from database with CoreData
    var controller: NSFetchedResultsController<Item>!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //generateTestData()
        attemptFetch()
    }

    // TableView Protocol Function
    // Allows the reuse of the initial cells created to fit the table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        
        configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
        
        return cell
    }
    
    // TableView Protocol Function
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    // TableView Protocol Function
    //NOTE: TableView uses a custom class 'ItemCell' to represent each cell in the table
    func configureCell(cell: ItemCell, indexPath: NSIndexPath) {
    
        let item = controller.object(at: indexPath as IndexPath)
        cell.configureCell(item: item)
        
    }
    
    // TableView Protocol Function
    // Number of rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let sections = controller.sections {
            
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        
        return 0
    }
    
    // TableView Protocol Function
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if let sections = controller.sections {
            return sections.count
        }
        
        return 0
    }

    // CoreData Manually Created Fetch Function: FetchedResultsController function
    func attemptFetch() {
        
        // Specifies a data fetch will be done and the CoreData model type being used
        let fetchRequest: NSFetchRequest<Item> = Item.fetchRequest()
        
        // Sorting of fetch results based on Item attributes found in CoreData table
        let dateSort = NSSortDescriptor(key: "created", ascending: false)
        let priceSort = NSSortDescriptor(key: "price", ascending: true)
        let titleSort = NSSortDescriptor(key: "title", ascending: true)

        
        // Adjust which NSSortDescriptor to use based on which button on the segment control has been clicked
        if segment.selectedSegmentIndex == 0 {
            fetchRequest.sortDescriptors = [dateSort]
        } else if segment.selectedSegmentIndex == 1 {
            fetchRequest.sortDescriptors = [priceSort]
        } else if segment.selectedSegmentIndex == 2 {
            fetchRequest.sortDescriptors = [titleSort]
        }

        // Create FetchController
        //NOTE: 'context' was setup in the 'AppDelegate' file
        self.controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        // Set this ViewController to be the Delegate for the NSFetchedResultsController
        // this allows all the NSFetchedResultsController protocol functions in this ViewController
        // to be called in response to NSFetchedResultsController events
        controller.delegate = self
        
        // FetchControllers can throw errors so Do/Catch block must be used
        do {
            try self.controller.performFetch()
        } catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    @IBAction func sortChanged(_ sender: UISegmentedControl) {

        attemptFetch()
        tableView.reloadData()

    }
    
    // NSFetchedResultsController Protocol Function
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        tableView.beginUpdates()
        
    }
    
    // NSFetchedResultsController Protocol Function
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        tableView.endUpdates()
        
    }
    
    // NSFetchedResultsController Protocol Function
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        // Function handles insert, delete, move, and update
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! ItemCell
                
                configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
                
            }
            break
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        }
        
    }

    //NOTE: handle selection of item in TableView and send that item to details view for editing
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let objs = controller.fetchedObjects, objs.count > 0 {

            let item = objs[indexPath.row]
            performSegue(withIdentifier: "ItemDetailsVC", sender: item)

        }

    }

    //NOTE: Always prepare for all segues from this controller to others
    // This method allows to you do specific actions based on the segue called
    // primarily used to pass objects from one controller to another using
    // public properties on the called controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "ItemDetailsVC" {
            if let destination = segue.destination as? ItemDetailsVC {
                if let item = sender as? Item {
                    destination.itemToEdit = item
                }
            }
        }

    }

    // Only used to generate test data, all calls to this function must be commented out
    func generateTestData(){
        
        let item = Item(context: context)
        item.title = "MacBook Pro"
        item.price = 1800.00
        item.details = "I can't wait until the September event, I hope they release new MBPs."
        
        let item2 = Item(context: context)
        item2.title = "Bose Noice Canceling Headphones"
        item2.price = 500.00
        item2.details = "Expensive, but man, it's so nice to be able to block out everyone's jabber."
        
        let item3 = Item(context: context)
        item3.title = "Tesla Model S"
        item3.price = 147000.00
        item3.details = "A car that can drive itself. Oh I cannot wait to get my hands on one of these!"
        
        
        ad.saveContext()
    }
}





















