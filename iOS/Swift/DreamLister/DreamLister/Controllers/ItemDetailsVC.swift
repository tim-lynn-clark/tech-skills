//
//  ItemDetailsVC.swift
//  DreamLister
//
//  Created by Timothy Clark on 10/4/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit
import CoreData


class ItemDetailsVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var storePicker: UIPickerView!
    @IBOutlet weak var titleField: CustomTextField!
    @IBOutlet weak var priceField: CustomTextField!
    @IBOutlet weak var detailsField: CustomTextField!
    @IBOutlet weak var thumbImage: UIImageView!
    
    
    var stores = [Store]()
    var itemToEdit: Item?
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()

        //NOTE: Uncomment to create test data in CoreData, run only once
        //generateTestData()

        // NOTE: removes the title from the Navigation bar links added to view controllers
        // called by another view controller
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }

        //NOTE: UIPickerView needs to be told that this class will be handling both supplying its data as well as all of its events
        storePicker.delegate = self
        storePicker.dataSource = self

        //NOTE: UIImagePickerController needs to be told that this class will be handling all of its events
        //NOTE: Allows the application to access the user's Camera Roll and select images
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        getStores()

        //NOTE: Check to see if we are in Create or Edit mode based on whether or not an Item was passed to this controller at the time it was called during a segue
        if itemToEdit != nil {
            loadItemData()
        }
        
    }

    //NOTE: PickerView Protocol Methods
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let store = stores[row]
        return store.name
    }
    
    //NOTE: Number of rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stores.count
    }
    
    //NOTE: Number of columns
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //NOTE: Used to update the selected component inthe PickerView
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    //NOTE: Pulling entities from CoreData
    func getStores() {
        let fetchRequest: NSFetchRequest<Store> = Store.fetchRequest()

        //NOTE: CoreData NSFetchRequests must have potential fatal errors handled to avoid application crashes
        do {
            self.stores = try context.fetch(fetchRequest)
            self.storePicker.reloadAllComponents()
        } catch {
            // Handle error thrown by CoreData FetchRequest.
            let realError = error as NSError // As weird as it looks, Xcode actually wants this forced conversion
            print(realError.localizedDescription)
//            switch realError.code {
//            case 257:
//                // Handle based on error type
//            case 260:
//                // Handle based on error type
//            default:
//                // Default handling if unexpected error is returned
//            }
        }
    }
    
    //NOTE: Saving a new item to the database
    @IBAction func savePressed(_ sender: Any) {
        
        var item: Item!

        if itemToEdit == nil {
            item = Item(context: context)
        } else {
            item = itemToEdit
        }
        
        if let title = titleField.text {
            item.title = title
        }
        
        if let details = detailsField.text {
            item.details = details
        }
        
        if let price = priceField.text {
            item.price = (price as NSString).doubleValue
        }

        if let image = thumbImage.image {
            let picture = Image(context: context)
            picture.image = image
            item.toImage = picture
        }

        item.toStore = stores[storePicker.selectedRow(inComponent: 0)]
        
        ad.saveContext()
        
        //NOTE: Go back to the view that called this one 
        navigationController?.popViewController(animated: true)
    }

    func loadItemData(){
        if let item = itemToEdit {

            titleField.text = item.title
            priceField.text = "\(item.price)"
            detailsField.text = item.details

            if let picture = item.toImage {
                //NOTE: CoreData is storing the Image as a 'Transformable' data type, this requires that the data
                // pulled for the images be cast back to the proper UIImage format to be used.
                thumbImage.image = picture.image as? UIImage
            }

            //Set selected Store in StorePicker
            if let store = item.toStore {

                var index = 0
                repeat {

                    let s = stores[index]
                    if s.name == store.name {
                        storePicker.selectRow(index, inComponent: 0, animated: false)
                        break
                    }
                    index += 1

                } while(index < stores.count)

            }

        }
    }

    //NOTE: Deleting objects from CoreData
    @IBAction func deletePressed(_ sender: Any) {

        if itemToEdit != nil {

            context.delete(itemToEdit!)
            ad.saveContext()

        }

        navigationController?.popViewController(animated: true)

    }

    //NOTE: UIImagePickerController protocol methods
    @IBAction func addImagePressed(_ sender: Any) {

        present(imagePicker, animated: true)

    }

    //NOTE: When using the user's Photo Library, it is a good idea to add the 'Privacy - Photo Library Usage Description'
    // key to the Info.plist as a new row with a custom message to be displayed to the user letting them know why
    // you are trying to access their photos.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {

        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {

            thumbImage.image = image

        }

        imagePicker.dismiss(animated: true)
    }

    // Only used to generate test data, all calls to this function must be commented out
    func generateTestData(){
        
        //NOTE: Hack for creating Store entities in CoreData for testing UI/App
        let store = Store(context: context)
        store.name = "Best Buy"
        let store2 = Store(context: context)
        store2.name = "Tesla Dealership"
        let store3 = Store(context: context)
        store3.name = "Fry's Electionics"
        let store4 = Store(context: context)
        store4.name = "Target"
        let store5 = Store(context: context)
        store5.name = "Amazon"
        let store6 = Store(context: context)
        store6.name = "Kmart"
        
        ad.saveContext()
    }
}


























