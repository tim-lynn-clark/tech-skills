//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Timothy Clark on 10/4/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//
//

import Foundation
import CoreData


public class Item: NSManagedObject {

    // When a new item is created and inserted into the DB, this function is called
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
        self.created = NSDate()
    }
}
