//: Playground - noun: a place where people can play

import UIKit

/* Question mark denotes an optional variable, it may or may not hold a valuable during execution. Seeing the ? reminds a programmer that they need to check the variable for nil before using it. */
var lotteryWinnings: Int?

if(lotteryWinnings != nil) {
    print(lotteryWinnings!)
}

lotteryWinnings = 10

//Not unwrapped
print(lotteryWinnings)

//Optional unwrapped for use, must use ! to unwrap an option for use
print(lotteryWinnings!)

/* If/Let syntax - preferred method for dealing with optionals */
if let winnings = lotteryWinnings {
    print(winnings)
}

class Car {
    var model: String?
}

var vehicle: Car?

// single if/let
if let car = vehicle {
    if let m = car.model {
        print(m)
    }
}

vehicle = Car()

// Access optional properties on an object by using the ? before dot notation access of the property. When using ? in dot notation you are telling the program you do not care if the property has a value or not when using it.
print(vehicle?.model)

// Assigning optional properties on an object by using the ? vefore dot notation access of the property
vehicle?.model = "Bronco"

// Multiple if/let chaining
if let v = vehicle, let m = v.model {
    print(m)
}


var cars: [Car]?

cars = [Car]()

// execute only if not nil and count is greater than 0
if let carArr = cars, carArr.count > 0 {
    
    
} else {
    cars?.append(Car())
    print(cars?.count)
}


// Implicitly unwrapped optional using !
class Person {


    //! after property states you guarantee you will have a value in this variable, but if it is never set and used it will crash the program
    //var age: Int!
    
    // Better option, initialize with a default
    //var age = 0;
    
    // Or better yet, use private properties with getter/setters
    private var _age: Int!
    
    var age: Int {
        if _age == nil {
            _age = 0;
        }
        
        return _age
    }
    
    
    func setAge(newAge: Int){
        self._age = newAge
    }
}

var jack = Person()

// No age, getter sets age to default 0
print(jack.age)


// No need for optionals or implicit optionals because species is guaranteed to have a value
class Dog {
    private var _species: String
    
    var species: String {
        return _species
    }
    
    init(selectedSpecies: String){
        self._species = selectedSpecies
    }
}

var myDog = Dog(selectedSpecies: "Akita")
print(myDog.species)






























