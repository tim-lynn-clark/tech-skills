//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

var implicitFloat = 40.0

var expliicitFloat: Float = 40.000

var combine = str + String(expliicitFloat)

var mutableString = "This is a test of the string \(implicitFloat)."

var arrayTest = ["test", "another test"]

var hashTest = [
    "test": "calling test",
    "another test": "Calling another test"
]

hashTest["yet another"] = "calling yet another"





