//
//  PlaySongVC.swift
//  Segues
//
//  Created by Timothy Clark on 8/23/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class PlaySongVC: UIViewController {
    
    @IBOutlet weak var songTitleLbl: UILabel!
    
    // <<-- TIM-NOTE -->>: Will be used to store the title passed to the view by the calling view
    private var _selectedSong: String = ""
    
    // <<-- TIM-NOTE -->>: Accessor/Mutators in Swift
    var selectedSong: String {
        get {
            return _selectedSong
        } set {
            _selectedSong = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        /* <<-- TIM-NOTE -->>: loading data passed by calling view */
        songTitleLbl.text = _selectedSong
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
