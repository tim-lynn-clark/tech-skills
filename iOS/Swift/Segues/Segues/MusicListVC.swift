//
//  MusicListVC.swift
//  Segues
//
//  Created by Timothy Clark on 8/23/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class MusicListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor.blue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        /* <<-- TIM-NOTE -->>: Instead of using a segue to move back to the home screen, always 'dismiss' the current screen
         this will remove the actual view and free up memory.
         This can be seen by using a segue to go back and forth between views and then opening the
         'Debug View Hierarchy' tab in the debug window, you will see tons of copies of the two views layered
         on top of each other. If you do the same with with 'dismiss' you will see only a single view at any one time */
        dismiss(animated: true, completion: nil)
    }

    @IBAction func LoadThirdScreenBtnPressed(_ sender: Any) {
        
        /* <<-- TIM-NOTE -->>: This shows how to call a generic segue in code. Generic segues are created by control dragging from one ViewController to another one. This segues must have an identifier in order to be called.  */
        performSegue(withIdentifier: "SongVCSegue", sender: "Achtung Baby")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        /* <<-- TIM-NOTE -->>: The perpare for segue function is called as a result of the 'performSegue' function being called in the above button press event. The code below checks to make sure the destination view is the 'PlaySongVC' and if it is it checks to see if the 'sender' object is the right type and if it is it sets the 'selectedSong' property of the new view. */
        if let destination = segue.destination as? PlaySongVC {
            
            if let song = sender as? String {
                destination.selectedSong = song
            }
            
        }
    }
    

}
