# PushMyNotifs Tutorial App
## Udemy Course: iOS 10 & Swift 3: From Beginner to Paid Professional
## Featuring course content created by DevSlopes.com

This app was built while learning the following Swift technologies/practices:

### Google Firebase

https://firebase.google.com/

Hosted, real-time, no-sql database for storing and synchronizing mobile application data and a whole lot more.

* Setting up Firebase for an iOS application
* Registering an AppId through dev.apple.com
* Creating an iOS Push Notification Service SSL (Sandbox) certificate in the development environment
* Installing iOs Puch Notification Service SSL certificate in Firebase via app > settings > cloud messaging
* Generating a .p12 file from the iOS Push Notification SSL certificate via Keychain Access and uploading it to the app on Firebase as a development certificate
* Setting iOS app entitlements allowing Push Notifications to be received via app > capabilities > push notifications and turning the push feature on for the app
* Registering an iOS application for push notifications, requesting permissions from user
* NOT COVERED BUT IMPORTANT: Setting up a developer provisioning profile for a specific app in order to install for testing on an actual iOS device
*