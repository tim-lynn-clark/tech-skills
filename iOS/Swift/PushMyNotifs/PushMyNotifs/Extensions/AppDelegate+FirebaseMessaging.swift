//
//  AppDelegate+FirebaseMessaging.swift
//  PushMyNotifs
//
//  Created by Timothy Clark on 12/31/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import Foundation
//NOTE: Library needed to use Firebase real-time database service
import Firebase
//NOTE: Libraries needed to use Firebase Messaging service for in-app notifications
import FirebaseInstanceID
import FirebaseMessaging

extension AppDelegate: MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        
        debugPrint("--->messaging:\(messaging)")
        debugPrint("--->didReceive Remote Message:\(remoteMessage.appData)")
        guard let data =
            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
            let prettyPrinted = String(data: data, encoding: .utf8) else { return }
        print("Received direct channel message:\n\(prettyPrinted)")
    }
    // [END ios_10_data_message]
    
}
