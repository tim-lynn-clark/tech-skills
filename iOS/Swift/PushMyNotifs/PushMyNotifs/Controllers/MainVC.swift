//
//  MainVC.swift
//  PushMyNotifs
//
//  Created by Timothy Clark on 12/31/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

//NOTE: Firebase Setup see AppDelegate file

import UIKit
//NOTE: Libraries required for Firebase Messaging use
import Firebase

class MainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //NOTE: Setup Firebase Messaging for use
       Messaging.messaging().subscribe(toTopic: "/topics/news")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
