//
//  ViewController.swift
//  RetroCalculator
//
//  Created by Timothy Clark on 8/23/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var btnSound: AVAudioPlayer!
    var runningNumber = ""
    var currentOperation = Operation.Empty
    var leftValString = ""
    var rightValString = ""
    var result = ""
    enum Operation: String {
        case Divide = "/"
        case Multiply = "*"
        case Subtract = "-"
        case Add = "+"
        case Empty = "Empty"
    }

    @IBOutlet weak var outputLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //<<-- TIM-CODE: Play Sounds -->>: Loading sound file
        let path = Bundle.main.path(forResource: "btn", ofType: "wav")
        let soundURL = URL(fileURLWithPath: path!)
        
        do{
            
            try btnSound = AVAudioPlayer(contentsOf: soundURL)
            btnSound.prepareToPlay()
            
        } catch let err as NSError {
            
            print(err.debugDescription)
            
        }
    }
    
    @IBAction func numberPressed(sender: UIButton){
        playSound()
        
        /* TIM-NOTE: This IBAction was dragged to all of the calculator buttons
         in order to avoid creating a new IBAction for every single button.
         To make sure we know what button was pressed we can use the 'tag' property
         found on the pressed button to pull a value, set the 'tag' property manually
         on each visual control. */
        runningNumber += "\(sender.tag)"
        
        outputLbl.text = runningNumber
    }
    
    @IBAction func onDividePressed(sender: UIButton){
        processOperation(operation: Operation.Divide)
    }
    
    @IBAction func onMultiplyPressed(sender: UIButton){
        processOperation(operation: Operation.Multiply)
    }
    
    @IBAction func onSubtractPressed(sender: UIButton){
        processOperation(operation: Operation.Subtract)
    }
    
    @IBAction func onAddPressed(sender: UIButton){
        processOperation(operation: Operation.Add)
    }
    
    @IBAction func onEqualPressed(sender: UIButton){
        processOperation(operation: currentOperation)
    }
    
    func processOperation(operation: Operation){
        
        playSound()
        
        if currentOperation != Operation.Empty {
            if runningNumber != "" {
                rightValString = runningNumber
                runningNumber = ""
                
                if currentOperation == Operation.Multiply {
                    result = "\(Double(leftValString)! * Double(rightValString)!)"
                } else if currentOperation == Operation.Divide {
                    result = "\(Double(leftValString)! / Double(rightValString)!)"
                } else if currentOperation == Operation.Subtract {
                    result = "\(Double(leftValString)! - Double(rightValString)!)"
                } else if currentOperation == Operation.Add {
                    result = "\(Double(leftValString)! + Double(rightValString)!)"
                }
                
                leftValString = result
                outputLbl.text = result
            }
            
            currentOperation = operation
        } else {
            leftValString = runningNumber
            runningNumber = ""
            currentOperation = operation
        }
    }

    func playSound(){
        if btnSound.isPlaying {
            btnSound.stop()
        }
        
        btnSound.play()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}





















