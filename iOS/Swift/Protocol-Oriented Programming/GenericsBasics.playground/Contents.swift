//: Playground - noun: a place where people can play

import UIKit

// ISSUE: When dealing with numbers, it would be nice to create one method
// that handles adding 1 to the number without needing to make a new type specific
// function for each type of number.

func intAdder(number: Int) -> Int {
    return number + 1
}

intAdder(number: 15)
//intAdder(number: 15.0) //ERROR: cannot convert value of type 'Double' to expected argument type 'Int'

func doubleAdder(number: Double) -> Double {
    return number + 1.0
}

doubleAdder(number: 15.0)

// SOLUTION: Generics solve the issue above by allowing you to create

//NOTE: Numeric is a Protocol in the standard library that allows arithmatic to be performed on mixed types
func genericAdder<T: Numeric>(number: T) -> T {
    return number + 1
}

genericAdder(number: 15)
genericAdder(number: 15.0)

func genericMultiplier<T: Numeric>(lhs: T, rhs: T) -> T {
    return lhs * rhs
}

genericMultiplier(lhs: 10, rhs: 10)
genericMultiplier(lhs: 5, rhs: 2)
genericMultiplier(lhs: 10.0, rhs: 10.0)
genericMultiplier(lhs: 5.0, rhs: 2.0)























