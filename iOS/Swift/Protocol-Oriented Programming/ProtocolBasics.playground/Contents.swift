//: Playground - noun: a place where people can play

// Reference: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html

import UIKit

//NOTE: Implementing a Vehicle using Protocol instead of Class
//NOTE: This Protocol also requires a Protocol from the standard library 'CustomStringConvertible', this will force all implementors of the Vehicle Protocol to implement all methods required by 'CustomStringConvertible'
protocol Vehicle: CustomStringConvertible {
    var isRunning: Bool { get set }
    
    //NOTE: If you will be using this protocol with stuctures, you must annotate each function that will alter a protocol attribute with the key word 'mutating'. The protocol can still be used with a Class with the 'mutating' declaration.
    mutating func start()
    mutating func turnOff()
}

//NOTE: Struct using a Protocol
struct SportsCar: Vehicle {
    //Required by Vehicle Protocol
    var isRunning: Bool = false
    
    //Required by CustomStringConvertible Protocol
    var description: String {
        if isRunning {
            return "Sports car currently running"
        } else {
            return "Sports car currently turned off"
        }
    }
    
    //Required by Vehicle Protocol
    mutating func start() {
        if isRunning {
            print("Already started!")
        } else {
            isRunning = true
            print("Vrooom")
        }
    }
    
    //Required by Vehicle Protocol
    mutating func turnOff() {
        if isRunning {
            isRunning = false
            print("Crickets")
        } else {
            print("Already off!")
        }
    }
    
    
}

//NOTE: Class using Protocol
class SemiTruck: Vehicle {
    //Required by Vehicle Protocol
    var isRunning: Bool = false
    
    //Required by CustomStringConvertible Protocol
    var description: String {
        if isRunning {
            return "Semi truck currently running"
        } else {
            return "Semi truck currently turned off"
        }
    }
    
    //Required by Vehicle Protocol
    func start() {
        if isRunning {
            print("Already started!")
        } else {
            isRunning = true
            print("Rumble")
        }
    }
    
    //Required by Vehicle Protocol
    func turnOff() {
        if isRunning {
            isRunning = false
            print("Studder..studder..silence")
        } else {
            print("Already off!")
        }
    }
    
    //Custom function specific to this type
    func blowAirHorn(){
        print("Tooooooot!")
    }
}

var car1 = SportsCar()
var truck1 = SemiTruck()

car1.start()
truck1.start()
truck1.blowAirHorn()

car1.turnOff()
truck1.turnOff()

//NOTE: Polymorphism via Protocol, various implementations of the Vehicle Protocol can be stored in an Array that is of type Vehicle Protocol
var vehicles: Array<Vehicle> = [car1, truck1]

for vehicle in vehicles {

    //NOTE: Array containing objects that implement a Protocol that is not explicitly marked as a Class Protocol, when they are iterated over they objects pulled are implied immutable 'let' varialbe, to use/modify them you need to make a local shadow copy first that is a 'var' variable
    //https://stackoverflow.com/questions/39330920/foreach-results-in-0-is-immutable-error
    var vehicle = vehicle
    
    //NOTE: Via polymorphism, you can access methods and attributes of all Vehicles as it is guaranteed that they all implement all required attributes and function of the Vehicle Protocol
    vehicle.start()

    print("\(vehicle.isRunning)")
    print("\(vehicle.description)")

    //NOTE: Special properties/functions of a struct or class must be accessed after first casting the Vehile to the specific type
    if let truck = vehicle as? SemiTruck {
        truck.blowAirHorn()
    }
}











