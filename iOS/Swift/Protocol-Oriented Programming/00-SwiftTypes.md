*Swift Types*

	Named Types
		Value Types
			Structures - when a structure is copied, the value is copied, there are no references to the same memory location (object), Apple recommends that Value Types be used whenever possible as they are thread-safe.

			Primatives - in Swift they are implemented as structures with extensions
				Integers
				Double
				Char
			Enumerations

	Reference Types
		Classes - not thread-safe due to references to the same memory location (object)

	Compound Types
		Functions
			Functions
			Closures
		Tuples