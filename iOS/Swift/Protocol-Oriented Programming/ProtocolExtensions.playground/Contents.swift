//: Playground - noun: a place where people can play

// Reference: https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html

import UIKit

//For details on this existing code see: ProtocolBasics playground in same folder
//This code condensed to remove distraction for new code and notes specific to Protocol Extensions
protocol Vehicle: CustomStringConvertible {
    var isRunning: Bool { get set }; var make: String { get set }; var model: String { get set }; mutating func start(); mutating func turnOff();
}

//NOTE: Using a Protocol extension to provide default implementations of Protocol Functions
extension Vehicle {
    
    //NOTE: Function Implementations and Computed Properties can be added to Protocol Extensions in order to provide default implementations
    
    var makeModel: String {
        return "\(make) \(model)"
    }
    
    mutating func start() {
        if isRunning {
            print("Already started.")
        } else {
            isRunning = true
            print("\(self.description) fired up!")
        }
    }
    
    mutating func turnOff() {
        if isRunning {
            isRunning = false
            print("\(self.description) shut down!")
        } else {
            print("Already off.")
        }
    }
    
}

//For details on this existing code see: ProtocolBasics playground in same folder
//This code condensed to remove distraction for new code and notes specific to Protocol Extensions
struct SportsCar: Vehicle {
    var isRunning: Bool; var make: String; var model: String;
    var description: String { return self.makeModel }
    
    //NOTE: with these Protocol required functions commented out, the SportsCar is receiving the default implementations from the Protocol Extension
    //mutating func start() { if isRunning { print("Already started!") } else { isRunning = true; print("Vrooom") } }
    //mutating func turnOff() { if isRunning { isRunning = false; print("Crickets") } else { print("Already off!") } }
}

//For details on this existing code see: ProtocolBasics playground in same folder
//This code condensed to remove distraction for new code and notes specific to Protocol Extensions
class SemiTruck: Vehicle {
    var isRunning: Bool; var make: String; var model: String;
    var description: String { return self.makeModel }
    
    init(isRunning: Bool, make: String, model: String){
        self.isRunning = isRunning
        self.make = make
        self.model = model
    }
    
    func start() { if isRunning { print("Already started!") } else { isRunning = true; print("Rumble") } }
    func turnOff() { if isRunning { isRunning = false; print("Studder..studder..silence") } else { print("Already off!") } }
    func blowAirHorn(){ print("Tooooooot!") }
}


//Note: When no default vaules are assigned to struct or class attributes, they must be provided as arguements to the constructor function at the time of creation
var car1 = SportsCar(isRunning: false, make: "Chevy", model: "Camaro")
var truck1 = SemiTruck(isRunning: false, make: "Chevy", model: "Task Force")

car1.start()
truck1.start()
truck1.blowAirHorn()

car1.turnOff()
truck1.turnOff()

//NOTE: Polymorphism via Protocol, various implementations of the Vehicle Protocol can be stored in an Array that is of type Vehicle Protocol
var vehicles: Array<Vehicle> = [car1, truck1]

for vehicle in vehicles {
    
    //NOTE: Array containing objects that implement a Protocol that is not explicitly marked as a Class Protocol, when they are iterated over they objects pulled are implied immutable 'let' varialbe, to use/modify them you need to make a local shadow copy first that is a 'var' variable
    //https://stackoverflow.com/questions/39330920/foreach-results-in-0-is-immutable-error
    var vehicle = vehicle
    
    //NOTE: Via polymorphism, you can access methods and attributes of all Vehicles as it is guaranteed that they all implement all required attributes and function of the Vehicle Protocol
    vehicle.start()
    
    print("\(vehicle.isRunning)")
    print("\(vehicle.description)")
    
    //NOTE: Special properties/functions of a struct or class must be accessed after first casting the Vehile to the specific type
    if let truck = vehicle as? SemiTruck {
        truck.blowAirHorn()
    }
}







