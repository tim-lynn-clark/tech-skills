//: Playground - noun: a place where people can play
// Credit: https://www.bignerdranch.com/blog/protocol-oriented-problems-and-the-immutable-self-error/

import UIKit

enum Direction {
    case None, Forward, Backward, Up, Down, Left, Right
}

protocol VehicleType {
    var speed: Int { get set }
    var direction: Direction { get }
    mutating func changeDirectionTo(direction: Direction)
    mutating func stop()
}

extension VehicleType {
    mutating func stop() {
        speed = 0
    }
}

class Car: VehicleType {
    var speed = 0
    private(set) var direction: Direction = .None
    
    func changeDirectionTo(direction: Direction) {
        self.direction = direction
        
        if direction == .None {
            //ERROR: “Cannot use mutating member on immutable value: ‘self’ is immutable” is thrown when calling the 'stop' function using the implied 'self'
            stop()
        }
    }
}
