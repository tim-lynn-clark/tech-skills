//
//  ViewController.swift
//  MiraclePill
//
//  Created by Timothy Clark on 8/22/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var statePicker: UIPickerView!
    @IBOutlet weak var stateBtn: UIButton!
    
    let states = ["Alaska", "Arkansas", "Alabama", "California", "Main", "New York"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        statePicker.dataSource = self
        statePicker.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // UIPicker Interface Functions
    @IBAction func stateBtnPressed(_ sender: Any) {
        statePicker.isHidden = !statePicker.isHidden
    }
    
    // like columns in a spreadsheet
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // rows in a spreadsheet
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    // title
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        stateBtn.setTitle(states[row], for: UIControlState.normal)
    }
}

