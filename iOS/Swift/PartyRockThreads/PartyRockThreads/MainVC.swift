//
//  ViewController.swift
//  PartyRockThreads
//
//  Created by Timothy Clark on 8/28/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

/* TIMS-NOTES: Lesson Topics
 
 - Using a UITableView
 - Using UITableViewCell and implementing a custom UITableViewCell class
 - Loading images from the web asynchronously
 - Loading a YouTube video into a UIWebView
 - Implementing UITableViewDelegate protocol 
 - Implementing UITableViewDataSource protocol
 - Handling UITableView row selection
 - Calling a manual segue using performSegue
 - Passing data between UIViewControllers using Prepare for segue 
 
*/

import UIKit


// TIMS-NOTES: Making the UIViewController into the delegate or handler for the UITableView by adding the interfaces (protocols)
class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var partyRocks = [PartyRock]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TIMS-NOTES: Hard coded data for testing
        let p1 = PartyRock(imageURL: "http://www.entertainmentmanagementonline.com/wp-content/uploads/2013/12/digital-music.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/NPyiLkNf_0c\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Relaxing Instrumental")
        let p2 = PartyRock(imageURL: "http://dynamicdigitalmusic.com/images/54637c2d92a11.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/HKU96i_Qh8Y\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Feeling Happy")
        let p3 = PartyRock(imageURL: "https://channelo.dstv.com/wp-content/uploads/2015/12/digital-music06.jpg.preview.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/GgaYWwuhVGE\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "The Best of Vocal Deep House")
        let p4 = PartyRock(imageURL: "http://static1.1.sqspcdn.com/static/f/207938/2678005/1237129651733/rhizome_wave.jpg?token=REweoEnCGJ6y1gLUsTa%2BnziH5qw%3D",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/gmQJVl51yCc\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Y3lloW - Deep House Vocals")
        let p5 = PartyRock(imageURL: "http://cdn03.androidauthority.net/wp-content/uploads/2012/10/shutterstock-digital-music.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/Bu3kAbpi5jo\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Best Melodic Dubstep Mix 2017")
        let p6 = PartyRock(imageURL: "http://www.entertainmentmanagementonline.com/wp-content/uploads/2013/12/digital-music.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/NPyiLkNf_0c\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Relaxing Instrumental")
        let p7 = PartyRock(imageURL: "http://dynamicdigitalmusic.com/images/54637c2d92a11.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/HKU96i_Qh8Y\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Feeling Happy")
        let p8 = PartyRock(imageURL: "https://channelo.dstv.com/wp-content/uploads/2015/12/digital-music06.jpg.preview.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/GgaYWwuhVGE\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "The Best of Vocal Deep House")
        let p9 = PartyRock(imageURL: "http://static1.1.sqspcdn.com/static/f/207938/2678005/1237129651733/rhizome_wave.jpg?token=REweoEnCGJ6y1gLUsTa%2BnziH5qw%3D",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/gmQJVl51yCc\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Y3lloW - Deep House Vocals")
        let p10 = PartyRock(imageURL: "http://cdn03.androidauthority.net/wp-content/uploads/2012/10/shutterstock-digital-music.jpg",
                           videoURL: "<iframe width=\"854\" height=\"480\" src=\"https://www.youtube.com/embed/Bu3kAbpi5jo\" frameborder=\"0\" allowfullscreen></iframe>",
                           videoTitle: "Best Melodic Dubstep Mix 2017")
        
        partyRocks.append(p1);
        partyRocks.append(p2);
        partyRocks.append(p3);
        partyRocks.append(p4);
        partyRocks.append(p5);
        partyRocks.append(p6);
        partyRocks.append(p7);
        partyRocks.append(p8);
        partyRocks.append(p9);
        partyRocks.append(p10);
        
        // TIMS-NOTES: Remember to set this to be the delegate for all UITableView events
        tableView.delegate = self
        
        // TIMS-NOTES: Remember to set this to be the source for data for the UITableView
        tableView.dataSource = self
    }

    // TIMS-NOTES: Required UITableView method from interface
    // Returns a recycled UITableViewCell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // TIMS-NOTES: verify that the incoming cell is a reusable cell and has been dequed (recycled by iOS)
        // also check to make sure it is of the right type 'PartyCell'
        // TIMS-NOTES: because a string identifier is used to locate the proper cell class to use 'PartyCell', 
        // you have to make sure that the UITableCell prototype in the story board has 'PartyCell' set as its
        // string identifier, if you do not it will not work.
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCell", for: indexPath) as? PartyCell {
            let partyRock = partyRocks[indexPath.row]
            cell.updateUI(partyRock: partyRock)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    // TIMS-NOTES: Required UITableView method from interface
    // How many rows do you want in your UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partyRocks.count;
    }
    
    /* TIMS-NOTES: UITableView protocol function to handle the selection of a row */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let partyRock = partyRocks[indexPath.row]
        
        // Perform a manual segue of a generic segue created between two UIViewControllers passing data
        performSegue(withIdentifier: "VideoVC", sender: partyRock)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        /* TIMS-NOTES: here we are checking to see if the segue being performed is actually
         the VideoVC segue to the VideoVC controller as destination. */
        if let destination = segue.destination as? VideoVC {
            
            /* TIMS-NOTES: here we are then checking to make sure that the data passed with the segue call
             is actually a PartyRock object, if it is we pass that object to the VideoVC destination controller. */
            if let party = sender as? PartyRock {
                destination.partyRock = party
            }
        }
    }

}

