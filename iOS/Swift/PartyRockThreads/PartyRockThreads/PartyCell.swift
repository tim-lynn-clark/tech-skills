//
//  PartyCell.swift
//  PartyRockThreads
//
//  Created by Timothy Clark on 8/28/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

/* TIMS-NOTES: UITableViewCells are recycled by iOS in order to 
 save on resources. Each time a cell moves off the screen it is reused
 and is pushed back onto the top. Because of this, these cells need to 
 be updated to hold new content when recycled. */

class PartyCell: UITableViewCell {
    
    
    @IBOutlet weak var videoPreviewImage: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    /* TIMS-NOTES: custom function to handle updating the cell's content
     when it is recyclted by iOS. */
    func updateUI(partyRock: PartyRock){
        videoTitle.text = partyRock.videoTitle
        
        /* TIMS-NOTES: Async image download, never block the UI thread. 
         In order for non-https images to be loaded into your app, you must
         add to Info.plist a new config section 'App Transport Security Settings' 
         with the setting 'Allow Arbitrary Loads' added and set to YES*/
        let url = URL(string: partyRock.imageURL)!
        
        /* TIMS-NOTES: Begin task on another thread so as not to block the UI thread. */
        DispatchQueue.global().async {
            do {
                let data = try Data(contentsOf: url)
                
                /* TIMS-NOTES: If loading the remote asset works, come back to the UI thread
                 and add it to the app. */
                DispatchQueue.global().sync {
                    self.videoPreviewImage.image = UIImage(data: data)
                }
            } catch {
                //TODO: handle error case
            }
        }
        
    }

}
