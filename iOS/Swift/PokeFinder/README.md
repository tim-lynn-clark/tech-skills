# PokeFinder Tutorial App
## Udemy Course: iOS 10 & Swift 3: From Beginner to Paid Professional
## Featuring course content created by DevSlopes.com

This app was built while learning the following Swift technologies/practices:

* Setting up a Firebase application and adding it to an iOS app
* Using GeoFire to store GeoLocation data and provide real-time geo queries in Firebase
* Installing GeoFire Objective-C source in Swift project by adding a 'bridging.h' file and then adding it to the project build settings to import the GeoFire Objective-C library
* Using the Map control and the MapKit library to work with maps
* Using Location Manager to get the users current geo location
* Implementing the MapViewDelegate protocol
* Using UserTrackingMode on the MapView to follow the user's current location while application is in the foreground
* Setting a custom image for the user's annotation pin used to mark the user's current location on the map
* Creating a custom MapView annotation class to allow custom pin images to be used for each Pokemon location
* Using Apple maps to set a destination and display a route from current location to that destination
* Setting Firebase database permissions to squash 'permission_denied' error messages