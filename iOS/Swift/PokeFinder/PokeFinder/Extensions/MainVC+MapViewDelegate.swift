//
//  MainVC+MapViewDelegate.swift
//  PokeFinder
//
//  Created by Timothy Clark on 1/4/18.
//  Copyright © 2018 ViolentAtom. All rights reserved.
//

import Foundation
import MapKit

extension MainVC: MKMapViewDelegate {
    
    //NOTE: Recenters the map on the user's current location when called
    func centerMapOnLocation(location: CLLocation){
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if let location = userLocation.location {
            
            //NOTE: We only want the map to center on user's location automatically when the view first loads
            if !mapHasCenteredOnce {
                centerMapOnLocation(location: location)
                mapHasCenteredOnce = true
            }
            
            
        }
    }
    
    //NOTE: Customize the icon used for the map pin on the user's current location
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        //NOTE: Creating the replacement annotation view that will be replacing the default
        let annotationIdentifier = "Pokemon"
        var annotationView: MKAnnotationView?
        
        //NOTE: Checking to see if the current annotation is the user type
        if annotation.isKind(of: MKUserLocation.self) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "User")
            annotationView?.image = UIImage(named: "ash")
        } else if let dequeuedAnnotation = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            
            //NOTE: Allows the MapView to reuse annotations on the map as they are dequeued as they move out of the visible map area.
            annotationView = dequeuedAnnotation
            annotationView?.annotation = annotation
        } else {
            
            //NOTE: Sets a default Annotation View
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        //NOTE: If any of the Annotation Views that come through this handler are of type PokeAnnotation, we need to set the image and map button for them.
        if let annotationView = annotationView, let annotation = annotation as? PokeAnnotation {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "\(annotation.pokemonNumber)")
            
            let button = UIButton()
            button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            button.setImage(UIImage(named: "map"), for: .normal)
            
            annotationView.rightCalloutAccessoryView = button
        }
        
        return annotationView
    }
    
    //NOTE: Event handler to handle panning around on the MapView, making sure that as the user pans PokeMon that are now within the map region are added to the map.
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        
        let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        showSightingsOnMapp(location: location)
    }
    
    //NOTE: Event handler to handle the user tapping on a Pokemon map marker
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //NOTE: Using Apple Maps to pull up a travel map to the selected Pokemon from the user's current location
        if let annotation = view.annotation as? PokeAnnotation {
            //NOTE: This is the location of the Pokemon pulled from the PokeAnnotation
            let place = MKPlacemark(coordinate: annotation.coordinate)
            let destination = MKMapItem(placemark: place)
            destination.name = "Pokemon Sighting"
            
            //NOTE: 
            let regionDistance: CLLocationDistance = 1000
            let regionSpan = MKCoordinateRegionMakeWithDistance(annotation.coordinate, regionDistance, regionDistance)
            
            //NOTE: Setting the options for the Apple map
            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span), MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving] as [String : Any]
            
            MKMapItem.openMaps(with: [destination], launchOptions: options)
        }
    }
}






















