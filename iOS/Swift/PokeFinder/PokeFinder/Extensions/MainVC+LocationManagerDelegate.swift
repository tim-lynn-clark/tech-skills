//
//  MainVC+LocationManagerDelegate.swift
//  PokeFinder
//
//  Created by Timothy Clark on 1/4/18.
//  Copyright © 2018 ViolentAtom. All rights reserved.
//

import Foundation

extension MainVC: CLLocationManagerDelegate {
    
    //NOTE: Handle authorization event after user grants this application the rights to use their location data (see MainVC locationAuthStatus function)
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            //TODO: Would be a good idea to notify the user that the application is useless without their location data
        }
    }
    
}
