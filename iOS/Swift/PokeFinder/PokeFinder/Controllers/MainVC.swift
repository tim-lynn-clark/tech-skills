//
//  MainVC.swift
//  PokeFinder
//
//  Created by Timothy Clark on 1/2/18.
//  Copyright © 2018 ViolentAtom. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase

class MainVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    //NOTE: Location Manager for accessing the user's current location if they have allowed its use
    let locationManager = CLLocationManager()
    var mapHasCenteredOnce = false
    var geoFire: GeoFire!
    var firebaseDatabaseRef: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //NOTE: Make sure to set this controller as the delegate responsible for responding to MapView events
        mapView.delegate = self
        //NOTE: The map control can be set to track the user while the application is in the foreground
        mapView.userTrackingMode = MKUserTrackingMode.follow
        
        //NOTE: Grants a reference to the current Firebase database instance for this application
        firebaseDatabaseRef = Database.database().reference()
        //NOTE: Pass the database reference to GeoFire so it can use it to store Geo Location data
        geoFire = GeoFire(firebaseRef: firebaseDatabaseRef)
    }
    
    //NOTE: The user's location can only be accessed if the user gives their permission, therefore each time this view is loaded the permissions must be checked and the user asked if no permissions have been granted
    override func viewDidAppear(_ animated: Bool) {
        locationAuthStatus()
    }
    
    func locationAuthStatus(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            
            let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
            showSightingsOnMapp(location: location)
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //NOTE: Called each time a poke is spotted
    func createSighting(forLocation location: CLLocation, withPokemon pokeId: Int) {
        //NOTE: GeoFire works its location management magic through this one function call
        geoFire.setLocation(location, forKey: "\(pokeId)")
    }
    
    //NOTE: Querying the Firebase database through the GeoFire API
    func showSightingsOnMapp(location: CLLocation) {
        //NOTE: GeoFire handles the math to build a query that has a circular radius to find all database location entries that have geo locations within the radius of the circle
        let circleQuery = geoFire!.query(at: location, withRadius: 2.5)
        
        //NOTE: As the circular query executes, we setup an observer to handle any returned results from the query. The observer is called each time a new result is returned by the query as it executes.
        _ = circleQuery?.observe(GFEventType.keyEntered, with: { (key, location) in
            
            //NOTE: If a query result is returned we want to add a new pin to the map using the custom map annotcation class PokeAnnotaction. This allows the custom Pokemon image to used as the pin on the map.
            if let key = key, let location = location {
                let anno = PokeAnnotation(coordinate: location.coordinate, pokemonNumber: Int(key)!)
                self.mapView.addAnnotation(anno)
            }
            
        })
    }

    //NOTE: Really for testing, when clicked a random Pokemon is added to the map at a random location
    @IBAction func spotRandomPokemonPressed(_ sender: UIButton) {
        
        //TODO: Make the location random instead of always at the user's location, testing from your desk you will never be at a different location.
        let randomLocation = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        let randomPokemon = arc4random_uniform(151) + 1
        
        createSighting(forLocation: randomLocation, withPokemon: Int(randomPokemon))
    }
    
}






















