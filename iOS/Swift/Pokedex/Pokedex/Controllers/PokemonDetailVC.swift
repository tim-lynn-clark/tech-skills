//
//  PokemonDetailVC.swift
//  Pokedex
//
//  Created by Timothy Clark on 9/30/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var defenseLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var pokedexIdLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var baseAttackLbl: UILabel!
    @IBOutlet weak var currentEvoImg: UIImageView!
    @IBOutlet weak var nextEvoImg: UIImageView!
    @IBOutlet weak var evoLbl: UILabel!
    
    //Variable to handle data passed via the segue call "PokemonDetailVC"
    var pokemon: Pokemon!

    override func viewDidLoad() {
        super.viewDidLoad()

        nameLbl.text = pokemon.name.capitalized
        pokedexIdLbl.text = String(pokemon.pokedexId)
        
        let img =  UIImage(named: String(pokemon.pokedexId))
        mainImg.image = img
        currentEvoImg.image = img
        
        pokemon.downloadPokemonDetails(completed: {
            // Executes once the async network call has completed fetching
            // the pokemon details from pokeapi.co
            self.updateUI()
        })
    }

    func updateUI(){
        // Update all label controls with data fectched in async call to pokeapi.co
        baseAttackLbl.text = pokemon.attack
        defenseLbl.text = pokemon.defense
        heightLbl.text = pokemon.height
        weightLbl.text = pokemon.weight
        typeLbl.text = pokemon.type
        descriptionLbl.text = pokemon.description
        
        if pokemon.nextEvolutionId == "" {
            evoLbl.text = "No Evolutions"
            nextEvoImg.isHidden = true
        } else {
            nextEvoImg.isHidden = false
            nextEvoImg.image = UIImage(named: pokemon.nextEvolutionId)
            
            evoLbl.text = "Next Evolution: \(pokemon.nextEvolutionName) - LVL \(pokemon.nextEvolutionLevel)"
        }
    }

    @IBAction func backBtnPressed(_ sender: UIButton) {
        // Dismiss the page in order to go back to the home page
        dismiss(animated: true, completion: nil)
        
    }
}
