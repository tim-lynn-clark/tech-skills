//
//  Constants.swift
//  Pokedex
//
//  Created by Timothy Clark on 10/1/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

// Closure for handling async callbacks from Alamofire
typealias DownloadComplete = () -> ()
