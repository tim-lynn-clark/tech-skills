//
//  Constants.swift
//  RainyShiny
//
//  Created by Timothy Clark on 9/13/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import Foundation

// http://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=b1b15e88fa797225412429c1c50c122a1
// https://samples.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=10&appid=b1b15e88fa797225412429c1c50c122a1

let BASE_URL = "http://samples.openweathermap.org/data/2.5"
let WEATHER_END_POINT = "/weather?"
let FORECAST_END_POINT = "/forecast/daily?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let DAY_COUNT = "&cnt="
let APP_ID = "&appid="
let API_KEY = "b1b15e88fa797225412429c1c50c122a1"

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "\(BASE_URL)\(WEATHER_END_POINT)\(LATITUDE)\(Location.sharedInstance.latitude!)\(LONGITUDE)\(Location.sharedInstance.longitude!)\(APP_ID)\(API_KEY)"
let CURRENT_FORECAST_URL = "\(BASE_URL)\(FORECAST_END_POINT)\(LATITUDE)\(Location.sharedInstance.latitude!)\(LONGITUDE)\(Location.sharedInstance.longitude!)\(DAY_COUNT)10\(APP_ID)\(API_KEY)"
