//
//  Location.swift
//  RainyShiny
//
//  Created by Timothy Clark on 9/19/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import CoreLocation

class Location {
    
    static var sharedInstance = Location()
    
    private init() {}
    
    var latitude: Double!
    var longitude: Double!
    
}
