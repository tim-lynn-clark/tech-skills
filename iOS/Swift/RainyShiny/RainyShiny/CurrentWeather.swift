//
//  CurrentWeather.swift
//  RainyShiny
//
//  Created by Timothy Clark on 9/13/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    
    private var _cityName: String!
    private var _date: String!
    private var _weatherType: String!
    private var _currentTemp: Double!
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
        
        return _date
    }
    
    var cityName: String {
        if self._cityName == nil {
            self._cityName = ""
        }
        return self._cityName
    }
    
    var weatherType: String {
        if self._weatherType == nil {
            self._weatherType = ""
        }
        return self._weatherType
    }
    
    var currentTemp: Double {
        if self._currentTemp == nil {
            self._currentTemp = 0.0
        }
        return self._currentTemp
    }
    
    func downloadWeatherDetails (completed: @escaping DownloadComplete) {
        //Alamofire download code
        
        print(CURRENT_WEATHER_URL)
        
        Alamofire.request(CURRENT_WEATHER_URL).responseJSON { response in
            let result = response.result
            
            print(result.value!)
            
            if let dict = result.value as? Dictionary<String, Any>{
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                }
                
                if let weather = dict["weather"] as? [Dictionary<String, Any>]{
                    
                    if let main = weather[0]["main"] as? String {
                        self._weatherType = main.capitalized
                    }
                }
                
                if let main = dict["main"] as? Dictionary<String, Any>{
                    if let currentTemp = main["temp"] as? Double {
                        let kelvinToFarenheit = (currentTemp * (9/5) - 459.76)
                        let temp = Double(round(10 * kelvinToFarenheit/10))
                        
                        self._currentTemp = temp
                    }
                }
            }
            
            completed()
        }
        
        
    }
}







































