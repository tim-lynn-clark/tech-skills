//
//  WeatherCellTableViewCell.swift
//  RainyShiny
//
//  Created by Timothy Clark on 9/19/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var weatherTypeLbl: UILabel!
    @IBOutlet weak var highTempLbl: UILabel!
    @IBOutlet weak var lowTempLbl: UILabel!
    

    func configureCell(forecast: Forecast) {
        lowTempLbl.text = forecast.lowTemp
        highTempLbl.text = forecast.highTemp
        weatherTypeLbl.text = forecast.weatherType
        dayLbl.text = forecast.date
        
        weatherIcon.image = UIImage(named: forecast.weatherType)
    }

}
