//
//  ViewController.swift
//  CustomNotifications
//
//  Created by Timothy Clark on 12/30/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Frameworks required for User Notifications
import UserNotifications

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //NOTE: To use notification you must get the user's permissions
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { (granted, error) in
            
            //NOTE: In-line handling of delegate functions for authorization request
            if granted {
                print("Notification access granted")
            } else {
                print(error?.localizedDescription as Any)
            }
        })
    }

    @IBAction func notificationButtonPressed(_ sender: UIButton) {
    
        //NOTE: Create a scheduled notification
        scheduleNotification(inSeconds: 5, completion: { success in
            if success {
                print("Notification scheduled successfully.")
            } else {
                print("Error: could not schedule notification.")
            }
        })
    
    }
    
    func scheduleNotification(inSeconds: TimeInterval, completion: @escaping (_ Success: Bool) -> ()) {
        
        //NOTE: Object to hold content to be sent in the notification
        let notif = UNMutableNotificationContent()
        
        //NOTE: When using a custom notification view extension, you must specify the category identifier you gave the custom view
        notif.categoryIdentifier = "customContentExtension"
        
        notif.title = "New Notification"
        notif.subtitle = "These are great!"
        notif.body = "This is a new user notification available starting in iOS 10. They will come in very handy."
        
        //NOTE: Creating an image attachment for a notification
        guard let imageUrl = Bundle.main.url(forResource: "15", withExtension: "png") else {
            completion(false)
            return
        }

        do {
            try notif.attachments = [UNNotificationAttachment(identifier: "15", url: imageUrl, options: .none)]
        } catch {
            print("Error: could not create attachment")
        }
        
        //NOTE: Adding an image attachment to a notification
        
        //NOTE: Utilizing a TimeInterval NotificationTrigger to initialize the sending of the notification based on a time interval
        let notifTrigger = UNTimeIntervalNotificationTrigger(timeInterval: inSeconds, repeats: false)
        
        //NOTE: Create a request to send Notification to user
        let request = UNNotificationRequest(identifier: "My Notification", content: notif, trigger: notifTrigger)
        
        //NOTE: Add notification request to the user notification center to be delivered by the local device
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error != nil {
                print(error as Any)
                completion(false)
            } else {
                completion(true)
            }
        })
    }

}

























