//
//  AppDelegate.swift
//  CustomNotifications
//
//  Created by Timothy Clark on 12/30/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Library needed for user notifications
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //NOTE: Let the current copy of the User Notification Center know that this app will be handling events while it is in the foreground
        UNUserNotificationCenter.current().delegate = self
        //NOTE: Call function that sets up the custom notification view for use with this application
        configureUserNotifications()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //NOTE: Configuring the use of a Custom Notification Content Extension, in this case 'customContentExtension'
    private func configureUserNotifications() {
        
        //NOTE: Custom actions within notifications
        let favAction = UNNotificationAction(identifier: "alienAction", title: "👽 Alien Action", options: [])
        let dismissAction = UNNotificationAction(identifier: "dismissAction", title: "✖️ Dismiss Action", options: [])
        
        //NOTE: Set the NotificationCategory of the custom notification view that is to be used for all notifications from this app
        let category = UNNotificationCategory(identifier: "customContentExtension", actions: [favAction, dismissAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
    
    
}

//NOTE: Extending AppDelegate to implement the User Notification System Delegate to allow user notifications to be seen when while the application is active, by default notifications scheduled by an application that is still in the foreground are not displayed
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //NOTE: Whenever the user notification center invokes the 'willPresent' event, the application will display those notifications as an alert within the application
        completionHandler(.alert)
        
    }
    
    //NOTE: To handle custom actions, the ones added above, we need to handle receiving those actions from the source
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //NOTE: For custom actions, you would implement code here. for this demo, we will just be printing the name of the action to the console to see it happen.
        completionHandler()
    }
}













