//
//  NotificationViewController.swift
//  CustomContentExtension
//
//  Created by Timothy Clark on 12/30/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

//NOTE: To use this custom NotificationView, it has to be given a UNNotificationExtensionCategory in the info.plist file. This unique name is needed for the view to be called and used from within the app using it. In this instance I used 'customContentExtension'

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        
        //NOTE: Since we are overriding the default notification view, we need to handle attachments ourselves, these notifications live outside the sandbox of the application from which they originate
        //NOTE: Here we are pulling the first attachment from the stack
        guard let attachment = notification.request.content.attachments.first else {
            return
        }
        
        //NOTE: Because this is a custom notification view, we are positive the attachments is an image, so we treat it as that file type
        //NOTE: We have to use 'startAccessingSecurityScopedResource' because the attachment is coming from our app
        if attachment.url.startAccessingSecurityScopedResource() {
            
            let imageData = try? Data.init(contentsOf: attachment.url)
            if let image = imageData {
                imageView.image = UIImage(data: image)
            }
        }
        
    }
    
    //NOTE: Handling actions
    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        
        //NOTE: Identify which custom action was invoked by the user in the notification based on the action identifier that was assigned
        if response.actionIdentifier == "alienAction" {
            //NOTE: Custom code would be here to handle the Alien Action
            completion(.dismissAndForwardAction)
        } else if response.actionIdentifier == "dismissAction" {
            //NOTE: Custom code would be here to handle Dismiss Action
            completion(.dismissAndForwardAction)
        }
        
    }

}
