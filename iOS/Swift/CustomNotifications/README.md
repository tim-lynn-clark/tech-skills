# Custom User Notifications Tutorial App
## Udemy Course: iOS 10 & Swift 3: From Beginner to Paid Professional
## Featuring course content created by DevSlopes.com

This app was built while learning the following Swift technologies/practices:

* Using User Notifications API to send local notifications to users from within custom applications
* Using User Notification Time Interval Triggers to schedule notifications to happen once or at set intervals
* Adding attachments to notification
* Extending AppDelegate to implement the User Notification System Delegate to allow user notifications to be seen when while the application is active, by default notifications scheduled by an application that is still in the foreground are not displayed. See modifications made to the AppDelegate file, extension added to handle delegate events and current notification center instructed to use the AppDelegate as its event handler when in the foreground.
* Using file > new > target > Notification Content Extension to customize user notification content layout through the creation of a custom view
* Implementing custom actions with custom notification content extensions