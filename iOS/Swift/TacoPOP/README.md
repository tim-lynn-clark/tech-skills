# TacoPOP Tutorial App
## Udemy Course: iOS 10 & Swift 3: From Beginner to Paid Professional
## Featuring course content created by DevSlopes.com

This app was built while learning the following Swift technologies/practices:

* Using Protocols and Extensions to add enhancements to existing types. Example: Adding DropShadow Protocol and Extension to UIView and applying it to a custom UIView called HeaderView.
* Using Enumerations to simplify model data comparisons as well as model data selection within the UI
* Using the Delegate Pattern to provide notification once data has been loaded by the DataService singleton. Example: loading Taco data
* Using a UICollectionView to display data
* Using a Nib and custom UICollectionViewCell to display data in the UIcollectionView
* Using a Protocol and Extension to handle String Reuse Identifiers used within StoryBoards to identify reusable components. Example TacoCell, the reusable UICollectionViewCell
* Using a Protocol and Extension to handle reusable UICollectionViewCells dequeue operations in UICollectionViewDataSource delegate functions
* Using a Protocol and Extension to add animations to UIViews. Example: UICollectionViewCell shaking upon touch.
