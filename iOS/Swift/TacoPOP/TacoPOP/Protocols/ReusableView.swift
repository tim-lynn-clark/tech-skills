//
// Created by Timothy Clark on 12/27/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Protocol and Extension used to fix the constant use of String Identifiers within iOS views
//EXAMPLE: TacoCell - instead of using "TacoCell" as the String Reuse Identifier for a reusable
// collection cell, once this protocol is implemented by a class it will use the class name as the
// Reuse Identifier
protocol ReusableView: class {}

extension ReusableView where Self: UIView {

    static var reuseIdentifier: String {
        return String(describing: self)
    }

}