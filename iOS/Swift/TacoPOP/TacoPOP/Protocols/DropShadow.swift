//
//  DropShadow.swift
//  TacoPOP
//
//  Created by Timothy Clark on 12/26/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

protocol DropShadow {}

//NOTE: It is possible to constrain your Protocols and Extensions to a specific type
// for this Extension, we are constraining it to only UIView types
//NOTE: When dealing with Protocols and Extensions, you refer to 'self' using 'Self'
extension DropShadow where Self: UIView {
    
    func addDropShadow(){
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5
    }
    
}






