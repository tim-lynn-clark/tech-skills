//
// Created by Timothy Clark on 12/27/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Protocol and Extension used to handle loadable Nib Identifiers
//EXAMPLE: TacoCell
protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {

    static var nibName: String {
        return String(describing: self)
    }

}