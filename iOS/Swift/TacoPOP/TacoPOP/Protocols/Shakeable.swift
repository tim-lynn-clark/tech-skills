//
// Created by Timothy Clark on 12/27/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Adding a little spunk to the UIViews within the application by allowing them to shake a little bit
protocol Shakeable {}

extension Shakeable where Self: UIView {

    func shake(){
        let anim = CABasicAnimation(keyPath: "position")
        anim.duration = 0.05
        anim.repeatCount = 5
        anim.autoreverses = true
        anim.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4.0, y: self.center.y))
        anim.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4.0, y: self.center.y))
        layer.add(anim, forKey: "position")
    }

}