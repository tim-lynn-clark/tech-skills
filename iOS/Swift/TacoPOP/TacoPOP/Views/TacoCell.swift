//
//  TacoCell.swift
//  TacoPOP
//
//  Created by Timothy Clark on 12/26/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

class TacoCell: UICollectionViewCell, NibLoadableView, Shakeable {

    @IBOutlet weak var tacoImage: UIImageView!
    @IBOutlet weak var tacoLabel: UILabel!

    var taco: Taco!

    func configureCell(taco: Taco){
        self.taco = taco

        //NOTE: To pull the correct image, we are using the taco's proteinId and extracting the raw value from the enumerated type
        tacoImage.image = UIImage(named: taco.proteinId.rawValue)
        tacoLabel.text = taco.productName
    }

}
