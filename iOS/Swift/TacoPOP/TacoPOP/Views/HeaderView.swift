//
//  HeaderView.swift
//  TacoPOP
//
//  Created by Timothy Clark on 12/26/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Custom UIView that implements the DropShadow Protocol,
// which has an Extension that implements the addDropShadow function
// that adds a dropshadow to the default UIView implementation
class HeaderView: UIView, DropShadow {

}
