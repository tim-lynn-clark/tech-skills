//
// Created by Timothy Clark on 12/26/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import Foundation

struct Taco {

    //Private Properties
    private var _id: Int!
    private var _productName: String!
    private var _shellId: TacoShell! //Enumerated Type
    private var _proteinId: TacoProtein! //Enumerated Type
    private var _condimentId: TacoCondiment! //Enumerated Type

    //Accessors
    var id: Int {
        return _id
    }

    var productName: String {
        return _productName
    }

    var shellId: TacoShell {
        return _shellId
    }

    var proteinId: TacoProtein {
        return _proteinId
    }

    var condimentId: TacoCondiment {
        return _condimentId
    }

    //Initializer
    init(id: Int, productName: String, shellId: Int, proteinId: Int, condimentId: Int){
        _id = id
        _productName = productName

        // Identify Shell Type
        switch shellId {
            case 2:
                _shellId = TacoShell.Corn
            default:
                _shellId = TacoShell.Flour
        }

        // Identify Protein Type
        switch proteinId {
        case 2:
            self._proteinId = TacoProtein.Chicken
        case 3:
            self._proteinId = TacoProtein.Brisket
        case 4:
            self._proteinId = TacoProtein.Fish
        default:
            self._proteinId = TacoProtein.Beef
        }

        // Identify Condiment Type
        switch condimentId {
        case 2:
            _condimentId = TacoCondiment.Plain
        default:
            _condimentId = TacoCondiment.Loaded
        }
    }
}



