//
//  MainVC.swift
//  TacoPOP
//
//  Created by Timothy Clark on 12/26/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

class MainVC: UIViewController, DataServiceDelegate {
    
    //NOTE: Header in the Main Storyboard now utilized the custom UIView 'HeaderView'
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var collectionView: UICollectionView!

    var ds: DataService = DataService.instance

    override func viewDidLoad() {
        super.viewDidLoad()

        //NOTE: Custom Delegate implemented on DataSource to notify this ViewController when the data has been loaded
        ds.delegate = self
        ds.loadDeliciousTacoData()
        ds.tacoArray.shuffle()

        //NOTE: With the use of the custom HeaderView UIView, the addDropShadow
        // function of the DropShadow Extension of the DropShadow Protocol can be executed.
        headerView.addDropShadow()

        collectionView.delegate = self
        collectionView.dataSource = self

        //NOTE: Loading Nib using String Reuse Identifier "TacoCell", WITHOUT NibLoadableView and ReusableView Protocols used
        //let nib = UINib(nibName: "TacoCell", bundle: nil)
        //collectionView.register(nib, forCellWithReuseIdentifier: "TacoCell")

        //NOTE: Loading Nib using NibLoadableView and ReusableView Protocols implemented on the TacoCell custom UICollectionViewCell
        collectionView.register(TacoCell.self)
    }

    func deliciousTacoDataloaded() {
        print("Delicious Taco Data Has Loaded!")
        //NOTE: If using non-local data such as Firebase or a WebAPI you would want to have the CollectionView reload the data. This is not necessary since all data is local in this example app
        //NOTE: You may want to also load a spinner above, when the ds.loadDeliciousTacoData func is called and then stop the spinner in this function when the Async call has completed
        //collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//NOTE: Utilizing an Extension to separate out Protocol function implementations can be useful for code readability

extension MainVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        //NOTE: Using the Shakeable Protocol and Extension function to shake TacoCells upon touch event
        if let cell = collectionView.cellForItem(at: indexPath) as? TacoCell {
            cell.shake()
        }
    }
}

extension MainVC: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return ds.tacoArray.count

    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        //NOTE: Old way to dequeue reusable cells, WITHOUT NibLoadableView and ReusableView Protocols used
        //if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TacoCell", for: indexPath) as? TacoCell {
            //cell.configureCell(taco: ds.tacoArray[indexPath.row])
            //return cell
        //}
        //return UICollectionViewCell()

        //NOTE: Dequeue reusable cells using NibLoadableView and ReusableView Protocols implemented on the TacoCell custom UICollectionViewCell
        let cell = collectionView.customDequeueReusableCell(forIndexPath: indexPath) as TacoCell
        cell.configureCell(taco: ds.tacoArray[indexPath.row])
        return cell
    }
}

extension MainVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 95)
    }
}
