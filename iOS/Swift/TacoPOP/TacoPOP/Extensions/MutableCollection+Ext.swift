//
// Created by Timothy Clark on 12/27/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import Foundation

//NOTE: Modifying the default behaviors of the built-in MutableCollection type, restricting the modifications to only MutableCollections that have Integer-based Indexing
extension MutableCollection where Index == Int {


    //NOTE: New function allowing the random shuffling of items held within the array
    mutating func shuffle() {

        //if array contains 1 or 0 items, simple return
        if count < 2 { return }

        //Begin shuffling through the array
        for i in startIndex ..< endIndex - 1 {
            //Get a random index within the bounds of the array
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            //check to see if i and j are the same index, if so continue without shuffling
            guard i != j else { continue }
            //swap the values found at index i and j
            self.swapAt(i, j)
        }
    }

}
