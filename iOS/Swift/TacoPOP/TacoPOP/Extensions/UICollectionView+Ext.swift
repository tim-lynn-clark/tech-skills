//
// Created by Timothy Clark on 12/27/17.
// Copyright (c) 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Extending the default abilities of the built-in UICollectionViews within this application
// Utilizes the ReusableView and NibLoadableView Protocols that handle String Reuse Identifiers
extension UICollectionView {

    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView{

        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func customDequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {

        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}

extension UICollectionViewCell: ReusableView {}
