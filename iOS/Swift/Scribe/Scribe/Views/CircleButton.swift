//
//  CircleButton.swift
//  Scribe
//
//  Created by Timothy Clark on 12/28/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit


@IBDesignable
class CircleButton: UIButton {

    //NOTE: Allows the addition of Custom Propertie Fields to the Properties Inspector of Interface Builder
    // This allows the values of Custom Properties to be set via Interface Builder
    @IBInspectable var cornerRadius: CGFloat = 30.0 {
        //NOTE: didSet must be called, why?
        didSet {
            setupView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView(){
        layer.cornerRadius = cornerRadius
    }

}
