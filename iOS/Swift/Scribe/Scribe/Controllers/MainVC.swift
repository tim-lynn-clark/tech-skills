//
//  MainVC.swift
//  Scribe
//
//  Created by Timothy Clark on 12/28/17.
//  Copyright © 2017 ViolentAtom. All rights reserved.
//

import UIKit

//NOTE: Libraries needed for speech recognition as well as accessing and playing audio files
import Speech
import AVFoundation

class MainVC: UIViewController {
    
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var transcriptionTextField: UITextView!
    
    var audioPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activitySpinner.isHidden = true
    }

    @IBAction func transcribePressed(_ sender: Any) {
        activitySpinner.isHidden = false
        activitySpinner.startAnimating()
        requestSpeechAuth()
    }
    
    func requestSpeechAuth(){
        
        //NOTE: You must first ask for authorization to use device audio, if not authorized the block will not run
        SFSpeechRecognizer.requestAuthorization { authStatus in
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized {
                
                //NOTE: Pulling the path to a bundled resource, in this case the test audio file
                if let path = Bundle.main.url(forResource: "test", withExtension: "m4a") {
                    
                    //NOTE: Accessing a bundled resource via IO should be wrapped in a do/catch block
                    do {
                        //NOTE: Reading contents of the path using built-in audio player capability
                        let sound = try AVAudioPlayer(contentsOf: path)
                        //NOTE: Make this ViewController the event handler for the audio player
                        self.audioPlayer = sound
                        self.audioPlayer.delegate = self
                        sound.play()
                    } catch {
                        print("Audio could not be played")
                    }
                    
                    //NOTE: Begin speech recognition by instantiating a SpeechRecognizer
                    let recognizer = SFSpeechRecognizer()
                    //NOTE: Setup a recognition request using the path to the audio file
                    let request = SFSpeechURLRecognitionRequest(url: path)
                    //NOTE: Setup an async recognition task to work through the audio file and convert speech to text
                    recognizer?.recognitionTask(with: request) { (result, error) in
                        if let error = error {
                            print("There was an erro: \(error)")
                        } else {
                            self.transcriptionTextField.text = result?.bestTranscription.formattedString
                        }
                    }
                }
            }
        }
        
    }
    
}

extension MainVC: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        player.stop()
        activitySpinner.stopAnimating()
        activitySpinner.isHidden = true
    }
    
}


















