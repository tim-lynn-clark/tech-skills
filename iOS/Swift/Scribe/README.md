# Scribe Tutorial App
## Udemy Course: iOS 10 & Swift 3: From Beginner to Paid Professional
## Featuring course content created by DevSlopes.com

This app was built while learning the following Swift technologies/practices:

* Using iOS Speech Recognition API
* Adding NSSpeechRecognitionUsageDescription to Info.plist to provide the user with a description as to why your application needs access to audio
* Subclassing UIButton to create a custom CircleButton
* Using @IBInspectable to add a new Corner Radius attribute to the Properties Inspector of the Interface Builder for all CircleButton UIButton subclasses
* Setting UIButton in Interface Builder to a custom UIButton subclass, CircleButton, to see the new Corner Radius attribute added to the Properties Inspector of Interface Builder
* Using UIActivityIndicatorView to indicate activity as well as cover and inactivate a button while activity is happening
* Using built-in libraries 'Speech' and 'AVFoundation' for speech recognition as well as accessing and playing audio files
* Implementing the AVAudioPlayerDelegate to be notified when audio playing has terminated