# iOS Mobile Development Portfolio

This is primarily a set of mobile applications built while updating my iOS development skills from Objective-C to Swift (3,4). The apps provided are either the result of completing various on-line tutorials, Udemy courses, or tutorials found in technical books. Credit to all tutorial authors is due.

The repository also contains projects completed while learning Sketch for Mobile UI design as well as PaintCode for the creation of dynamic Swift UI components.

Some professional iOS projects I have worked on are proprietary with NDAs attached, this restricts my ability to retain or display the source code and/or identify some of these projects. Those projects I can identify/describe are listed below.

## Professional iOS Projects

### Life Enhancement Assistant, , 2015-Current

Engineered, designed, and implemented a health coaching and assessment system that utilizes a JSON API back-end running on Rails API backed by a PostgreSQL database. This core system provides services for the following client application:


**Mobile-First Responsive Web Interface in React.js**


**Wellness Client in iOS**


**Wellness Coach in iOS**


**Wellness Program Administrator in iOS**



### Mobile Quiz Platform, CourseSaver LLC, 2014-2015

Engineered, designed, and implemented an iOS quiz application in Objective­‐C that allows CourseSaver.com students to take quizzes in preparation for taking their DAT, PCAT, MCAT, NBDE, and OAT test while in dental school. The application utilized a PHP back-end web service for pulling quiz questions as well as recording quiz scores and providing analytics to the user.

### Mobile Image Chain of Custody, Evident Solutions Inc., 2013-2015

Engineered, designed, and implemented an iOS image capture application that integrated with a central JSON web service as well as Amazon’s S3 for image storage. The application targets the public safety sector, specifically police departments. Due to NDA additional details cannot be disclosed.