//
//  ViewController.swift
//  devslopeprogress
//
//  Created by Timothy Clark on 1/27/18.
//  Copyright © 2018 ViolentAtom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var progressBarView: ProgressBarView!
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func sliderMoved(_ sender: Any) {
        
        progressBarView.progress = CGFloat(slider.value)
        
    }
    
}

