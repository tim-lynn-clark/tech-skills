//
//  ProgressBarView.swift
//  devslopeprogress
//
//  Created by Timothy Clark on 1/27/18.
//  Copyright © 2018 ViolentAtom. All rights reserved.
//

import UIKit

class ProgressBarView: UIView {
    
    private var _innerProgress:CGFloat = 0.0;
    
    var progress:CGFloat {
        set(newProgress){
            if newProgress > 1.0 {
                _innerProgress = 1
            } else if newProgress < 0.0 {
                _innerProgress = 0
            } else {
                _innerProgress = newProgress
            }
            //NOTE: Must be called to get the view to redraw
            setNeedsDisplay()
        }
        get {
            return _innerProgress * bounds.width
        }
    }

    //NOTE: draw function of the UIView has to be overridden in order to draw the custm PaintCode component.
    override func draw(_ rect: CGRect) {
        
        //NOTE: call to draw the custom PaintCode progress bar component.
        ProgressBarDraw.drawProgressBar(frame: bounds, progress: progress)
    }

}
