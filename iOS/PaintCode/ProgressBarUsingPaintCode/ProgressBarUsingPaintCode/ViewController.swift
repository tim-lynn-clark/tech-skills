//
//  ViewController.swift
//  ProgressBarUsingPaintCode
//
//  Created by Timothy Clark on 8/29/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var progressBarView: ProgressBarView!
    @IBOutlet weak var slider: UISlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func sliderMoved(_ sender: Any) {
        progressBarView.progress = CGFloat(slider.value)
    }
}

