//
//  ProgressBarView.swift
//  ProgressBarUsingPaintCode
//
//  Created by Timothy Clark on 8/29/17.
//  Copyright © 2017 Timothy Clark. All rights reserved.
//

import UIKit

class ProgressBarView: UIView {
    
    private var _innerProgress: CGFloat = 0.0
    
    
    var progress: CGFloat {
        get {
            return _innerProgress * bounds.width
        }
        
        set (newProgress) {
            if newProgress > 1.0 {
            _innerProgress = 1.0
            } else if newProgress < 0.0 {
                _innerProgress = 0.0
            } else {
                _innerProgress = newProgress
            }
            
            /* TIMS-NOTES: Forces the view to refresh after update */
            setNeedsDisplay()
        }
    }

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        ProgressBarDraw.drawProgressBar(frame: bounds, progress: progress)
    }
    

}
