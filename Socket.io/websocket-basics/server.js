const http = require('http');
const websocket = require('ws');

const server = http.createServer((req, res) => {
    res.end(">> Web Server: Handling standard HTTP connections");
})

// NOTE: A websocket server observes an HTTP server for websocket-specific connections
const wss = new websocket.Server({server});

wss.on('headers', (headers, req) => {
    console.log(">> Websocket Server:", headers);
})

wss.on('connection', (socket, req) => {
    console.log(socket);
    socket.send(">> Websocket Server: Welcome!", (event) => {
        console.log(">> Websocket Server: Message sent over socket.");
    });
    socket.onmessage = (msg) => {
        console.log(">> Websocket Server: received message: ", msg.data);
    }
})

server.listen(8888);