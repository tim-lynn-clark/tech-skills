const express = require('express');
const socketIO = require('socket.io');

const app = express();

app.use(express.static(__dirname + '/public'));

const server = app.listen(9000);
const ioServer = socketIO(server);

ioServer.on('connection', (socket) => {
    socket.emit('messageFromServer', {data: ">> IO Server: Welcome to chat."});
    socket.on('messageToServer', data => {
        console.log(data);
    })
});

