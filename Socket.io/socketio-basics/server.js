const http = require('http');
const socketio = require('socket.io');

const server = http.createServer((req, res) => {
    res.end(">> Web Server: Handling standard HTTP connections");
})

const io = socketio(server, {
    cors: {
        origin: '*',
    },
});
io.on('connection', (socket, req) => {
    console.log(">> Socket.io Server: Connection with client established.");
    // NOTE: Socket.io allows for custom event types using Emit (pub/sub design)
    socket.emit("welcome", ">> Socket.io Server: Welcome!", (event) => {
        console.log(">> Socket.io Server: Message sent over socket.");
    });
    socket.on('message', (msg) => {
        console.log(">> Socket.io Server: received message: ", msg.data);
    });
})

server.listen(8000);

