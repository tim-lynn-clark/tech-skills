
let socket = io('ws://localhost:8000');
console.log(socket);

socket.on('welcome', (message) => {
    console.log(message);
});

socket.emit('message', { data: ">>> Client: Thank you."});

socket.onmessage = (event) => {
    console.log(event);
};
