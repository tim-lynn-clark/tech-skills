# Custom React Webpack Configuration
## Learning Webpack

# Steps

1. Add NPM to the project folder (yarn can also be used as the package manager)

    `npm init`
   

2. Add Webpack and the Webpack Development Server as development dependencies

- `webpack` as our build manager
- `webpack-cli` to allow the webpack-dev-server to be run by the NPM start script
- `webpack-dev-server` allows the project to be run locally in a web server for development purposes


    `npm install --save-dev webpack webpack-cli webpack-dev-server`


3. Add React dependencies

- `react` so we can build single page web application using the React library
- `react-dom` so the React SPA can be injected into index.html
- `react-router` and `react-router-dom` so the SAP can simulate having multiple pages


   `npm install --save react react-dom react-router react-router-dom `


4.  Add a `webpack.config.js` file to the root of the application folder

5. Do basic config setup. See webpack.config.js for notes on the following:

- mode
- entry
- output
- devtool

6. Configuration to handle ES6 transpilation to ES5 will use Babel. Install the following Babel packages:

- `@babel/core` Babel JavaScript transpiler 
- `@babel/preset-env` 
- `@babel/preset-react` Allows the transpiling of JSX to JavaScript
- `@babel/preset-stage-2`
- `babel-loader` The load used to process JavaScript files
- `@babel/plugin-proposal-class-properties` 


   `npm install --save-dev @babel/core @babel/preset-env @babel/preset-react @babel/preset-stage-2 babel-loader @babel/plugin-proposal-class-properties`
   

7. Add a `module` section with `rules` that define what should be done with files as they are run into during the build process. This is done based on the file extension.

8. Setup a `rule` for handling `.js` files where those files are passed to Babel

9. Create a `.babelrc` configuration file, which holds configuration information in JSON format used by Babel

- presets: is an array that holds a list of all presents Babel should use when processing files
   - "@babel/preset-env"   
      - targets
         - browsers: specify what browsers you would like Babel to target in its transpilation of your JavaScript
   - "@babel/preset-react" no configuration, it does everything itself, just need to add it.
- plugins: allow you to add extra behavior to Babel
   - "@babel/plugin-proposal-class-properties" Helps with syntax control when parsing JavaScript?
   
10. Install NPM packages that help support the compilation of CSS during the build process.

- `style-loader`
- `css-loader`
- [`postcss-loader`](https://www.npmjs.com/package/postcss-loader)
- `autoprefixer`


   `npm install --save-dev style-loader css-loader postcss-loader autoprefixer`
   

11. Configure Webpack to use the CSS packages to handle CSS files when they are loaded. This is done by adding another `rule`.

12. Add `"browserslist": "> 1%, last 2 versions",` to `package.json` so that the autoprefixer has access to it.

13. Add support for imported images in JavaScript files.

- [`url-loader`](https://webpack.js.org/loaders/url-loader/) Assists in loading assets like images that were imported into React component JavaScript files.
- `file-loader`


    `npm install --save-dev url-loader file-loader`


14. Configure the `url-loader` in the webpack config as another `rule`

15. Add support for injecting the bundled JavaScript, CSS, and images into `index.html`.

- `html-webpack-plugin`


    `npm install --save-dev html-webpack-plugin`

16. Configure `html-webpack-plugin` in webpack in the plugins section. 