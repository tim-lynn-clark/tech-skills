/* NOTE: Webpack is a Node.js command-line build tool so it uses the Node.js module system
* unlike the module system used by React. */

/* NOTE: Webpack will use Node.js packages to work through files */
const path = require('path');

/* NOTE: Importing HtmlWebpackPlugin so the generated bundles can be inserted into index.html after compilation. */
const HtmlWebpackPlugin = require('html-webpack-plugin');

/* NOTE: This is the configuration object that will be used by Webpack when it
* is run and tries to build the React SAP application.
* To see all config options visit: https://webpack.js.org/*/
module.exports = {
    /* NOTE: Tells Webpack which Node environment this config is targeting (separate config for production) */
    mode: "development",
    /* NOTE: Webpack needs a single file as its entry point into the application.
    * it will use this as it starting point for identifying, resolving, consolidating, and including
    * dependencies referenced in each file referenced by the entry file. */
    entry: "./src/index.js",
    /* NOTE: Webpack needs a location to store all of the consolidated code it
    * combines into bundles. */
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.js",
        publicPath: ""
    },
    /* NOTE: devtool allows you to manage how source maps are created for your
    * bundled JavaScript. Source maps allow your minified and uglified code to be
    * easily understood by debugging tools. */
    devtool: 'eval-cheap-module-source-map',
    module: {
        /* NOTE: Tells Webpack what to do with specific file types. */
        rules: [
            /* NOTE: In this example
            * we are telling it to look for .js files and use Babel to manage those files during
            * the build process.*/
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    { loader: "style-loader" },
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: "[name]__[local]__[hash:base64:5]"
                            }
                        }
                    },
                    {
                        loader: "postcss-loader",
                    },
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: "url-loader",
                options: {
                    limit: 8192
                }
            }

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/src/index.html",
            filename: "index.html",
            inject: 'body'
        })
    ]
};