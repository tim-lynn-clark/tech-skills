import React from 'react';

import classes from './PizzaImage.css';
import pizzaImage from '../../assets/pizza.jpg';

function PizzaImage(props) {

    return (
        <div className={classes.PizzaImage}>
            <img src={pizzaImage} />
        </div>
    );
}

export default PizzaImage;