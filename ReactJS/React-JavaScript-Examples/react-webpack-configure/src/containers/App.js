import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';

import Users from './Users';
import asyncComponent from '../hoc/asyncComponent';

/* NOTE: Demonstrating the loading of components asynchronously in order to make
* sure the Webpack config handles it. */
const AsyncPizza = asyncComponent(() => {
    return import('./Pizza');
})

class App extends Component {

    render() {
        return (
            <div>
                <div>
                    <Link to="/"> Users</Link>
                    <Link to="/pizza">Pizza</Link>
                </div>
                <div>
                    <Route path="/" exact component={Users} />
                    <Route path="/pizza" exact component={Pizza} />
                </div>
            </div>
        );
    }
}

export default App;