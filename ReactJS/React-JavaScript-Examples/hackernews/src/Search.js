import React from 'react';

// NOTE: evolution of a search component
//--------------------------------------


// Note: Stateless Functional Component + ES6 Arrow Function + Destructured Props
const Search = ({ value, onChange, children }) =>
    <form>
        {children}
        <input
            type="text"
            value={value}
            onChange={onChange} />
    </form>;

export default Search;

// Note: Stateless Functional Component + Destructured Props
// Using ES6 destructuring of props
// function Search({ value, onChange, children }) {
//   return (
//       <form>
//         {children}
//         <input
//             type="text"
//             value={value}
//             onChange={onChange}
//         />
//       </form>
//   );
// }

// Note: Stateless Functional Component + Props
// function Search(props) {
//
//   const { value, onChange, children } = props;
//   return (
//       <form>
//         {children}
//         <input
//           type="text"
//           value={value}
//           onChange={onChange}
//         />
//       </form>
//   );
// }

// Note: Stateful ES6 Class Component
// class Search extends Component {
//
//   render() {
//     const { value, onChange, children } = this.props;
//     return (
//         <form>
//           {children}
//           <input
//               type="text"
//               value={value}
//               onChange={onChange} />
//         </form>
//     );
//   }
// }
