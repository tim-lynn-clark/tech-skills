import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


// Note: Allows React to hot reload its components instead of
// completely refreshing the entire browser window
// super helpful when developing when you are working on
// or debugging complete state-based components
// if (module.hot) { module.hot.accept(); }
