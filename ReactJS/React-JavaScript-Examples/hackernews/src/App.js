import React, { Component } from 'react';
import './App.css';
import Search from './Search';
import Table  from './Table';

const DEFAULT_QUERY = 'redux';
const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';

// ES5: Higher-Order Functions
// ---------------------------
// function isSearched(searchTerm) {
//   return function (item) {
//     return item.title.toLowerCase().includes(searchTerm.toLowerCase());
//   }
// }

// ES6: Higher-Order Functions using arrow functions
// // ---------------------------
const isSearched = searchTerm => item => item.title.toLowerCase().includes(searchTerm.toLowerCase());

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      result: null,
      searchTerm: DEFAULT_QUERY
    }

    /* 'this' binding of a Class method, required if you use normal functions
    * as Class methods for event handling. It is not required if you use
    * arrow function syntax and allow ES6 class binding to handle 'this'
    * binding for you. */
    // this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount() {
    const { searchTerm } = this.state;

    fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}`)
        .then(response => response.json())
        .then(result => this.setSearchTopStories(result))
        .catch(error => error);
  }

  setSearchTopStories = result => {
    this.setState({ result });
  };

  onDismiss = (id) => {
    const isNotId = item => item.objectID !== id;
    const updatedHits = this.state.result.hits.filter(isNotId);

    //ES6 Merging modified items back into original object property
    // this.setState({
    //   result: Object.assign({}, this.state.result, { hits: updatedHits })
    // });

    //ES6 Using the spread operator to merge modified list back into a new object
    // first merging in all other original properties from the source object in the new object
    this.setState({
      result: { ...this.state.result, hits: updatedHits }
    });
  };

  onSearchChange = (event) => {
    this.setState({ searchTerm: event.target.value });
  };

  render() {

    console.log(this.state);

    // Destructuring the local state for ease of access in render method
    const { searchTerm, result } = this.state;

    if(!result) { return null; }

    return (
        <div className="page">
          <div className="interactions">
            <Search
                value={searchTerm}
                onChange={this.onSearchChange} >
              Search
            </Search>
          </div>
          {
            // result ?
            //   <Table
            //       isSearched={isSearched}
            //       list={result.hits}
            //       pattern={searchTerm}
            //       onDismiss={this.onDismiss}
            //   />
            //   : null

              result &&
              <Table
                  isSearched={isSearched}
                  list={result.hits}
                  pattern={searchTerm}
                  onDismiss={this.onDismiss}
              />
          }

        </div>
    );
  }
}

export default App;
