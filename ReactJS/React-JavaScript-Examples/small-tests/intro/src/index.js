import React from 'react';
import ReactDOM from 'react-dom';
import Person from './modules/person';

const title = 'Intro to React';

let app = (
    <div>
      <Person name="Bob" age="20"/>
      <Person name="Sally" age="25"/>
    </div>
);

ReactDOM.render(
  app,
  document.getElementById('app')
);

module.hot.accept();
