import React, {Component} from 'react';
//NOTE: Removing global import of CSS
//import './App.css';
// NOTE: Importing classes from CSS using scoped CSS module import
import classes from './App.css';
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";
import Cockpit from '../../components/Cockpit/Cockpit';
import PersonList from '../../components/Persons/PersonList';
import StackedWrapperExample from '../../components/StackedWrapperExample/StackedWrapperExample';
import FragmentExample from '../../components/FragmentExample/FragmentExample';
import WithClassHOComponent from '../../hoc/WithClass/WithClassComponent';
import _ from 'lodash';
/* NOTE: Importing a React context object that will be made available to all
* application components at the point at which it is introduced within the
* component hierarchy. */
import AuthContext from '../../contexts/authContext';

class App extends Component {

    /* NOTE: Lifecycle methods - Not usually used*/
    constructor(props) {
        /* NOTE: If using the constructor for component setup (rarely used)
        * You must make sure you call super and pass props to is. */
        super(props);

        console.log('[App.js] Lifecycle Event 1: Constructor Called...');

        /* NOTE: The old method for setting up state in a stateful component
        * was done in the construct. With new JS syntax, it can be setup as done
        * below with the `state = {}` syntax */
        // this.state = {
        //     persons: [
        //         { id: '123', name: 'Freddy', age: 25 },
        //         { id: '234', name: 'Sally', age: 32 },
        //         { id: '345', name: 'Sue', age: 21 },
        //     ],
        //     showPersons: false
        // }
    }

    /* NOTE: Lifecycle methods - Not usually used*/
    static getDerivedStateFromProps(props, state) {
        /* NOTE: Can be used to build a stateful component's state from the
        * props array passed into it at initialization. */
        console.log('[App.js] Lifecycle Event 2: getDerivedStateFromProps Called...');

        /* NOTE: Since this methods purpose is to setup state based on props,
        * it is important that state is returned from this method after manipulation. */
        return state;
    }

    /* NOTE: Lifecycle methods - Not usually used*/
    componentWillUnmount() {
        /* NOTE: Can be used to clean up a component prior to if being removed from the DOM.  */
        console.log('[App.js] Lifecycle Event (Final): componentWillUnmount Called...');
    }

    /* NOTE: Lifecycle methods - Commonly used*/
    componentDidMount() {
        /* NOTE: Executes after the component output has been rendered to the DOM.
        * Common method used to make calls to external asynchronous APIs.
        * Once the async data has been returned, the component's HTML can be modified for displaying that data.
        * Could be used to kick of a timer that will be used within the component.   */
        console.log('[App.js] Lifecycle Event 4: componentDidMount Called...');
    }

    /* NOTE: Lifecycle methods - Not usually used*/
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        /* NOTE: Used to determine if the component should actually rerender the HTML.
        * May be used by the component developer to make performance improvements to a component
        * by taking control over how frequently a component will rerender to the screen.
        * This should only be used if the developer actually understands how React
        * determines if a component should rerender and they want to cancel those automatic rerenderings
        * based on some factors React is unaware of. */
        console.log('[App.js] Lifecycle Event (As needed): shouldComponentUpdate Called...');

        /* NOTE: Because this method provides access to the proposed changed props and state that are
        * initiating the component update, it is a good place to compare the current props vs. the altered props
        * and the current state vs. the altered state, to determine if a rerender meets your special conditions
        * that React is unaware of. */

        /* NOTE: Since this method determines if a rerender should take place, it must return true or false */
        return true;


        /* NOTE: If you find that you are checking to see if there are changes on each and every
        * attribute of your props or state inside your shouldComponentUpdate method, there is a
        * very good chance you should be extending your component from PureComponent rather than Component.
        * React provides the PureComponent and it already implements shouldComponentUpdate functionality
        * that checks all of the attributes of your props to determine if any significant changes
        * have occurred before updating and rerendering your component. */
    }

    /* NOTE: Lifecycle methods - Not usually used*/
    getSnapshotBeforeUpdate(prevProps, prevState) {
        /* NOTE: Can be used to get a snapshot of the props and state prior to an update to the component
        * and rerendering. This can be useful if the component is performing a dangerous action and the
        * developer would like to have the ability to reset or undo the update based on some factor. */
        console.log('[App.js] Lifecycle Event (Fired after render and before componentDidUpdate): getSnapshotBeforeUpdate Called...');

        /* NOTE: Must return either a snapshot value or null. */
        return {
            message: 'This is an example snapshot holding data. It will be passed to the componentDidUpdate method.'
        };
    }

    /* NOTE: Lifecycle methods - Not usually used*/
    componentDidUpdate(prevProps, prevState, snapshot) {
        /* NOTE: Used in conjunction with getSnapshotBeforeUpdate. Is called when the component
        * completes an update  */
        console.log('[App.js] Lifecycle Event (Fired after getSnapshotBeforeUpdate): componentDidUpdate Called...');
        console.log('SNAPSHOT', snapshot);
    }

    // NOTE: Modern way of setting state without needing a constructor function
    state = {
        persons: [
            { id: '123', name: 'Freddy', age: 25 },
            { id: '234', name: 'Sally', age: 32 },
            { id: '345', name: 'Sue', age: 21 },
        ],
        showPersons: false,
        showCockpit: true,
        nameChangeCounter: 0,
        authenticated: false,
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    toggleCockpit = () => {
        this.setState({
            showCockpit: !this.state.showCockpit
        })
    }

    loginHandler = () => {
        this.setState({
            authenticated: !this.state.authenticated
        })
    }

    deletePersonHandler = (index) => {
        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons.splice(index, 1);
        this.setState({
            persons: alteredPersons
        })
    }

    nameChangedHandler = (event, id) => {
        let newName = event.target.value;

        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        const person = alteredPersons.find(person => {
            return person.id === id;
        });
        person.name = newName;

        /* NOTE: Set state does not guarantee the state will be updated immediately.
        * It queues for an update that may happen some time in the future. Adding anything like
        * a counter that should be accurate to the state and using a general setState call will
        * likely end in that counter being incorrect over time. */
        this.setState({
            persons: alteredPersons,
        });

        /* NOTE: if you need to guarantee state does not get stale between calls when
        * you have a super active component, you will need to use the alternate form of
        * setState that will guarantee the state stays synchronized. */
        this.setState((prevState, props) => {
           return {
               nameChangeCounter: prevState.nameChangeCounter + 1
           }
        });

    }

    renderPersonsList = () => {
        let personsList = null;
        if(this.state.showPersons) {
            personsList = <PersonList
                persons={this.state.persons}
                click={this.deletePersonHandler}
                changed={this.nameChangedHandler}
            />
        }
        return personsList;
    }

    renderCockpit = () => {
        let cockpit = null;
        if(this.state.showCockpit) {
            cockpit = <Cockpit
                recordCount={this.state.persons.length}
                listVisible={this.state.showPersons}
                click={this.togglePersonsHandler}
            />
        }
        return cockpit;
    }

    /* NOTE: Lifecycle methods - Always used*/
    render() {
        console.log('[App.js] Lifecycle Event 3: render Called...');

        /* NOTE: HOC example - Using the WithClass higher-order component to wrap the JSX of this component with
        * a div that has specified CSS classes added to it. */

        /* NOTE: AuthContext.Provider is used to wrap both the Cockpit and PersonsList components.
        * By doing this, both of those components will be given access to the AuthContext, which is a
        * React Context object.
        *
        * The context provider takes a value that sets any attributes of the
        * context object you would like to have access to in the child components
        * that are wrapped in the context.  */
        return (
            <ErrorBoundary>
                <WithClassHOComponent classes={classes.App}>
                    <button
                        onClick={this.toggleCockpit}
                    >Toggle Cockpit</button>
                    <AuthContext.Provider value={{
                        authenticated: this.state.authenticated,
                        login: this.loginHandler
                    }}>
                        { this.renderCockpit() }
                        { this.renderPersonsList() }
                    </AuthContext.Provider>
                    <StackedWrapperExample />
                    <FragmentExample />
                </WithClassHOComponent>
            </ErrorBoundary>
        );
    }
}

export default App;
