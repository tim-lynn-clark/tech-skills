import React, {useState} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

const App = props => {

    /* NOTE: Using a large state object as in class-based components should be avoided.
    * State is no longer merged for you with the useState React Hook. */
    const [state, setState] = useState({
        persons: [
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ],
        otherState: 'Some other value'
    })

    const switchNameHandler = () => {
        let alteredState = _.cloneDeep(state);
        alteredState.persons[0].name = "Frederick Heickel";

        /* NOTE: Downside to Stateful Functional Components */
        /* NOTE: unlike class-based components, when using the state hook,
            there is no merging of the new and old state done for you. The state object
            is completely replaced each time, you are then responsible to provide the
            entire state of the component each and every time state is set */
        setState(alteredState)
    }

    return (
        <div className="App">
            <button onClick={switchNameHandler}>Switch Name</button>
            <Person name={state.persons[0].name} age={state.persons[0].age} />
            <Person name={state.persons[1].name} age={state.persons[1].age} />
            <Person name={state.persons[2].name} age={state.persons[2].age}>
                My Hobbies: Camping
            </Person>
        </div>
    );
}

export default App;
