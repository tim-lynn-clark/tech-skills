import React, {Component} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

class App extends Component {

    state = {
        persons: [
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ],
        showPersons: false
    }

    switchNameHandler = () => {
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[0].name = "Frederick Heickel";

        this.setState({
            persons: alteredPersons,
        })
    }

    nameChangedHandler = (event) => {
        let newName = event.target.value;

        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[1].name = newName;

        this.setState({
            persons: alteredPersons,
        })
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    render() {

        /* NOTE: JSX Inline styling. As with HTML, avoid inline styles and use CSS imports. */
        const style = {
            backgroundColor: 'white',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px'
        }

        return (
            // NOTE: using a ternary expression to conditionally render the personList list
            <div className="App">
                <button
                    style={style}
                    onClick={this.togglePersonsHandler}>
                    Show People
                </button>
                { this.state.showPersons ?
                  <div>
                    <Person name={this.state.persons[0].name} age={this.state.persons[0].age}/>
                    <Person
                        name={this.state.persons[1].name}
                        age={this.state.persons[1].age}
                        changed={this.nameChangedHandler}
                    />
                    <Person name={this.state.persons[2].name} age={this.state.persons[2].age}>
                        My Hobbies: Camping
                    </Person>
                </div> : null }
            </div>
        );
    }
}

export default App;
