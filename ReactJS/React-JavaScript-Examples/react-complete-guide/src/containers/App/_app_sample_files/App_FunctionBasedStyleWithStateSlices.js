import React, {useState} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

const App = props => {

    /* NOTE: Using a large state object as in class-based components should be avoided.
    * State is no longer merged for you with the useState React Hook. */

    /* Note: It is now preferred that we use small state 'slices' to manage complex state
    * within a stateful functional component that uses the useState React Hook.
    *
    * This can be done by calling useState multiple times to obtain state objects and setState
    * update functions for each piece of a complex component state.
    */
    const [personsState, setPersonsState] = useState([
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ]);

    /* NOTE: Because we are no longer using a large state object, these state slices can now be independent JavaScript types
    * as simple as a string. */
    const [otherState, setOtherState] = useState('My Hobbies: Camping');

    const switchNameHandler = () => {
        let alteredState = _.cloneDeep(personsState);
        alteredState[0].name = "Frederick Heickel";

        /* NOTE: Downside to Stateful Functional Components */
        /* NOTE: unlike class-based components, when using the state hook,
            there is no merging of the new and old state done for you. The state object
            is completely replaced each time, you are then responsible to provide the
            entire state of the component each and every time state is set */
        setPersonsState(alteredState)
    }

    const useOtherStateHandler = () => {
        /* NOTE: You can see that the personsState object is not affected by a change to otherState */
        setOtherState("My hobby has changed to dancing...")
    }

    return (
        <div className="App">
            <button onClick={switchNameHandler}>Switch Name</button>
            <Person name={personsState[0].name} age={personsState[0].age} />
            <Person name={personsState[1].name} age={personsState[1].age} />
            <Person name={personsState[2].name} age={personsState[2].age}>
                {otherState}
            </Person>
            <button onClick={useOtherStateHandler}>Switch Name</button>
        </div>
    );
}

export default App;
