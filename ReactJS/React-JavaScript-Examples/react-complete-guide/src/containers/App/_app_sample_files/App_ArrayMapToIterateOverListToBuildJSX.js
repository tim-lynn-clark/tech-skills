import React, {Component} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

class App extends Component {

    state = {
        persons: [
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ],
        showPersons: false
    }

    switchNameHandler = () => {
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[0].name = "Frederick Heickel";

        this.setState({
            persons: alteredPersons,
        })
    }

    nameChangedHandler = (event) => {
        let newName = event.target.value;

        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[1].name = newName;

        this.setState({
            persons: alteredPersons,
        })
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    renderPersonsList = () => {
        let personsList = null;
        if(this.state.showPersons) {
            personsList = (
                <div>
                    { this.state.persons.map(person => {
                        return <Person name={person.name} age={person.age}/>
                    })}
                </div>
            )
        }
        return personsList;
    }

    render() {

        /* NOTE: JSX Inline styling. As with HTML, avoid inline styles and use CSS imports. */
        const style = {
            backgroundColor: 'white',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px'
        }

        return (
            <div className="App">
                <button
                    style={style}
                    onClick={this.togglePersonsHandler}>
                    Show People
                </button>
                { this.renderPersonsList() }
            </div>
        );
    }
}

export default App;
