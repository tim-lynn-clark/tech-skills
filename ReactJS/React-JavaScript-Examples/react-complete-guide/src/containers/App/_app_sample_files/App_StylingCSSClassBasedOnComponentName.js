import React, {Component} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

class App extends Component {

    state = {
        persons: [
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ]
    }

    switchNameHandler = () => {
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[0].name = "Frederick Heickel";

        this.setState({
            persons: alteredPersons,
        })
    }

    render() {
        return (
            <div className="App">
                <button onClick={this.switchNameHandler}>Switch Name</button>
                <Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
                <Person name={this.state.persons[1].name} age={this.state.persons[1].age} />
                <Person name={this.state.persons[2].name} age={this.state.persons[2].age}>
                    My Hobbies: Camping
                </Person>
            </div>
        );
    }
}

export default App;
