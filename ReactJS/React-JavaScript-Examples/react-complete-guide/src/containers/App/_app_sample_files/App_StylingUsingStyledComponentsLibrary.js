import React, {Component} from 'react';
import './App.css';
import Person from './Person/Person';
import _ from 'lodash';
import styled from 'styled-components';

/* NOTE: Using styled-component to build a styled button
* styled-components uses the JavaScript feature Tagged Template Literals
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#tagged_templates
* They take in CSS styling and return a React component for that HTML element type.
* Which can then be used within your JSX as a regular component. */

/* NOTE: Because Styled-Components are indeed React functional components, they accept
* props arguments as attributes on the component when used in JSX, see below. Here we are
* checking the alt attribute on the component passed through props to determine if the button
* colors should be changed based on some state, in this case the state of showPersons. */
const StyledButton  = styled.button`
  background-color: ${props => props.alt ? 'red' : 'green'};
  color: white;
  font: inherit;
  border: 1px solid blue;
  padding: 8px;
  cursor: pointer;
  &:hover {
    background-color: ${props => props.alt ? 'pink' : 'lightgreen'};
    color: black;
  }
`;

class App extends Component {

    state = {
        persons: [
            { id: '123', name: 'Freddy', age: 25 },
            { id: '234', name: 'Sally', age: 32 },
            { id: '345', name: 'Sue', age: 21 },
        ],
        showPersons: false
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    deletePersonHandler = (index) => {
        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons.splice(index, 1);
        this.setState({
            persons: alteredPersons
        })
    }

    nameChangedHandler = (event, id) => {
        let newName = event.target.value;

        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        const person = alteredPersons.find(person => {
            return person.id === id;
        });
        person.name = newName;

        this.setState({
            persons: alteredPersons,
        })
    }

    renderPersonsList = () => {
        let personsList = null;
        if(this.state.showPersons) {
            personsList = (
                <div>
                    { this.state.persons.map((person, index) => {
                        return <Person
                            key={person.id}
                            name={person.name}
                            age={person.age}

                            click={() => this.deletePersonHandler(index)}
                            changed={(event) => this.nameChangedHandler(event, person.id)}
                        />
                    })}
                </div>
            )
        }
        return personsList;
    }

    render() {

        /* NOTE: Dynamically adjusting the CSS classes added to an element
        * based on some display logic.
        * .red class added if down to two elements
        * .red & .bold class added if down to one element
        * */
        const classes = [];
        if(this.state.persons.length <= 2) {
            classes.push('red');
        }
        if(this.state.persons.length <= 1) {
            classes.push('bold');
        }

        /* NOTE: Using the new StyledButton we created above using the Styled-Component library */
        /* NOTE: Using the alt attribute on the StyledButton to get data into the Styled-Component
        * This allows us to alter the CSS dynamically based on what is passed in the props argument.
        * In this case we will change the color of the button based on the boolean status of showPersons. */
        return (
            <div className="App">
                <h1>This is my React App</h1>
                <p className={classes.join(' ')}>Building some stuff...</p>
                <StyledButton
                    alt={this.state.showPersons}
                    onClick={this.togglePersonsHandler}>
                    Show People
                </StyledButton>
                { this.renderPersonsList() }
            </div>
        );
    }
}

export default App;
