import React, {Component} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

class App extends Component {

    state = {
        persons: [
            { name: 'Freddy', age: 25 },
            { name: 'Sally', age: 32 },
            { name: 'Sue', age: 21 },
        ],
        showPersons: false
    }

    switchNameHandler = () => {
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[0].name = "Frederick Heickel";

        this.setState({
            persons: alteredPersons,
        })
    }

    nameChangedHandler = (event) => {
        let newName = event.target.value;

        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons[1].name = newName;

        this.setState({
            persons: alteredPersons,
        })
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    /* NOTES: Using a sub-render method for conditionally rendering JSX in the main render method.
    * As you find yourself using sub-render methods, it is a good time to really think about your component's design.
    *
    * Questions to ask your self:
    * - Is the component doing more than one thing and one thing well?
    * - Can the sub-rendered content be a stateless display component of its own?
    *
    * If the answer is yes to either of these questions, it is a good time to refactor and break the sub-rendered content
    * out into another component.
    * */
    renderPersonsList = () => {
        let personsList = null;
        if(this.state.showPersons) {
            personsList = (
                <div>
                    <Person name={this.state.persons[0].name} age={this.state.persons[0].age}/>
                    <Person
                        name={this.state.persons[1].name}
                        age={this.state.persons[1].age}
                        changed={this.nameChangedHandler}
                    />
                    <Person name={this.state.persons[2].name} age={this.state.persons[2].age}>
                        My Hobbies: Camping
                    </Person>
                </div>
            )
        }
        return personsList;
    }

    render() {

        /* NOTE: JSX Inline styling. As with HTML, avoid inline styles and use CSS imports. */
        const style = {
            backgroundColor: 'white',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px'
        }

        return (
            <div className="App">
                <button
                    style={style}
                    onClick={this.togglePersonsHandler}>
                    Show People
                </button>
                { this.renderPersonsList() }
            </div>
        );
    }
}

export default App;
