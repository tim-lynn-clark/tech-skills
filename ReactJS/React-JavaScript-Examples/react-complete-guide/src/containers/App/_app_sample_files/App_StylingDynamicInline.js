import React, {Component} from 'react';
import '../App.css';
import Person from '../../../components/Persons/Person/Person';
import _ from 'lodash';

class App extends Component {

    state = {
        persons: [
            { id: '123', name: 'Freddy', age: 25 },
            { id: '234', name: 'Sally', age: 32 },
            { id: '345', name: 'Sue', age: 21 },
        ],
        showPersons: false
    }

    togglePersonsHandler = () => {
        this.setState({
            showPersons: !this.state.showPersons
        })
    }

    deletePersonHandler = (index) => {
        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        alteredPersons.splice(index, 1);
        this.setState({
            persons: alteredPersons
        })
    }

    nameChangedHandler = (event, id) => {
        let newName = event.target.value;

        //NOTE: Do not mutate state directly.
        let alteredPersons = _.cloneDeep(this.state.persons);
        const person = alteredPersons.find(person => {
            return person.id === id;
        });
        person.name = newName;

        this.setState({
            persons: alteredPersons,
        })
    }

    renderPersonsList = () => {
        let personsList = null;
        if(this.state.showPersons) {
            personsList = (
                <div>
                    { this.state.persons.map((person, index) => {
                        return <Person
                            key={person.id}
                            name={person.name}
                            age={person.age}

                            click={() => this.deletePersonHandler(index)}
                            changed={(event) => this.nameChangedHandler(event, person.id)}
                        />
                    })}
                </div>
            )
        }
        return personsList;
    }

    render() {

        /* NOTE: JSX Inline styling. As with HTML, avoid inline styles and use CSS imports. */
        const style = {
            /* NOTE: Everything is JavaScript and therefore can be made dynamic
            * Here the background color is altered based on the state of the showPersons
            * */
            backgroundColor: this.state.showPersons ? 'red' : 'green',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px'
        }

        return (
            <div className="App">
                <button
                    style={style}
                    onClick={this.togglePersonsHandler}>
                    Show People
                </button>
                { this.renderPersonsList() }
            </div>
        );
    }
}

export default App;
