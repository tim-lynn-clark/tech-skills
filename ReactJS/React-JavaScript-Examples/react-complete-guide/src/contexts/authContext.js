import React from 'react';

/* NOTE: Utilizing React's createContext function to build a context object
* that can be made available to the entire React application globally. This
* allows data and state to be accessed throughout the application
* without the need to pass it down through the component hierarchy
* manually through props. */
const authContext = React.createContext({
    authenticated: false,
    login: () => { console.log('FAKER: Login function...') }
});

export default authContext;