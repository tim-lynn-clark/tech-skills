import React, { Component } from 'react';
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary";
import Person from "./Person/Person";
/* NOTE: Importing a React context object that will be made available to all
* application components at the point at which it is introduced within the
* component hierarchy. */
import AuthContext from '../../contexts/authContext';

/* NOTE: Converted from functional component to class component
* in order to take advantage of life-cycle methods.*/
class PersonList extends Component{

    /* NOTE: With React 16 React Context objects can now be accessed externally to the
    * render or return block that returns JSX. This is only available in class-based components.
    * Using the static property contextType pointing to the context you wish to use in this component,
    * React will bind the this.context property of the component to the context object you provided to
    * the static contextType property. The context object can then be accessed throughout the
    * component's code using the this.context property. */
    static contextType = AuthContext;

    /* NOTE: Using the componentDidMount life-cycle method to demonstrate access to this.context set
    by the static contextType above.*/
    componentDidMount() {
        console.log('[PersonList.js] <CONTEXT> AuthContext', this.context);
    }

    /* NOTE: Using shouldComponentUpdate in order to do some performance optimizing.
    * if the persons array has not changed, we do not want the list with all of the
    * person components to be redrawn.
    *
    * In this example, we are using the toggle cockpit button in App.js to remove the
    * cockpit component from the screen. When that happens, the UI is redrawn and would
    * normally trigger the entire PersonList to be updated and redrawn. Through the use of
    * shouldComponentUpdate, we can stop the list from being updated and redrawn just because
    * there was a change in the parent component that did not affect this child component.*/
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // const update = nextProps.persons.length !== this.props.persons.length;
        // console.log('[PersonList.js] shouldComponentUpdate? ' + update);
        // return update;
        return true;
    }

    render() {
        /* NOTE: Utilizing the React Context.Consumer to wrap the persons list JSX.
        * This will allow access to all of the data and functions provided within the
        * context for use within this component without the need for that data to be
        * passed down manually through props.
        *
        * In this example, a function is provided to the Context.Consumer. That function is
        * then executed and the context object is them provided as a parameter to that function.
        * Within the function body, the JSX for this component can be setup based on the
        * data and function provided in the context object. */
        let propsAccessor = this.props;
        return (
            <AuthContext.Consumer>
                {(context) => {
                    if (context.authenticated) {
                        return propsAccessor.persons.map((person, index) => {

                            return <Person
                                key={person.id}
                                name={person.name}
                                age={person.age}

                                click={() => propsAccessor.click(index)}
                                changed={(event) => propsAccessor.changed(event, person.id)}
                            />
                        })
                    } else {
                        return <ErrorBoundary>
                            <h2>User is not yet authenticated.</h2>
                        </ErrorBoundary>
                    }
                }}
            </AuthContext.Consumer>
        );
    }


}

export default PersonList;