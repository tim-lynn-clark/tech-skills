/* NOTE: useEffect is a function component hook that provides
* some lifecycle-like functionality found in class-based components. */
import React, { useEffect, Fragment }  from 'react';
//NOTE: Removing global import of CSS
// import './Person.css';
// NOTE: Importing classes from CSS using scoped CSS module import
import classes from './Person.css';
import withClassHOFunction from '../../../hoc/WithClass/WithClassFunction';
/* NOTE: Importing the prop-types NPM package to add checking to properties
* passed in on the props object. See below after component definition before export.  */
import PropTypes from 'prop-types';

let person = (props) => {

    /* NOTE: useEffect can be made to run only once when the component first loads by passing an empty
    * dependency array. This would be the option for loading async data from an API.
    * Adding a return with another function provides a cleanup function similar to componentWillUnmount in class-based components*/
    useEffect(() => {
        console.log('[Person.js] Hook: useEffect - This function will be called only once when the component is added to the DOM.');

        return () => {
            console.log('[Person.js] Hook useEffect - Clean-up function - will run once when the component is removed from the DOM');
        }
    }, []);

    /* NOTE: Since no dependency array was provided as a second argument to useEffect,
    * this will run on every render and the cleanup return function will run everytime afterwards as well */
    useEffect(() => {
        console.log('[Person.js] Hook: useEffect - This function will be called on every render.');

        return () => {
            console.log('[Person.js] Hook useEffect - Clean-up function - will run once upon component removeal from DOM ');
        }
    });

    return (
        /* NOTE: Using classes.Person from the scoped CSS module import */
        <Fragment>
            <p onClick={props.click}>Hi! My name is {props.name}, I am {props.age} years old.</p>
            <p>{props.children}</p>
            <input
                type="text"
                value={props.name}
                onChange={props.changed}
            />
        </Fragment>
    );
}

/* NOTE: Using the PropTypes package to check that properties passed into this component
* exist and are of the right types. With the props and their JavaScript types identified,
* developers using this component will get console warning messages if they pass the wrong
* type to a property of the component. */
person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func,
};

/* NOTE: * Surrounding the component with and example of a HOC function that modifies a React component
* by wrapping itself around this component. See WithClassFunction.js for implementation. Purley a contrived
* example... */
export default withClassHOFunction(person, classes.Person);