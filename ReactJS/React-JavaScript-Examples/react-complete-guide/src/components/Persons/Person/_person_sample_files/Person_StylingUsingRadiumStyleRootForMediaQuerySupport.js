import React from 'react';
import './Person.css';
// Using radium for better inline CSS styling (using media queries)
import Radium from "radium";

let person = (props) => {

    /* NOTE: Using Radium to create inline CSS styles that support media queries*/
    const style = {
        '@media (min-width: 500px)': {
            width: '450px'
        }
    };

    return (
        <div className="Person" style={style}>
            <p onClick={props.click}>Hi! My name is {props.name}, I am {props.age} years old.</p>
            <p>{props.children}</p>
            <input type="text" value={props.name} onChange={props.changed} />
        </div>
    );
}

/* NOTE: Using radium for better inline CSS styling (using media queries).
* Radium uses a higher-order component to wrap your custom component. The higher-order
* component will then interpret all complex inline CSS styles within your component */
export default Radium(person);
/* NOTE: Using media queries requires not only that the component be wrapped in a
* Radium higher-order component, but the app's root JSX needs to be wrapped in a special radium
* component called StyleRoot (see App.js Render method return) */