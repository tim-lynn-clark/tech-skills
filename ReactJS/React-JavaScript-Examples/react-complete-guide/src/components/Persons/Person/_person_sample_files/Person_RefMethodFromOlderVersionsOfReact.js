/* NOTE: useEffect is a function component hook that provides
* some lifecycle-like functionality found in class-based components. */
import React, { Component, Fragment }  from 'react';
//NOTE: Removing global import of CSS
// import './Person.css';
// NOTE: Importing classes from CSS using scoped CSS module import
import classes from './Person.css';
import withClassHOFunction from '../../../hoc/WithClass/WithClassFunction';
/* NOTE: Importing the prop-types NPM package to add checking to properties
* passed in on the props object. See below after component definition before export.  */
import PropTypes from 'prop-types';

class Person extends Component {

    componentDidMount() {
        /* NOTE: Using the element reference to the input in the JSX,
        * we can set it to be the focused element when rendered. */
        this.nameInput.focus();
    }

    render() {
        return (
            /* NOTE: Using the ref attribute of the input element in the JSX, we can setup a
            * reference to that element for use elsewhere in the component. See how the element was
            * set as the focus in the browser in the componentDidMount meth of this component. */
            <Fragment>
                <p onClick={this.props.click}>Hi! My name is {this.props.name}, I am {this.props.age} years old.</p>
                <p>{this.props.children}</p>
                <input
                    ref={(inputEl) => { this.nameInput = inputEl }}
                    type="text"
                    value={this.props.name}
                    onChange={this.props.changed}
                />
            </Fragment>
        );
    }
}

/* NOTE: Using the PropTypes package to check that properties passed into this component
* exist and are of the right types. With the props and their JavaScript types identified,
* developers using this component will get console warning messages if they pass the wrong
* type to a property of the component. */
Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func,
};

/* NOTE: * Surrounding the component with and example of a HOC function that modifies a React component
* by wrapping itself around this component. See WithClassFunction.js for implementation. Purley a contrived
* example... */
export default withClassHOFunction(Person, classes.Person);