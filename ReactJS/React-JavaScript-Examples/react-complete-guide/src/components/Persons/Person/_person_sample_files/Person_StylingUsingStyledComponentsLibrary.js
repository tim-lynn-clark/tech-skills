import React from 'react';
// NOTE: Removing the CSS import for this component as we are using styled-component now
// import './Person.css';
import styled from 'styled-components';

/* NOTE: Using styled-component to build a styled div
* styled-components uses the JavaScript feature Tagged Template Literals
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#tagged_templates
* They take in CSS styling and return a React component for that HTML element type.
* Which can then be used within your JSX as a regular component. */
const StyledDiv = styled.div`
            width: 60%;
            margin: 16px auto;
            border: 1px solid #eee;
            box-shadow: 0 2px 3px #ccc;
            padding: 16px;
            text-align: center;    
            @media (min-width: 500px) {
              width: 450px;
            }    
        `;

let person = (props) => {

    return (
        /* NOTE: Using the styled-component to render a div with CSS handled by the library. */
        <StyledDiv>
            <p onClick={props.click}>Hi! My name is {props.name}, I am {props.age} years old.</p>
            <p>{props.children}</p>
            <input type="text" value={props.name} onChange={props.changed} />
        </StyledDiv>
    );
}

export default person;