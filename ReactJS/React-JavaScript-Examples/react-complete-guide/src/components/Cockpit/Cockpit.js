/* NOTE: useEffect is a function component hook that provides
* some lifecycle-like functionality found in class-based components. */
import React, { useEffect, useContext } from 'react';
import classes from './Cockpit.css';
import withClassHOFunction from '../../hoc/WithClass/WithClassFunction';
/* NOTE: Importing a React context object that will be made available to all
* application components at the point at which it is introduced within the
* component hierarchy. */
import AuthContext from '../../contexts/authContext';

const cockpit = (props) => {

    /* NOTE: useContext hook to gain access to the AuthContext outside of the JSX
    * return block. */
    const authContext = useContext(AuthContext);
    console.log('[Cockpit.js] <CONTEXT>', authContext);

    /* NOTE: useEffect to simulate lifecycle methods from class-based components
    * in functional components. The function provided to useEffect is executed on
    * each render cycle of the functional component. It can be thought of as
    * componentDidMount and componentDidUpdate wrapped in a single event. */
    useEffect(() => {
        console.log('[Cockpit.js] Hook: useEffect - This function will be called on every render because no dependencies were provided.');
    })

    /* NOTE: useEffect, just like useState, can be used more than once within a component. */
    /* NOTE: The second argument to useEffect allows you to provide an array of dependencies.
    * These dependencies are pieces of data that if changed, will cause the function to be executed
    * on a render. If not provided, the function is executed on every render cycle, this allows you to
    * control when the function is executed. */
    useEffect(() => {
        console.log('[Cockpit.js] Hook: useEffect - This function will be called only when the recordCount variable in props has its value changed.');
    }, [props.recordCount]);

    /* NOTE: useEffect can be made to run only once when the component first loads by passing an empty
    * dependency array. This would be the option for loading async data from an API. */
    useEffect(() => {
        const timer = setTimeout(() => {
            console.log('[Cockpit.js] Hook: useEffect - This function will be called only once.');
        }, 1000);

        //NOTE: providing a clean-up function to remove the timer
        return () => {
            clearTimeout(timer);
        }
    }, [])

  /* NOTE: Dynamically adjusting the CSS classes added to an element
   * based on some display logic.
   * .red class added if down to two elements
   * .red & .bold class added if down to one element
   * */
  const assignedClasses = [];
  if(props.recordCount <= 2) {
    // NOTE: Styling using classes.red from the CSS module import
    assignedClasses.push(classes.redText);
  }
  if(props.recordCount <= 1) {
    // NOTE: Styling using classes.red from the CSS module import
    assignedClasses.push(classes.boldText);
  }

  /* NOTE: Dynamically changing CSS based on state using scoped styles from CSS module import */
  const btnClasses = [];
  if(props.listVisible) btnClasses.push(classes.red);

  /* NOTE: Using a higher-order component function to wrap this JSX in a div with CSS classes.
  * Using React.Fragment to wrap the JSX that does not have a root element (mostly just to show
  * how the HOC function works. See export default statement below. */
  return (
      <React.Fragment>
        <h1>This is my React App</h1>
        <p className={assignedClasses.join(' ')}>Building some stuff...</p>
        <button
            className={btnClasses.join(' ')}
            onClick={props.click}>
          Show People
        </button>
          <AuthContext.Consumer>
              {context => <button onClick={context.login}>Log in</button>}
          </AuthContext.Consumer>
      </React.Fragment>
  );
};

/* NOTE: Like using shouldComponentUpdate in a class-based component to stop updating and
* rerendering of child components when there has been no change to their state when a partent
* component is updated, functional components can use the Memo higher oder wrapper
* provided by React.
*
* Surrounding the component in React.memo will cause React to take a snapshot of the component
* each time it is used and compare that snapshot to the current state to determine if any
* state or props have changed before determining it needs to be updated and rerendered. */
export default React.memo(
    /* NOTE: * Surrounding the component with and example of a HOC function that modifies a React component
    * by wrapping itself around this component. See WithClassFunction.js for implementation. */
     withClassHOFunction(
         cockpit, classes.cockpit
     )
);