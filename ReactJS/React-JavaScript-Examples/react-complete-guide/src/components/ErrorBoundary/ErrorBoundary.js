import React, {Component} from 'react';

class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: '',
    }

    componentDidCatch(error, errorInfo) {
        this.setState({
            hasError: true,
            errorMessage: error,
        });
    }

    render () {
        let toRender = this.props.children;

        if(this.state.hasError) {
            toRender = <h1>Something went wrong!</h1>;
        }

        return toRender;
    }
}

export default ErrorBoundary;