import React from 'react';
import StackedWrapper from '../../hoc/StackWrapper/StackWrapper';

const stackedWrapperExample = props => {
    return (
        <StackedWrapper>
            <h4>Example of StackedWrapper HOC for JSX Fragments</h4>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
        </StackedWrapper>
    )
}

export default stackedWrapperExample;