import React, { Fragment } from 'react';
import StackedWrapper from "../../hoc/StackWrapper/StackWrapper";
// NOTE: React now provides a built-in wrapper for JSX fragments that do not have root elements
// import StackedWrapper from '../StackWrapper/StackWrapper';

const fragmentExample = props => {
    return (
        <Fragment>
            <h4>Example of built-in React.Fragment HOC for JSX Fragments</h4>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
            <p>This text should not be wrapped in a dive</p>
        </Fragment>
    )
}

export default fragmentExample;