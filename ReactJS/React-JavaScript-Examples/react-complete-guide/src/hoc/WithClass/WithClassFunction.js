import React from 'react';

/* NOTE: More complex example of a higher-order component, one that adds behind the scenes
* functionality to another component. This one takes in a another component as an argument, wraps
* that component, and returns itself in place of the original component. This is different than
* the other example where the higher-order component is used as a component within another component's
* JSX. This one will be used to wrap a component in that component's export default statement. */
const withClassFunction = (WrappedComponent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponent {...props} />
        </div>
    );
}

export default withClassFunction;