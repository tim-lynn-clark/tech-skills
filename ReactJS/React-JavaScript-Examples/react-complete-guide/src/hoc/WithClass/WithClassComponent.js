import React from 'react';

/* NOTE: Simplistic example of a higher-order component, one that adds behind the scenes
* functionality to another component. This one simply wraps the JSX of another component
* in a div and adds the specified CSS classes to that div, then renders the JSX children
* of the wrapped component. */
const withClassComponent = props => (
    <div className={props.classes}>
        {props.children}
    </div>
);

/* NOTE: In this example we create a React component that is returned and can be used
* as a JSX component within another component's JSX. There is another way of doing
* higher-order components that utilizes a function that returns a component. See WithClassFunction.js */
export default withClassComponent;