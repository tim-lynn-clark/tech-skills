import React from 'react';

/* NOTE: OLD METHOD. React now provides a built-in HOC to handle
* JSX fragments missing a root element (see FragmentExample.js). */

/* NOTE: Normally, when you create the return in the render function of
* a React component, you have to provide a root HTML element to wrap
* all the JSX for that component. Most of the time this is fine as
* you would use a DIV and it would be used to provide scoped styling
* for the component <div className='customer-card'>.
*
* But, there are times when a component will need to render HTML via JSX
* and you do not want it wrapped in a DIV or any other root element.
*
* One way to get around this is to create a higher-order component to wrap the JSX of
* other components when you do not want to be forced to include
* a root wrapping div in the render function of your component.
* By simply returning the built-in props.children as the only return
* for the wrapper component, you can get away with no root HTML/JSX in
* components using the wrapper component.
* */
const stackWrapper = props => props.children;

export default stackWrapper;