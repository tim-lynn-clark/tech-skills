# ReactJS Tutorial

## NPM Packages Introduced

[Lodash](https://www.npmjs.com/package/lodash) for deep cloning state objects

[Radium](https://www.npmjs.com/package/radium) for better inline CSS support within components

    npm install --save radium --legacy-peer-deps

[Styled-Components](https://www.npmjs.com/package/styled-components) another component styling library

    npm install --save styled-components

[Prop-Types](https://www.npmjs.com/package/prop-types)
Used to add control to the parameters passed in the props object.

    npm install --save prop-types

## Things that work better in Class-based Components
- Code organization is cleaner (in my opinion)
- State is easier to set up and manage in class-based components as it is more prescriptive in nature where as functional
  components are the wild west in state management as there can be many state slices which all have to be kept in sync manually
  as there is no automatic merging of state in functional components as there is in class-based.
- All life-cycle hooks. Functional components have useEffect that will simulate the combination of componentDidMount 
  and componentDidUpdate, but it does not allow fine-tuned control over all of the component's life-cycle methods as 
  can be done in class-based components.
- Fine control over optimizing component rendering can only be done in class-based components using the shouldComponentUpdate
  life-cycle method. There are hacky ways of doing it in functional components.
- Using ref to access elements within the component's JSX. This is far easier to accomplish in class-based components 
  as it requires the use of the constructor function to set up the element references. Functional components do have the 
  useRef hook to set up JSX element references, but it requires the use of the useEffect hook to 
  simulate life-cycle methods. 
- 
