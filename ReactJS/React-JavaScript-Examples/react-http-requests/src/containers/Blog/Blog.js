import React, { Component } from 'react';
import axios from "axios";

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.css';

class Blog extends Component {

    state = {
        posts: [],
        selectedPost: null,
    }

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response => {
                const posts = response.data.slice(0 ,4);
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: "Tim Clark"
                    }
                })
                this.setState({
                    posts: updatedPosts
                })
            })
    }

    handlePostSelection = (post) => {
        this.setState({
            selectedPost: post
        })
    }

    renderPosts() {
        const jsx = this.state.posts.map(post => {
            return <Post
                key={post.id}
                title={post.title}
                author={post.author}
                click={() => this.handlePostSelection(post)}
            />;
        });
        return jsx;
    }

    render () {
        return (
            <div>
                <section className="Posts">
                    {this.renderPosts()}
                </section>
                <section>
                    <FullPost post={this.state.selectedPost} />
                </section>
                <section>
                    <NewPost />
                </section>
            </div>
        );
    }
}

export default Blog;