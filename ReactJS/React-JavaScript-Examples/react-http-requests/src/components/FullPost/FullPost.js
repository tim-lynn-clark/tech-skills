import React, { Component, Fragment } from 'react';

import './FullPost.css';

class FullPost extends Component {
    render () {
        let body = <p>Please select a Post!</p>;
        if(this.props.post) {
            body = <Fragment>
                <h1>{this.props.post.title}</h1>
                <p>{this.props.post.body}</p>
                <div className="Edit">
                    <button className="Delete">Delete</button>
                </div>
            </Fragment>
        }

        let post = (
            <div className="FullPost">
                {body}
            </div>

        );
        return post;
    }
}

export default FullPost;