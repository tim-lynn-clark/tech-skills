import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
/* NOTE: Adding Redux-Thunk in order to handle asynchronous calls
* within Action Creators and prior to Action Dispatch execution. */
import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';

import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';

/* NOTE: Setting up Redux Store from Reducer. */
import reducerBurgerBuilder from "./store/reducers/burgerBuilder";

/* NOTE: Activating Redux Devtools. Ony turn it on if in development mode. */
const reduxDevtoolsInit = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION__COMPOSE__ : null) || compose;

const store = createStore(
    reducerBurgerBuilder,
    reduxDevtoolsInit(
        applyMiddleware(thunk)
    )
);

/* NOTE: Using React Router Dom to wrap app so routing is available in all components
* in the component tree. */
const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
