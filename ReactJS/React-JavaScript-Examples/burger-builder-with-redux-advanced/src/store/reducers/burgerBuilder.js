import * as actionTypes from '../actions/actionTypes';

const initialState = {
    ingredients: {
        salad: 0,
        bacon: 0,
        cheese: 0,
        meat: 0,
    },
    totalPrice: 4.0,
    error: false,
}

const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3,
}

function burgerBuilder(state = initialState, action) {
    let newState;

    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            newState = {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
            }
            break;
        case actionTypes.REMOVE_INGREDIENT:
            newState = {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
            }
            break;
        case actionTypes.SET_INGREDIENTS:
            newState = {
                ...state,
                ingredients: action.ingredients,
                error: false,
            }
            break;
        case actionTypes.FETCH_INGREDIENTS_FAILED:
            newState = {
                ...state,
                ingredients: {
                    ...state.ingredients
                },
                error: true,
            }
            break;
        default:
            newState = {
                ...state,
                ingredients: {
                    ...state.ingredients
                },
            };
    }

    return newState;
}

export default burgerBuilder;