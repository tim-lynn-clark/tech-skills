import * as actionTypes from './actionTypes';

/* NOTE: Refactoring - adding action creators to extract out the creation of
* Actions from Dispatch call code.  */

export const addIngredient = (name) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredientName: name,
    }
}

export const removeIngredient = (name) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredientName: name,
    }
}

/* NOTE: Example of an Action Creator that is delayed in order to accomplish
* some asynchronous task (like calling an AP). */
export const initIngredients = () => {
    return dispatch => {
        setTimeout( () => {
            dispatch(setIngredients({
                salad: 0,
                bacon: 0,
                cheese: 0,
                meat: 0,
            },));
        }, 2000 );
    }
}

/* NOTE: Private synchronous Action Creator called by the Thunk delayed Action Creator
* above (initIngredients). */
const setIngredients = (ingredients) => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients: ingredients,
    }
}

/* NOTE: Handling error states using Reducer Actions. */
export const fetchIngredientsFiled = () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_FAILED,
    }
}