import React from 'react';
import classes from './NavItems.css';
import NavItem from './NavItem/NavItem';

function NavItems (props) {

    return (
        <ul className={classes.NavItems}>
            <NavItem link="/" exact>Burger Builder</NavItem>
            <NavItem link="/orders">Orders</NavItem>
        </ul>
    );
}

export default NavItems;