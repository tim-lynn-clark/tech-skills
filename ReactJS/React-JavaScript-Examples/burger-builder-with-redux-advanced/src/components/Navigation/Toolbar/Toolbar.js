import React from 'react';
import classes from './Toolbar.css';
import Logo from '../../Shared/Logo/Logo';
import NavItems from '../NavItems/NavItems';
import DrawerToggle from "../SideDrawer/DrawerToggle/DrawerToggle";

function Toolbar(props) {

    return (
        <header className={classes.Toolbar}>
            <DrawerToggle clicked={props.drawerToggleClicked} />
            <div className={classes.Logo}>
                <Logo />
            </div>
            <nav className={classes.DesktopOnly}>
                <NavItems />
            </nav>
        </header>
    );
}

export default Toolbar;