import React from 'react';
import classes from './Burger.css';
import Ingredient from './Ingredient/Ingredient';

function Burger(props) {

    const ingredientNames = Object.keys(props.ingredients);
    let jsx = ingredientNames
        .map(key => {
            return [...Array(props.ingredients[key])].map((_, i) => {
               return <Ingredient key={key+i} type={key} />
            });
        })
        .reduce((arr, el) => {
            return arr.concat(el);
        }, []);

    if(jsx.length === 0) {
        jsx = <p>Please start adding ingredients.</p>;
    }

    return (
        <div className={classes.Burger}>
            <Ingredient type="bread-top" />
            {jsx}
            <Ingredient type="bread-bottom" />
        </div>
    );
}

export default Burger;