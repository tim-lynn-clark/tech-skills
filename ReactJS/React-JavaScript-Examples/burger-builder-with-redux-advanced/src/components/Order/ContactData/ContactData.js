import React, { Component } from 'react';

/* NOTE: Importing Redux connect in order to wrap this component
* and provide it with access to the Redux Store. */
import { connect } from 'react-redux';

import classes from './ContactData.css';
import Button from "../../Shared/Button/Button";

class ContactData extends Component {

    state = {
        name: '',
        email: '',
        address: {
            street: '',
            postalCode: '',
        },
    }

    handleOrder = (event) => {
        event.preventDefault();
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            customer: {
                name: 'Tim Clark',
                address: {
                    street: 'Test Street 123',
                    postalCode: '84043',
                    country: 'USA',
                },
                email: 'test@test.com',
            },
            deliveryMethod: 'Super Fast',
        }

        // Post to an API if one existed
        console.log(order);

        this.props.history.push('/');
    }

    render() {
        return (
            <div className={classes.ContactData}>
                <h4>Enter your contact data</h4>
                <form>
                    <input type="text" name="name" placeholder="full name" />
                    <input type="text" name="email" placeholder="email" />
                    <input type="text" name="name" placeholder="street" />
                    <input type="text" name="name" placeholder="postal code" />
                    <Button btnType="Success" clicked={this.handleOrder}>Order</Button>
                </form>
            </div>
        );
    }
}

/* NOTE: Setting up mappings between local component props and the data held in the
* Redux Store. */
const mapStateToProps = state => {
    return {
        ingredients: state.ingredients,
        totalPrice: state.totalPrice,
    }
}

/* NOTE: Wrapping this component with a HOC that is returned from the call to the `connect` function
* of Redux, which sets up the mappings of states stored in the Redux Store and mappings of Dispatch calls
* to Reducer Action Functions.  */
export default connect(mapStateToProps)(ContactData);