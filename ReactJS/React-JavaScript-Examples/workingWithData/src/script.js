var Card = React.createClass({
    getInitialState: function(){
        return {

        };
    },
    componentDidMount: function(){
        var that = this;
        $.get("https://api.github.com/users/schleichermann", function(data){
            that.setState(data);
        });
    },
    render: function(){
        return (
            <div>
                <img src={this.state.avatar_url} width="80"/>
                <h3>{this.state.login}</h3>
                <hr/>
            </div>
        );
    }
});

var Main = React.createClass({
    render: function(){
        return (
            <div>
                <Card login="schleichermann" />
            </div>
        );
    }
});

ReactDOM.render(<Main />, document.getElementById("root"));