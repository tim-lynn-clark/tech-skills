var Card = React.createClass({displayName: "Card",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: "https://avatars.githubusercontent.com/u/626596?v=3", width: "80"}), 
                React.createElement("h3", null, "schleichermann"), 
                React.createElement("hr", null)
            )
        );
    }
});

var Main = React.createClass({displayName: "Main",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement(Card, {login: ""})
            )
        );
    }
});

ReactDOM.render(React.createElement(Main, null), document.getElementById("root"));