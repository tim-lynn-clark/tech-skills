var Card = React.createClass({displayName: "Card",
    getInitialState: function(){
        return {

        };
    },
    componentDidMount: function(){
          
    },
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: "", width: "80"}), 
                React.createElement("h3", null), 
                React.createElement("hr", null)
            )
        );
    }
});

var Main = React.createClass({displayName: "Main",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement(Card, {login: "schleichermann"})
            )
        );
    }
});

ReactDOM.render(React.createElement(Main, null), document.getElementById("root"));