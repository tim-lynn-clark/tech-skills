var Card = React.createClass({displayName: "Card",
    getInitialState: function(){
        return {

        };
    },
    componentDidMount: function(){
        var that = this;
        $.get("https://api.github.com/users/schleichermann", function(data){
            that.setState(data);
        });
    },
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: "", width: "80"}), 
                React.createElement("h3", null, this.state.login), 
                React.createElement("hr", null)
            )
        );
    }
});

var Main = React.createClass({displayName: "Main",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement(Card, {login: "schleichermann"})
            )
        );
    }
});

ReactDOM.render(React.createElement(Main, null), document.getElementById("root"));