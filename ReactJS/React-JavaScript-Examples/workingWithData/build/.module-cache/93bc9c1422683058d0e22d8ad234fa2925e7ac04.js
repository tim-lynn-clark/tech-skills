var Card = React.createClass({displayName: "Card",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: ""}), 
                React.createElement("h3", null), 
                React.createElement("hr", null)
            )
        );
    }
});

var Main = React.createClass({displayName: "Main",
    render: function(){
        return (
            React.createElement("div", null, 
                React.createElement(Card, null)
            )
        );
    }
});

React.render(React.createElement(Main, null), document.getElementById("root"));