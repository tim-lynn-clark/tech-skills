import * as actionTypes from './actions';

/* NOTE: An initial state can be provided to the reducer
* so that the first time it is called it can load that state
* instead of being null.*/
const initialState = {
    persons: []
};

/* NOTE: Here we can see the initialState being used as the default
* value for the Redux state being setup by the Reducer the first time it
* is executed. */
const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_PERSON:
            const newPerson = {
                id: Math.random(), // not really unique but good enough here!
                name: action.personData.name,
                age: action.personData.age
            }
            return {
                ...state,
                persons: state.persons.concat( newPerson )
            }
        case actionTypes.REMOVE_PERSON:
            return {
                ...state,
                persons: state.persons.filter(person => person.id !== action.personId)
            }
    }
    return state;
};

export default reducer;