import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducer from './store/reducer';

/* NOTE: Since the Redux Store will be used through out the entire application and will be accessed
* by a majority of the React Components. Due to that, the `index.js` where the App is initialized
* is also a great place for the Redux Store to be initialized with its corresponding Reducer.  */
const store = createStore(reducer);

/* NOTE: Using React-Redux's Provider Component to provide access to the Redux Store from within our
* React application. */
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
