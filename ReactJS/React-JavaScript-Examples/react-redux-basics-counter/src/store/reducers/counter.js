/* NOTE: Demonstrates how Reducers can be broken up based on concerns and
* then combined into a single Reducer for use in a Redux Store*/

import * as actionTypes from "../actions";

/* NOTE: An initial state can be setup for the Redux Store so that it does
* not begin as NULL. */
const initialState = {
    counter: 0
}

/* NOTE: The Reducer is responsible for setting up the Redux Store the first time
* it is executed. After first initialization, the state will be maintained internally by
* Redux using this Reducer. */
function reducer(state = initialState, action) {

    let newState = {};

    /* NOTE: Using the Action.type to determine what should happen to the
    * state within the Redux Store. */
    switch (action.type) {
        case actionTypes.INCREMENT:
            newState = {
                ...state,
                counter: state.counter + 1,
            };
            break;
        case actionTypes.DECREMENT:
            newState = {
                ...state,
                counter: state.counter - 1,
            };
            break;
        case actionTypes.ADD:
            /* NOTE: Instead of using hard-coded values within the reducer functions,
            * the a key (selected by the developer, does not have to be value) is used to
            * access data passed as part of the Action that was passed to the Dispatch call. */
            newState = {
                ...state,
                counter: state.counter + action.value,
            };
            break;
        case actionTypes.SUBTRACT:
            /* NOTE: Instead of using hard-coded values within the reducer functions,
            * the a key (selected by the developer, does not have to be value) is used to
            * access data passed as part of the Action that was passed to the Dispatch call. */
            newState = {
                ...state,
                counter: state.counter - action.value,
            };
            break;
    }

    return newState;
}

export default reducer;

