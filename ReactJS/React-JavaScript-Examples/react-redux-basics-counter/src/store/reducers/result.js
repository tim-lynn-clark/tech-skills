/* NOTE: Demonstrates how Reducers can be broken up based on concerns and
* then combined into a single Reducer for use in a Redux Store*/

import * as actionTypes from "../actions";

/* NOTE: An initial state can be setup for the Redux Store so that it does
* not begin as NULL. */
const initialState = {
    results: [],
}

/* NOTE: The Reducer is responsible for setting up the Redux Store the first time
* it is executed. After first initialization, the state will be maintained internally by
* Redux using this Reducer. */
function reducer(state = initialState, action) {

    let newState = {};

    /* NOTE: Using the Action.type to determine what should happen to the
    * state within the Redux Store. */
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            newState = {
                ...state,
                results: state.results.concat({
                    value: action.result,
                    id: new Date().toUTCString(),
                })
            };
            break;
        case actionTypes.DELETE_RESULT:
            const resultsClone = state.results.filter(result => {
                return result.id !== action.id;
            });

            newState = {
                ...state,
                results: resultsClone,
            }
            break;
    }

    return newState;
}

export default reducer;

