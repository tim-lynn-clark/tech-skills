/* NOTE: Reducers can become extremely large and complex as the application
* grows and in response the state grows. Reducers can be broken up as demonstrated
* as this Reducer is broken up into the counter.js and result.js Reducers in the
* reducers folder. This file left for demonstration purposes. */

import * as actionTypes from "./actions";

/* NOTE: An initial state can be setup for the Redux Store so that it does
* not begin as NULL. */
const initialState = {
    counter: 0,
    results: [],
}

/* NOTE: The Reducer is responsible for setting up the Redux Store the first time
* it is executed. After first initialization, the state will be maintained internally by
* Redux using this Reducer. */
function reducer(state = initialState, action) {

    let newState = {};

    /* NOTE: Using the Action.type to determine what should happen to the
    * state within the Redux Store. */
    switch (action.type) {
        case actionTypes.INCREMENT:
            newState = {
                ...state,
                counter: state.counter + 1,
            };
            break;
        case actionTypes.DECREMENT:
            newState = {
                ...state,
                counter: state.counter - 1,
            };
            break;
        case actionTypes.ADD:
            /* NOTE: Instead of using hard-coded values within the reducer functions,
            * the a key (selected by the developer, does not have to be value) is used to
            * access data passed as part of the Action that was passed to the Dispatch call. */
            newState = {
                ...state,
                counter: state.counter + action.value,
            };
            break;
        case actionTypes.SUBTRACT:
            /* NOTE: Instead of using hard-coded values within the reducer functions,
            * the a key (selected by the developer, does not have to be value) is used to
            * access data passed as part of the Action that was passed to the Dispatch call. */
            newState = {
                ...state,
                counter: state.counter - action.value,
            };
            break;
        case actionTypes.STORE_RESULT:
            newState = {
                ...state,
                results: state.results.concat({
                    value: state.counter,
                    id: new Date().toUTCString(),
                })
            };
            break;
        case actionTypes.DELETE_RESULT:
            const resultsClone = state.results.filter(result => {
                return result.id !== action.id;
            });

            newState = {
                ...state,
                results: resultsClone,
            }
            break;
    }

    return newState;
}

export default reducer;

