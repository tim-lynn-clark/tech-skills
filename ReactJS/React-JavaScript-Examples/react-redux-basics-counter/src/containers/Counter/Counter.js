import React, { Component } from 'react';
import * as actionTypes from "../../store/actions";

/* NOTE: Using the connect HOC from the React-Redux package. */
import { connect } from 'react-redux';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';

class Counter extends Component {

    render () {
        /* NOTE: Notice the use of the `ctr` property of `props`, which was provided to this component
        * by React-Redux via the mapStateToProps config function that was passed to the React-Redux Connect
        * function, which in turn wrapped this component with a HOC that passed state data from the Redux Store
        * to this component using Props. */

        /* NOTE: Notice the use of the Dispatch functions being used as the click handlers below. The Dispatch functions
        * were setup below in the mapDispatchToProps config function that is passed to the React-Redux connect function.  */
        return (
            <div>
                <CounterOutput value={this.props.counter} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add 5" clicked={this.props.onAddCounter}  />
                <CounterControl label="Subtract 5" clicked={this.props.onSubtractCounter}  />
                <button onClick={() => this.props.onStoreResult(this.props.counter)}>Store Result</button>
                <ul>
                    {this.props.results.map(result => {
                       return <li key={result.id} onClick={() => this.props.onDeleteResult(result.id)}>{result.value}</li>
                    })}
                </ul>
            </div>
        );
    }
}

/* NOTE: This is the configuration function that will be passed to the React-Redux Connect function, which is used
* to configure the React-Redux HOC that will wrap this component and provide it access to the Redux State.
* The primary use of this config function is to map data from the Redux Store to props that will be passed
* to the wrapped component. The `state` parameter passed into this config function will be provided by React-Redux
* at the time it calls the config function to setup access to data in the Redux Store.  */
const mapStateToProps = state =>  {
    /* NOTE: Notice the use of counterManager and resultManager, these are the namespaces we created
    * when we used the combineReducers method in index.js. They each manage a different slice of the
    * Redux Store. */
    return {
        counter: state.counterManager.counter,
        results: state.resultManager.results,
    }
}

/* NOTE: This configuration function is passed to the React-Redux Connect function and is used to map data within
* this component's props to Redux Dispatch calls that specify the Action to be taken by the Reducer within the
* Redux Store as well as the data that is to be used by that Action to manipulate the Redux Store. */
const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({type: actionTypes.INCREMENT}),
        onDecrementCounter: () => dispatch({type: actionTypes.DECREMENT}),
        onAddCounter: () => dispatch({type: actionTypes.ADD, value: 8}),
        onSubtractCounter: () => dispatch({type: actionTypes.SUBTRACT, value: 10}),
        onStoreResult: (result) => dispatch({type: actionTypes.STORE_RESULT, result: result}),
        onDeleteResult: (id) => dispatch({type: actionTypes.DELETE_RESULT, id: id}),
    }
}

/* NOTE: The React-Redux Connect function takes in some configuration information as parameters to its initial execution, which results
* in the creation of HOC function that wraps a component and provides that component access to the Redux Store  */
export default connect(mapStateToProps, mapDispatchToProps)(Counter);