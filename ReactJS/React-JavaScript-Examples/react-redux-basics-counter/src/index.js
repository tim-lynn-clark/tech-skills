import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* NOTE: The createStore function will create a Redux Store using
* a passed in Reducer. */
/* NOTE: The combineReducers allows you to separate out Reducer logic into
* multiple reducers to reduce complexity as the Redux Store increases in size
* and complexity with the application.  */
import { createStore, combineReducers } from 'redux';


import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducerCounter from "./store/reducers/counter";
import reducerResult from "./store/reducers/result";

/* NOTE: The combineReducers allows you to separate out Reducer logic into
* multiple reducers to reduce complexity as the Redux Store increases in size
* and complexity with the application. When Redux adds the reducers to the store,
* it creates separate namespaces based on the property name you provide that
* points to the reducer. In this example counterManager and resultManager will be the
* namespaces pointing to the Reducers. */
const rootReducer = combineReducers({
    counterManager: reducerCounter,
    resultManager: reducerResult,
});

/* NOTE: Since the Redux Store will be used through out the entire application and will be accessed
* by a majority of the React Components. Due to that, the `index.js` where the App is initialized
* is also a great place for the Redux Store to be initialized with its corresponding Reducer.  */
const store = createStore(rootReducer);

/* NOTE: Using React-Redux's Provider Component to provide access to the Redux Store from within our
* React application. */
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
