const redux = require('redux');
const createStore = redux.createStore;

/* NOTE: An initial state can be setup for the Redux Store so that it does
* not begin as NULL. */
const initialState = {
    counter: 0
}

/* NOTE: The Reducer is responsible for setting up the Redux Store the first time
* it is executed. After first initialization, the state will be maintained internally by
* Redux using this Reducer. */
const rootReducer = (state = initialState, action) => {
    /* NOTE: Actions are simply unique identifiers that represent an application-specific manipulation of the
     * state that needs to take place. Actions determine what should be done with the incoming state. */
    if (action.type === 'INC_COUNTER') {
        return {
            ...state,
            counter: state.counter + 1
        };
    }
    if (action.type === 'ADD_COUNTER') {
        /* NOTE: The State should not be manipulated directly (no side-effects). Even using the spread operator `...`
        * will cause side effects if there are reference types being held within the state. You will need to
        * deep copy those objects prior to reinserting them into the state. */
        return {
            ...state,
            counter: state.counter + action.value
        };
    }
    /* NOTE: The reducer then returns the newly constructed state for storage globally by Redux. */
    return state;
};

/* NOTE: The Redux Store is created by calling Redux.createStore and passing that function a Reducer function.
* The Redux Store will then use that Reducer function as the primary method for manipulating the state.
* (browser window level for client applications, global leve for node applications)
* https://redux.js.org/tutorials/fundamentals/part-1-overview#the-redux-store */
const store = createStore(rootReducer);
console.log(store.getState());

/* NOTE: A Subscription is a method for receiving a notification that the state has been manipulated in some
* manner and can provides access to the new state.  */
store.subscribe(() => {
    console.log('[Subscription]', store.getState());
});

/* NOTE: A Dispatch is a proxy function that ultimately calls the Redux Store's root Reducer and passes to that
* reducer the current State from within the Redux Store and an Action. The Dispatch call only accepts an Action object
* that includes the Type (representing the specific change to state that is being requested) and any data that is
* required for that state change to take place. */
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER', value: 10});

/* NOTE: Normally a new copy of the Redux Store's State is received through a Subscription, it can also be accessed
* on the Redux Store itself through a call to the getState method of the Store. */
console.log(store.getState());
