import React, { Component, Fragment } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControl from '../../components/Burger/BuildControl/BuildControl';
import Modal from '../../components/Shared/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';

const INGREDIENT_PRICES = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3,
}

class BurgerBuilder extends Component {

    state = {
        ingredients: {
            salad: 0,
            bacon: 0,
            cheese: 0,
            meat: 0,
        },
        totalPrice: 4.0,
        purchasable: false,
        purchasing: false,
    }

    updatePurchasable = (ingredients) => {
        const ingredientCount = Object.keys(ingredients)
            .map(key => {
                return ingredients[key];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

        this.setState({
            purchasable: ingredientCount > 0
        })
    }

    handleAddIngredient = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;

        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;

        this.setState({
            ingredients: updatedIngredients,
            totalPrice: newPrice,
        });

        this.updatePurchasable(updatedIngredients);
    }

    handleRemoveIngredient = (type) => {
        const oldCount = this.state.ingredients[type];
        if(oldCount <= 0) return;
        const updatedCount = oldCount - 1;

        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceSubtraction = INGREDIENT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceSubtraction;

        this.setState({
            ingredients: updatedIngredients,
            totalPrice: newPrice,
        });

        this.updatePurchasable(updatedIngredients);
    }

    handlePurchasing = (value) => {
        this.setState({
            purchasing: value
        })
    }

    handlePurchaseContinue = () => {
        /* NOTE: Build a query string to pass with the path being set through
        * React Router Dom's history object that was injected into the props
        * of all components being loaded via the router. */
        const queryParams = [];
        for(let i in this.state.ingredients) {
            queryParams.push(
                encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i])
            );
        }
        queryParams.push('price=' + this.state.totalPrice);
        const queryString = queryParams.join('&');

        /* NOTE: Using the complex form of history.push to pass query string parameters
        * on a new route. */
        this.props.history.push({
            pathname: '/checkout',
            search: '?' + queryString,
        });
    }

    controlDisplayState = () => {
        const displayState = {
            ...this.state.ingredients
        };

        for(let key in displayState) {
            displayState[key] = displayState[key] > 0
        }
        return displayState;
    }

    render() {
        return (
            <Fragment>
                <Modal
                    show={this.state.purchasing}
                    modalClosed={() => this.handlePurchasing(false)}
                >
                    <OrderSummary
                        ingredients={this.state.ingredients}
                        totalPrice={this.state.totalPrice}
                        cancelPurchase={() => this.handlePurchasing(false)}
                        continuePurchase={this.handlePurchaseContinue}
                    />
                </Modal>
                <Burger ingredients={this.state.ingredients} />
                <BuildControl
                    price={this.state.totalPrice}
                    purchasable={this.state.purchasable}
                    purchasing={() => this.handlePurchasing(true)}
                    displayState={this.controlDisplayState()}
                    ingredientAdded={this.handleAddIngredient}
                    ingredientRemoved={this.handleRemoveIngredient}
                />
            </Fragment>
        );
    }
}

export default BurgerBuilder;