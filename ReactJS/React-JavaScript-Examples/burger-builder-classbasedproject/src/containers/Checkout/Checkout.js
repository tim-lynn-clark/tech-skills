import React, { Component } from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from "../../components/Order/ContactData/ContactData";
import {Route} from "react-router-dom";

class Checkout extends Component {

    // TODO: this is dummy data
    state = {
        ingredients: null,
        price: 0.0
    }

    componentWillMount() {
        /* NOTE: Parsing out the query string parameters that were passed
        * when this component was made visible by the React Router. */
        const query = new URLSearchParams(this.props.location.search);
        const ingredients = {};
        let price = 0.0;

        for(let param of query.entries()) {
            if(param[0] === 'price') {
                price = param[1];
            } else {
                ingredients[param[0]] = +param[1];
            }
        }

        this.setState({
            ingredients,
            price,
        })
    }

    handleCancel = () => {
        this.props.history.goBack();
    }

    handleContinue = () => {
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        return (
            <div>
                <CheckoutSummary
                    ingredients={this.state.ingredients}
                    cancel={this.handleCancel}
                    continue={this.handleContinue}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    render={(props) => {
                        return (
                            <ContactData
                                ingredients={this.state.ingredients}
                                price={this.state.price}
                                {...props}
                            />
                        );
                    }}
                />
            </div>
        );
    }
}

export default Checkout;