import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import Layout from '../../components/Layout/Layout';
import BurgerBuilder from "../BurgerBuilder/BurgerBuilder";
import Checkout from "../Checkout/Checkout";
import OrderHistory from "../../components/Order/OrderHistory/OrderHistory";

class App extends Component {
    render() {
        return (
            <div>
                <Layout>
                    <Route path="/orders" component={OrderHistory}/>
                    <Route path="/checkout" component={Checkout}/>
                    <Route path="/" exact component={BurgerBuilder}/>
                </Layout>
            </div>
        );
    }
}

export default App;
