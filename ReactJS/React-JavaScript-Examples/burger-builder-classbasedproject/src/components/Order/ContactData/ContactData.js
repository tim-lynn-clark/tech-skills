import React, { Component } from 'react';

import classes from './ContactData.css';

import Button from "../../Shared/Button/Button";

class ContactData extends Component {

    state = {
        name: '',
        email: '',
        address: {
            street: '',
            postalCode: '',
        },
    }

    handleOrder = (event) => {
        event.preventDefault();
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {
                name: 'Tim Clark',
                address: {
                    street: 'Test Street 123',
                    postalCode: '84043',
                    country: 'USA',
                },
                email: 'test@test.com',
            },
            deliveryMethod: 'Super Fast',
        }

        // Post to an API if one existed
        console.log(order);

        this.props.history.push('/');
    }

    render() {
        return (
            <div className={classes.ContactData}>
                <h4>Enter your contact data</h4>
                <form>
                    <input type="text" name="name" placeholder="full name" />
                    <input type="text" name="email" placeholder="email" />
                    <input type="text" name="name" placeholder="street" />
                    <input type="text" name="name" placeholder="postal code" />
                    <Button btnType="Success" clicked={this.handleOrder}>Order</Button>
                </form>
            </div>
        );
    }
}

export default ContactData;