import React, { Component } from 'react';
import classes from './OrderHistory.css';

import OrderDisplay from "../OrderDisplay/OrderDisplay";

class OrderHistory extends Component {

    //TODO: Dummy data
    state = {
        orders: [
            {
                id: 1,
                ingredients: {
                    salad: 1,
                    bacon: 1,
                    cheese: 1,
                    meat: 1,
                },
                price: 6.00,
                customer: {
                    name: 'Tim Clark',
                    address: {
                        street: 'Test Street 123',
                        postalCode: '84043',
                        country: 'USA',
                    },
                    email: 'test@test.com',
                },
                deliveryMethod: 'Super Fast',
            },
            {
                id: 2,
                ingredients: {
                    salad: 1,
                    bacon: 3,
                    cheese: 0,
                    meat: 2,
                },
                price: 7.00,
                customer: {
                    name: 'Joe Bob',
                    address: {
                        street: 'Test Street 123',
                        postalCode: '84043',
                        country: 'USA',
                    },
                    email: 'test@test.com',
                },
                deliveryMethod: 'Super Fast',
            },
            {
                id: 3,
                ingredients: {
                    salad: 2,
                    bacon: 0,
                    cheese: 1,
                    meat: 1,
                },
                price: 5.50,
                customer: {
                    name: 'Sally Smith',
                    address: {
                        street: 'Test Street 123',
                        postalCode: '84043',
                        country: 'USA',
                    },
                    email: 'test@test.com',
                },
                deliveryMethod: 'Super Fast',
            }
        ]
    }

    render() {
        return (
            <div className={classes.OrderHistory}>
                {
                    this.state.orders.map(order => {
                        return <OrderDisplay
                            key={order.id}
                            order={order}
                        />
                    })
                }
            </div>
        );
    }
}

export default OrderHistory;