import React from 'react';
import classes from './CheckoutSummary.css';
import Burger from "../../Burger/Burger";
import Button from "../../Shared/Button/Button";

function CheckoutSummary(props) {

    return (
        <div className={classes.CheckoutSummary}>
            <h1>It's going to be tasty!</h1>
            <div className={classes.preview}>
                <Burger ingredients={props.ingredients} />
            </div>
            <Button
                btnType="Danger"
                clicked={props.cancel}
            >Cancel</Button>
            <Button
                btnType="Success"
                clicked={props.continue}
            >Continue</Button>
        </div>
    );
}

export default CheckoutSummary;