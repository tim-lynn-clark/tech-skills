import React from 'react';
import PropTypes from 'prop-types';
import classes from './Ingredient.css';

function Ingredient(props) {
    let jsx = null;

    switch(props.type) {
        case('bread-bottom'):
            jsx = <div className={classes.BreadBottom} />;
            break;
        case('bread-top'):
            jsx = (
                <div className={classes.BreadTop}>
                    <div className={classes.Seeds1} />
                    <div className={classes.Seeds2} />
                </div>
            );
            break;
        case('meat'):
            jsx = <div className={classes.Meat} />;
            break;
        case('cheese'):
            jsx = <div className={classes.Cheese} />;
            break;
        case('salad'):
            jsx = <div className={classes.Salad} />;
            break;
        case('bacon'):
            jsx = <div className={classes.Bacon} />;
            break;
        default:
            jsx = null;
    }

    return jsx;
}

Ingredient.propTypes = {
    type: PropTypes.string.isRequired,
};

export default Ingredient;