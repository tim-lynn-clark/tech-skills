import React from 'react';
import classes from './NavItem.css';

/* NOTE: Using React Router Dom's NavLink to manage navigation links.
* Benefit is that it will handle which link is active and set the style
* for us. */
import { NavLink } from "react-router-dom";

function NavItem(props) {

    return (
        <li className={classes.NavItem}>
            <NavLink
                to={props.link}
                exact={props.exact}
                activeClassName={classes.active}
            >
                {props.children}
            </NavLink>
        </li>
    );
}

export default NavItem;