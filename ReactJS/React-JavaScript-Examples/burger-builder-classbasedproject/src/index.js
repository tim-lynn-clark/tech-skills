import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';

/* NOTE: Using React Router Dom to wrap app so routing is available in all components
* in the component tree. */
const app = (
    <BrowserRouter>
        <App />
    </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
