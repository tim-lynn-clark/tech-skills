import React, { Component } from 'react';

class ToDoItemDetails extends Component {

    constructor(props){
        super(props);


        this.state = {
            todo: props.todo
        };
    }

    static getDerivedStateFromProps(props, state) {
        return {
            todo: props.todo
        }
    }

    render () {
        return (
            <div>
                <div>
                    <label>Complete?:</label>
                    <input type="checkbox" checked={this.state.todo.isComplete}/>
                </div>
                <div>
                    <label>Title:</label>
                    <input type="text" value={this.state.todo.title}/>
                </div>
                <div>
                    <label>Description:</label>
                    <input type="text" value={this.state.todo.desc}/>
                </div>
                <div>
                    <label>Location:</label>
                    <input type="text" value={this.state.todo.place}/>
                </div>
                <div>
                    <label>Due Date:</label>
                    <input type="text" value={this.state.todo.dueDate}/>
                </div>
                <div>
                    <label>Category:</label>
                    <input type="text" value={this.state.todo.category}/>
                </div>
            </div>
        );
    }
}

export default ToDoItemDetails;
