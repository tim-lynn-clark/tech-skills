import React from 'react';
import ToDoItemCondensed from "./ToDoItemCondensed";

function ToDoList (props) {

    function todoItemHandler (todo) {
        props.todoPassHandler(todo);
    }

    let todoItemJSX = props.todosArray.map(todo =>
        <ToDoItemCondensed todo={todo} key={ todo._id } todoItemHandler={todoItemHandler} /> );

    return (
      <ul>
          { todoItemJSX }
      </ul>
    );
}

export default ToDoList;
