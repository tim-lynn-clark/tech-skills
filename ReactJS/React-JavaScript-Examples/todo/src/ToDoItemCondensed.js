import React from 'react';

function ToDoItemCondensed (props) {

    return (
        <li onClick={ () => props.todoItemHandler(props.todo) }>
            <span>{ props.todo.title }</span>
            <span>{ props.todo.dueDate.toLocaleDateString() }</span>
        </li>
    );
}

export default ToDoItemCondensed;

