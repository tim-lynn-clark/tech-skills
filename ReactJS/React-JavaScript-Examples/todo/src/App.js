import React, { Component } from 'react';
import './App.css';
import ToDoList from './ToDoList';
import ToDoItemDetails from './ToDoItemDetails';

const dummyData = [
    {
        _id: 1,
        title: 'Exercise',
        desc: 'Get your butt up',
        place: 'Home',
        dueDate: new Date(),
        isComplete: true,
        category: 'Wellness',
        subtasks: [
            {
                tile: '',
                dueDate: '',
                isComplete: false
            },
            {
                tile: '',
                dueDate: '',
                isComplete: false
            }
        ]
    },
    {
        _id: 2,
        title: 'Go to work',
        desc: 'Get paid',
        place: 'Neumont',
        dueDate: new Date(),
        isComplete: false,
        category: 'Employment',
        subtasks: []
    },
    {
        _id: 3,
        title: 'Eat dinner',
        desc: 'Get fat',
        place: 'Home',
        dueDate: new Date(),
        isComplete: false,
        category: 'Personal Care',
        subtasks: []
    }
];

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
        todos: dummyData,
        todoItemClicked: dummyData[0]
    };
  }

  todoPassHandler = (todo) => {
      console.log(todo);
      this.setState({
          todoItemClicked: todo
      })
  };

  render() {
    return (
        <div className="App">
            <ToDoList todosArray={this.state.todos} todoPassHandler={this.todoPassHandler} />
            <ToDoItemDetails todo={this.state.todoItemClicked} />
        </div>
    );
  }
}

export default App;
