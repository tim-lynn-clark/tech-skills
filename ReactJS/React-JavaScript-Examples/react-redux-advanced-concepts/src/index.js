import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';

/* NOTE: Importing Redux-Thunk in order to support asynchronous code
* when Dispatching an Action needs to be delayed or only if certain conditions are met.
* https://www.npmjs.com/package/redux-thunk*/
import thunk from 'redux-thunk';

import counterReducer from './store/reducers/counter';
import resultReducer from './store/reducers/result';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const rootReducer = combineReducers({
    ctr: counterReducer,
    res: resultReducer
});

/* NOTES: Creating a Redux Middleware function that can be inserted prior to
* the Reducer being activated.
* Middleware composition:
* - Function that returns a Next function (similar to Node.js Next)
* - Next function that returns an a custom function that accepts the Action from the Dispatch that kicked the whole thing off.
* - Custom function that accepts the Action is where you would execute all of your custom code that needs
* to happen prior to the Reducer being called with the Action. */
const logger = store => {
    return next => {
        return action => {
            // Custom code to be done prior to the Reducer receiving the Action passed in the Dispatch call
            console.log('[Middleware] Dispatching', action);
            // After custom code has been executed prior to the Reducer, you can call Next with the Action as its
            // parameter and the Action will be passed to the Reducer like normal.
            const result = next(action);
            // Additional custom code can be executed after the Action has been passed to the Reducer.
            console.log('[Middleware] next state', store.getState());
            return result;
        }
    }
};

/* NOTE: This is custom setup code when using the Redux Devtools for debugging in the browser.
* https://github.com/zalmoxisus/redux-devtools-extension#installation  */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/* NOTE: When creating a Redux Store, a Middleware function can be supplied by using the applyMiddleware function
* which accepts one or more custom middleware functions separated by comas. */
// const store = createStore(rootReducer, applyMiddleware(logger));

/* NOTE: Store creation in which middleware are wrapped with the Redux Devtools composeEnhancers from above. */
// const store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger)));

/* NOTE: Adding the Redux-Thunk middleware to the creation of the Redux Store. */
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger, thunk)));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
