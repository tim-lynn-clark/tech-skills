import * as actionTypes from './actionTypes';

/* NOTE: Action creators, used with Asynchronous reducer calls. */


/* NOTE: This is the synchronous Action Creator used by the asynchronous delayed
* Thunk-style action creator storeResults below. saveResults is only called after
* the simulated asynchronous call in storeResult has completed, it then returns
* an Action Object containing the Type and the Result of the asynchronous call that was made. */
export const saveResult = ( res ) => {
    return {
        type: actionTypes.STORE_RESULT,
        result: res
    };
}


/* NOTE: Simulating an asynchronous call using setTimeout to demonstrate the use of Thunk to delay the
* Dispatching of an Action.   */
export const storeResult = ( res ) => {
    /* NOTE: getState is the second argument passed by Redux-Thunk to your delayed dispatch function.
    * It can be used to access the current applications state if it is needed in your asynchronous
    * request. Like pulling the current user's id when making an API call.  */
    return (dispatch, getState) => {
        setTimeout( () => {
            dispatch(saveResult(res));
        }, 2000 );
    }
};

export const deleteResult = ( resElId ) => {
    return {
        type: actionTypes.DELETE_RESULT,
        resultElId: resElId
    };
};