export const updateObject = (oldObject, updatedValues) => {
    /* NOTE: If the oldObject that you are updating contains reference types,
    * you will need to use another method for copying the values from the old object
    * to the new object prior to updating properties with the new values.
    * You will need a "Deep Cloning" function, something like Underscore.js deepClone function. */
    return {
        ...oldObject,
        ...updatedValues
    }
};