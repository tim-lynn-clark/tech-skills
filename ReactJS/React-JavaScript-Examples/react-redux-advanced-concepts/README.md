# React-Redux Advanced Concepts Demo Project

## Concepts Covered
- Middleware: 
  - creating and adding to Redux Store (see `index.js`)
  - multiple middleware (see `index.js`)
- Redux Devtools: setup (see `index.js`)
- Action Creators to DRY-up the creation of action objects (see `store/actions/*`) 
- Executing asynchronous code prior to dispatching actions to reducers 
  -  Use of Redux-Thunk middleware to warp Action Creators with delayed dispatch functions (see `store/actions/result.js`)
- Lean Reducers
  - Utility functions for cloning and manipulating the state from within Reducers (see `/store/utility.js`)
  - Extract logic out of the Reducer's Action switch statement into helper methods
  