# React Testing Components

## Libraries Introduced
- (Enzyme)[https://www.npmjs.com/package/enzyme] Allows you to manipulate, 
  traverse, and in some ways simulate runtime output of the component.
-  (Jest)[https://www.npmjs.com/package/jest] Delightful JavaScript testing library.
- (react-test-renderer)
- (enzyme-adapter-react-16) 