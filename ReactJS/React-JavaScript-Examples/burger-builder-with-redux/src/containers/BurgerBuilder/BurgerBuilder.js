import React, { Component, Fragment } from 'react';
/* NOTE: Importing the connect HOC to provide this component access to the
* Redux Store. */
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';

import Burger from '../../components/Burger/Burger';
import BuildControl from '../../components/Burger/BuildControl/BuildControl';
import Modal from '../../components/Shared/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';

class BurgerBuilder extends Component {

    state = {
        purchasing: false,
    }

    updatePurchasable = (ingredients) => {
        const ingredientCount = Object.keys(ingredients)
            .map(key => {
                return ingredients[key];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);
        return ingredientCount > 0;
    }

    handlePurchasing = (value) => {
        this.setState({
            purchasing: value
        })
    }

    handlePurchaseContinue = () => {
        this.props.history.push('/checkout');
    }

    controlDisplayState = () => {
        const displayState = {
            ...this.props.ingredients
        };

        for(let key in displayState) {
            displayState[key] = displayState[key] > 0
        }
        return displayState;
    }

    render() {
        return (
            <Fragment>
                <Modal
                    show={this.state.purchasing}
                    modalClosed={() => this.handlePurchasing(false)}
                >
                    <OrderSummary
                        ingredients={this.props.ingredients}
                        totalPrice={this.props.totalPrice}
                        cancelPurchase={() => this.handlePurchasing(false)}
                        continuePurchase={this.handlePurchaseContinue}
                    />
                </Modal>
                <Burger ingredients={this.props.ingredients} />
                <BuildControl
                    price={this.props.totalPrice}
                    purchasable={this.updatePurchasable(this.props.ingredients)}
                    purchasing={() => this.handlePurchasing(true)}
                    displayState={this.controlDisplayState()}
                    ingredientAdded={this.props.onIngredientAdded}
                    ingredientRemoved={this.props.onIngredientRemoved}
                />
            </Fragment>
        );
    }
}


/* NOTE: Setting up mappings between local component props and the data held in the
* Redux Store. */
const mapStateToProps = state => {
    return {
        ingredients: state.ingredients,
        totalPrice: state.totalPrice,
    }
}
/* NOTE: Setting up mappings between the Reducer Action Functions via calls to
* Dispatch that pass Actions with Types and Payloads. */
const mapDispatchToProps = dispatch =>  {
    return {
        onIngredientAdded: (ingredientName) => dispatch({type: actionTypes.ADD_INGREDIENT, ingredientName: ingredientName}),
        onIngredientRemoved: (ingredientName) => dispatch({type: actionTypes.REMOVE_INGREDIENT, ingredientName: ingredientName}),
    }
}

/* NOTE: Wrapping this component with a HOC that is returned from the call to the `connect` function
* of Redux, which sets up the mappings of states stored in the Redux Store and mappings of Dispatch calls
* to Reducer Action Functions.  */
export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);