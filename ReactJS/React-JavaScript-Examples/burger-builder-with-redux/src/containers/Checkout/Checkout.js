import React, { Component } from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from "../../components/Order/ContactData/ContactData";
import {Route} from "react-router-dom";

/* NOTE: Importing Redux connect in order to wrap this component
* and provide it with access to the Redux Store. */
import { connect } from 'react-redux';

class Checkout extends Component {

    handleCancel = () => {
        this.props.history.goBack();
    }

    handleContinue = () => {
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        return (
            <div>
                <CheckoutSummary
                    ingredients={this.props.ingredients}
                    cancel={this.handleCancel}
                    continue={this.handleContinue}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    render={(props) => {
                        return (
                            <ContactData
                                ingredients={this.props.ingredients}
                                price={this.props.totalPrice}
                                {...props}
                            />
                        );
                    }}
                />
            </div>
        );
    }
}

/* NOTE: Setting up mappings between local component props and the data held in the
* Redux Store. */
const mapStateToProps = state => {
    return {
        ingredients: state.ingredients,
        totalPrice: state.totalPrice,
    }
}

/* NOTE: Wrapping this component with a HOC that is returned from the call to the `connect` function
* of Redux, which sets up the mappings of states stored in the Redux Store and mappings of Dispatch calls
* to Reducer Action Functions.  */
export default connect(mapStateToProps)(Checkout);