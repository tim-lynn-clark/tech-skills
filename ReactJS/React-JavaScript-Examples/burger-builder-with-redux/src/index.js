import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from "redux";

import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';

/* NOTE: Setting up Redux Store from Reducer. */
import reducer from "./store/reducers/reducer";
const store = createStore(reducer);

/* NOTE: Using React Router Dom to wrap app so routing is available in all components
* in the component tree. */
const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
