import React, { Component, Fragment } from 'react';
import classes from './Layout.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends Component {

    state = {
        showSideDrawer: false,
    }

    handleSideDrawerToggle = () => {
        this.setState((prevState) => {
            return {showSideDrawer: !prevState.showSideDrawer}
        })
    }

    render() {
        return (
            <Fragment>
                <div>
                    <Toolbar drawerToggleClicked={this.handleSideDrawerToggle}  />
                    <SideDrawer show={this.state.showSideDrawer} toggle={this.handleSideDrawerToggle} />
                    , SideDrawer, Backdrop
                </div>
                <main className={classes.content}>
                    {this.props.children}
                </main>
            </Fragment>
        );
    }
}

export default Layout;