import React, { Fragment } from 'react';
import classes from './SideDrawer.css';
import Logo from '../../Shared/Logo/Logo';
import NavItems from '../NavItems/NavItems';
import Backdrop from '../../Shared/Backdrop/Backdrop';

function SideDrawer(props) {

    const attachedClasses = [classes.SideDrawer];
    if(props.show) {
        attachedClasses.push(classes.Open);
    } else {
        attachedClasses.push(classes.Close);
    }

    return (
        <Fragment>
            <Backdrop clicked={props.toggle} show={props.show} />
            <div className={attachedClasses.join(' ')}>
                <div className={classes.Logo}>
                    <Logo />
                </div>
                <nav>
                    <NavItems />
                </nav>
            </div>
        </Fragment>
    );
}

export default SideDrawer;