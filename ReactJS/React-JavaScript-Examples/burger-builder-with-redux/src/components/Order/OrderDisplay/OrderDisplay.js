import React from 'react';
import classes from './OrderDisplay.css';

function OrderDisplay({order}) {

    const ingredients = [];
    for(let ingredientName in order.ingredients) {
        ingredients.push({
            name: ingredientName,
            amount: order.ingredients[ingredientName]
        });
    }

    const jsx = ingredients.map(ingredient => {
       return <span key={ingredient.name}>{ingredient.name} ({ingredient.amount})</span>
    });


    return (
        <div className={classes.OrderDisplay}>
            <p>Ingredients: {jsx}</p>
            <p>Price: <strong>${order.price.toFixed(2)}</strong> </p>
        </div>
    );
}

export default OrderDisplay;