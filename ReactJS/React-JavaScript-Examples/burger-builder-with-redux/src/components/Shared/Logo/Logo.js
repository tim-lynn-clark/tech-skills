import React from 'react';
import classes from './Logo.css';
import burgerLogo from '../../../assets/images/burger-logo.png';

function Logo (props) {

    return (
        <div className={classes.Logo}>
            <img src={burgerLogo} alt="Burger Builder 1.0"  />
        </div>
    );
}

export default Logo;