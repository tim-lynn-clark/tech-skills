import React, { Fragment } from 'react';
import classes from './OrderSummary.css';
import Button from '../../Shared/Button/Button';

function OrderSummary(props) {
    const summary = Object.keys(props.ingredients)
        .map(key => {
            return (
                <li
                    key={key}
                    className={classes.ingredient}
                >
                    <span>{key}</span>: {props.ingredients[key]}
                </li>
            );
        })

    return (
        <Fragment>
            <h3>Your Order</h3>
            <p>A delicious burger with the following ingredients:</p>
            <ul>
                {summary}
            </ul>
            <p><strong>Total Price: ${props.totalPrice.toFixed(2)}</strong></p>
            <p>Continue to checkout?</p>
            <Button
                btnType='Danger'
                clicked={props.cancelPurchase}
            >
                Cancel
            </Button>
            <Button
                btnType='Success'
                clicked={props.continuePurchase}
            >
                Continue
            </Button>
        </Fragment>
    );
}

export default OrderSummary;