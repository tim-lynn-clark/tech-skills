import axios from 'axios';

const API_KEY = 'e11fa3bb95aeb528162a0fc169517287';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city){
    const url = `${ROOT_URL}&q=${city},us`;
    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    }
    // redux-promise middleware takes over the request promise if a promise is set to the payload property,
    // it stops the action from completing until the promise is resolved then assigns the result of the promise as the state
}