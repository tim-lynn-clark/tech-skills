import {FETCH_WEATHER} from '../actions/index';

export default function(state = [], action){

    switch(action.type){
        case FETCH_WEATHER:
            //never manipulate the state, concat creates a new array that includes the current state plus any new additions
            return state.concat([action.payload.data]);
            //return [ action.payload.data, ...state ]; //ES6 syntax
    }

    return state;
}