export function selectBook(book){
    // SelectBook is an ActionCreator, it needs to return an action
    // Actions are objects with a type property
    return {
        type: 'BOOK_SELECTED',
        payload: book
    }
}