import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectBook} from '../actions/index';
import {bindActionCreators} from 'redux';

class BookList extends Component {

    renderList(){
        return this.props.books.map((book) => {
            return (
                <li
                    key={book.title}
                    className="list-group-item"
                    onClick={ () => { this.props.selectBook(book); } }>
                    {book.title}
                </li>
            )
        });
    }

    render(){
        return (
            <ul className="list-group col-sm-4">
                { this.renderList() }
            </ul>
        )
    }
}

// Object returned by this function is used by react-redux in the connect function
// to map state within redux to the props of a react component
function mapStateToProps(state){
    return {
        books: state.books
    }
}

// Anything returned by this function will end up as props on the BookList container
function mapDispatchToProps(dispatch){
    // Whenever selectBook is called, the result should be passed to all of the reducers
    return bindActionCreators({selectBook: selectBook}, dispatch);
}


// Promote BookList from a component to a container with Redux state.
export default connect(mapStateToProps, mapDispatchToProps)(BookList);