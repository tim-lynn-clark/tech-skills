import React, {Component} from 'react';
import {connect} from 'react-redux';
// import {selectBook} from '../actions/index';
// import {bindActionCreators} from 'redux';

class BookDetail extends Component {

    render(){

        if(!this.props.book){
            return <div>Select a book to get started</div>
        }

        return (
            <div className="">
                <h3>Details for:</h3>
                <div>Title: {this.props.book.title}</div>
                <div>Pages: {this.props.book.pages}</div>
            </div>
        );
    }
}


// Object returned by this function is used by react-redux in the connect function
// to map state within redux to the props of a react component
function mapStateToProps(state){
    return {
        book: state.activeBook
    }
}

// Anything returned by this function will end up as props on the BookList container
// function mapDispatchToProps(dispatch){
//     // Whenever selectBook is called, the result should be passed to all of the reducers
//     return bindActionCreators({selectBook: selectBook}, dispatch);
// }


// Promote BookList from a component to a container with Redux state.
// export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
export default connect(mapStateToProps)(BookDetail);