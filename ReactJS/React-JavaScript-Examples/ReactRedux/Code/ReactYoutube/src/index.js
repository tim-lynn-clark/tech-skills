// Import the React library
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import _ from 'lodash';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
const API_KEY = 'AIzaSyCIqBKAb8_1NnWHU_d7e6ElzAph2a0juus';


// Create new component: this component should produce some HTML
class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            selectedVideo: null,
            videos: []
        };

        this.videoSearch('surfboards');
    }

    videoSearch(term){
        YTSearch({
            key: API_KEY,
            term: term
        }, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            })
        });
    }


    render(){

        //Using lodash.debounce to slow down how often the function is called
        const videoSearch = _.debounce((term) => { this.videoSearch(term)}, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={ this.state.videos } />
            </div>
        );
    }

}

// Insert into DOM
ReactDOM.render(<App />, document.querySelector('.container'));