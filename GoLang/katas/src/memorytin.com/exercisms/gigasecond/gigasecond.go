/*
Package gigasecond implements a simple library manipulating time types
*/
package gigasecond

import (
	"time"
)

// AddGigasecond takes a time type and adds 10^9 seconds to it
// and returns the new time that is exactly 1 gigasecond forward
// in time.
func AddGigasecond(t time.Time) time.Time {
	return t.Add(time.Second * 1e9)
}
