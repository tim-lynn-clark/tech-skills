package main

import "fmt"

//Hands-on exercise #1
//Create your own type “person” which will have an underlying type of “struct” so that it can store the following data:
//first name
//last name
//favorite ice cream flavors
//Create two VALUES of TYPE person. Print out the values, ranging over the elements in the slice which stores the favorite flavors.

type person struct {
	first           string
	last            string
	favoriteFlavors []string
}

func main() {
	p1 := person{
		first:           "Bob",
		last:            "Savage",
		favoriteFlavors: []string{"Vanilla Bean", "Cookies & Cream", "Mint Chocolate Chip"},
	}

	fmt.Println(p1)

	fmt.Printf("%v %v\n", p1.first, p1.last)
	fmt.Printf("\tFavorite Flavors:\n")
	fmt.Printf("\t-----------------\n")
	for _, v := range p1.favoriteFlavors {
		fmt.Printf("\t%v\n", v)
	}
}
