package main

import "fmt"

//Hands-on exercise #8
//Create a program that uses a switch statement with no switch expression specified.

func main() {

	a := 15
	b := 15

	switch {
	case a < b:
		fmt.Println("'a' is smaller than 'b'")
	case b < a:
		fmt.Println("'b' is smaller than 'a'")
	default:
		fmt.Println("'a' and 'b' must be equal")
	}
}
