package main

import "fmt"

//Hands-on exercise #7
//Create a slice of a slice of string ([][]string). Store the following data in the multi-dimensional slice:
//
//"James", "Bond", "Shaken, not stirred"
//"Miss", "Moneypenny", "Helloooooo, James."
//
//Range over the records, then range over the data in each record.

func main() {
	a := []string{"James", "Bond", "Shaken, not stirred"}
	b := []string{"Miss", "Moneypenny", "Helloooooo, James."}
	c := [][]string{a, b}

	fmt.Println(c)

	for i, v := range c {
		fmt.Println(v)
		for _, k := range c[i] {
			fmt.Println(k)
		}
	}
}
