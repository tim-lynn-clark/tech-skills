package main

import "fmt"

//Hands-on exercise #4
//Write a program that
//assigns an int to a variable
//prints that int in decimal, binary, and hex
//shifts the bits of that int over 1 position to the left, and assigns that to a variable
//prints that variable in decimal, binary, and hex

func main() {

	a := 2
	fmt.Printf("%d, %b, %#x\n", a, a, a)

	b := a << 1
	fmt.Printf("%d, %b, %#x\n", b, b, b)

	// Just for fun
	c := a << 4
	fmt.Printf("%d, %b, %#x\n", c, c, c)
}
