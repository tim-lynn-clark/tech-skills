package main

import "fmt"

//Hands-on exercise #9
//Using the code from the previous example, add a record to your map. Now print the map out using the “range” loop

func main() {
	x := map[string][]string{}
	x[`bond_james`] = []string{`Shaken, not stirred`, `Martinis`, `Women`}
	x[`moneypenny_miss`] = []string{`James Bond`, `Literature`, `Computer Science`}
	x[`no_dr`] = []string{`Being evil`, `Ice cream`, `Sunsets`}

	fmt.Println(x)

	x[`guy_new`] = []string{`tech`, `cars`, `boats`}

	fmt.Println(x)

	for i, v := range x {
		fmt.Printf("Key: %v\n", i)
		for j, k := range v {
			fmt.Printf("\tIndex: %v, Value: %v\n", j, k)
		}
	}
}
