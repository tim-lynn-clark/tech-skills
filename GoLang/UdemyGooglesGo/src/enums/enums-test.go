package main

import (
	"fmt"
)

func main() {
	fmt.Println("Simulating Enums in Go")

	type Weekday int

	const (
		Sunday    Weekday = 0
		Monday    Weekday = 1
		Tuesday   Weekday = 2
		Wednesday Weekday = 3
		Thursday  Weekday = 4
		Friday    Weekday = 5
		Saturday  Weekday = 6
	)

	fmt.Println(Sunday)
	fmt.Println(Wednesday)
	fmt.Println(Friday)
}
