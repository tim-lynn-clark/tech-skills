package main

import "fmt"

func main() {
	result := sum(5, 6, 7, 8, 9)
	fmt.Println("The total is ", result)

	// Variadic parameters also allow you to pass no arguments to the function call
	// this is because the ... means zero or more arguments
	results2 := sum()
	fmt.Println(results2)
}

// func (r receiver) identifier(parameters) (return(s)) {...}

// Variadic parameters allow a function to accept an unlimited number of
// arguments of a specific type. Comes in as a slice of the type containing
// all of the parameters passed in.
// ... is an operator
func sum(x ...int) int {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	fmt.Println(len(x))
	fmt.Println(cap(x))
	fmt.Printf("%T\n", x)

	sum := 0
	for i, v := range x {
		sum += v
		fmt.Println("for item in index position, ", i, " we are now adding, ", v, " to the total, which is now ", sum)
	}

	return sum
}
