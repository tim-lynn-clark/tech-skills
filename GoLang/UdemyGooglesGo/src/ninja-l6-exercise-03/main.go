package main

import "fmt"

//Hands-on exercise #3
//Use the “defer” keyword to show that a deferred func runs after the func containing it exits.

func openNetworkFileStream() {
	fmt.Println("Opening file stream")
}

func closeNetworkFileStream() {
	fmt.Println("Closing file stream")
}

func main() {

	openNetworkFileStream()
	defer closeNetworkFileStream()

	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("Editing file")
	fmt.Println("DONE EDITING")
}
