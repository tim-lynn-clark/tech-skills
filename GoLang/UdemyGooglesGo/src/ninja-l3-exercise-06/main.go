package main

import "fmt"

//Hands-on exercise #6
//Create a program that shows the “if statement” in action.

func main() {
	a := 55
	b := 65

	if a < b {
		fmt.Println(a)
	}
}
