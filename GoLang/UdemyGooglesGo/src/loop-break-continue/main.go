package main

import "fmt"

func main() {
	x := 1

	// Go does not have a while loop
	// must use break to simulate it
	for {
		if x > 9 {
			break
		}

		fmt.Println(x)
		x++
	}

	// Only prints if x is even
	x = 1
	for {
		x++
		if x > 9 {
			break
		}

		if x%2 != 0 {
			continue
		}
		fmt.Println(x)
	}

	// Only prints if x is odd
	x = 1
	for {
		x++
		if x > 9 {
			break
		}

		if x%2 == 0 {
			continue
		}
		fmt.Println(x)
	}

	fmt.Println("done.")
}
