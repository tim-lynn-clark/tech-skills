package main

import "fmt"

// Looks like a class in other languages
// Composite data type allowing the composition of other data types into a single type
type person struct {
	first string
	last  string
	age   int
}

type secretAgent struct {
	person
	licenseToKill bool
}

func main() {
	p1 := person{
		// Composite literal syntax
		first: "Bob",
		last:  "Savage",
		age:   30,
	}

	fmt.Println(p1)

	// Properties are accessible via dot notation
	fmt.Println(p1.first)
	fmt.Println(p1.last)
	fmt.Println(p1.age)

	sa1 := secretAgent{
		person: person{
			first: "James",
			last:  "Bond",
			age:   35,
		},
		licenseToKill: true,
	}

	// The fields of the encapsulated type 'person'
	// were promoted to first class fields of the 'secretAgent' type
	fmt.Println(sa1.first)
	fmt.Println(sa1.last)
	fmt.Println(sa1.age)
	fmt.Println(sa1.licenseToKill)

}
