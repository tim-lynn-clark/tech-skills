package main

import (
	"encoding/json"
	"fmt"
)

// Marshaling fails if the type (in this case a struct) supplied
// does not have capitalized fields so they can be accessed externally to the package
type person struct {
	First string
	Last  string
	Age   int
}

func main() {
	p1 := person{
		First: "James",
		Last:  "Bond",
		Age:   32,
	}

	p2 := person{
		First: "Miss",
		Last:  "Moneypenny",
		Age:   27,
	}

	people := []person{p1, p2}
	fmt.Println(people)

	// Marshal slice containing people into JSON data
	bs, err := json.Marshal(people)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(bs))

	// Unmarshal JSON data into slice of people containing person structs
	var people2 []person
	err = json.Unmarshal(bs, &people2)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Printf("%+v", people2)
}
