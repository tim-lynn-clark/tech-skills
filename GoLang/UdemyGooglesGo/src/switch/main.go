package main

import "fmt"

func main() {
	// Expression Switch
	n := "Bond"
	switch n {
	case "Moneypenny", "Bond", "Do No":
		fmt.Println("Welcome Miss Money, Bond, or Dr. No")
	case "M":
		fmt.Println("Hello Miss M, do you want your morning coffee?")
	case "Q":
		fmt.Println("Hello Mr. Q, would you like to see the latest gear?")
	default:
		fmt.Println("Go away or die")
	}

	// Switch as an if-else-if-else chain
	g := 10
	switch {
	case g >= 5:
		fmt.Println("G is 5 or greater")
	case g == 6:
		fmt.Println("G is 6")
	default:
		fmt.Println("G is not important to me")
	}
}
