package main

import "fmt"

//Hands-on exercise #4
//Create a for loop using this syntax
//for { }
//Have it print out the years you have been alive.

func main() {
	birthYear := 1977
	for {
		if birthYear > 2020 {
			break
		}
		fmt.Println(birthYear)
		birthYear++
	}
}
