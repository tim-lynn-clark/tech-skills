package main

import "fmt"

//Hands-on exercise #2
//create a func with the identifier foo that
//takes in a variadic parameter of type int
//pass in a value of type []int into your func (unfurl the []int)
//returns the sum of all values of type int passed in
//create a func with the identifier bar that
//takes in a parameter of type []int
//returns the sum of all values of type int passed in

func foo(x ...int) int {
	sum := 0
	for _, v := range x {
		sum += v
	}

	return sum
}

func bar(x []int) int {
	sum := 0
	for _, v := range x {
		sum += v
	}

	return sum
}

func main() {
	a := []int{1, 2, 3, 4, 5, 6}
	results := foo(a...)
	fmt.Println(results)

	results = bar(a)
	fmt.Println(results)
}
