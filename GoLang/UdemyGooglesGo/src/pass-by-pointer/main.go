package main

import "fmt"

func main() {
	// Pointers can be used when you have:
	// a large chunk of data that needs to be passed around the application
	x := 0
	fmt.Println(">> In main")
	fmt.Println(&x)
	fmt.Println(x)
	foo(&x)
	fmt.Println(">> In main")
	fmt.Println(&x)
	fmt.Println(x)
}

func foo(y *int) {
	fmt.Println(">> In foo")
	fmt.Println(y)
	fmt.Println(*y)
	*y = 43
	fmt.Println(">> In foo")
	fmt.Println(y)
	fmt.Println(*y)
}
