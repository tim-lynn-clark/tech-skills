package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println(runtime.GOOS)   //OS
	fmt.Println(runtime.GOARCH) //Architecture
}
