package main

import "fmt"

//Hands-on exercise #3
//Create a for loop using this syntax
//for condition { }
//Have it print out the years you have been alive.

func main() {
	birthYear := 1977
	for birthYear <= 2020 {
		fmt.Println(birthYear)
		birthYear++
	}
}
