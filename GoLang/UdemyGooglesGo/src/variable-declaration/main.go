package main

import (
	"fmt"
)

// Go Language is:
// - Strongly typed with duck typing of variables at initialization
// - STATIC not dynamic

// Specifies the type of the variable explicitly
var a int

// Will not work. When you do not provide a value for the compiler to
// use to determine the type (duck typing), you are not allowed
// to create the variable
// var a

// Standard variable declaration
// DECLARES a variable and ASSIGNS a VALUE at the same time
var y = 43

// Specifies the type of the variable explicitly
// DECLARES a variable and ASSIGNS a VALUE at the same time
var z int = 65

func main() {

	// Short variable declaration
	// DECLARES a variable and ASSIGNS a VALUE at the same time
	x := "This is a string"

	// If uncommented, this would create an error as we are trying to assign
	// a string to a variable that was initialized with a type of int
	//y = "change to string"

	fmt.Println(a)
	fmt.Printf("%T\n", a)
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	fmt.Println(y)
	fmt.Printf("%T\n", y)
	fmt.Println(z)
	fmt.Printf("%T\n", z)
}
