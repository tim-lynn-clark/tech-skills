package main

import (
	"fmt"
)

const a = 41
const b = 42

const (
	c = 345.3453
	d = "this is a constant"
)

// If uncommented will throw error, const must be given a value at declaration
//const e int

// Program will still compile and run when a const is not used at module scope
const e int = 67

// Program will still compile and run when a variable is not initialized or used at module scope
var f int

func main() {
	fmt.Println(a)
	fmt.Printf("%T\n", a)

	fmt.Println(b)
	fmt.Printf("%T\n", b)

	fmt.Println(c)
	fmt.Printf("%T\n", c)

	fmt.Println(d)
	fmt.Printf("%T\n", d)

	// Will throw error when variables are not used in function scope
	//var g int
}
