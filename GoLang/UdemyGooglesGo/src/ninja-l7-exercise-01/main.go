package main

import "fmt"

//Hands-on exercise #1
//Create a value and assign it to a variable.
//Print the address of that value.

func main() {
	x := 56
	fmt.Println(x)
	fmt.Println(&x)
}
