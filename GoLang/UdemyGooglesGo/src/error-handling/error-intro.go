package main

import (
	"fmt"
)

// References:
// - https://golang.org/doc/faq#exceptions
// - https://godoc.org/github.com/pkg/errors
// - https://blog.golang.org/error-handling-and-go
// - https://blog.golang.org/go1.13-errors

func main() {
	// Println returns the number of bytes written
	// as well as an error a possible error
	n, err := fmt.Println("Hello")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)
}
