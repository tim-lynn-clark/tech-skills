package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	f, err := os.Create("names.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()

	// Creates a reader that can read the bytes
	// from a string
	r := strings.NewReader("James Bond")

	// Pass the reader and the file to write the bytes to
	io.Copy(f, r)
}
