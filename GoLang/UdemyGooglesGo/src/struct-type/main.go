package main

import "fmt"

// Looks like a class in other languages
// Composite data type allowing the composition of other data types into a single type
type person struct {
	first string
	last  string
	age   int
}

func main() {
	p1 := person{
		first: "Bob",
		last:  "Savage",
		age:   30,
	}

	fmt.Println(p1)

	// Properties are accessible via dot notation
	fmt.Println(p1.first)
	fmt.Println(p1.last)
	fmt.Println(p1.age)
}
