package main

import "fmt"

//Hands-on exercise #9
//Create a program that uses a switch statement with the switch expression specified as a variable of TYPE string with the IDENTIFIER “favSport”.

func main() {
	favSport := "UFC"

	switch favSport {
	case "UFC":
		fmt.Println("Ultimate Fighting, I love it!")
	case "Golf":
		fmt.Println("Ug, it's like playing baseball with yourself")
	default:
		fmt.Println("Um, never heard of that sport before")
	}
}
