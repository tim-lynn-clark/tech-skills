package main

import "fmt"

func main() {
	// When allocating a slice using make, you are determining the total capacity
	// that will be used when allocating the underlying array belonging to the slice
	// the 10 in this example sets the starting length of the slice
	// the 100 in this example sets the total length of the underlying array
	// By doing this, if you know what the total length of the slice may be, you are
	// optimizing the slice so that the underlying array does not need to, when appended to,
	// be copied to a new array and then destroyed
	x := make([]int, 10, 100)
	fmt.Println(x)
	// length of the slice
	fmt.Println(len(x))
	// total capacity of the slice
	fmt.Println(cap(x))

	x[0] = 45
	x[5] = 66
	// if uncommented an error will be thrown even though the capacity is 100
	// for the underlying array, this is because we are using the slice
	//x[10] = 44

	// We must still use the append function to add to the slice
	// the slice will increase its length based on the number of elements added
	// but the underlying array will NOT be copied to a new array and destroyed,
	// since it has plenty of indexes to use so long as you do not go over its capacity
	x = append(x, 5, 4, 6)
	fmt.Println(x)
	fmt.Println(len(x))
	fmt.Println(cap(x))

	// NOTE: if you do end up going over the original capacity, you were wrong,
	// when the append executes that will overshoot the capacity,
	// the go will simply increase the capacity and revert to its original expensive array copying
	// method for increasing the size of the slice
}
