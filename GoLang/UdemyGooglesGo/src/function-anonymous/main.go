package main

import "fmt"

func main() {

	// Anonymous self-executing functions
	func() {
		fmt.Println("I executed myself fool.")
	}()

	// With parameters
	func(name string) {
		fmt.Println("I executed myself,", name)
	}("bob")

	// n number of parameters
	func(x ...int) {
		fmt.Println("I executed myself fool and I did it while yelling: ")
		for _, v := range x {
			fmt.Println(v)
		}
	}(2, 3, 4, 5, 6)

	// Functional Expression
	// Placed into a variable for reference
	myFunc := func() {
		fmt.Println("I am stuck in a variable until called...")
	}
	myFunc()
}
