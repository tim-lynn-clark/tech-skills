package main

import "fmt"

func main() {
	x := []int{5, 8, 6, 7, 345, 67, 34, 2, 4, 5657}
	fmt.Println(x)

	// Deleting from a slice using append and slice ranges
	// deletes index 3 & 4
	x = append(x[:3], x[5:]...)
	fmt.Println(x)
}
