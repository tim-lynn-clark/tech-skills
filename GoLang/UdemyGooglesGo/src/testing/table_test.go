package testing

import "testing"

func mySum(xi ...int) int {
	sum := 0
	for _, v := range xi {
		sum += v
	}
	return sum
}

func TestMySum(t *testing.T) {

	type test struct {
		data   []int
		answer int
	}

	tests := []test{
		{
			data:   []int{1, 2},
			answer: 3,
		},
		{
			data:   []int{1, 2, 65},
			answer: 68,
		},
		{
			data:   []int{1},
			answer: 1,
		},
	}

	for _, test := range tests {
		v := mySum(test.data...)
		if v != test.answer {
			t.Error("Expected", test.answer, "got", v)
		}
	}

}
