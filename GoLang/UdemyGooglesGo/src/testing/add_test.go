package testing

import "testing"

func mySum(xi ...int) int {
	sum := 0
	for _, v := range xi {
		sum += v
	}
	return sum
}

func TestMySum(t *testing.T) {
	v := mySum(3, 4, 5, 7)
	if v != 12 {
		t.Error("Expected 12, got", v)
	}
}
