package main

import (
	"fmt"
	"sync"
)

//Hands-on exercise #1
//in addition to the main goroutine, launch two additional goroutines
//each additional goroutine should print something out
//use waitgroups to make sure each goroutine finishes before your program exists

var wg sync.WaitGroup

func main() {

	wg.Add(2)

	go doThisThing("Routine 2 (after main Go routine")
	go doThisThing("Routine 3 (after main routine 2")

	wg.Wait()
}

func doThisThing(thing string) {
	for i := 0; i < 100; i++ {
		fmt.Println("I am doing", thing)
	}
	wg.Done()
}
