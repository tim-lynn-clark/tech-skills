package main

import (
	"fmt"
)

var a int

// Creating your own type
type hotdog int

var b hotdog

func main() {
	a = 43
	fmt.Println(a)
	fmt.Printf("%T\n", a)

	b = 55
	fmt.Println(b)
	fmt.Printf("%T\n", b)

	// If uncommented, will throw an error
	// because go is a static language
	a = b

}
