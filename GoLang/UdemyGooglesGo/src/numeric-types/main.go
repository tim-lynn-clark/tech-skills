package main

import (
	"fmt"
)

// Go will optimize the uses of int, uint
// based on the architecture that is the build target
// switching from 32 to 64
var x int
var y float64

var a int8 = -128

// Note:
// byte = uint8
// rune = int32

func main() {
	x = 42
	y = 42.4564564

	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(a)

	fmt.Printf("%T\n", x)
	fmt.Printf("%T\n", y)
	fmt.Printf("%T\n", a)
}
