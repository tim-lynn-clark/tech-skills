package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func main() {

	fmt.Println("CPUs\t\t", runtime.NumCPU())
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	var counter int64
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			runtime.Gosched()
			atomic.AddInt64(&counter, 1)
			v := atomic.LoadInt64(&counter)
			fmt.Println(v)
			wg.Done()
		}()
		fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	}

	wg.Wait()
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	fmt.Println("Final Counter Value:", counter)
}
