package main

import "fmt"

// Print ascii characters 1-127 from their decimal form to actual character
// using a loop

func main() {
	n := 1

	for {

		if n > 127 {
			break
		}

		fmt.Printf("%d, %b, %#x, %U, %c\n", n, n, n, n, n)
		n++
	}
}
