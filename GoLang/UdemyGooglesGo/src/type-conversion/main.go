package main

import "fmt"

type number int

func main() {

	var x number = 45
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	var y int
	// Type conversion
	y = int(x)
	fmt.Println(y)
	fmt.Printf("%T\n", y)
}
