package main

import "fmt"

func main() {

	// Structure of a loop
	//for init; condition; post {
	//
	//}

	for i := 0; i <= 100; i++ {
		fmt.Println(i)
	}
}
