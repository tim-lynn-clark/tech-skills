package main

import (
	"fmt"
)

// Iota is reset to 0 at the beginning of each const declaration
// it increments by 1 each time it is used within the const declaration
const (
	a = iota
	b
	c
)

const (
	d = iota
	e
	f
)

func main() {
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
}
