package main

import "fmt"

//Hands-on exercise #7
//Assign a func to a variable, then call that func

func main() {

	myFunc := func(n int) {
		fmt.Println(n * n)
	}

	myFunc(5)
	myFunc(25)
}
