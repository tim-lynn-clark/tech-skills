package main

import "fmt"

//Hands-on exercise #2
//This exercise will reinforce our understanding of method sets:
//create a type person struct
//	attach a method speak to type person using a pointer receiver
//	*person
//	create a type human interface
//	to implicitly implement the interface, a human must have the speak method
//	create func “saySomething”
//	have it take in a human as a parameter
//	have it call the speak method
//	show the following in your code
//	you CAN pass a value of type *person into saySomething
//	you CANNOT pass a value of type person into saySomething
//	here is a hint if you need some help

type person struct{}

func (p *person) speak() {
	fmt.Println("I am saying something to you as a person.")
}

func saySomething(h human) {
	h.speak()
}

type human interface {
	speak()
}

func main() {
	tim := person{}

	// Throws an error
	// Cannot pass a human value
	//saySomething(tim)

	// Works
	// Can pass a reference to a human value
	saySomething(&tim)
}
