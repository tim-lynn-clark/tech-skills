package main

import "fmt"

//Hands-on exercise #2
//Using the following operators, write expressions and assign their values to variables:
//==
//<=
//>=
//!=
//<
//>
//Now print each of the variables.

func main() {
	a := 5 == 5
	fmt.Println(a)

	b := 5 <= 6
	fmt.Println(b)

	c := 5 >= 6
	fmt.Println(c)

	d := 5 != 6
	fmt.Println(d)

	e := 5 < 6
	fmt.Println(e)

	f := 5 > 6
	fmt.Println(f)

}
