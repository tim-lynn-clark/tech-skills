package main

import "fmt"

func main() {
	// Functional Expression
	// Placed into a variable for reference
	myFunc := func() {
		fmt.Println("I am stuck in a variable until called...")
	}
	myFunc()
}
