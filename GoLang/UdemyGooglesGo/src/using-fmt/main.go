package main

import (
	"fmt"
)

var y = 911

func main() {

	// print a single line
	fmt.Println(y)

	// Print with formatting (no new line added)
	fmt.Printf("Type: %T\n", y)
	fmt.Printf("Binary: %b\n", y)
	fmt.Printf("Hex: %x\n", y)
	fmt.Printf("Hex-0x: %#x\n", y)

	// Printf multiple arguments
	fmt.Printf("Type: %T\n Binary: %b\n Hex: %x\n Hex-0x: %#x\n", y, y, y, y)

	// Catch the return value of Sprintf
	s := fmt.Sprintf("Type: %T\n Binary: %b\n Hex: %x\n Hex-0x: %#x\n", y, y, y, y)
	fmt.Println(s)
}
