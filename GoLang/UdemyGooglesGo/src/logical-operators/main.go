package main

import "fmt"

func main() {
	// && both sides of the expression must evaluate to true
	// for the expression to evaluate to true
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(false && true)
	fmt.Println("\n")
	// || valuates to true if:
	// both sides of the expression equate to true
	// only the left side equates to true
	// only the right side equates to true
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(false || true)
	fmt.Println(false || false)
	fmt.Println("\n")
	// ! (not operator) gives you the opposite of the expression following it
	fmt.Println(!true)
	fmt.Println(!false)
}
