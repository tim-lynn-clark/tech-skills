package main

import "fmt"

func main() {

	// Slices are initialized without a size and it will be populated with the passed in values
	// Composite literal -> x := []type{values}
	x := []int{5, 7, 3, 5, 7}
	fmt.Println(x)
	// but you will get an error when you try to index it
	//fmt.Println(x[0])
	x = append(x, 1)
	fmt.Println(x)
	// Can be indexed
	fmt.Println(x[0])
}
