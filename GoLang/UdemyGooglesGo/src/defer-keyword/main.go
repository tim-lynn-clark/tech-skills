package main

import "fmt"

func main() {

	// https://blog.golang.org/defer-panic-and-recover

	/* Defer statements
	* A defer statement invokes a function whose execution is deferred to the moment
	* the surrounding function returns, either because the surrounding function executed
	* a return statement, reached the end of its function body, or because the corresponding goroutine is panicking
	 */

	// NOTE: useful when dealing with streams as you can immediately defer call the
	// close function of that stream after you open it. Instead of waiting until the end
	// of a chain of code and then forgetting to close the stream later on.

	openMyFile()
	defer closeMyFile()

	readFile()
	editFile()
	readFile()
	editFile()
	readFile()
	editFile()
	readFile()
	editFile()
	readFile()
	editFile()
}

func readFile() {
	fmt.Println("...Reading open file")
}

func editFile() {
	fmt.Println("...Editing open file")
}

func openMyFile() {
	fmt.Println("Opening a file stream for reading")
}

func closeMyFile() {
	fmt.Println("Closing the open file stream after reading")
}
