package main

import "fmt"

func main() {
	m := map[string]int{
		"James":           32,
		"Miss Moneypenny": 27,
	}
	fmt.Println(m)

	fmt.Println(m["James"])
	fmt.Println(m["Miss Moneypenny"])

	// If you ask for a key that is not in the map
	// you will get the zero value for the type used for the value
	fmt.Println(m["Not Mapped"])

	// comma OK idiom
	// Common method for checking if the key actually exists in the map
	// before trying to use the value
	v, ok := m["Not Mapped"]
	fmt.Println(v)
	fmt.Println(ok)

	if v, ok := m["Not Mapped"]; ok {
		fmt.Println("This is the idiomatic IF check", v)
	}

	if v, ok := m["James"]; ok {
		fmt.Println("This is the idiomatic IF check", v)
	}
}
