package main

import "fmt"

//Hands-on exercise #6
//Build and use an anonymous func

func main() {
	func(n int) {
		fmt.Println(n * n)
	}(5)
}
