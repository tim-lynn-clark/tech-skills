package main

import (
	"fmt"
	"runtime"
	"sync"
)

// https://godoc.org/runtime

// https://godoc.org/sync
// https://godoc.org/sync#WaitGroup
var wg sync.WaitGroup

func main() {
	// https://godoc.org/runtime#NumCPU
	// https://godoc.org/runtime#NumGoroutine
	fmt.Println("OS\t\t", runtime.GOOS)
	fmt.Println("ARCH\t\t", runtime.GOARCH)
	fmt.Println("CPUs\t\t", runtime.NumCPU())
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	// Utilizing a WaitGroup prior to spinning off a new go routine
	// the 1 indicates that there is exactly one go routine we are waiting to complete
	wg.Add(1)

	// To make a function run concurrently, you simply add 'go' in front of it
	// Launches a new go routine to execute the foo() function's code
	go foo()
	bar()

	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	// Telling go to wait for the WaitGroup to return before exiting
	// It waits for all items added to the WaitGroup to call Done() before moving on
	wg.Wait()
}

func foo() {
	for i := 0; i < 10; i++ {
		fmt.Println("foo:", i)
	}

	// Notifying the WaitGroup that the go routine has completed its work
	wg.Done()
}

func bar() {
	for i := 0; i < 10; i++ {
		fmt.Println("bar:", i)
	}
}
