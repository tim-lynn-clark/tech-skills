package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	fmt.Println("CPUs\t\t", runtime.NumCPU())
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	counter := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)
	var mut sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			mut.Lock()
			v := counter
			runtime.Gosched()

			v++

			counter = v
			fmt.Println(v)
			mut.Unlock()
			wg.Done()
		}()
		fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	}

	wg.Wait()
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	fmt.Println("Final Counter Value:", counter)
}
