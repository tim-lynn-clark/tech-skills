package main

import "fmt"

//Hands-on exercise #9
//A “callback” is when we pass a func into a func as an argument. For this exercise,
//pass a func into a func as an argument

func makeAsyncCall(route string, callback func(data string)) {
	fmt.Println("Making async call to: ", route)
	fmt.Println("Waiting on network call...")
	fmt.Println("Doing some work...")
	fmt.Println("Doing some work...")
	fmt.Println("DONE")
	callback("[{comment},{comment}]")
}

func receiveSomeData(data string) {
	fmt.Println("Here is the returned data: ", data)
}

func main() {
	makeAsyncCall("/products/comments", receiveSomeData)
}
