package main

import "fmt"

func main() {
	a := 42
	fmt.Println(a)
	fmt.Printf("%T\n", a)
	// & gives you the address in memory where the value is located
	fmt.Println(&a)
	// *int this is a pointer to where an int is stored
	fmt.Printf("%T\n", &a)

	// Will not work because int and *int are completely different types
	//var b int = &a
	// Will work as *int and the pointer that comes out of &a are both pointers to an int
	var b *int = &a
	fmt.Println(b)
	// Using a * on a variable that holds a pointer will give you back the value stored at that location in memory
	fmt.Println(*b)

	// Using the pointer *b and assigning it a new value 43
	// it will also change a since *b is referencing the same memory location as a
	*b = 43
	fmt.Println(a)
}
