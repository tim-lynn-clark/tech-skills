package main

import (
	"fmt"
	"rsc.io/quote"
	"strconv"
)

// NOTE: Program setup using go.mod init
func main() {
	fmt.Println("> This is main")
	fmt.Println("> " + quote.Go())
	fmt.Println("> This is still in 'main'")
	foo()
	fmt.Println("> This is still in 'main'")

	for i := 0; i < 100; i++ {
		fmt.Println("- Inside a loop inside of main: " + strconv.Itoa(i))
	}

	// Short variable declaration := (declares and assigns)
	// - must declare at least one new variable when used
	// - existing variables will be reassigned the new value
	var n int = 0
	// When uncommented will cause the next line to error
	// as no new variables would be declared while using :=
	//e := errors.New("this is an error")
	n, e := fmt.Println("Exiting 'main'")
	fmt.Println(n, e)
}

func foo() {
	// Using _ you can throw away return values that you are not interested in
	// here we throw away the error object returned
	n, _ := fmt.Println(">> This is in 'foo'")
	fmt.Println(">> Foo Println results: ", n)
}
