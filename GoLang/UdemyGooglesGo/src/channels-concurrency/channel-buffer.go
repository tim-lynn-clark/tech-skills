package main

import "fmt"

func main() {

	// Declares a channel that only allows a single int to live on it
	c := make(chan int, 1)

	// Passing a single int onto the channel
	c <- 42

	// Pulling the single int off the channel
	fmt.Println(<-c)
}
