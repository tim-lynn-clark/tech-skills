package main

import "fmt"

func main() {

	// Declares a channel that only allows a single int to live on it
	c := make(chan int, 1)

	// Passing a single int onto the channel
	c <- 42

	// When trying to pass another value onto the channel,
	// outside of its allowed buffer, the channel will block
	// until there is a free spot in the buffer
	c <- 55

	// Pulling the single int off the channel
	fmt.Println(<-c)
}
