package main

import "fmt"

func main() {

	// Declares a channel that only allows a single int to live on it
	c := make(chan int, 2)

	// Passing a single int onto the channel
	c <- 42
	c <- 55

	x := <-c
	fmt.Println("pulling from channel:", x)
	x = <-c
	fmt.Println("pulling from channel:", x)

	// fatal error: all goroutines are asleep - deadlock!
	// the channel buffer is empty and an attempt to pull from it
	// causes a deadlock
	fmt.Println(<-c)
}
