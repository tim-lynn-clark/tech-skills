package main

import "fmt"

func main() {

	// https://gobyexample.com/channels
	// Channels are the pipes that connect concurrent goroutines.
	//You can send values into channels from one goroutine and
	//receive those values into another goroutine.

	// Creating a channel onto which integers can be placed
	c := make(chan int)

	// Attempting to place the integer 42 onto the channel
	c <- 42

	// Execution of main stops at previous line of execution
	// because channels block if the number of spaces in the
	// buffer is not set at creation. (see channel-buffer)
	//
	// fatal error: all goroutines are asleep - deadlock!
	// goroutine 1 [chan send]:

	fmt.Println(<-c)
}
