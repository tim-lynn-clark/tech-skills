package main

import "fmt"

func main() {

	// Executing in the Go MAIN routine

	// Creates a new channel onto which strings can be passed
	// between Go routines
	messages := make(chan string)

	myMessage := "ping"
	anotherMessage := "ping ping"

	// Anonymous function being executed in a new go routine
	go func() {
		// myMessage variable being passed to the routine
		messages <- myMessage
	}()

	// Anonymous function being executed in a new go routine
	go func() {
		// myMessage variable being passed to the routine
		messages <- anotherMessage
	}()

	// String message being pulled off of the channel
	// Channels are LIFO queues
	msg := <-messages
	aMsg := <-messages

	// Unlike WaitGroups, when a channel is used within a routine
	// the function in which they are used will automatically wait

	fmt.Println(msg)
	fmt.Println(aMsg)
}
