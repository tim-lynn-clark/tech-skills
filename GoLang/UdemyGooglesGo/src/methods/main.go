package main

import "fmt"

type person struct {
	first string
	last  string
}

type secretAgent struct {
	person
	licenseToKill bool
}

// func (r receiver) identifier(parameters) (return(s)) { code }
//func (s secretAgent) speak() {
//	fmt.Println("I am", s.first, s.last)
//}

// You can also add the function to an embedded type as well
// and still get to it as a top-level method of the outer object
func (s person) speak() {
	fmt.Println("I am", s.first, s.last)
}

func main() {
	sa1 := secretAgent{
		person: person{
			first: "James",
			last:  "Bond",
		},
		licenseToKill: true,
	}

	fmt.Println(sa1)
	sa1.speak()
}
