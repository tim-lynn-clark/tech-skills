package main

import (
	"fmt"
)

func main() {

	// Can be done with double quotes
	s := "Hello Go!"
	fmt.Println(s)
	fmt.Printf("%T\n", s)

	// Can be done with back-ticks as a string literal
	// so any characters within the string are included
	// in this example the tabs used to make the code pretty
	// are printed out in the string
	sl := `Hello Go!, 
	here is some more 
	text on another line`
	fmt.Println(sl)
	fmt.Printf("%T\n", sl)

	// String are literally a slice of bytes
	bs := []byte(s)
	fmt.Println(bs)
	fmt.Printf("%T\n", bs)

	// Prints out the ASCii character codes
	for i := 0; i < len(s); i++ {
		fmt.Printf("%v, ", s[i])
	}

	fmt.Println("")

	// Print out each character in UTF8 code
	for i := 0; i < len(s); i++ {
		fmt.Printf("%#U, ", s[i])
	}

	// U+0048 is RUNE
}
