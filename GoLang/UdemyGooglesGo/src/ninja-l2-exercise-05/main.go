package main

import "fmt"

//Hands-on exercise #5
//Create a variable of type string using a raw string literal. Print it.

func main() {

	a := `this is a raw string literal...
every line...
	including tabs...
will be...
interpreted...
and will be printed.`
	fmt.Println(a)
}
