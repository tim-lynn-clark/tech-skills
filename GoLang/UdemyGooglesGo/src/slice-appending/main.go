package main

import "fmt"

func main() {
	x := []int{5, 8, 6, 7}
	fmt.Println(x)

	// Append a single element
	x = append(x, 99)
	fmt.Println(x)

	// Append many elements
	x = append(x, 99, 34, 33, 65, 87)
	fmt.Println(x)

	y := []int{456, 3453, 678, 789}
	fmt.Println(y)
	// Appending a slice to a slice, using ... to unpack the slice
	// variadic parameters (allowing n number of parameters to be passed as arguments to a function
	x = append(x, y...)
	fmt.Println(x)
	fmt.Println(y)
}
