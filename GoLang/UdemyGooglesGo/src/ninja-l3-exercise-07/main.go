package main

import "fmt"

//Hands-on exercise #7
//Building on the previous hands-on exercise, create a program that uses “else if” and “else”.

func main() {
	a := 55
	b := 55

	if a < b {
		fmt.Println("'a' is smaller than 'b'")
	} else if b < a {
		fmt.Println("'b' is smaller than 'a'")
	} else {
		fmt.Println("'a' and 'b' must be equal")
	}
}
