package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	fmt.Println("CPUs\t\t", runtime.NumCPU())
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	counter := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	// Intentionally creating race conditions
	// Spinning off 100 individual go routines
	for i := 0; i < gs; i++ {
		// Each executing an anonymous function
		go func() {
			// pulls the current value of the shared counter
			v := counter
			// Stall the execution (way too fast normally)
			// time.Sleep(time.Second)
			// De-prioritizes the execution of this code
			// Tells go if it needs to run something else it can
			// and this code will wait for its turn
			runtime.Gosched()
			// increments the copy of the counter
			v++
			// Assigns it back to the shared counter
			counter = v
			fmt.Println(v)
			wg.Done()
		}()
		fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	}

	fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	fmt.Println(counter)

	// NOTE: Go has a flag that can be used when running a program to check
	// if there are any race conditions within the code
	// go run -race main.go
	// Output should look like below
	// Found 1 data race(s)
}
