package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	fmt.Println("CPUs\t\t", runtime.NumCPU())
	fmt.Println("GoRoutines\t", runtime.NumGoroutine())

	counter := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)

	// https://godoc.org/sync#Mutex
	// Mutex can be used to control race conditions
	var mu sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			// Mutex locks at the beginning of the racing code
			mu.Lock()
			// counter variable is locked down for writing until the mutex surrounding it unlocks
			v := counter
			runtime.Gosched()
			v++
			counter = v
			fmt.Println(v)
			// Mutex unlocks after racing code completes
			mu.Unlock()
			wg.Done()
		}()
		fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	}

	fmt.Println("GoRoutines\t", runtime.NumGoroutine())
	fmt.Println(counter)

	// NOTE: Go has a flag that can be used when running a program to check
	// if there are any race conditions within the code
	// go run -race main.go
	// Output should look like below (nothing as Mutex fixed the race condition)
	//
}
