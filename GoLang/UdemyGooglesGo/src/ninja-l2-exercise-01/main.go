package main

import "fmt"

//Hands-on exercise #1
//Write a program that prints a number in decimal, binary, and hex

func main() {
	a := 77
	fmt.Printf("%d, %b, %#x", a, a, a)

}
