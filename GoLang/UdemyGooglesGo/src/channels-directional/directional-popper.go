package main

import "fmt"

func main() {
	// adding the '<-' operator before the chan within the channel declaration
	// will make the channel a read-only
	// meaning you can only pop values off the channel
	// you cannot push them on
	c := make(<-chan int, 2)

	// These two lines will throw errors
	// invalid operation: c <- 42 (send to receive-only type <-chan int)
	//c <- 42
	//c <- 43

	// These work fine as we are reading from the channel
	fmt.Println(<-c)
	fmt.Println(<-c)
	// A deadlock will occur since the channel does not have anything in it to read

	fmt.Println("-----")
	fmt.Printf("%T\n", c)
}
