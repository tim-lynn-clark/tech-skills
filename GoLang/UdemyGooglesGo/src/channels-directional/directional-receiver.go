package main

import "fmt"

func main() {
	// adding the '<-' operator between the chan and type within the channel declaration
	// will make the channel write-only
	// meaning you can only push values onto the channel
	// you cannot pop them off
	c := make(chan<- int, 2)

	// These work fine as they are placing ints on the channel
	c <- 42
	c <- 43

	// These two lines will throw errors
	// invalid operation: <-c (receive from send-only type chan<- int)
	//fmt.Println(<-c)
	//fmt.Println(<-c)

	fmt.Println("-----")
	fmt.Printf("%T\n", c)
}
