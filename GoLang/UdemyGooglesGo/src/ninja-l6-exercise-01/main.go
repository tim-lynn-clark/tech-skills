package main

import "fmt"

//Hands-on exercise #1
//create a func with the identifier foo that returns an int
//create a func with the identifier bar that returns an int and a string
//call both funcs
//print out their results

func foo() int {
	return 55
}

func bar() (int, string) {
	return 56, "Oh yeah"
}

func main() {
	a := foo()
	fmt.Println(a)

	b, c := bar()
	fmt.Println(b)
	fmt.Println(c)
}
