package main

import (
	"fmt"
)

//Hands-on exercise #1
//Using a COMPOSITE LITERAL:
//create an ARRAY which holds 5 VALUES of TYPE int
//assign VALUES to each index position.
//Range over the array and print the values out.
//Using format printing
//print out the TYPE of the array

func main() {
	x := [5]int{5, 6, 7, 8, 9}

	for _, v := range x {
		fmt.Println(v)
	}

	fmt.Printf("%T", x)
}
