package main

import "fmt"

func main() {
	m := map[string]int{
		"James":           32,
		"Miss Moneypenny": 27,
		"Q":               45,
		"Retiring":        65,
		"Dead":            99,
	}
	fmt.Println(m)

	// Delete with no key check
	delete(m, "Retiring")

	fmt.Println(m)

	// Delete using comma ok idiom
	if v, ok := m["Dead"]; ok {
		fmt.Println(v)
		delete(m, "Dead")
	}

	fmt.Println(m)

	for k, v := range m {
		fmt.Printf("key: %v, value: %v \n", k, v)
	}
}
