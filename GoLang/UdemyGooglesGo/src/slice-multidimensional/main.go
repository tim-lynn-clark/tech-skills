package main

import "fmt"

func main() {

	dinners := []string{"Chili", "Ham", "Tacos", "Fish"}
	fmt.Println(dinners)

	desserts := []string{"Ice Cream", "Cake", "Pie", "Cheesecake"}
	fmt.Println(desserts)

	foods := [][]string{dinners, desserts}
	fmt.Println(foods)
}
