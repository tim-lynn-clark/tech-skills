package main

import "fmt"

func main() {
	// Recursion: function calling itself until a end case is met
	fmt.Println(factorial(3))
	fmt.Println(factorialLoop(3))

	fmt.Println(factorial(5))
	fmt.Println(factorialLoop(5))

	fmt.Println(factorial(6))
	fmt.Println(factorialLoop(6))

	fmt.Println(factorial(8))
	fmt.Println(factorialLoop(8))
}

func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}

func factorialLoop(n int) int {
	total := 1
	for ; n > 0; n-- {
		total *= n
	}
	return total
}
