package main

import "fmt"

//Hands-on exercise #4
//Create and use an anonymous struct.

func main() {
	hippo := struct {
		name     string
		color    string
		isAnimal bool
	}{
		name:     "Miss Sally",
		color:    "Grey",
		isAnimal: true,
	}

	fmt.Println(hippo)
}
