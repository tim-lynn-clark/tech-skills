package main

import "fmt"

func main() {

	// NOTE: Go says don't use Arrays, use Slices instead

	// Arrays must be initialized with a size
	// each index will be populated with the zero value for the specified data type
	var y [5]int
	fmt.Println(y)
	// Zero index as most languages
	y[3] = 99
	fmt.Println(y)
}
