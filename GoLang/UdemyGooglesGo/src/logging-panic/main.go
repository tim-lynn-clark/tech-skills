package main

import (
	"fmt"
	"os"
)

func main() {

	f, err := os.Create("log.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	_, err = os.Open("no-file.txt")
	if err != nil {
		// Writes to standard output
		fmt.Println("Error has occurred:", err)

		// https://godoc.org/builtin#panic
		// Stops normal execution of the current goroutine
		// program is terminated with a non-zero exit code
		// can be controlled by built-in recover function
		panic(err)

		// https://godoc.org/builtin#recover
		// https://blog.golang.org/defer-panic-and-recover

	}
}
