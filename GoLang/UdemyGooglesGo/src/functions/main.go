package main

import (
	"fmt"
)

func main() {
	foo()
	bar("James")
	reply := woo("Bob")
	fmt.Println(reply)
	x, y := mouse("George", "Costanza")
	fmt.Println(x)
	fmt.Println(y)
}

// func (r receiver) identifier(paramters) (return(s)) {...}
func foo() {
	fmt.Println("Hello from foo.")
}

// everything in GO is PASS-BY-VALUE
// Have no return
func bar(s string) {
	fmt.Println("Hello, ", s)
}

// Have one return
func woo(st string) string {
	return fmt.Sprint("Hello from woo, ", st)
}

// Have multiple returns
func mouse(firstName string, lastName string) (string, bool) {
	a := fmt.Sprint(firstName, " ", lastName, ` says, "Hello"`)
	return a, true
}
