package main

import "fmt"

func main() {
	xi := []int{5, 6, 7, 8, 9}
	// Unfurling a slice to pass as multiple arguments to a function call
	result := sum(xi...)
	fmt.Println("The total is ", result)
}

// func (r receiver) identifier(parameters) (return(s)) {...}

// Variadic parameters allow a function to accept an unlimited number of
// arguments of a specific type. Comes in as a slice of the type containing
// all of the parameters passed in.
// ... is an operator
func sum(x ...int) int {
	fmt.Println(x)
	fmt.Printf("%T\n", x)

	sum := 0
	for i, v := range x {
		sum += v
		fmt.Println("for item in index position, ", i, " we are now adding, ", v, " to the total, which is now ", sum)
	}

	return sum
}
