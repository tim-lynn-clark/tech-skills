package main

import (
	"fmt"
	"sort"
)

type Person struct {
	First string
	Age   int
}

//https://godoc.org/sort#Interface
//A type, typically a collection, that satisfies sort.Interface can be sorted
//by the routines in this package. The methods require that the elements of
//the collection be enumerated by an integer index.
type ByAge []Person

func (a ByAge) Len() int           { return len(a) }
func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByAge) Less(i, j int) bool { return a[i].Age < a[j].Age }

//https://godoc.org/sort#Interface
type ByName []Person

func (bn ByName) Len() int           { return len(bn) }
func (bn ByName) Swap(i, j int)      { bn[i], bn[j] = bn[j], bn[i] }
func (bn ByName) Less(i, j int) bool { return bn[i].First < bn[j].First }

func main() {
	p1 := Person{"James", 32}
	p2 := Person{"Moneypenny", 27}
	p3 := Person{"Q", 64}
	p4 := Person{"M", 56}

	people := []Person{p1, p2, p3, p4}

	fmt.Println(people)
	//https://godoc.org/sort#Sort
	//Sort sorts data. It makes one call to data.Len to determine n,
	//and O(n*log(n)) calls to data.Less and data.Swap.
	//The sort is not guaranteed to be stable.
	sort.Sort(ByAge(people))
	fmt.Println(people)

	fmt.Println(people)
	//https://godoc.org/sort#Sort
	sort.Sort(ByName(people))
	fmt.Println(people)

}
