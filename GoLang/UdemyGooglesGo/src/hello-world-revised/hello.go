package main

import (
	"fmt"
	"rsc.io/quote"
	"strconv"
)

// NOTE: Program setup using go.mod init
func main() {
	fmt.Println("> This is main")
	fmt.Println("> " + quote.Go())
	fmt.Println("> This is still in 'main'")
	foo()
	fmt.Println("> This is still in 'main'")

	for i := 0; i < 100; i++ {
		fmt.Println("- Inside a loop inside of main: " + strconv.Itoa(i))
	}

	fmt.Println("Exiting 'main'")
}

func foo() {
	fmt.Println(">> This is in 'foo'")
}
