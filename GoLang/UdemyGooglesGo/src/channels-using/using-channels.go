package main

import (
	"fmt"
)

func main() {

	c := make(chan int)

	// foo is past to a new go routine for concurrent execution
	// useful if foo doe something resource intense and using another
	// processor would be beneficial to speed things up
	go foo(c)

	// bar blocks execution as it waits for the channel to have something
	// in it that it can print
	bar(c)

	fmt.Println("about to exit")

	// NOTE: verify there is no race condition using:
	// go run -race [name-of-script]
}

// Function accepts a channel
// using the <- operator, the channel is being
// cast to a write-only channel
func foo(c chan<- int) {
	c <- 42
}

// Function accepts a channel
// using the <- operator, the channel is being
// cast to a read-only channel
func bar(c <-chan int) {
	fmt.Println(<-c)
}
