package main

import "fmt"

//Hands-on exercise #2
//Take the code from the previous exercise, then store the values of type person
//in a map with the key of last name. Access each value in the map.
//Print out the values, ranging over the slice.

type person struct {
	first           string
	last            string
	favoriteFlavors []string
}

func main() {
	p1 := person{
		first:           "Bob",
		last:            "Savage",
		favoriteFlavors: []string{"Vanilla Bean", "Cookies & Cream", "Mint Chocolate Chip"},
	}
	fmt.Println(p1)

	p2 := person{
		first:           "Alice",
		last:            "Peru",
		favoriteFlavors: []string{"Chocolate", "Rocky Road", "Peanut Butter"},
	}
	fmt.Println(p2)

	peeps := map[string]person{}
	peeps[p1.last] = p1
	peeps[p2.last] = p2

	fmt.Println(peeps)

	for _, person := range peeps {
		fmt.Printf("%v %v\n", person.first, person.last)
		fmt.Printf("\tFavorite Flavors:\n")
		fmt.Printf("\t-----------------\n")
		for _, v := range person.favoriteFlavors {
			fmt.Printf("\t%v\n", v)
		}
	}

}
