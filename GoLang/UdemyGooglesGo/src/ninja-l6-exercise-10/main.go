package main

import "fmt"

//Hands-on exercise #10
//Closure is when we have “enclosed” the scope of a variable in some code block.
//For this hands-on exercise, create a func which “encloses” the scope of a variable

func containsClosure() func() int {
	counter := 0
	return func() int {
		counter++
		return counter
	}
}

func main() {
	counter := containsClosure()
	fmt.Println(counter())
	fmt.Println(counter())
	fmt.Println(counter())
	fmt.Println(counter())
	fmt.Println(counter())

}
