package main

import "fmt"

func main() {

	x := 2
	fmt.Printf("%d\t\t%b\n", x, x)

	// Bit Shifting Operator <<
	// changes from 2 to 4
	y := x << 1
	fmt.Printf("%d\t\t%b\n", y, y)

	// changes from 4 to 8
	w := y << 1
	fmt.Printf("%d\t\t%b\n", w, w)

}
