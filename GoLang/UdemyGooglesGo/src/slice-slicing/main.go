package main

import "fmt"

func main() {
	x := []int{5, 8, 6, 7}
	fmt.Println(x)

	// : is used to set a range to use to slice the slice
	// 1: will return a slice that begins at index 1 to the end
	fmt.Println(x[1:])
	// 1:3 will return a slice beginning at 1 to index 3 (exclusive)
	fmt.Println(x[1:3])

	length := len(x)
	for i := 0; i < length; i++ {
		fmt.Println(x[i])
	}
}
