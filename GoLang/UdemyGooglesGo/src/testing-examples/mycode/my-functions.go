// Provides examples of documentation and testing through
// comments and code examples
package mycode

// MySum function will take in 'n' number of int parameters
// add them together and return the sum as an int
func MySum(xi ...int) int {
	sum := 0
	for _, v := range xi {
		sum += v
	}
	return sum
}
