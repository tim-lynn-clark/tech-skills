package mycode

import (
	"fmt"
	"testing"
)

// This is an example of how to use the MySum function.
// This example acts as documentation but is also a full blown test
// that can be executed with `go test` command
func ExampleMySum() {
	fmt.Println(MySum(2, 3))
	// Output:
	// 5
}

// This is test of the MySum function
func TestMySum(t *testing.T) {

	type test struct {
		data   []int
		answer int
	}

	tests := []test{
		{
			data:   []int{1, 2},
			answer: 3,
		},
		{
			data:   []int{1, 2, 65},
			answer: 68,
		},
		{
			data:   []int{1},
			answer: 1,
		},
	}

	for _, test := range tests {
		v := MySum(test.data...)
		if v != test.answer {
			t.Error("Expected", test.answer, "got", v)
		}
	}

}
