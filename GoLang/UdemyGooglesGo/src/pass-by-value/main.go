package main

import "fmt"

func main() {
	// To start EVERYTHING IN GO IS PASS BY VALUE
	x := 0
	fmt.Println("In main")
	fmt.Println(&x)
	fmt.Println(x)
	foo(x)
	fmt.Println("In main")
	fmt.Println(&x)
	fmt.Println(x)
}

func foo(y int) {
	fmt.Println("In foo")
	fmt.Println(&y)
	fmt.Println(y)
	y = 43
	fmt.Println(&y)
	fmt.Println(y)
}
