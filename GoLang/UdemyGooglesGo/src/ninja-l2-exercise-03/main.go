package main

import "fmt"

//Hands-on exercise #3
//Create TYPED and UNTYPED constants. Print the values of the constants.

const (
	a     = 45
	b int = 67
)

func main() {
	fmt.Println(a, b)
}
