package main

import (
	"fmt"
)

// Scope of x is package level
// it is accessible by all functions within this package
var x int

func foo() {
	x++
}

func main() {
	fmt.Println(x)
	x++
	fmt.Println(x)
	foo()
	fmt.Println(x)

	// Scope of a is within the main function
	a := 34
	fmt.Println(a)
	a++
	fmt.Println(a)

	// Using a closure
	incroA := incrementor()
	incroB := incrementor()

	// Each instance of the incrementor function has its own counter
	fmt.Println(incroA())
	fmt.Println(incroB())
	fmt.Println(incroA())
	fmt.Println(incroA())
	fmt.Println(incroB())
	fmt.Println(incroB())
}

// Closure example
func incrementor() func() int {
	// x is enclosed in the incrementor function
	var x int = 0
	return func() int {
		// x is accessible within the anonymous function
		x++
		return x
	}
}
