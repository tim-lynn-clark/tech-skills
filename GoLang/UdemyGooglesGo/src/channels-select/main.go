package main

import "fmt"

func main() {
	even := make(chan int)
	odd := make(chan int)
	quit := make(chan int)

	// write to channels
	go write(even, odd, quit)

	// read from channels
	read(even, odd, quit)

	fmt.Println("about to exit")

	//NOTE: verify there is no race condition
	// go run -race [nameofscript]
}

func write(even, odd, quit chan<- int) {
	for i := 0; i < 100; i++ {
		if i%2 == 0 {
			even <- i
		} else {
			odd <- i
		}
	}

	close(even)
	close(odd)
	quit <- 0
}

func read(even, odd, quit <-chan int) {
	for {
		select {
		case v := <-even:
			fmt.Println("from the even channel:", v)
		case v := <-odd:
			fmt.Println("from the odd channel:", v)
		case v := <-quit:
			fmt.Println("from the quit channel:", v)
			return
		}
	}
}
