package main

import "fmt"

//Hands-on exercise #8
//Create a func which returns a func
//assign the returned func to a variable
//call the returned func

func doubleTime() func(n int) int {
	return func(n int) int {
		return n * 2
	}
}

func main() {
	f := doubleTime()
	fmt.Println(f(5))
	fmt.Println(f(25))
}
