package main

import (
	"fmt"
)

func main() {

	c := make(chan int)
	go foo(c)
	bar(c)

	fmt.Println("about to exit")

	// NOTE: verify there is no race condition using:
	// go run -race [name-of-script]
}

func foo(c chan<- int) {
	// using loop to push values onto channel
	for i := 0; i < 5; i++ {
		c <- 2 * i
	}
	// the write-only channel needs to be closed after writing is complete
	// or you will end up with a deadlock
	close(c)
}

func bar(c <-chan int) {
	// using a range loop to iteratively pop each value out of the channel's buffer
	for v := range c {
		fmt.Println(v)
	}
}
