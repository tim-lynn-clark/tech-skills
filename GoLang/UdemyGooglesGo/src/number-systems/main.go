package main

import (
	"fmt"
)

func main() {
	h := "H"
	fmt.Println(h)

	bs := []byte(h)
	fmt.Println(bs)

	n := bs[0]
	fmt.Println(n)
	fmt.Printf("%T\n", n)
	fmt.Printf("%b\n", n)
	fmt.Printf("%x\n", n)
	fmt.Printf("%#x\n", n)

}
