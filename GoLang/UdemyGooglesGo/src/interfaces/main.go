package main

import "fmt"

// Interface declares the required functions a type
// should have to be included in the interface type group
type human interface {
	speak()
}

type person struct {
	first string
	last  string
}

func (s person) speak() {
	fmt.Println("I am", s.first, s.last, "and I am a person")
}
func (s person) scream() {
	fmt.Println("Please nooooooo............")
}

type secretAgent struct {
	person
	licenseToKill bool
}

func (s secretAgent) speak() {
	fmt.Println("I am", s.first, s.last, "and I am a secret agent")
}
func (s secretAgent) laugh() {
	fmt.Println("I laugh at your methods. Ha.")
}

// Polymorphism through interfaces
func makeEmSpeak(h human) {
	fmt.Println("I called for a human to torture...")
	fmt.Println("What is your name?")
	h.speak()
	// Asserting the type after type checking
	// Allows you to get to type specific methods that are not part of the generic interface
	switch h.(type) {
	case person:
		h.(person).scream()
	case secretAgent:
		h.(secretAgent).laugh()
	}
}

func main() {
	sa1 := secretAgent{
		person: person{
			first: "James",
			last:  "Bond",
		},
		licenseToKill: true,
	}

	fmt.Println(sa1)
	fmt.Printf("%T\n", sa1)
	sa1.speak()

	p1 := person{
		first: "Tim",
		last:  "Clark",
	}

	fmt.Println(p1)
	fmt.Printf("%T\n", p1)
	p1.speak()

	makeEmSpeak(sa1)
	makeEmSpeak(p1)
}
