package main

import "fmt"

func main() {
	// Anonymous struct (was NOT defined as a full type within the module
	// Both the struct definition as well as the initialization of an instance
	// of that struct are combined in the composite literal syntax
	p1 := struct {
		first string
		last  string
		age   int
	}{
		first: "Bob",
		last:  "Savage",
		age:   30,
	}

	fmt.Println(p1)

	// Properties are accessible via dot notation
	fmt.Println(p1.first)
	fmt.Println(p1.last)
	fmt.Println(p1.age)

}
