package main

import "fmt"

func main() {
	x := 40

	if x != 0 {
		fmt.Println("X is not 0")
	}

	if x > 40 {
		fmt.Println("X is greater than 40")
	} else {
		fmt.Println("X is 40 or less than")
	}

	if x == 40 {
		fmt.Println("num is 40")
	} else if x == 41 {
		fmt.Println("num is 41")
	} else if x == 42 {
		fmt.Println("num is 42")
	} else {
		fmt.Println("Not one of the options")
	}
}
