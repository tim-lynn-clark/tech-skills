package main

import (
	"fmt"
	"log"
	"os"
)

func main() {

	f, err := os.Create("log.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	_, err = os.Open("no-file.txt")
	if err != nil {
		// Writes to standard output
		fmt.Println("Error has occurred:", err)
		// Writes to standard output or to a specified log file
		log.SetOutput(f)
		log.Println("Error has occurred:", err)
		// Logs error then terminates the process with an exit code 1
		log.Fatalln(err)
	}
}
