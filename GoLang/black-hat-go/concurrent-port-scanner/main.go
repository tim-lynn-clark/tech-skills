package main

import (
	"fmt"
	"sync"
)

// function that handles all the work passed to the ports channel
func worker(ports chan int, wg *sync.WaitGroup) {
	// Ranges over the ports channel looking for work to do, this continues until the channel is closed
	for p := range ports {
		fmt.Println(p)
		wg.Done()
	}
}

func main() {
	// Using make to create a channel that holds Integers and has a buffer limited 100
	// The channel can only hold 100 pending items before it will block and wait for work to complete
	ports := make(chan int, 100)
	// Creating the WaitGroup that will monitor the work being done for completion by workers
	var wg sync.WaitGroup
	// Creating workers until the cap set on the channel when it was created is reached
	for i := 0; i < cap(ports); i++ {
		// Creating a single worker, providing it with the channel and the WaitGroup in needs to report to when work is completed
		go worker(ports, &wg)
	}
	// Iterating over all the TCP ports
	for i := 1; i <= 1024; i++ {
		// Incrementing the number of active jobs in the WaitGroup
		wg.Add(1)
		// Adding work (ports) to the channel for the workers to work on
		ports <- i
	}
	// Waiting until the WaitGroup has confirmed that all registered work has been compeleted by the workers
	wg.Wait()
	// Closing the channel and thereby shutting down the workers
	close(ports)
}

//func main() {
//	var wg sync.WaitGroup
//	for i := 1; i <= 65535; i++ {
//		wg.Add(1)
//		go func(j int) {
//			defer wg.Done()
//			//address := fmt.Sprintf("scanme.nmap.org:%d", j)
//			address := fmt.Sprintf("127.0.0.1:%d", j)
//			conn, err := net.Dial("tcp", address)
//
//			if err != nil {
//				return
//			}
//
//			conn.Close()
//			fmt.Printf("Dialed: %s \n", address)
//			fmt.Printf("Connection successful: %d open \n", j)
//		}(i)
//	}
//	wg.Wait()
//}
