package main

import (
	"fmt"
	"net"
)

func main() {
	for i := 1; i <= 1024; i++ {
		/* Naive concurrent strategy - launches a go routine for each port
		* but the application exits as soon as the for loop ends and never waits for the
		* the routines to finish and it exits.*/
		go func(j int) {
			address := fmt.Sprintf("scanme.nmap.org:%d", j)
			conn, err := net.Dial("tcp", address)

			fmt.Printf("Dialing: %s \n", address)
			if err != nil {
				return
			}

			conn.Close()
			fmt.Printf("Connection successful: %d open \n", j)
		}(i)
	}
}
