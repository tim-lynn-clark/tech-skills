package main

import (
	"io"
	"log"
	"net"
)

func handler(sourceServer net.Conn) {
	// Connect to blocked website
	destinationServer, err := net.Dial("tcp", "https://osxdaily.com:80"); if err != nil {
		log.Fatalln("Unable to connect to our unreachable host")
	}
	defer destinationServer.Close()

	// Use Go routine to prevent io.Copy from blocking
	go func() {
		// Copy our source's output to the destination
		if _, err := io.Copy(destinationServer, sourceServer); err != nil {
			log.Fatalf("Destination refused output from Source: %s \n", err)
		}
	}() // self-invoking

	// Copy our destination's output back to our source
	if _, err := io.Copy(sourceServer, destinationServer); err != nil {
		log.Fatalf("Source refused output from Destination: %s", err)
	}
}

func main() {
	// Listen on local port
	listener, err := net.Listen("tcp", ":20080"); if err != nil {
		log.Fatalln("Unable to bind to port")
	}

	for {
		conn, err := listener.Accept(); if err != nil {
			log.Fatalln("Unable to accept connection")
		}
		go handler(conn)
	}
}