package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func main() {
	getRequestExample, err := http.Get("http://www.google.com/robots.txt")
	if err != nil {
		log.Fatalln("HTTP GET to Google for robots.txt failed.")
	}
	defer getRequestExample.Body.Close()
	fmt.Println(getRequestExample.Status)

	headRequestExample, err := http.Head("http://www.google.com/robots.txt")
	if err != nil {
		log.Fatalln("HTTP HEAD to Google for robots.txt failed.")
	}
	defer headRequestExample.Body.Close()

	form := url.Values{}
	form.Add("foo", "bar")
	// NOTE: Manual form post
	//postRequestExample, err := http.Post(
	//	"https://www.google.com/robots.txt",
	//	"application/x-www-form-urlencoded",
	//	strings.NewReader(form.Encode()))
	// NOTE: Simplified form post
	postRequestExample, err := http.PostForm(
		"https://www.google.com/robots.txt",
		form)
	if err != nil {
		log.Fatalln("HTTP HEAD to Google for robots.txt failed.")
	}
	defer postRequestExample.Body.Close()

	deleteRequestExample, err := http.NewRequest("DELETE", "http://www.google.com/robots.txt", nil)
	if err != nil {
		log.Fatalln("HTTP DELETE to Google failed for robots.txt.")
	}
	var client http.Client
	response, err := client.Do(deleteRequestExample)
	fmt.Println(response.Status)
	defer deleteRequestExample.Body.Close()
}
