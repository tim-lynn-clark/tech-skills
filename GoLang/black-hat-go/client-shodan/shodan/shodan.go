package shodan

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

type Client struct {
	baseUrl string
	apiKey  string
}

func New() *Client {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln("Error loading .env file")
	}
	shodanApiUrl := os.Getenv("SHODAN_API_URL")
	shodanApiKey := os.Getenv("SHODAN_API_KEY")

	return &Client{baseUrl: shodanApiUrl, apiKey: shodanApiKey}
}
