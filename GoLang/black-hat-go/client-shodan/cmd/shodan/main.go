package main

import (
	"fmt"
	"log"
	"memorytin.com/shodan-client/shodan"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("Usage: provide a Shodan search term")
	}
	client := shodan.New()
	info, err := client.APIInfo()
	if err != nil {
		log.Panicln(err)
	}
	fmt.Printf(
		"Query Credits: %d\nScan Credits: %d\n\n",
		info.QueryCredits,
		info.ScanCredits,
	)

	hostSearch, err := client.HostSearch(os.Args[0])
	if err != nil {
		log.Panicln(err)
	}

	for _, host := range hostSearch.Matches {
		fmt.Printf("%18s%8d\n", host.IPString, host.Port)
	}
}
