package main

import (
	"fmt"
	"net"
)

func main() {
	for i := 1; i <= 1024; i++ {
		address := fmt.Sprintf("scanme.nmap.org:%d", i)
		conn, err := net.Dial("tcp", address)

		fmt.Printf("Dialing: %s \n", address)
		if err != nil {
			continue
		}

		conn.Close()
		fmt.Printf("Connection successful: %d open \n", i)
	}
}
