# Tools

## Shodan

https://www.shodan.io/

a search engine that lets the user find specific types of computers (webcams, routers, servers, etc.) connected to the internet using a variety of filters. Some have also described it as a search engine of service banners, which are metadata that the server sends back to the client.[1] This can be information about the server software, what options the service supports, a welcome message or anything else that the client can find out before interacting with the server.

Shodan collects data mostly on web servers (HTTP/HTTPS – ports 80, 8080, 443, 8443), as well as FTP (port 21), SSH (port 22), Telnet (port 23), SNMP (port 161), IMAP (ports 143, or (encrypted) 993), SMTP (port 25), SIP (port 5060),[2] and Real Time Streaming Protocol (RTSP, port 554). The latter can be used to access webcams and their video stream.[3] 


## Metasploit

https://www.metasploit.com/

The world’s most used penetration testing framework
Knowledge is power, especially when it’s shared. A collaboration between the open source community and Rapid7, Metasploit helps security teams do more than just verify vulnerabilities, manage security assessments, and improve security awareness; it empowers and arms defenders to always stay one step (or two) ahead of the game.


## FOCA

https://foca.en.softonic.com/

FOCA is an open-source software bundle which allows users to perform basic metadata analyses in order to determine whether or not a specific website may contain corrupted documents. Some commonly supported formats include DOC, ODT, ODS, SVG, and PPT. Results are collated in a user-friendly format, so it is easy to uncover any issues that may be present.