package main

import (
	"flag"
	"fmt"
	"net"
	"sort"
)

// Worker function, spun up as many times as needed to perform work
func worker(scanAddress string, ports, results chan int) {
	// Ranging over the ports provided and performing network dials
	for p := range ports {
		address := fmt.Sprintf("%s:%d", scanAddress, p)
		conn, err := net.Dial("tcp", address)

		fmt.Printf(":%d -scanned \n", p)

		if err != nil {
			// Reporting back a failed port represented as a 0
			results <- 0
			continue
		}
		// closing successful connections to be polite
		conn.Close()
		// Reporting back successful ports
		results <- p
	}
}

func main() {
	// Command-line arguments via flags
	workerCount := flag.Int("workerCount", 100, "an int representing the number of workers to use")
	scanFromPort := flag.Int("scanFromPort", 1, "an int representing the port to beginning scanning")
	scanToPort := flag.Int("scanToPort", 65535, "an int representing the port to stop scanning")
	address := flag.String("address", "scanme.nmap.org", "the DNS name or IP of the computer to scan.")

	fmt.Printf("Now scanning %s \n", *address)

	// Creating a channel for workers to communicate on, limiting the channel to 100 workers
	ports := make(chan int, *workerCount)
	// Creating a channel for ports (successful and unsuccessful) to be reported back on
	results := make(chan int)
	// Creating a Slice to hold reported back open ports
	var openPorts []int

	// Adding workers to the ports channel based on the cap limit set on the channel
	for i := 0; i < cap(ports); i++ {
		// Creating a worker and passing it the ports and results channels to pull work from and push results to respectively
		go worker(*address, ports, results)
	}

	// Anonymous auto-executing function to add the range of ports to be scanned to the ports channel
	go func() {
		for i := *scanFromPort; i <= *scanToPort; i++ {
			ports <- i
		}
	}()

	// Pulling results out of the results channel until the number of processed results equals the range of ports scanned
	for i := 0; i < *scanToPort; i++ {
		port := <-results
		if port != 0 {
			openPorts = append(openPorts, port)
		}
	}

	// Closing channels after all results are accounted for
	close(ports)
	close(results)

	// Sort the open ports found
	fmt.Println("Compiling results...")
	sort.Ints(openPorts)

	for _, port := range openPorts {
		fmt.Printf("Connection successful: %d open \n", port)
	}
}
