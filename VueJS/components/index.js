// Define a new component called todo-item
Vue.component('todo-item', {
    // The todo-item component now accepts a
    // "prop", which is like a custom attribute.
    // This prop is called todo.
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
});

let app = new Vue({
    //ID of the Root HTML element comprising the Vue Component
    //to be managed by this Vue Class
    el: '#app',
    //Data accessible to the Vue Component
    data: {
        groceryList: [
            { id: 0, text: 'Vegetables' },
            { id: 1, text: 'Cheese' },
            { id: 2, text: 'Whatever else humans are supposed to eat' }
        ]
    },
    //Methods accessible to the Vue Component
    methods: {

    }

});