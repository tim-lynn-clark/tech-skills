Vue.component('blog-post', {
    props: ['title'],
    template: '<h3>{{ title }}</h3>'
});

let app = new Vue({
    //ID of the Root HTML element comprising the Vue Component
    //to be managed by this Vue Class
    el: '#app',
    //Data accessible to the Vue Component
    data: {
        id: '234',
        ok: false,
        number: 99,
        url: 'https://www.memorytin.com',
        message: 'Hello Vue!',
        tooltip: `You landed on this page at ${new Date().toLocaleString()}`,
        show: true,
        todos: [
            { id: 1, text: 'Learn JavaScript', isComplete: false },
            { id: 2, text: 'Learn Vue', isComplete: true },
            { id: 3, text: 'Build something awesome', isComplete: false }
        ],
        person: {
            firstName: 'John',
            lastName: 'Doe',
            age: 30
        },
        rawHtml: '<h1>Should be a header</h1>',
        firstName: 'Tim',
        lastName: 'Clark',

        isActive: true,
        hasError: false,
        classObject: {
            active: true,
            'text-danger': false
        },
        activeClass: 'active',
        errorClass: 'text-danger',
        type: 'C',
        checkedNames: [],
        posts: [
            { id: 1, title: 'My journey with Vue' },
            { id: 2, title: 'Blogging with Vue' },
            { id: 3, title: 'Why Vue is so fun' }
        ]
    },
    // computed properties are cached based on their dependencies.
    // A computed property will only re-evaluate when some of its
    // dependencies have changed. Each call to the computed property
    // will return the cache until something changes
    computed: {
        fullName: function(){
            // first time run a string interpolation happens and 'Clark, Tim' is returned
            // so long as the values of lastName and firstName are not changed in some way
            // the cached 'Clark, Tim' is returned without the string interpolation happening again
            // this is extremely useful for computational expensive computed properties
            // of a Vue Component
            return `${this.lastName}, ${this.firstName}`;
        },
        // a computed getter
        reversedMessageTwo: function () {
            // `this` points to the vm instance
            return this.message.split('').reverse().join('')
        },
        // Computed properties are by default getter-only, but you can also
        // provide a setter when you need it:
        age: {
            get: function () {
                return this.age;
            },
            set: function(newValue){
                this.age = newValue;
            }
        },
        classObjectComputed: function () {
            return {
                active: this.isActive && !this.error,
                'text-danger': this.error && this.error.type === 'fatal'
            }
        },
        //https://vuejs.org/v2/guide/list.html#Displaying-Filtered-Sorted-Results
    },
    //Methods accessible to the Vue Component
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        },
        onSubmit: function(){
            console.log('prevent submit');
        },
        doSomething: function(){
            console.log('do something');
        }
    },
    // Alternative to computed, but less favored over computed properties
    // are properties generated based on watching other values and then
    // recalculating based on a change to that value
    watch: {
        firstName: function (val) {
            this.fullName = val + ' ' + this.lastName
        },
        lastName: function (val) {
            this.fullName = this.firstName + ' ' + val
        }
    }

});