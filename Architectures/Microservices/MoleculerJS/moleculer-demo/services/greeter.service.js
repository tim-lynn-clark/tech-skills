"use strict";

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	// NOTE: Name of the services, used within the broker service for identification purposes
	name: "greeter",

	/**
	 * Settings:  Use to configure the services at startup
	 */
	settings: {

	},

	/**
	 * Dependencies: other services this service depends on and must be active prior to this service starting.
	 */
	dependencies: [],

	/**
	 * Actions: functions that other services can call on this service.
	 */
	actions: {

		/**
		 * Say a 'Hello' action.
		 *
		 * @returns
		 */
		hello: {
			rest: {
				method: "GET",
				path: "/hello"
			},
			async handler(ctx) {
				this.logger.info("<< Context Object ->", ctx);
				const payload = `Hello from greeter hello action. ${this.broker.nodeID}`;
				ctx.emit("hello.called", payload);

				const number = await ctx.call("helper.random");
				this.logger.info("<< RANDOM -> ", number);


				return {payload, number};
			}
		},

		/**
		 * Welcome, a username
		 *
		 * @param {String} name - User name
		 */
		welcome: {
			rest: "/welcome",
			params: {
				name: "string"
			},
			/** @param {Context} ctx  */
			async handler(ctx) {
				return `Welcome, ${ctx.params.name}`;
			}
		}
	},

	/**
	 * Events: If the services is to listen for and handle events,
	 * those event handlers are setup here.
	 */
	events: {

	},

	/**
	 * Methods: Private methods that are only accessible within the service's scope.
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
