module.exports = {
	name: "helper",

	actions: {
		random() {
			return Math.round(Math.random() * 10);
		}
	},

	events: {
		"hello.called" (payload) {
			this.logger.info("<< Helper service called. Caught the hello.called event ->", payload);
		}
	}
};
