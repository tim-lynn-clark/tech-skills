const Config = require('../lib/config')
const env = require('../lib/env')
const accountComponentControls = require('../lib/account-component/controls')

const config = Config({ env })

const open = accountComponentControls.commands.open.example()
const streamName = `account:command-${open.data.accountId}`

config.messageStore
  .write(streamName, open)
  .catch(err => {
    console.log({ err })
  })
  .finally(() => config.messageStore.stop())
