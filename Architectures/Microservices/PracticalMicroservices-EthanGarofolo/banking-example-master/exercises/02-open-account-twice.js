const Config = require('../lib/config')
const env = require('../lib/env')
const accountComponentControls = require('../lib/account-component/controls')

const config = Config({ env })

const open = accountComponentControls.commands.open.example()
const entityStreamName = `account-${open.data.accountId}`

config.accountComponent.commandHandlers
  .Open(open)
  .then(() => config.accountComponent.commandHandlers.Open(open))
  .then(() =>
    config.messageStore.read(entityStreamName).then(messages => {
      console.log({ messages })
    })
  )
  .catch(err => {
    console.log({ err })
  })
  .finally(() => config.messageStore.stop())
