const { v4: uuid } = require('uuid')

const Config = require('../lib/config')
const env = require('../lib/env')
const accountComponentControls = require('../lib/account-component/controls')
const fundsTransferComponentControls = require('../lib/funds-transfer-component/controls')

const config = Config({ env })

const fundsTransferId = uuid()

const account1Opened = accountComponentControls.events.opened.example()
const account2Opened = accountComponentControls.events.opened.example()

const account1StreamName = `account-${account1Opened.data.accountId}`
const account2StreamName = `account-${account2Opened.data.accountId}`

const transfer = fundsTransferComponentControls.commands.transfer.example(
  fundsTransferId,
  account1Opened.data.accountId,
  account2Opened.data.accountId,
  35
)

const fundsTransferStreamName = `fundsTransfer:command-${fundsTransferId}`

config.messageStore
  .write(account1StreamName, account1Opened)
  .then(() => config.messageStore.write(account2StreamName, account2Opened))
  .then(() => config.messageStore.write(fundsTransferStreamName, transfer))
  .catch(err => {
    console.log({ err })
  })
  .finally(() => config.messageStore.stop())
