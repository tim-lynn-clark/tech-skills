const test = require('blue-tape')
const supertest = require('supertest')
const { v4: uuid } = require('uuid')

const { app, config } = require('../../../../automated-init')

test('Receiving a request to transfer funds', t => {
  const fundsTransferId = uuid()
  const withdrawalAccountId = uuid()
  const depositAccountId = uuid()
  const amount = 42

  const attributes = {
    fundsTransferId,
    withdrawalAccountId,
    depositAccountId,
    amount
  }
  const commandStreamName = `fundsTransfer:command-${fundsTransferId}`

  return supertest(app)
    .post('/funds-transfers')
    .type('form')
    .send(attributes)
    .expect(301)
    .then(() =>
      config.messageStore.read(commandStreamName).then(messages => {
        t.equals(messages.length, 1, '1 message written')

        const [transfer] = messages

        t.equals(transfer.type, 'Transfer', 'It is an Transfer command')
        t.equals(
          transfer.data.fundsTransferId,
          fundsTransferId,
          'Same fundsTransferId'
        )
        t.equals(
          transfer.data.withdrawalAccountId,
          withdrawalAccountId,
          'Same withdrawalAccountId'
        )
        t.equals(
          transfer.data.depositAccountId,
          depositAccountId,
          'Same depositAccountId'
        )
        t.ok(transfer.data.time, 'It has a time')
      })
    )
})
