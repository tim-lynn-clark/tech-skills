const test = require('blue-tape')
const supertest = require('supertest')
const { v4: uuid } = require('uuid')

const { app, config } = require('../../../../automated-init')

test('Receiving a request to open a new account', t => {
  const accountId = uuid()
  const customerId = uuid()

  const attributes = { accountId, customerId }
  const commandStreamName = `account:command-${accountId}`

  return supertest(app)
    .post('/')
    .type('form')
    .send(attributes)
    .expect(301)
    .then(() =>
      config.messageStore.read(commandStreamName).then(messages => {
        t.equals(messages.length, 1, '1 message written')

        const [open] = messages

        t.equals(open.type, 'Open', 'It is an Open command')
        t.equals(open.data.accountId, accountId, 'Same accountId')
        t.equals(open.data.customerId, customerId, 'Same customerId')
        t.ok(open.data.time, 'It has a time')
      })
    )
})
