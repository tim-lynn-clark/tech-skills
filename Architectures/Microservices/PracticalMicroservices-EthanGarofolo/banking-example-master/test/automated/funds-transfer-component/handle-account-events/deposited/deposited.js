const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const clockControls = require('../../../../../lib/clock/controls')
const AccountEventHandlers = require('../../../../../lib/funds-transfer-component/handlers/account/events')
const { config } = require('../../../../automated-init')

test('When an account Deposited event is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const fundsTransferId = uuid()
  const streamName = `fundsTransfer-${fundsTransferId}`
  const accountDeposit = accountComponentControls.events.deposit.example()
  accountDeposit.metadata.correlationStreamName = streamName

  const accountEventHandlers = AccountEventHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return accountEventHandlers
    .Deposited(accountDeposit)
    .then(() => accountEventHandlers.Deposited(accountDeposit))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [deposit] = messages

        t.equals(deposit.type, 'Deposited', 'It is a Deposited')
        t.equals(
          deposit.data.depositId,
          accountDeposit.data.depositId,
          'Same depositId'
        )
        t.equals(
          deposit.data.accountId,
          accountDeposit.data.accountId,
          'Same accountId'
        )
        t.equals(deposit.data.time, accountDeposit.data.time, 'Same time')
        t.equals(deposit.data.amount, accountDeposit.data.amount, 'Same amount')
        t.equals(
          deposit.data.fundsTransferId,
          fundsTransferId,
          'Same fundsTransferId'
        )
        t.equals(
          deposit.data.processedTime,
          processedTime,
          'Same processedTime'
        )
      })
    )
})
