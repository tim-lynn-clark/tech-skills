const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const clockControls = require('../../../../../lib/clock/controls')
const AccountEventHandlers = require('../../../../../lib/funds-transfer-component/handlers/account/events')
const { config } = require('../../../../automated-init')

test('When an account Withdrawn event is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const fundsTransferId = uuid()
  const streamName = `fundsTransfer-${fundsTransferId}`
  const accountWithdrawn = accountComponentControls.events.withdrawn.example()
  accountWithdrawn.metadata.correlationStreamName = streamName

  const accountEventHandlers = AccountEventHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return accountEventHandlers
    .Withdrawn(accountWithdrawn)
    .then(() => accountEventHandlers.Withdrawn(accountWithdrawn))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [withdrawn] = messages

        t.equals(withdrawn.type, 'Withdrawn', 'It is a Withdrawn')
        t.equals(
          withdrawn.data.withdrawalId,
          accountWithdrawn.data.withdrawalId,
          'Same withdrawalId'
        )
        t.equals(
          withdrawn.data.accountId,
          accountWithdrawn.data.accountId,
          'Same accountId'
        )
        t.equals(withdrawn.data.time, accountWithdrawn.data.time, 'Same time')
        t.equals(
          withdrawn.data.amount,
          accountWithdrawn.data.amount,
          'Same amount'
        )
        t.equals(
          withdrawn.data.fundsTransferId,
          fundsTransferId,
          'Same fundsTransferId'
        )
        t.equals(
          withdrawn.data.processedTime,
          processedTime,
          'Same processedTime'
        )
      })
    )
})
