const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const fundsTransferComponentControls = require('../../../../../lib/funds-transfer-component/controls')
const { config } = require('../../../../automated-init')

test('When a Withdrawn event is received', t => {
  // Arrange
  const fundsTransferId = uuid()
  const initiated = fundsTransferComponentControls.events.initiated.example(
    fundsTransferId
  )
  const withdrawn = fundsTransferComponentControls.events.withdrawn.example(
    fundsTransferId
  )
  const streamName = `fundsTransfer-${fundsTransferId}`
  const depositAccountId = initiated.data.depositAccountId
  const commandStreamName = `account:command-${depositAccountId}`

  withdrawn.streamName = streamName

  return (
    config.messageStore
      .write(streamName, initiated, -1)
      // Act
      .then(() =>
        config.fundsTransferComponent.eventHandlers.Withdrawn(withdrawn)
      )
      .then(() =>
        // Assert
        config.messageStore.read(commandStreamName).then(messages => {
          t.equals(messages.length, 1, 'Only one message written')

          const [deposit] = messages

          t.equals(deposit.type, 'Deposit', 'It is a Deposit command')
          t.equals(
            deposit.metadata.correlationStreamName,
            streamName,
            'Tagged with correlation stream'
          )
          t.equals(
            deposit.data.depositId,
            initiated.data.depositId,
            'Same depositId'
          )
          t.equals(
            deposit.data.accountId,
            depositAccountId,
            'Same depositAccountId'
          )
          t.equals(deposit.data.amount, initiated.data.amount, 'Same amount')
          t.ok(deposit.data.time, 'time is set')
        })
      )
  )
})
