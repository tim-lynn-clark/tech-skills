const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const fundsTransferComponentControls = require('../../../../../lib/funds-transfer-component/controls')
const { config } = require('../../../../automated-init')

test('When a Deposited event is received', t => {
  // Arrange
  const fundsTransferId = uuid()
  const initiated = fundsTransferComponentControls.events.initiated.example(
    fundsTransferId
  )
  const deposited = fundsTransferComponentControls.events.deposited.example(
    fundsTransferId
  )
  const streamName = `fundsTransfer-${fundsTransferId}`

  return (
    config.messageStore
      .write(streamName, initiated, -1)
      // Act
      .then(() =>
        config.fundsTransferComponent.eventHandlers.Deposited(deposited)
      )
      .then(() =>
        config.fundsTransferComponent.eventHandlers.Deposited(deposited)
      )
      .then(() =>
        // Assert
        config.messageStore.read(streamName).then(messages => {
          t.equals(
            messages.length,
            2,
            'The Initiated and Transferred are in the stream'
          )

          const [_, transferred] = messages

          t.equals(transferred.type, 'Transferred', 'It is a Transferred')
          t.equals(
            transferred.data.fundsTransferId,
            fundsTransferId,
            'Same fundsTransferId'
          )
          t.equals(
            transferred.data.withdrawalId,
            initiated.data.withdrawalId,
            'Same withdrawalId'
          )
          t.equals(
            transferred.data.withdrawalAccountId,
            initiated.data.withdrawalAccountId,
            'Same withdrawalAccountId'
          )
          t.equals(
            transferred.data.depositId,
            initiated.data.depositId,
            'Same depositId'
          )
          t.equals(
            transferred.data.depositAccountId,
            initiated.data.depositAccountId,
            'Same depositAccountId'
          )
          t.equals(
            transferred.data.amount,
            initiated.data.amount,
            'Same amount'
          )
          t.equals(transferred.data.time, initiated.data.time, 'Same time')
          t.ok(transferred.data.processedTime, 'processedTime is set')
        })
      )
  )
})
