const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const fundsTransferComponentControls = require('../../../../../lib/funds-transfer-component/controls')
const { config } = require('../../../../automated-init')

test('When an Initiated event is received', t => {
  // Arrange
  const initiated = fundsTransferComponentControls.events.initiated.example()
  const fundsTransferId = initiated.data.fundsTransferId
  const streamName = `fundsTransfer-${fundsTransferId}`
  const withdrawalAccountId = initiated.data.withdrawalAccountId
  const commandStreamName = `account:command-${withdrawalAccountId}`

  initiated.streamName = streamName

  // Act
  return config.fundsTransferComponent.eventHandlers
    .Initiated(initiated)
    .then(() =>
      // Assert
      config.messageStore.read(commandStreamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [withdraw] = messages

        t.equals(withdraw.type, 'Withdraw', 'It is a Withdraw command')
        t.equals(
          withdraw.metadata.correlationStreamName,
          streamName,
          'Tagged with correlation stream'
        )
        t.equals(
          withdraw.data.withdrawalId,
          initiated.data.withdrawalId,
          'Same withdrawalId'
        )
        t.equals(
          withdraw.data.accountId,
          withdrawalAccountId,
          'Same withdrawalAccountId'
        )
        t.equals(withdraw.data.amount, initiated.data.amount, 'Same amount')
        t.equals(withdraw.data.time, initiated.data.time, 'Same time')
      })
    )
})
