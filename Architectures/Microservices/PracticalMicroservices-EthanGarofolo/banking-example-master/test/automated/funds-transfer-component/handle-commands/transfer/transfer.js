const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const fundsTransferComponentControls = require('../../../../../lib/funds-transfer-component/controls')
const CommandHandlers = require('../../../../../lib/funds-transfer-component/handlers/commands')
const clockControls = require('../../../../../lib/clock/controls')
const { config } = require('../../../../automated-init')

test('When a Transfer command is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const fundsTransferId = uuid()
  const depositAccountId = uuid()
  const withdrawalAccountId = uuid()
  const amount = 42
  const transfer = fundsTransferComponentControls.commands.transfer.example(
    fundsTransferId,
    depositAccountId,
    withdrawalAccountId,
    amount
  )
  const streamName = `fundsTransfer-${fundsTransferId}`

  const commandHandlers = CommandHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return commandHandlers
    .Transfer(transfer)
    .then(() => commandHandlers.Transfer(transfer))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [initiated] = messages

        t.equals(initiated.type, 'Initiated', 'It is an initiated event')
        t.equals(
          initiated.data.fundsTransferId,
          fundsTransferId,
          'Same fundsTransferId'
        )
        t.equals(
          initiated.data.depositAccountId,
          depositAccountId,
          'Same depositAccountId'
        )
        t.ok(initiated.data.depositId, 'Has a depositId')
        t.equals(
          initiated.data.withdrawalAccountId,
          withdrawalAccountId,
          'Same withdrawalAccountId'
        )
        t.ok(initiated.data.withdrawalId, 'Has a withdrawalId')
        t.equals(initiated.data.amount, amount, 'Same amount')
        t.equals(
          initiated.data.processedTime,
          processedTime,
          'processedTime matches supplied value'
        )
      })
    )
})
