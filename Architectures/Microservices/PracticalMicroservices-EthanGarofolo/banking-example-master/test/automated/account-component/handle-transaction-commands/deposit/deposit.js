const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const CommandHandlers = require('../../../../../lib/account-component/handlers/transactions/commands')
const clockControls = require('../../../../../lib/clock/controls')
const { config } = require('../../../../automated-init')

test('When a transaction Deposit command is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const accountId = uuid()
  const depositId = uuid()
  const amount = 42
  const correlationStreamName = 'correlationStreamName'
  const deposit = accountComponentControls.commands.deposit.example(
    accountId,
    depositId,
    amount
  )
  deposit.globalPosition = 0
  deposit.metadata.correlationStreamName = correlationStreamName
  const streamName = `account-${accountId}`

  const commandHandlers = CommandHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return commandHandlers
    .Deposit(deposit)
    .then(() => commandHandlers.Deposit(deposit))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [deposited] = messages

        t.equals(deposited.type, 'Deposited', 'It is a deposited event')
        t.equals(deposited.data.accountId, accountId, 'Same accountId')
        t.equals(deposited.data.depositId, depositId, 'Same depositId')
        t.equals(deposited.data.amount, amount, 'Same amount')
        t.equals(
          deposited.data.processedTime,
          processedTime,
          'processedTime matches supplied value'
        )
        t.equals(
          deposited.metadata.correlationStreamName,
          correlationStreamName,
          'correlationStreamName is chained'
        )
      })
    )
})
