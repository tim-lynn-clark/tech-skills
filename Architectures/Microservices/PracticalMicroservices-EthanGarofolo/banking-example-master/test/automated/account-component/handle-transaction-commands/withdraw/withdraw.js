const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const CommandHandlers = require('../../../../../lib/account-component/handlers/transactions/commands')
const clockControls = require('../../../../../lib/clock/controls')
const { config } = require('../../../../automated-init')

test('When a transaction Withdraw command is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const accountId = uuid()
  const withdrawalId = uuid()
  const amount = 42
  const correlationStreamName = 'correlationStreamName'
  const withdraw = accountComponentControls.commands.withdraw.example(
    accountId,
    withdrawalId,
    amount
  )
  withdraw.globalPosition = 0
  withdraw.metadata.correlationStreamName = correlationStreamName
  const streamName = `account-${accountId}`

  const commandHandlers = CommandHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return commandHandlers
    .Withdraw(withdraw)
    .then(() => commandHandlers.Withdraw(withdraw))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [withdrawn] = messages

        t.equals(withdrawn.type, 'Withdrawn', 'It is a withdrawn event')
        t.equals(withdrawn.data.accountId, accountId, 'Same accountId')
        t.equals(withdrawn.data.withdrawalId, withdrawalId, 'Same withdrawalId')
        t.equals(withdrawn.data.amount, amount, 'Same amount')
        t.equals(
          withdrawn.data.processedTime,
          processedTime,
          'processedTime matches supplied value'
        )
        t.equals(
          withdrawn.metadata.correlationStreamName,
          correlationStreamName,
          'correlationStreamName is chained'
        )
      })
    )
})
