const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const CommandHandlers = require('../../../../../lib/account-component/handlers/commands')
const clockControls = require('../../../../../lib/clock/controls')
const { config } = require('../../../../automated-init')

test('When an outside Withdraw command is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const accountId = uuid()
  const withdrawalId = uuid()
  const amount = 42
  const correlationStreamName = 'correlationStreamName'
  const withdraw = accountComponentControls.commands.withdraw.example(
    accountId,
    withdrawalId,
    amount
  )
  withdraw.globalPosition = 0
  withdraw.metadata.correlationStreamName = correlationStreamName
  const streamName = `accountTransaction-${accountId}+${withdrawalId}`

  const commandHandlers = CommandHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return commandHandlers
    .Withdraw(withdraw)
    .then(() => commandHandlers.Withdraw(withdraw))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [withdraw] = messages

        t.equals(withdraw.type, 'Withdraw', 'It is a withdraw command')
        t.equals(withdraw.data.accountId, accountId, 'Same accountId')
        t.equals(withdraw.data.withdrawalId, withdrawalId, 'Same withdrawalId')
        t.equals(withdraw.data.amount, amount, 'Same amount')
      })
    )
})
