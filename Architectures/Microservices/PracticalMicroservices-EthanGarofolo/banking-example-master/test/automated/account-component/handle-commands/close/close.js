const test = require('blue-tape')
const { v4: uuid } = require('uuid')

const accountComponentControls = require('../../../../../lib/account-component/controls')
const CommandHandlers = require('../../../../../lib/account-component/handlers/commands')
const clockControls = require('../../../../../lib/clock/controls')
const { config } = require('../../../../automated-init')

test('When a Close command is received', t => {
  // Arrange
  const processedTime = '2020-11-01T00:00:00.000Z'
  const processedTimeDate = new Date(processedTime)
  const clock = clockControls.example(processedTimeDate)
  const accountId = uuid()
  const close = accountComponentControls.commands.close.example(accountId)
  const streamName = `account-${accountId}`

  const commandHandlers = CommandHandlers({
    clock,
    messageStore: config.messageStore
  })

  // Act
  return commandHandlers
    .Close(close)
    .then(() => commandHandlers.Close(close))
    .then(() =>
      // Assert
      config.messageStore.read(streamName).then(messages => {
        t.equals(messages.length, 1, 'Only one message written')

        const [closed] = messages

        t.equals(closed.type, 'Closed', 'It is a closed event')
        t.equals(closed.data.accountId, accountId, 'Same accountId')
        t.equals(
          closed.data.processedTime,
          processedTime,
          'processedTime matches supplied value'
        )
      })
    )
})
