const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const projection = require('../../projection')
const Account = require('../../account')

function build ({ clock, messageStore }) {
  return {
    async Deposit (deposit) {
      const accountId = deposit.data.accountId
      const depositSequence = deposit.globalPosition
      const streamName = `account-${accountId}`

      // project the account in question
      const account = new Account()
      await messageStore.fetch(streamName, projection, account)

      if (account.processed(depositSequence)) {
        console.log(`Deposit ${deposit.id} already processed.  Skipping`)

        return
      }

      const deposited = {
        id: uuid(),
        type: 'Deposited',
        metadata: {
          traceId: deposit.metadata.traceId,
          causationId: deposit.id,
          correlationStreamName: deposit.metadata.correlationStreamName
        },
        data: {
          depositId: deposit.data.depositId,
          accountId,
          amount: deposit.data.amount,
          time: deposit.data.time,
          processedTime: clock(),
          sequence: deposit.globalPosition
        }
      }

      return messageStore.write(streamName, deposited)
    },

    async Withdraw (withdraw) {
      const accountId = withdraw.data.accountId
      const withdrawSequence = withdraw.globalPosition
      const streamName = `account-${accountId}`

      // project the account in question
      const account = new Account()
      await messageStore.fetch(streamName, projection, account)

      if (account.processed(withdrawSequence)) {
        console.log(`Withdrawal ${withdraw.id} already processed.  Skipping`)

        return
      }

      const withdrawn = {
        id: uuid(),
        type: 'Withdrawn',
        metadata: {
          traceId: withdraw.metadata.traceId,
          causationId: withdraw.id,
          correlationStreamName: withdraw.metadata.correlationStreamName
        },
        data: {
          withdrawalId: withdraw.data.withdrawalId,
          accountId,
          amount: withdraw.data.amount,
          time: withdraw.data.time,
          processedTime: clock(),
          sequence: withdraw.globalPosition
        }
      }

      return messageStore.write(streamName, withdrawn)
    }
  }
}

module.exports = build
