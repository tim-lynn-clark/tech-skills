const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const projection = require('../projection')
const Account = require('../account')

function build ({ clock, messageStore }) {
  return {
    async Open (open) {
      const accountId = open.data.accountId
      const streamName = `account-${accountId}`

      // project the account in question
      const account = new Account()
      await messageStore.fetch(streamName, projection, account)

      if (account.opened()) {
        console.log(`Open ${open.id} already processed.  Skipping`)

        return
      }

      const opened = {
        id: uuid(),
        type: 'Opened',
        metadata: {
          traceId: open.metadata.traceId,
          causationId: open.id
        },
        data: {
          accountId,
          customerId: open.data.customerId,
          time: open.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(streamName, opened, account.version)
    },

    async Close (close) {
      const accountId = close.data.accountId
      const streamName = `account-${accountId}`

      // project the account in question
      const account = new Account()
      await messageStore.fetch(streamName, projection, account)

      if (account.closed()) {
        console.log(`Close ${close.id} already processed.  Skipping`)

        return
      }

      const closed = {
        id: uuid(),
        type: 'Closed',
        metadata: {
          traceId: close.metadata.traceId,
          causationId: close.id
        },
        data: {
          accountId,
          time: close.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(streamName, closed)
    },

    async Deposit (deposit) {
      const { accountId, depositId } = deposit.data
      const transactionStreamName = `accountTransaction-${accountId}+${depositId}`

      const transactionDeposit = {
        id: uuid(),
        type: 'Deposit',
        metadata: {
          traceId: deposit.metadata.traceId,
          correlationStreamName: deposit.metadata.correlationStreamName,
          causationId: deposit.id
        },
        data: {
          ...deposit.data
        }
      }

      return messageStore
        .write(transactionStreamName, transactionDeposit, -1)
        .catch(err => {
          if (err.name !== 'VersionConflictError') {
            throw err
          }
        })
    },

    async Withdraw (withdraw) {
      const { accountId, withdrawalId } = withdraw.data
      const transactionStreamName = `accountTransaction-${accountId}+${withdrawalId}`

      const transactionWithdraw = {
        id: uuid(),
        type: 'Withdraw',
        metadata: {
          traceId: withdraw.metadata.traceId,
          correlationStreamName: withdraw.metadata.correlationStreamName,
          causationId: withdraw.id
        },
        data: {
          ...withdraw.data
        }
      }

      return messageStore
        .write(transactionStreamName, transactionWithdraw, -1)
        .catch(err => {
          if (err.name !== 'VersionConflictError') {
            throw err
          }
        })
    }
  }
}

module.exports = build
