const CommandHandlers = require('./handlers/commands')
const TransactionCommandHandlers = require('./handlers/transactions/commands')

function build ({ clock, messageStore }) {
  const commandHandlers = CommandHandlers({ clock, messageStore })
  const transactionCommandHandlers = TransactionCommandHandlers({
    clock,
    messageStore
  })

  // This is how the component listens to the flow of messages
  const commandSubscription = messageStore.createSubscription({
    streamName: 'account:command',
    handlers: commandHandlers,
    subscriberId: 'account:command'
  })

  const transactionSubscription = messageStore.createSubscription({
    streamName: 'accountTransaction',
    handlers: transactionCommandHandlers,
    subscriberId: 'accountTransaction'
  })

  function start () {
    commandSubscription.start()
    transactionSubscription.start()
  }

  return {
    commandHandlers,
    start
  }
}

module.exports = build
