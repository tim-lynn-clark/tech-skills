// The projection is an object whose key-value pairs connect handling functions
// to the *event* type they handle.  Remember, projections never deal with
// commands.
//
// The keys are event types.  The values are functions that apply that event
// type to the projection.

module.exports = {
  Opened (account, opened) {
    account.id = opened.data.accountId
    account.customerId = opened.data.customerId
    account.openedTime = opened.data.time
    account.version = opened.position
  },

  Closed (account, closed) {
    account.closedTime = closed.data.time
    account.version = closed.position
  },

  Deposited (account, deposited) {
    const { amount } = deposited.data
    const depositSequence = deposited.globalPosition

    account.deposit(amount)
    account.handledTransaction(depositSequence)
    account.version = deposited.position
  },

  Withdrawn (account, withdrawn) {
    const { amount } = withdrawn.data
    const withdrawalSequence = withdrawn.globalPosition

    account.withdraw(amount)
    account.handledTransaction(withdrawalSequence)
    account.version = withdrawn.position
  }
}
