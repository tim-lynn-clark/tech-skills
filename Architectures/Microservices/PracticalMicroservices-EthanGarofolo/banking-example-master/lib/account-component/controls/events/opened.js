const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example () {
    return {
      id: IdControls.example(),
      type: 'Opened',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        accountId: IdControls.example(),
        customerId: IdControls.example(),
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
