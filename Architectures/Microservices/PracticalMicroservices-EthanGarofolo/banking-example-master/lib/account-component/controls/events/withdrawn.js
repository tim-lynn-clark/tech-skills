const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    accountId = IdControls.example(),
    withdrawalId = IdControls.example(),
    amount = 5
  ) {
    return {
      id: IdControls.example(),
      type: 'Withdrawn',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        withdrawalId,
        accountId,
        amount,
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
