const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    accountId = IdControls.example(),
    depositId = IdControls.example(),
    amount = 5
  ) {
    return {
      id: IdControls.example(),
      type: 'Deposited',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        depositId,
        accountId,
        amount,
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
