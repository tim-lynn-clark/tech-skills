const commands = require('./commands')
const events = require('./events')
const id = require('./id')
const time = require('./time')

module.exports = {
  commands,
  events,
  id,
  time
}
