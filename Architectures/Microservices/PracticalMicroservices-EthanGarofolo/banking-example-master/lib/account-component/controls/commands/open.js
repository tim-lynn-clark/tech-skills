const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example () {
    const open = {
      id: IdControls.example(),
      type: 'Open',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        accountId: IdControls.example(),
        customerId: IdControls.example(),
        time: TimeControls.Effective.example()
      }
    }

    return open
  }
}
