const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (accountId = IdControls.example()) {
    const close = {
      id: IdControls.example(),
      type: 'Close',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        accountId,
        time: TimeControls.Effective.example()
      }
    }

    return close
  }
}
