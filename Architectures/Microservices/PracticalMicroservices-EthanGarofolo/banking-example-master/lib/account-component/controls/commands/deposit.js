const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    accountId = IdControls.example(),
    depositId = IdControls.example(),
    amount = 5
  ) {
    const deposit = {
      id: IdControls.example(),
      type: 'Depost',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        depositId,
        accountId,
        amount,
        time: TimeControls.Effective.example()
      }
    }

    return deposit
  }
}
