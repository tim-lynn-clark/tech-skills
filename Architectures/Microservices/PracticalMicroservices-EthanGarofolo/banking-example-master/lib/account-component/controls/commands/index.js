const close = require('./close')
const deposit = require('./deposit')
const open = require('./open')
const withdraw = require('./withdraw')

module.exports = {
  close,
  deposit,
  withdraw,
  open
}
