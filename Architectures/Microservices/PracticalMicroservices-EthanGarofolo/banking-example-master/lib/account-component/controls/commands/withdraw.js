const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    accountId = IdControls.example(),
    withdrawalId = IdControls.example(),
    amount = 5
  ) {
    const withdraw = {
      id: IdControls.example(),
      type: 'Withdraw',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        withdrawalId,
        accountId,
        amount,
        time: TimeControls.Effective.example()
      }
    }

    return withdraw
  }
}
