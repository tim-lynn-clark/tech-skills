// This is the entity, the Account.
//
// This is where the business rules around accounts are implemented.

class Account {
  constructor (balance = 0, handledDeposits = new Set()) {
    this.balance = balance
    this.handledDeposits = handledDeposits
    this.version = -1
  }

  opened () {
    return !!this.openedTime
  }

  closed () {
    return !!this.closedTime
  }

  deposit (amount) {
    this.balance += amount
  }

  withdraw (amount) {
    this.balance -= amount
  }

  handledTransaction (transactionSequence) {
    this.sequence = transactionSequence
  }

  processed (messageSequence) {
    return this.sequence >= messageSequence
  }
}

module.exports = Account
