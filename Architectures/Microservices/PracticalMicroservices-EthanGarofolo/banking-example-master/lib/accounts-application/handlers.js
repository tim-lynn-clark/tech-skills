const { v4: uuid } = require('uuid')

function Handlers ({ clock, messageStore }) {
  return {
    handleOpenForm (req, res) {
      const accountId = uuid()
      const customerId = uuid()

      res.render('accounts-application/templates/open', {
        accountId,
        customerId
      })
    },

    handleOpen (req, res, next) {
      const { accountId, customerId } = req.body

      const open = {
        id: uuid(),
        type: 'Open',
        metadata: {
          traceId: req.context.traceId
        },
        data: {
          accountId,
          customerId,
          time: clock()
        }
      }
      const streamName = `account:command-${accountId}`

      messageStore
        .write(streamName, open)
        .then(() => res.redirect(301, 'account-open-requested'))
        .catch(next)
    },

    handleOpenRequested (req, res) {
      res.render('accounts-application/templates/open-requested')
    }
  }
}

module.exports = Handlers
