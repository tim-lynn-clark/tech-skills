const bodyParser = require('body-parser')
const express = require('express')

const Handlers = require('./handlers')

function build ({ clock, messageStore }) {
  const handlers = Handlers({ clock, messageStore })

  const router = express.Router()

  router
    .route('/')
    .get(handlers.handleOpenForm)
    .post(bodyParser.urlencoded({ extended: false }), handlers.handleOpen)

  router.route('/account-open-requested').get(handlers.handleOpenRequested)

  return {
    handlers,
    router
  }
}

module.exports = build
