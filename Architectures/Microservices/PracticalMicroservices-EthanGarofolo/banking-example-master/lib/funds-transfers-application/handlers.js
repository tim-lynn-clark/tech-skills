const { v4: uuid } = require('uuid')

function Handlers ({ clock, messageStore }) {
  return {
    handleFundsTransferForm (req, res) {
      const fundsTransferId = uuid()

      res.render('funds-transfers-application/templates/transfer', {
        fundsTransferId
      })
    },

    handleTransfer (req, res, next) {
      const {
        fundsTransferId,
        withdrawalAccountId,
        depositAccountId,
        amount
      } = req.body
      const numericAmount = parseFloat(amount)

      const transfer = {
        id: uuid(),
        // WHAT TYPE?
        type: '',
        metadata: {
          traceId: req.context.traceId
        },
        data: {
          // What data go here?  Where can you find what goes here?
        }
      }
      // What streamName do we use? What are we writing, and where do we write
      // those?
      const streamName = `CATEGORY-${IDENTIFIER}`

      messageStore
        // Why do we not have an expected version here?
        .write(streamName, transfer)
        .then(() => res.redirect(301, 'funds-transfers/transfer-requested'))
        .catch(next)
    },

    handleTransferRequested (req, res) {
      res.render('funds-transfers-application/templates/transfer-requested')
    }
  }
}

module.exports = Handlers
