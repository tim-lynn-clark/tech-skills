const bodyParser = require('body-parser')
const express = require('express')

const Handlers = require('./handlers')

function build ({ clock, messageStore }) {
  const handlers = Handlers({ clock, messageStore })

  const router = express.Router()

  router
    .route('/')
    .get(handlers.handleFundsTransferForm)
    .post(bodyParser.urlencoded({ extended: false }), handlers.handleTransfer)

  router.route('/transfer-requested').get(handlers.handleTransferRequested)

  return {
    handlers,
    router
  }
}

module.exports = build
