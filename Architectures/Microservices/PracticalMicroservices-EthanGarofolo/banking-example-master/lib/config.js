/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const MessageStore = require('@suchsoftware/proof-of-concept-message-store')

const PostgresClient = require('./postgres-client')
const AccountComponent = require('./account-component')
const Clock = require('./clock')
const FundsTransferComponent = require('./funds-transfer-component')
const AccountsApplication = require('./accounts-application')
const FundsTransfersApplication = require('./funds-transfers-application')

function createConfig ({ env }) {
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  const messageStore = MessageStore({ session: postgresClient })

  const clock = Clock()

  const accountComponent = AccountComponent({ clock, messageStore })
  const fundsTransferComponent = FundsTransferComponent({ clock, messageStore })

  const components = [accountComponent, fundsTransferComponent]

  const accountsApplication = AccountsApplication({ clock, messageStore })
  const fundsTransfersApplication = FundsTransfersApplication({
    clock,
    messageStore
  })

  return {
    env,
    messageStore,
    components,
    accountComponent,
    fundsTransferComponent,
    accountsApplication,
    fundsTransfersApplication
  }
}

module.exports = createConfig
