const defaultClock = () => new Date()

function Clock (now = defaultClock) {
  return () => now().toISOString()
}

module.exports = Clock
