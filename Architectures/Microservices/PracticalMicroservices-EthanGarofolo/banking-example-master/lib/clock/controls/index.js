const Clock = require('..')

module.exports = {
  example (time = new Date('December 25, 2020 00:00:00')) {
    const clock = Clock(() => time)

    return clock
  }
}
