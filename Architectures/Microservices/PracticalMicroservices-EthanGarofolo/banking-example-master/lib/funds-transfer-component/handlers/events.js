const { v4: uuid } = require('uuid')

const projection = require('../projection')
const FundsTransfer = require('../funds-transfer')

function build ({ clock, messageStore }) {
  return {
    async Initiated (initiated) {
      const {
        fundsTransferId,
        withdrawalAccountId,
        withdrawalId,
        amount
      } = initiated.data
      const commandStreamName = `account:command-${withdrawalAccountId}`
      const correlationStreamName = `fundsTransfer-${fundsTransferId}`

      // Note that there's no idempotence check.  We're sending a command.
      // account-component is expected to be idempotent

      const withdraw = {
        id: uuid(),
        type: 'Withdraw',
        metadata: {
          traceId: initiated.metadata.traceId,
          causationId: initiated.id,
          correlationStreamName
        },
        data: {
          accountId: withdrawalAccountId,
          withdrawalId,
          amount,
          time: initiated.data.time
        }
      }

      return messageStore.write(commandStreamName, withdraw)
    },

    async Withdrawn (withdrawn) {
      const { fundsTransferId } = withdrawn.data
      const streamName = `fundsTransfer-${fundsTransferId}`

      const fundsTransfer = new FundsTransfer()
      await messageStore.fetch(streamName, projection, fundsTransfer)

      const { depositAccountId, depositId, amount } = fundsTransfer
      const commandStreamName = `account:command-${depositAccountId}`
      const correlationStreamName = withdrawn.streamName

      // Note that there's no idempotence check.  We're sending a command.
      // account-component is expected to be idempotent

      const deposit = {
        id: uuid(),
        type: 'Deposit',
        metadata: {
          traceId: withdrawn.metadata.traceId,
          causationId: withdrawn.id,
          correlationStreamName
        },
        data: {
          accountId: depositAccountId,
          depositId,
          amount,
          time: clock()
        }
      }

      return messageStore.write(commandStreamName, deposit)
    },

    async Deposited (deposited) {
      const { fundsTransferId } = deposited.data
      const streamName = `fundsTransfer-${fundsTransferId}`

      const fundsTransfer = new FundsTransfer()
      await messageStore.fetch(streamName, projection, fundsTransfer)

      if (fundsTransfer.transferred()) {
        console.log(`Deposited ${deposited.id} already processed.  Skipping`)

        return
      }

      const transferred = {
        id: uuid(),
        type: 'Transferred',
        metadata: {
          traceId: deposited.metadata.traceId,
          causationId: deposited.id
        },
        data: {
          fundsTransferId,
          withdrawalAccountId: fundsTransfer.withdrawalAccountId,
          depositAccountId: fundsTransfer.depositAccountId,
          withdrawalId: fundsTransfer.withdrawalId,
          depositId: fundsTransfer.depositId,
          amount: fundsTransfer.amount,
          time: deposited.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(streamName, transferred)
    }
  }
}

module.exports = build
