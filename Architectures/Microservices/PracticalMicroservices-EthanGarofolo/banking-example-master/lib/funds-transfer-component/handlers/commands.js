const Bluebird = require('bluebird')
const { v4: uuid } = require('uuid')

const projection = require('../projection')
const FundsTransfer = require('../funds-transfer')

function build ({ clock, messageStore }) {
  return {
    async Transfer (transfer) {
      const { fundsTransferId } = transfer.data

      const streamName = `fundsTransfer-${fundsTransferId}`
      const fundsTransfer = new FundsTransfer()
      await messageStore.fetch(streamName, projection, fundsTransfer)

      if (fundsTransfer.initiated()) {
        console.log(`Transfer ${transfer.id} already processed.  Skipping`)

        return
      }

      const depositId = uuid()
      const withdrawalId = uuid()
      const initiated = {
        id: uuid(),
        type: 'Initiated',
        metadata: {
          traceId: transfer.metadata.traceId,
          causationId: transfer.id
        },
        data: {
          fundsTransferId,
          depositAccountId: transfer.data.depositAccountId,
          withdrawalAccountId: transfer.data.withdrawalAccountId,
          withdrawalId,
          depositId,
          amount: transfer.data.amount,
          time: transfer.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(streamName, initiated, -1)
    }
  }
}

module.exports = build
