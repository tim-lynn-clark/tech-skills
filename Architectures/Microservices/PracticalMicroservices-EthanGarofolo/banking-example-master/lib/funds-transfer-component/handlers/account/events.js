const { v4: uuid } = require('uuid')

const splitStreamName = require('../../../split-stream-name')
const FundsTransfer = require('../../funds-transfer')
const projection = require('../../projection')

function build ({ clock, messageStore }) {
  return {
    async Withdrawn (accountWithdrawn) {
      const { correlationStreamName } = accountWithdrawn.metadata

      if (!correlationStreamName) {
        return
      }

      const [category, fundsTransferId] = splitStreamName(correlationStreamName)

      if (category !== 'fundsTransfer') {
        return
      }

      const fundsTransfer = new FundsTransfer()
      await messageStore.fetch(correlationStreamName, projection, fundsTransfer)

      if (fundsTransfer.withdrawn()) {
        console.log(
          `Account Withdrawn ${accountWithdrawn.id} already processed.  Skipping`
        )

        return
      }

      const withdrawn = {
        id: uuid(),
        type: 'Withdrawn',
        metadata: {
          traceId: accountWithdrawn.metadata.traceId,
          causationId: accountWithdrawn.metadata.causationId
        },
        data: {
          fundsTransferId,
          withdrawalId: accountWithdrawn.data.withdrawalId,
          accountId: accountWithdrawn.data.accountId,
          amount: accountWithdrawn.data.amount,
          time: accountWithdrawn.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(
        correlationStreamName,
        withdrawn,
        fundsTransfer.version
      )
    },

    async Deposited (accountDeposited) {
      const { correlationStreamName } = accountDeposited.metadata

      if (!correlationStreamName) {
        return
      }

      const [category, fundsTransferId] = splitStreamName(correlationStreamName)

      if (category !== 'fundsTransfer') {
        return
      }

      const fundsTransfer = new FundsTransfer()
      await messageStore.fetch(correlationStreamName, projection, fundsTransfer)

      if (fundsTransfer.deposited()) {
        console.log(
          `Account Deposited ${accountDeposited.id} already processed.  Skipping`
        )

        return
      }

      const deposited = {
        id: uuid(),
        type: 'Deposited',
        metadata: {
          traceId: accountDeposited.metadata.traceId,
          causationId: accountDeposited.metadata.causationId
        },
        data: {
          fundsTransferId,
          depositId: accountDeposited.data.depositId,
          accountId: accountDeposited.data.accountId,
          amount: accountDeposited.data.amount,
          time: accountDeposited.data.time,
          processedTime: clock()
        }
      }

      return messageStore.write(
        correlationStreamName,
        deposited,
        fundsTransfer.version
      )
    }
  }
}

module.exports = build
