const AccountEventHandlers = require('./handlers/account/events')
const CommandHandlers = require('./handlers/commands')
const EventHandlers = require('./handlers/events')

function build ({ clock, messageStore }) {
  const accountEventHandlers = AccountEventHandlers({ clock, messageStore })
  const commandHandlers = CommandHandlers({ clock, messageStore })
  const eventHandlers = EventHandlers({ clock, messageStore })

  const accountEventSubscription = messageStore.createSubscription({
    streamName: 'account',
    handlers: accountEventHandlers,
    subscriberId: 'fundsTransferAccount'
  })

  const commandSubscription = messageStore.createSubscription({
    streamName: 'fundsTransfer:command',
    handlers: commandHandlers,
    subscriberId: 'fundsTransfer:command'
  })

  const eventSubscription = messageStore.createSubscription({
    streamName: 'fundsTransfer',
    handlers: eventHandlers,
    subscriberId: 'fundsTransfer'
  })

  function start () {
    accountEventSubscription.start()
    commandSubscription.start()
    eventSubscription.start()
  }

  return {
    accountEventHandlers,
    commandHandlers,
    eventHandlers,
    start
  }
}

module.exports = build
