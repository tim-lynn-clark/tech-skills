// The projection is an object whose key-value pairs connect handling functions
// to the *event* type they handle.  Remember, projections never deal with
// commands.
//
// The keys are event types.  The values are functions that apply that event
// type to the projection.

module.exports = {
  Initiated (fundsTransfer, initiated) {
    fundsTransfer.id = initiated.data.fundsTransferId
    fundsTransfer.depositAccountId = initiated.data.depositAccountId
    fundsTransfer.withdrawalAccountId = initiated.data.withdrawalAccountId
    fundsTransfer.withdrawalId = initiated.data.withdrawalId
    fundsTransfer.depositId = initiated.data.depositId
    fundsTransfer.amount = initiated.data.amount
    fundsTransfer.initiatedTime = initiated.data.time

    fundsTransfer.version = initiated.position
  },

  Withdrawn (fundsTransfer, withdrawn) {
    fundsTransfer.withdrawnTime = withdrawn.data.time

    fundsTransfer.version = withdrawn.position
  },

  Deposited (fundsTransfer, deposited) {
    fundsTransfer.depositedTime = deposited.data.time

    fundsTransfer.version = deposited.position
  },

  Transferred (fundsTransfer, transferred) {
    fundsTransfer.transferredTime = transferred.data.time

    fundsTransfer.version = transferred.position
  }
}
