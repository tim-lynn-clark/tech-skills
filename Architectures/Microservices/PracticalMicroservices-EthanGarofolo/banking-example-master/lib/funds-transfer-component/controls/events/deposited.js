const AmountControls = require('../amount')
const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    fundsTransferId = IdControls.example(),
    accountId = IdControls.example(),
    depositId = IdControls.example(),
    amount = AmountControls.example()
  ) {
    return {
      id: IdControls.example(),
      type: 'Deposited',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        fundsTransferId,
        accountId,
        depositId,
        amount,
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
