const AmountControls = require('../amount')
const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    fundsTransferId = IdControls.example(),
    accountId = IdControls.example(),
    withdrawalId = IdControls.example(),
    amount = AmountControls.example()
  ) {
    return {
      id: IdControls.example(),
      type: 'Withdrawn',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        fundsTransferId,
        accountId,
        withdrawalId,
        amount,
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
