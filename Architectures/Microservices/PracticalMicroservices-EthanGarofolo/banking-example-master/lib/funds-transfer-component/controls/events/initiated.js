const accountControls = require('../../../account-component/controls')
const AmountControls = require('../amount')
const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    fundsTransferId = IdControls.example(),
    depositAccountId = accountControls.id.example(),
    withdrawalAccountId = accountControls.id.example(),
    amount = AmountControls.example()
  ) {
    const depositId = IdControls.example()
    const withdrawalId = IdControls.example()

    return {
      id: IdControls.example(),
      type: 'Initiated',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        fundsTransferId,
        depositAccountId,
        depositId,
        withdrawalAccountId,
        withdrawalId,
        amount,
        time: TimeControls.Effective.example(),
        processedTime: TimeControls.Processed.example()
      }
    }
  }
}
