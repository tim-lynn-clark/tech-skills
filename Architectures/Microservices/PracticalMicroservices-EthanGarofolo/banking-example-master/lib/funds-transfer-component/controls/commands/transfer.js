const accountControls = require('../../../account-component/controls')
const AmountControls = require('../amount')
const IdControls = require('../id')
const TimeControls = require('../time')

module.exports = {
  example (
    fundsTransferId = IdControls.example(),
    depositAccountId = accountControls.id.example(),
    withdrawalAccountId = accountControls.id.example(),
    amount = AmountControls.example()
  ) {
    const transfer = {
      id: IdControls.example(),
      type: 'Transfer',
      metadata: {
        traceId: IdControls.example(),
        causationId: IdControls.example()
      },
      data: {
        fundsTransferId,
        depositAccountId,
        withdrawalAccountId,
        amount,
        time: TimeControls.Effective.example()
      }
    }

    return transfer
  }
}
