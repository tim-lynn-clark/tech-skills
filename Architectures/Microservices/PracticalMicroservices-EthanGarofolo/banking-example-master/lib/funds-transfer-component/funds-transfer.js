class FundsTransfer {
  constructor () {
    this.version = -1
  }

  initiated () {
    return !!this.initiatedTime
  }

  withdrawn () {
    return !!this.withdrawnTime
  }

  deposited () {
    return !!this.depositedTime
  }

  transferred () {
    return !!this.transferredTime
  }
}

module.exports = FundsTransfer
