function splitStreamName (streamName) {
  if (!streamName) {
    return null
  }

  return streamName.split(/-(.+)/)
}

module.exports = splitStreamName
