const App = require('./express')
const createConfig = require('./config')
const env = require('./env')

const config = createConfig({ env })
const app = App({ config, env })

function start () {
  config.components.forEach(s => s.start())

  app.listen(env.port, signalAppStart)
}

function signalAppStart () {
  console.log(`${env.appName} started`)
  console.table([
    ['Version', env.version],
    ['Port', env.port],
    ['Environment', env.env]
  ])
}

module.exports = {
  app,
  config,
  start
}
