/**
 * Created by tclark on 4/18/17.
 */

import { Mongo } from 'meteor/mongo';
// NPM library
import validUrl from 'valid-url';
import { check, Match } from 'meteor/check';

Meteor.methods({
    'links.insert': function(url){

        // Validation code
        check(url, Match.Where(url => validUrl.isUri(url)));

        // Validation passed, no error created by previous line of code
        const token = Math.random().toString(36).slice(-5);

        console.log(url, token);

        Links.insert({
            url, //url: url,
            token, //token: token,
            clicks: 0
        })
    }
});


export const Links = new Mongo.Collection('links');