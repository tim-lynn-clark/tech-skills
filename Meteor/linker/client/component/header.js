/**
 * Created by tclark on 4/18/17.
 */

import React from 'react';

const Header = () => {
    return (
        <nav className="nav navbar-default">
            <div className="navbar-header">
                <a className="navbar-brand">Linker</a>
            </div>
        </nav>
    )
};

export default Header;