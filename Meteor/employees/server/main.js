/**
 * Created by tclark on 4/17/17.
 */

// Only executes on server
import { Meteor } from "meteor/meteor";
import { Employees } from "../imports/collections/employees";
import _ from "lodash";
import { image, helpers } from "faker";

Meteor.startup(() => {
    // Great place to generate data

    // Check to see if data exists in collection
    const numberRecords = Employees.find({}).count();

    console.log(numberRecords);

    if(!numberRecords){
        // Generate some fake data
        _.times(5000, () => {
            const {name, email, phone } = helpers.createCard();

            Employees.insert({
                name,
                email,
                phone,
                avatar: image.avatar()
            });
        });
    }


    Meteor.publish('employees', function(per_page){
        return Employees.find({}, { limit: per_page });
    })

});