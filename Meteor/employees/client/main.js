/**
 * Created by tclark on 4/17/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import EmployeeList from './components/employee-list';

const App = () => {
    return (
        <div>
            <EmployeeList/>
        </div>
    );
};

// Wait for Meteor to render the HTML DOM before running client JS
Meteor.startup(() => {
    ReactDOM.render(<App/>, document.querySelector(".container"));
});

