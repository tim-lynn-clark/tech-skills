#Meteor Project Setup Steps

Create App 
    
    meteor create appname
    
Secure App
    
    meteor remove insecure
    meteor remove autopublish
    meteor npm install --save bcrypt
    

Setup React for UI (instead of Blaze)

    meteor add react-meteor-data
    npm install --save react
    npm install --save react-dom
    npm install --save react-addons-pure-render-mixin
    npm install --save react-router
    
Add Twitter Bootstrap for Styling

    meteor add twbs:bootstrap@3.3.6
    
Add Built-in Authentication

    meteor add accounts-ui
    meteor add accounts-password
    
ES6 Transpilation to ES5 (already included in all new projects)

    meteor add ecmascript
    meteor add es5-shim

JS Code Linting and adding an .eslintrc file to the root of the project

    npm install -g eslint babel-eslint
    
    {
      // This file should actually be called '.eslintrc'
      // Create it at the root directory of your Meteor app
      
      
      // I want to use babel-eslint for parsing!
      "parser": "babel-eslint",
      "env": {
        // I write for browser
        "browser": true,
        // and for the server
        "node": true,
        // with Meteor
        "meteor": true,
      },
      // To give you an idea how to override rule options:
      "rules": {
        "quotes": [2, "single"],
        "eol-last": [0],
        "no-mixed-requires": [0],
        "no-underscore-dangle": [0],
        "no-extra-strict": [0],
        "strict": [0],
        "curly": [0]
      },
      "globals": {
        // define your apps globals here
        
        // Packages
        "Picker": false,
        "FlowRouter": false,
        "FlowLayout": false,
        "GeoJSON": false,
        "EventEmitter": false,
        
        // Components
    
        // Collections
        "Customers": true
      }
    }

