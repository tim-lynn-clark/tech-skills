/**
 * Created by tclark on 4/19/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import App from './components/app';
import BinsMain from './components/bins/bins-main';
import BinsList from './components/bins/bins-list';

// Imported specifically to make Meteor methods
// in the Mongo collection available on the client
// due to this it is never directly referenced in code
import { Bins } from '../imports/collections/bins';

const routes = (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={BinsList}/>
            <Route path="bins/:binId" component={BinsMain}/>
        </Route>
    </Router>

);

Meteor.startup(() => {
    ReactDOM.render(routes, document.querySelector('.render-target'));
});