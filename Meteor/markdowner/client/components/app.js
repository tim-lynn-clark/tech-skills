/**
 * Created by tclark on 4/19/17.
 */

import React from 'react';
import Header from './header';
import BinsList from './bins/bins-list';

const App = (props) => {
    return (
        <div>
            <Header/>
            {props.children}
        </div>
    )
};

export default App;

