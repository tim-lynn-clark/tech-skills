/**
 * Created by tclark on 4/17/17.
 */

// Import React Library
import React, { Component } from "react";
import ReactDOM from "react-dom";
import ImageList from "./components/image-list";

//Temp AJAX
import axios from 'axios';


// Create component
class App extends Component {

    constructor(props){
        super(props);

        this.state = { images: [] };
    }

    componentWillMount(){
        axios.get("https://api.imgur.com/3/gallery/hot/viral/0")
            .then(response => {
                this.setState({
                    images: response.data.data
                })
            });
    }

    render(){
        return (
            <div>
                <ImageList images={ this.state.images }/>
            </div>
        )
    }

}


// Render to the screen
// Meteor.startup is a function that takes a function to be run
// after Meteor has compiled and rendered the DOM in the browser
Meteor.startup(() => {
        ReactDOM.render(<App />, document.querySelector(".container"));

    });
