/**
 * Created by tclark on 4/17/17.
 */

import React, { Component } from 'react';
import ImageDetail from "./image-detail";

const ImageList = (props) => {

    const validImages = props.images.filter((image) => {
       return !image.is_album;
    });

    const RenderedImages = validImages.map((image) => {
        return (
            <ImageDetail key={image.title} image={image} />
        )
    });

    return (
        <ul className="media-list list-group">
            { RenderedImages }
        </ul>
    )
};

export default ImageList;