var shareImageButton = document.querySelector('#share-image-button');
var createPostArea = document.querySelector('#create-post');
var closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn');

function openCreatePostModal() {
  createPostArea.style.display = 'block';

  // NOTE: Controlling the install prompt based on app event
  if(deferredInstallPrompt) {
    deferredInstallPrompt.prompt();
    deferredInstallPrompt.userChoice.then(function(choiceResult) {
      console.log(choiceResult.outcome);
      switch (choiceResult.outcome) {
        case 'dismissed':
          console.log('[feed.js] User canceled installation if PWA.');
          break;
        case 'accepted':
          console.log('[feed.js] User is installing the PWA.');
          break;
        default:
          console.log('[feed.js] Unknown.')
      }
    });
  }
}

function closeCreatePostModal() {
  createPostArea.style.display = 'none';
}

shareImageButton.addEventListener('click', openCreatePostModal);

closeCreatePostModalButton.addEventListener('click', closeCreatePostModal);
