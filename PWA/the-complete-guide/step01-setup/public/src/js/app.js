if(navigator.serviceWorker) {
    navigator.serviceWorker
        /* NOTE: Scope is an optional configuration attribute that
        * allows you to specify the scope of the service worker
        * instead of relying on the location of the service worker file,
        * which is the default used by the browser when not specified
        * in the config object.
        * */
        .register('/sw.js', {scope: '/'})
        .then(function(){
            console.log("Service worker has been registered.");
        });
}

/* NOTE: You can sort of control when the install app banner is displayed once
* all if the criteria have been met (Chrome especially). */
let deferredInstallPrompt; // See use of install prompt in feed.js on shareImageButton
window.addEventListener('beforeinstallprompt', function (event) {
    console.log('[app.js] beforeinstallprompt: install suspended.')
    event.preventDefault(); // Prevent the install prompt
    deferredInstallPrompt = event; // Store the install prompt for later use
    return false;
})