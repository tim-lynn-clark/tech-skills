self.addEventListener('install', function(event) {
    console.log('<<Service Worker>> Installing service worker...', event);
});

self.addEventListener('activate', function(event) {
    console.log('<<Service Worker>> Activating service worker...', event);
    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
    console.log('<<Service Worker>> Fetching stuff...', event);
    // NOTE: Just for demonstration purposes. Sends the fetch based on what was intercepted in the SW (proxies the request so we can log it)
    event.respondWith(fetch(event.request));
});