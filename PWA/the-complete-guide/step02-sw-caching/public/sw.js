
/* NOTES: A common method for altering the service worker so that a new one is loaded when
* cached application resources are changed as the application changes. This can be simply done by
* incrementing the cache name using versioning syntax. Even if the cache file list does not change
* if the content of any of the cached files changes, the version of the SW caches will need to be
* incremented in order for the browser to determine that a new version of the service worker is
* available and thereby reinstalling it. The new cache name based on the version, will then load
* all the cached content new as it will be empty to begin with. */
var CACHE_STATIC_NAME = 'static-v4';
var CACHE_DYNAMIC_NAME = 'dynamic-v2';

self.addEventListener('install', function(event) {
  console.log('[Service Worker] Installing Service Worker ...', event);
  /*NOTE: waitUntil will wait until a promise is fulfilled before completing the event handler. */
  event.waitUntil(
      /*NOTE: Caching the 'App Shell', all of the static content that does not dynamically change. */
    caches.open(CACHE_STATIC_NAME)
      .then(function(cache) {
        console.log('[Service Worker] Precaching App Shell');
        cache.addAll([
          '/',
          '/index.html',
          '/src/js/app.js',
          '/src/js/feed.js',
          '/src/js/promise.js',
          '/src/js/fetch.js',
          '/src/js/material.min.js',
          '/src/css/app.css',
          '/src/css/feed.css',
          '/src/images/main-image.jpg',
          'https://fonts.googleapis.com/css?family=Roboto:400,700',
          'https://fonts.googleapis.com/icon?family=Material+Icons',
          'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css'
        ]);
      })
  )
});

self.addEventListener('activate', function(event) {
  console.log('[Service Worker] Activating Service Worker ....', event);
    /*NOTE: waitUntil will wait until a promise is fulfilled before completing the event handler. */
  event.waitUntil(
      /* NOTE: Cleaning up old caches when cache is versioned and a new service worker is installed. */
    caches.keys()
      .then(function(keyList) {
        return Promise.all(keyList.map(function(key) {
            /* NOTE: Removing old caches based on the key not matching the current cache name (key). */
          if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
            console.log('[Service Worker] Removing old cache.', key);
            return caches.delete(key);
          }
        }));
      })
  );
  return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
      /*NOTE: Will always return successfully, the response will either have a cached value or null. */
    caches.match(event.request)
      .then(function(response) {
          // If there is a response, return it to the browser
        if (response) {
          return response;
        } else {
            // If not in the cache, send the actual fetch across the wire
          return fetch(event.request)
            .then(function(res) {
                // Dynamically cache content that is not included in the master cache list of the App Shell above
              return caches.open(CACHE_DYNAMIC_NAME)
                .then(function(cache) {
                  cache.put(event.request.url, res.clone());
                  return res;
                })
            })
            .catch(function(err) {

            });
        }
      })
  );
});