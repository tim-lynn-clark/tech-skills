# Service Workers

JavaScript that runs on a separate thread than that of the HTML UI and its associated JavaScript.

## Attributes

- Single threaded as all JS is
- Run on a separate thread than that of the UI JavaScript
- Cannot access the DOM as it is decoupled from the HTML
- Is not attached to a single HTML page like web workers 
- Is available to all pages within a given scope of the web application. 
    - This is determined by the folder in which the service worker is defined.
- Continue to live and operate even after all pages have been closed
- Background processes
- Can only run on HTTPS

## What Can They Do?

- Listening for and respond to events
    - Events emitted by regular page JavaScript
    - Fetch requests: HTTP requests for resources (network proxy)
        - Returning a cached asset
        - Returning a cached HTTP response from an API
    - Web push notifications from browser vendor servers
    - Show a notification to a user based on some action they performed
    - Background synchronization: storing changes a user made to application data while offline and then synchronizing that data with a server once reconnected to the internet.
    - Responding to Service Worker lifecycle events
    
## Lifecycle

- index.html loads
- app.js loads
- service worker is registered by app.js
- service worker is installed by browser
    - install event is executed
- service worker is queued for activation
    - if it has never been registered, it will activate pretty much instantly
    - if there is a previous version of the service worker registered, the new version will be queued to replace the old version once all browser windows containing pages within the service worker's scope are closed, and the application is revisited in a new tab.
    - the browser detects if there is a new version of the service worker each time the registration code is executed. It compares the cached service worker file to the new one byte-by-byte to see if there are any changes.
- service worker is activated
    - activation event is executed
- once activated, if no events occur, the service worker will move to an idle mode and will be reactivated when an event within its scope occurs.






