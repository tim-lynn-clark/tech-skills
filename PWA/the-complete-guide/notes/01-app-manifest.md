# App Manifest

A single JSON file at the root of the web app that provides the device browser information needed to install the web application on the home screen.

## What it does

- Makes the web application installable on a mobile device that supports progressive web apps
- Allows the creation of the app icon on the home screen of the device
- Lives in the root of the web application
- Is linked from the index.html and every page the website delivers to the browser (just like a stylesheet)

## What it contains

- name: long name of the app, shown on the splash screen.
- short_name: short name of the app, shown below the app icon on the home screen.
- start_url: the page from which the application will start up when the app icon is clicked.
- scope: list of the web application pages that make up the PWA experience, does not have to be the entire application, can be only a subsection.
- display: how the app should look as a standalone app. The value "standalone" removes the browser controls from the top of the device browser when loaded from the app icon.
- background_color: color of the background as the application loads and on the splash screen.
- theme_color: color of the theme used on the top bar and task switcher.
- description: texted used when the app is placed in favorites collection.
- dir: the reading direction of the application. Values: "ltr" / "rtl".
- lang: the language(s) the application supports. Values are the standard 4 letter dashed language-country codes.
- orientation: the screen orientations the application supports. Values look them up ;) 
- icons: and array of icon objects holding meta-data about each icon that can be used for the app icon on the home screen.
- related_applications: an array of application objects containing meta-data regarding native applications that can be installed for your app. If you do not have a native app, do not include this in the manifest.


## Useful Links:

- Web App Manifest - Browser Support: http://caniuse.com/#feat=web-app-manifest
- MDN Article on the Web App Manifest (includes List of all Properties): https://developer.mozilla.org/en-US/docs/Web/Manifest
- A detailed Web App Manifest Explanation by Google: https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/
- More about the "Web App Install Banner" (including Requirements): https://developers.google.com/web/fundamentals/engage-and-retain/app-install-banners/
 