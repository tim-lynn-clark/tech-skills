# Android Emulator for Testing

- Install Android studio
- Create a new project to gain easy access to AVD manager
- Open AVD manager
- Select a virtual device and run it
- Open Chrome on the VD
- Use the 10.0.2.2 proxy on the VD to get to local host on the host machine (127.0.0.1 refers to localhost on the VD so it cannot be used)

## Add to Home Screen

- Once all the PWA criteria have been met, the user should be prompted to install the PWA on their device
- If not all PWA criteria have been met yet (like while developing), you can:
    - Click the three-dot menu in chrome
    - Click on add to home screen
    - App icon should appear on the home screen

## Preparing the VD for all PWA Features

In the previous video, we set up an emulated Android device to test the PWA - this is optional.

Here are some more details about the process and how to prepare the device for the rest of the course though.

Setting up Android Studio:

In case you don't have Android Studio installed, make sure to do so. It's free!

You can download it from this URL: https://developer.android.com/studio/index.html

We only install it to get easy access to the Android Virtual Device (AVD) Manager though. You can access that Manager under "Tools" => "Android" => "AVD Manager".

Detailed instructions on how to create a device with it can be found here: https://developer.android.com/studio/run/managing-avds.html

Updating Chrome on the Virtual Device

With an emulated device up and running, you're well-prepared to test your manifest.json file. For other features you learn about in this course, the pre-installed Chrome browser is too old though (at least at the point of time this course is created).

You can easily update Chrome on your virtual device though. Get an updated APK (basically the app installation file) from this link: https://www.apkmirror.com/apk/google-inc/chrome/#variants

Feel free to choose the latest one, download and install it. Give the device the permission to install from "unsafe" sources. If it fails, try a different APK version.