# Resources

https://caniuse.com/?search=manifest

https://jakearchibald.github.io/isserviceworkerready/

Are Service Workers Ready? - Check Browser Support: https://jakearchibald.github.io/isserviceworkerready/

Setting up Remote Debugging on Chrome: https://developers.google.com/web/tools/chrome-devtools/remote-debugging/

Getting that "Web App Install Banner": https://developers.google.com/web/fundamentals/engage-and-retain/app-install-banners/

Getting Started with Service Workers (don't read too far, there's stuff in there we'll learn later ;-)): https://developers.google.com/web/fundamentals/getting-started/primers/service-workers

## Want to dive deeper into Promises?

- Introduction to Promises (MDN): https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
- Introduction to Promises (Google): https://developers.google.com/web/fundamentals/getting-started/primers/promises

## Dive deeper into the Fetch API:

- An Overview on MDN: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
- Detailed Usage Guide (MDN): https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
- Detailed Usage Guide (and comparison with XMLHttpRequest): https://davidwalsh.name/fetch
- Introduction to Fetch (Google): https://developers.google.com/web/updates/2015/03/introduction-to-fetch

## Caching

- About Cache Persistence and Storage Limits: https://jakearchibald.com/2014/offline-cookbook/#cache-persistence
- Learn more about Service Workers: https://developer.mozilla.org/en/docs/Web/API/Service_Worker_API
- Google's Introduction to Service Workers: https://developers.google.com/web/fundamentals/getting-started/primers/service-workers
- Great overview over Strategies - the Offline Cookbook: https://jakearchibald.com/2014/offline-cookbook/
- Advanced Caching Guide: https://afasterweb.com/2017/01/31/upgrading-your-service-worker-cache/
- Mozilla Strategy Cookbook: https://serviceworke.rs/strategy-cache-and-update_service-worker_doc.html