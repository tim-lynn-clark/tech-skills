# App Install Banner & Chrome 68+

In the next lecture, I'll show you how Chrome automatically shows an "App Install Banner". Starting with Chrome version 68, it'll not do this automatically anymore. You instead have to listen to a specific event (beforeinstallprompt ) and then show the banner manually.

This approach is covered in this course! You'll learn all about it in lecture 38 - don't skip the next lectures though, they're still important.

Read more about this change: https://developers.google.com/web/fundamentals/app-install-banners/

Also, if you want to manually trigger the installation of the app, you might not see the "Add to Homescreen" button in the same place I do in the lecture.

You can manually trigger installation via the Chrome website menu ("Install Instagram as Progressive Web App"):

