## Connecting Chrome Developer Tools to a Real/ Emulated Device

It's easy to connect your Chrome Developer Tools with a Real or Emulated Android Device (event though the latter unfortunately never worked for me).

The following article explains it step-by-step and by using helpful images: https://developers.google.com/web/tools/chrome-devtools/remote-debugging/

Make sure you enabled "Developer Mode" on your Device! You do that by tapping your Android Build Number (in the Settings) 7 times. Yes, this is no joke ;-)

## Steps: Activate Developer Mode in Android Device
- Go to settings
- Go to About Device (About emulated device)
- Scroll to bottom to build number section
- tab the build number 4-6 times, a notification pop up should appear telling you the number of times to tap until developer mode is turned on
- By default, USB debugging should be turned on by default when developer mode is activated
- Verify by going back to settings
- Go to System
- Go to Advanced
- Go to Developer options
- Scroll down to Debugging section
- Verify the USB debugging radio button is active

## Connecting to device through desktop chrome
- Navigate to chrome://inspect/#devices in the navigation field
- Should see all remote devices attached (physical or virtual)
- Click on port forwarding and check the "enable port forwarding" checkbox
- Under the device you are using, you should see the name of your PWA application that is actively running on the device
- There should be an inspect button under the PWA application, click it
- A new Chrome window should open with what looks like a simulated device in the window, and the dev tools open next to it
- This simulated devices is just cloning the output from your device into the Chrome window, so you can more easily debug the application