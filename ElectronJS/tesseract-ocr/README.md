# Tesseract OCR

Optical character recognition experiment using the Tesseract library.

## Dev Run
1. `npm run dev` runs build process and starts webpack dev server
2. `npm run start` runs Electron app that loads contents from webpack dev server
3. Development will happen in the `src` directory

## Dev Setup Steps
1. `npm init` initialize a npm/node application
2. `npm install --save-dev electron` install electron as a dev dependency
3. `"start": "electron ."` add to the scripts section of your package file
4. `npm install -g vue-cli` using Vue.js as the view engine for the application, install the Vue CLI to allow vue template compiling
5. `vue init webpack app` using Webpack as a workflow engine
    - Use this answers for the wizard
    - ? Project name app
    - ? Project description xxxxxxxxxx
    - ? Author xxxxxx <xxxxxx@domain.com>
    - ? Vue build standalone
    - ? Install vue-router? Yes
    - ? Use ESLint to lint your code? Yes
    - ? Pick an ESLint preset Standard
    - ? Set up unit tests No
    - ? Setup e2e tests with Nightwatch? No
6. Merge Vue.js and Electron projects together
    - combine `/package.json` and `/app/package.json` files
        - copy dependencies
        - copy engines setup
    - remove `readme`, `.gitignore`, and `package.json` file from `/app`
    - most the rest of the `/app` folder contents to `/` including all `.xxx` hidden files
    - update `package.json` `scripts` section with
        - `"start": "electron .",`
        - `"dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",`
        - `"lint": "eslint --ext .js,.vue src",`
        - `"build": "node build/build.js"`
7. `npm install concurrently --save-dev` concurrently package to handle script building in order
8. `"start": "concurrently --kill-others \"npm run dev\" \"electron .\"",` update start script
9. `screen.loadURL('http://localhost:8080/')` update main.js `createMainWindow` loadURL to load from local webpack server
10. `setTimeout(renderApp, 3000);` update main.js `createMainWindow` to add a wait for the webpack server to start up
    - NOTE: this is just for development purposes and should be removed before building for production deployment
11. `src` is where all UI development will happen for the Electron app
12. `npm install vuetify --save` used for component development
13. `npm install tesseract.js --save` Tesseract for OCR in native JavaScript

