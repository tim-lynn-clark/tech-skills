const electron      = require('electron');
const path          = require('path');
const remote        = electron.remote;
const ipc           = electron.ipcRenderer;

const closeBtn = document.getElementById('closeBtn');

closeBtn.addEventListener('click', function (event) {
    let window = remote.getCurrentWindow();
    window.close();
});

const updateBtn = document.getElementById('updateBtn');

updateBtn.addEventListener('click', function () {

    // sending data to a subscription setup on main.js to the IPC Main pub-sub
    ipc.send('update-notify-value', document.getElementById('notifyVal').value);

    // close window after setting the notify value
    let window = remote.getCurrentWindow();
    window.close();
});