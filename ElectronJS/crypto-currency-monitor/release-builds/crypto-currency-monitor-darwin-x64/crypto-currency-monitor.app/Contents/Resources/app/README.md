# Crypto Currency Monitor

Small demo cross-platform desktop application written on top of the Electron JS framework 
that monitors crypto currencies and provides their current dollar value.

### Lessons Learned

* Creating Menus (main.js)
* Handling CMD+Q for closing app in OS X using globalShortcut (main.js)
* Using BrowserWindow to load a Modal Dialog (index.js)
* Adding transparency to the Modal Dialog by setting transparent attribute and using CSS (add.css)
* Making Modal Dialog stay on top of other windows
* Closing a Modal Dialog (add.js)
* Using the Axios library to make XMLHttpRequests to a web API (index.js)
* using Electron IPC (Inter-Process-Communication) library to share data between windows (built-in pub-sub system in electron)
    * Declare Main IPC and setting up custom event subscriptions to which windows can publish data (main.js)
    * Using IPC Renderer to send data to a published named subscription (add.js)
    * Using IPC Renderer to receive data published to a named subscription (index.js)
* Creating and sending the OS notifications to show the user (index.js)
* Deploying the application to OS X/Linux/Windows

### Technology

* JavaScript + HTML + CSS
* Node JS + NPM
* Electron JS - https://electronjs.org
* Axios JS - https://www.npmjs.com/package/axios

### References

* Learn Electron in Less than 60 Minutes - https://www.youtube.com/watch?v=2RxHQoiDctI
* Electron Quick Start - https://electronjs.org/docs/tutorial/quick-start
* Electron API Demos - https://github.com/electron/electron-api-demos
* Debugging Electron with Webstorm IDE - https://blog.jetbrains.com/webstorm/2016/05/getting-started-with-electron-in-webstorm/
* Electron packager tutorial - https://www.christianengvall.se/electron-packager-tutorial/
