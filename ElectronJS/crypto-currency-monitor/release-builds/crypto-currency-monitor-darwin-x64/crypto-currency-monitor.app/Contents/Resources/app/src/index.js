const electron      = require('electron');
const path          = require('path');
const BrowserWindow = electron.remote.BrowserWindow;
const axios         = require('axios');
const ipc           = electron.ipcRenderer;

const notifyBtn     = document.getElementById('notifyBtn');
let price           = document.querySelector('h1');
let targetPrice     = document.getElementById('targetPrice');
let targetPriceVal;

// Notification object for use with OS system notifications from app
const notification = {
    title: 'BTC Alert',
    body: 'BTC just beat your target price!',
    icon: path.join(__dirname, '../assets/images/btc.png')
};

/**
 * Pulls BTC value in USD from Crypto API using the Axios library
 */
function getBTC() {
    axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC&tsyms=USD')
        .then(res => {
            const cryptos = res.data.BTC.USD;
            price.innerHTML = '$' + cryptos.toLocaleString('en');

            // Setup OS notification to user when BTC USD value exceeds base value set by user
            if(targetPrice.innerHTML !== '' && targetPriceVal < res.data.BTC.USD){
                const myNotification = new window.Notification(notification.title, notification);
            }
        })
}
getBTC();
setInterval(getBTC, 1800000);

notifyBtn.addEventListener('click', function (event) {
    const modalPath = path.join('file://', __dirname, 'add.html');
    // 'frame' will make it a frameless modal
    // 'transparent' adds transparency to the window (must add CSS see add.css)
    // 'alwaysOnTop' forces windows to remain on top of other windows
    let win = new BrowserWindow({
        frame: false,
        transparent: true,
        alwaysOnTop: true,
        width: 400,
        height: 200
    });
    win.on('close', function () {
        win = null;
    });
    win.loadURL(modalPath);
    win.show();
});

// IPC event to listen for data being published with the subscription name 'targetPriceVal' sent from main.js
// in response to data being published to the subscription named 'update-notify-value'
ipc.on('targetPriceVal', function (event, arg) {
    targetPriceVal = Number(arg);
    targetPrice.innerHTML = '$' + targetPriceVal.toLocaleString('en');
});