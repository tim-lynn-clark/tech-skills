// Application entry point

/**
 * Boilerplate code copied from Electron Quick Start
 * https://electronjs.org/docs/tutorial/first-app#electron-development-in-a-nutshell
 */

// <<-- MOD: Menu added to Electron imports -->>
// <<-- MOD: Global Shortcut module added to Electron imports  -->>
const {app, BrowserWindow, Menu, globalShortcut} = require('electron');
const path = require('path');
const url = require('url');
// <<-- MOD: Importing Shell from Electron to add the ability to open a new browser window -->>
const shell = require('electron').shell;
// <<-- MOD: Import IPC for inter-window data sharing -->>
const ipc = require('electron').ipcMain;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createWindow () {
    // Create the browser window.
    win = new BrowserWindow({
        title: 'Crypto Currency Monitor',
        width: 800,
        height: 600});

    // and load the index.html of the app.
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'src/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //win.webContents.openDevTools();

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null
    });

    // <<-- MOD: Menu object setup -->>
    let menu = Menu.buildFromTemplate([
        {
            label: 'Menu',
            submenu: [
                { label: 'Adjust Notification Value' },
                { type: 'separator' },
                {
                    label: 'CoinMarketCap',
                    click() {

                         // Shell allows the app to request that the OS open a new browser windows using the
                         // system default browser and load the provided URL in that window.
                        shell.openExternal('http://coinmarketcap.com');
                    }
                },
                { type: 'separator' },
                {
                    label: 'Exit',
                    click() {
                        app.quit();
                    }
                },
            ]
        },
        {
            label: 'Info',
            submenu: [
                { label: 'About the developer',
                click() {

                    // Shell allows the app to request that the OS open a new browser windows using the
                    // system default browser and load the provided URL in that window.
                    shell.openExternal('https://memorytin.com/the-professional/');
                }
}
            ]
        }
    ]);
    // <<-- MOD: Menu added to app -->>
    Menu.setApplicationMenu(menu);

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

// <<-- MOD: IPC event handlers for inter-window data sharing -->>
// IPC is a built-in pub-sub system within electron, which allows windows to
// publish data to a defined subscription
ipc.on('update-notify-value', function (event, arg) {
    // data from the event is then sent to the index.html/index.js
    // which make up the view of the main window represented by the win object below
    win.webContents.send('targetPriceVal', arg);
});