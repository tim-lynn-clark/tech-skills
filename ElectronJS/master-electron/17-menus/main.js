const electron = require('electron');
const {app, BrowserWindow, Menu, MenuItem, session, globalShortcut, dialog} = electron;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

//NOTE: Create a new menu
// let mainMenu = new Menu();
//NOTE: on OS X, the first menu item is always the name of the application
//NOTE: unfortunately, no matter what you set the label of the first menu item to, it always display 'Electron' until the app name is configured
//NOTE: submenus are arrays of menuItems, you can create them manually and add them to the array or you can use menuItem literals (templates)
// let menuItem1 = new MenuItem({
//     label: 'My App',
//     submenu: [
//         { label: 'Item 1' },
//         { label: 'Item 2' },
//         { label: 'Item 3' }
//     ]
// });
//NOTE: add menu item to menu
// mainMenu.append(menuItem1);

//NOTE: Menus can also be created using menu literals (template objects) instead of manually creating each menu and menu item object(s)
//NOTE: Menus templates can be imported from another JS module using require, see mainMenu.js
let mainMenu = Menu.buildFromTemplate(require('./mainMenu'));

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
//NOTE: extending on-ready event to not only create the new window but add a menu to the app as well
app.on('ready', () => {
    createWindow();
    //NOTE: add custom menu as app menu
    Menu.setApplicationMenu(mainMenu);
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
