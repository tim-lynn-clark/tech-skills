
//NOTE: functionality can be added to menu items using the click event
//NOTE: keyboard shortcuts can be added to menu items using accelerators, the shortcut is automatically added to the menu item's label text
//NOTE: menu items can be enabled and disabled using the 'enabled' property
//NOTE: menu items can be assigned roles or predefined behaviors provided by Electron (common system functionality: copy, paste, undo, toggle dev tools)
//NOTE: some roles provide their own accelerators and labels, see action menu below
module.exports = [
    {
        label: 'My App',
        submenu: [
            {
                label: 'Item 1',
                click: () => {
                    console.log('=>> Menu item 1 clicked');
                },
                accelerator: 'Shift+Alt+G'
            },
            {
                label: 'Item 2',
                enabled: false
            },
            {
                label: 'Toggle Dev Tools',
                role: 'toggledevtools'
            }
        ]
    },{
        label: 'Actions',
        submenu: [
            { role: 'togglefullscreen' },
            { role: 'copy' },
            { role: 'paste' },
            { role: 'undo' },
            { role: 'redo' }
        ]
    }
];
//NOTE: with the addition of the copy, paste, undo, redo roles being added to the menu, Electron automatically adds a 'start dictation' menu item and an 'emoji & symbols' menu item as well.