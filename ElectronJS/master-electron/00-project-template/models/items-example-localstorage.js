
// Track items
exports.itemsToRead = JSON.parse(localStorage.getItem('itemsToRead')) || [];

// Use default session and local storage to save the item list
exports.saveItems = () => {
    localStorage.setItem('itemsToRead', JSON.stringify(this.itemsToRead))
};

exports.selectItem = (e) => {

    // Remove selection CSS from all others as a new item is being selected
    $('.read-item').removeClass('is-active');
    $(e.currentTarget).addClass('is-active');
};

// Select next/previous
exports.changeItem = (direction) => {
    //Get currently selected item
    let activeItem = $('.read-item.is-active');
    //check direction and select accordingly
    let newItem = (direction === 'down') ? activeItem.next('.read-item') : activeItem.prev('.read-item');
    // Only if item exists, make selection change
    if(newItem.length){
        activeItem.removeClass('is-active');
        newItem.addClass('is-active');
    }
};

// Open the selected item in the system default browser not a new window
exports.openInBrowser = window.openInBrowser = () => {
    //only if items to read exist
    if(!this.itemsToRead.length) return;

    //get selected item
    let targetItem  = $('.read-item.is-active');

    //Open in system default browser using electron shell API
    require('electron').shell.openExternal(targetItem.data('url'));
};

// Open item for reading
// Allowing access to this module method both on the module and on the window object in which the module gets loaded
// in this case we will be accessing this method through a menu item that does not have access to the module but the window
exports.openItem = window.openItem = () => {
    //only if items to read exist
    if(!this.itemsToRead.length) return;

    //get selected item
    let targetItem  = $('.read-item.is-active');

    //get content URL from item
    let contentURL  = encodeURIComponent(targetItem.data('url'));
    let itemIndex   = targetItem.index() - 1;

    console.log('=>> Opening item: ' + contentURL);

    // Reader window url, local file
    let readerWinURL = `file://${__dirname}/reader.html?url=${contentURL}&itemIndex=${itemIndex}`;

    //Open content URL in a new proxy browser window
    let readerWindow = window.open(readerWinURL, targetItem.data('title'));
};


exports.addItem = (item) => {

    // Add to collection


    // Hide 'no items' message if active
    $('#no-items').hide();

    // New Item HTML
    let itemHTML = `<a class="panel-block read-item" data-url="${item.url}" data-title="${item.title}">
                        <figure class="image has-shadow is-64x64 thumbnail">
                            <img src="${item.screenshot}"/>
                        </figure>
                        <h2 class="title is-4 column">${item.title}</h2>
                    </a>`;

    // Append to read-list
    $('#read-list').append(itemHTML);

    // Handle selection event on each item added
    $('.read-item')
        .off('click, dblclick')
        .on('click', this.selectItem)
        .on('dblclick', this.openItem);
};

// Delete item by index
// Allowing access to this module method both on the module and on the window object in which the module gets loaded
// in this case we will be accessing this method through a menu item that does not have access to the module but the window
exports.deleteItem = window.deleteItem = (i = false) => {
    console.log(i);

    // SEt i to active item if not passed set
    if(i === false) i = ($('.read-item.is-active').index() -1);

    // Remove from DOM
    $('.read-item').eq(i).remove();

    // Remove from Item store
    this.itemsToRead = this.itemsToRead.filter((item, index) => {
        return index !== i;
    });

    // Update store in local storage
    this.saveItems();

    // Select a new item
    if(this.itemsToRead.length){
        // if first item was deleted, select the new first item
        let newIndex = (i === 0) ? 0 : i-1;
        // Assign active class to item at new index
        $('.read-item').eq(newIndex).addClass('is-active')
    } else {
        //No items left in the array show no items message
        $('.no-items').show();
    }
};