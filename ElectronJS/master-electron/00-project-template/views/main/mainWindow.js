// Electron Packages
const {BrowserWindow} = require('electron');

// Node Packages
const path  = require('path');
const url = require('url');

// Third-Party Packages
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

// Local Packages
const debug         = require('../../lib/utilities/debuggers').component.mainLogger;
const contextMenu   = require('../../lib/menus/contextMenu');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
exports.win = null;

exports.createWindow = () => {
    debug('=>> creating mainWindow...');

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // By default Chromium automatically throttles down all browser windows when they are not
    // the active window. This means that any processing the window is doing when it loses focus
    // will practically stop until the window is re-focused. To stop the behavior, when you have
    // work that must be accomplished even though the window in which the works is being done is
    // no longer in focus, you must use the 'webPreferences' configuration object in your
    // window configuration to set 'backgroundThrottling' to false.
    // webPreferences: { backgroundThrottling: false }

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    debug('=>> loading index.html into mainWindow');
    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(global.appRoot, '/views/main/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    //EXAMPLE: Implementing the touch bar on Mac OS X
    if(process.platform === 'darwin') {
        //Add touch bar to the main window
        const touchBar = require('../../lib/menus/touchBar');
        touchBar.createBar();
        mainWindow.setTouchBar(touchBar.bar);
    }

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        debug('=>> mainWindow closed');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // Handle the call for a context menu
    mainWindow.webContents.on('context-menu', (e) => {
        debug('=>> activating context menu on right click event');
        e.preventDefault();
        contextMenu.createMenu();
        contextMenu.menu.popup({});
    });
};