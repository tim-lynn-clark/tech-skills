// Electron Packages
const electron = require('electron');
const {app, Menu, session, ipcMain, globalShortcut} = electron;

// Node Packages
const path  = require('path');

// Third-Party Packages

// Local Packages
const debug      = require('./lib/utilities/debuggers').component.mainLogger;
const mainWindow = require('./views/main/mainWindow');
const mainMenu   = require('./lib/menus/mainMenu');
const trayMenu   = require('./lib/menus/trayMenu');

//-------------------------------------------------------------------------------------------
//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);
//-------------------------------------------------------------------------------------------

// Debug: application execution
debug('=>> main.js executing');

// <<-- MOD: Set root path for application folder -->
global.appRoot = path.resolve(__dirname);
//remote.getGlobal('appRoot'); //use this to access from the renderer process

//--------------------------------------------
//EXAMPLE: IPC messaging between main process and renderer process
// IPC listener on the main application process
ipcMain.on('test-channel', (e, args) => {
    console.log('=>> SERVER - IPC ARGS: ' + args);

    //responding to a message sent via this channel
    e.sender.send('test-channel', 'Message from IPCRenderer received on main process: ' + args);
});
//--------------------------------------------

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {

    //To hide app dock icon if using tray icon
    // app.doc.hide()

    debug('=>> Application ready: creating main window');
    mainWindow.createWindow(app);
    debug('=>> Application ready: creating tray icon + menu');
    trayMenu.createTray();
    debug('=>> Application ready: setting up application menu');
    mainMenu.createMenu();
    Menu.setApplicationMenu(mainMenu.menu);

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            debug('=>> CMD+Q called on Mac, closing application');
            app.quit();
        })
    }

    // Monitoring computer power state changes
    electron.powerMonitor.on('on-ac', () => {
        debug('=>> Power state change: on AC power');
    });
    electron.powerMonitor.on('on-battery', () => {
        debug('=>> Power state change: running on batteries');
    });
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        debug('=>> All app windows closed: quitting app on all platforms except Mac');
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        mainWindow.createWindow(app)
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
