// Electron Packages
const electron = require('electron');
const {Menu} = electron;

exports.template = [
    { role: 'copy' },
    { role: 'paste' },
    { type: 'separator' },
    { role: 'undo' },
    { role: 'redo' },
    { type: 'separator' },
    { role: 'togglefullscreen' },
];

exports.menu = null;

exports.createMenu = () => {
    this.menu = Menu.buildFromTemplate(this.template);
};