// Electron Packages
const electron = require('electron');
const {Menu, Tray} = electron;

exports.template = [
    { label: 'Tray Menu Item' },
    { label: 'quit app', role: 'quit' }
];

exports.tray = null;

// Function to create a new Tray so your application shows up in the OS tray
exports.createTray = () => {
    this.tray = new Tray('./assets/images/tray-icon24.png');
    this.tray.setToolTip('My Electron App');

    const trayMenu = Menu.buildFromTemplate(this.template);
    this.tray.setContextMenu(trayMenu);
};