// Electron Packages
const electron = require('electron');
const {Menu} = electron;

let template = [
    {
        label: 'My App',
        submenu: [
            {
                label: 'Item 1',
                click: () => {
                    console.log('=>> Menu item 1 clicked');
                },
                accelerator: 'Shift+Alt+G'
            },
            {
                label: 'Item 2',
                enabled: false
            },{
                role: 'reload'
            }
        ]
    },{
        label: 'Actions',
        submenu: [
            { role: 'togglefullscreen' },
            { role: 'copy' },
            { role: 'paste' },
            { role: 'undo' },
            { role: 'redo' }
        ]
    }
];

exports.menu = null;

exports.createMenu = () => {
    addDevToolsItem();
    this.menu = Menu.buildFromTemplate(template);
};

function addDevToolsItem() {
    if (process.env.NODE_ENV !== 'production') {
        template[0]['submenu'].push({
            label: 'Toggle Dev Tools',
            accelerator: 'CmdOrCtrl+Shift+T',
            click(item, focusedWindow) {
                if(focusedWindow.isDevToolsOpened()) {
                    focusedWindow.closeDevTools();
                } else {
                    focusedWindow.openDevTools({mode: 'detach'});
                }
            }
        })
    }
}