// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
exports.bar = null;

exports.createBar = () => {
//Only initialize and add touch bar support if the current platform is Mac
    if(process.platform === 'darwin') {
        // Import Touch Bar Modules to be used
        const path  = require('path');
        const electron = require('electron');
        const {TouchBar} = electron;
        const {TouchBarLabel, TouchBarButton, TouchBarSpacer, TouchBarColorPicker, TouchBarSlider, TouchBarPopover} = TouchBar;

        //New touch bar button
        const updateButton = new TouchBarButton({
            label: 'Reload',
            icon: path.join(__dirname, '/assets/images/reload-2x.png'),
            iconPosition: 'left',
            backgroundColor: '#00E28B',
            click: () => {
                app.relaunch();
                app.exit(0);
            }
        });
        const spacer = new TouchBarSpacer({
            size: 'large'
        });
        const labelMyTouchBar = new TouchBar.TouchBarLabel({
            label: 'Theme'
        });
        const colorPicker = new TouchBarColorPicker({
            change: (color) => {
                mainWindow.webContents.insertCSS(`body{background:${color};}`);
            }
        });

        //To provide the silder with as much touch bar space as possible, so it is usable, we use a Popover touch bar
        const slider = new TouchBarSlider({
            label: 'size',
            minValue: 500,
            maxValue: 1000,
            value: 500,
            change: (val) => {
                mainWindow.setSize(val, val, true);
            }
        });
        const popoverTouchBar = new TouchBar([
            slider
        ]);
        const sliderPopover = new TouchBarPopover({
            items: popoverTouchBar,
            label: 'Size'
        });

        //Create new touch bar
        this.bar = new TouchBar([
            labelMyTouchBar,
            colorPicker,
            spacer,
            sliderPopover,
            spacer,
            updateButton
        ]);
    }
};
