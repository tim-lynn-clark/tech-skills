const electron = require('electron');
//NOTE: importing Tray from Electron for use
const {app, BrowserWindow, Menu, MenuItem, session, globalShortcut, dialog, Tray} = electron;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Setup main app menu
let mainMenu = Menu.buildFromTemplate(require('./mainMenu'));
// Setup a context menu
let contextMenu = Menu.buildFromTemplate(require('./contextMenu'));

//NOTE: New function to create a new Tray so your application shows up in the OS tray
//NOTE: when creating a new tray, you must supply an icon in the proper format
function createTray(){
    tray = new Tray('./tray-icon24.png');
    tray.setToolTip('My Electron App');

    //NOTE: a context menu can be added to the tray icon for the app
    const trayMenu = Menu.buildFromTemplate([
        { label: 'Tray Menu Item' },
        { label: 'quit app', role: 'quit' }
    ]);
    tray.setContextMenu(trayMenu);
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // Handle the call for a context menu
    mainWindow.webContents.on('context-menu', (e) => {
        e.preventDefault();
        contextMenu.popup({});
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    createWindow();
    //NOTE: create tray when app is ready
    createTray();
    // add main menu as app menu
    Menu.setApplicationMenu(mainMenu);
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
