const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
// <<-- MOD: Global Shortcut module added to Electron imports  -->>
const globalShortcut = electron.globalShortcut;

const path = require('path');
const url = require('url');

//TODO: Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    //NOTE: Create the browser window.
    //NOTE: Graceful showing of content, first set the show property to false
    mainWindow = new BrowserWindow({width: 1200, height: 800, show: false});

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    //NOTE: Graceful showing of content, second use the once method to listen for the ready-to-show event
    //NOTE: the 'once' method sets up an event listener that once executed a single time, is destroyed (unlike the 'on' method)
    mainWindow.once('ready-to-show', function (e) {
        //NOTE: use the show method on the window to show the content only once the content has been loaded
        //NOTE: and the window is ready to show/display the loaded content
        mainWindow.show();
    });

    //NOTE: and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    //NOTE: does not need to load local files, you can load https://www.google.com if you like
    // mainWindow.loadURL('https://www.google.com');

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
