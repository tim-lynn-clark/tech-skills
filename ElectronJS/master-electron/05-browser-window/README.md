# Browser Window
https://electronjs.org/docs/api/browser-window#browserwindow

* Loading local files (done in most Electron apps)
* Can also load remote HTML page (google.com)

## Showing window gracefully 
https://electronjs.org/docs/api/browser-window#showing-window-gracefully

Used to stop the flicker that happens when the app window shows and it takes 
some time to load the actual content. This is especially important when the 
content of the window has an image or color background. The window is white 
and takes a second to load the content then flickers to the image or color 
of the loaded HTML. 

