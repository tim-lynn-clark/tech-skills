
module.exports = [
    { role: 'copy' },
    { role: 'paste' },
    { type: 'separator' },
    { role: 'undo' },
    { role: 'redo' },
    { type: 'separator' },
    { role: 'togglefullscreen' },
];