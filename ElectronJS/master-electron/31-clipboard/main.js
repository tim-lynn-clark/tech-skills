// Electron Packages
const electron = require('electron');
const {app, BrowserWindow, Menu, MenuItem, session, globalShortcut, dialog, Tray, ipcMain} = electron;

// Node Packages
const path  = require('path');
const url   = require('url');

// Third-Party Packages
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

// Local Packages
const debug                 = require('./lib/utilities/debuggers').component.mainLogger;
const mainMenuTemplate      = require('./lib/menus/mainMenu');
const contextMenuTemplate   = require('./lib/menus/contextMenu');
const trayMenuTemplate      = require('./lib/menus/trayMenu');

//-------------------------------------------------------------------------------------------
//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);
//-------------------------------------------------------------------------------------------

// Debug: application execution
debug('=>> main.js executing');

// <<-- MOD: Set root path for application folder -->
global.appRoot = path.resolve(__dirname);
//remote.getGlobal('appRoot'); //use this to access from the renderer process

// Setup main app menu
let mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
// Setup a context menu
let contextMenu = Menu.buildFromTemplate(contextMenuTemplate);
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

//--------------------------------------------
//EXAMPLE: IPC messaging between main process and renderer process
// IPC listener on the main application process
ipcMain.on('test-channel', (e, args) => {
    console.log('=>> SERVER - IPC ARGS: ' + args);

    //responding to a message sent via this channel
    e.sender.send('test-channel', 'Message from IPCRenderer received on main process: ' + args);
});
//--------------------------------------------

// Function to create a new Tray so your application shows up in the OS tray
function createTray(){
    tray = new Tray('./assets/images/tray-icon24.png');
    tray.setToolTip('My Electron App');

    const trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
    tray.setContextMenu(trayMenu);
}

function createWindow () {
    debug('=>> creating mainWindow...');

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    debug('=>> loading index.html into mainWindow');
    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, '/views/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    //--------------------------------------------
    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();
    //--------------------------------------------

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        debug('=>> mainWindow closed');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // Handle the call for a context menu
    mainWindow.webContents.on('context-menu', (e) => {
        debug('=>> activating context menu on right click event');
        e.preventDefault();
        contextMenu.popup({});
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            debug('=>> CMD+Q called on Mac, closing application');
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    debug('=>> Application ready: creating main window');
    createWindow();
    debug('=>> Application ready: creating tray icon + menu');
    createTray();
    debug('=>> Application ready: setting up application menu');
    Menu.setApplicationMenu(mainMenu);

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // Monitoring computer power state changes
    electron.powerMonitor.on('on-ac', () => {
        debug('=>> Power state change: on AC power');
    });
    electron.powerMonitor.on('on-battery', () => {
        debug('=>> Power state change: running on batteries');
    });
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        debug('=>> All app windows closed: quitting app on all platforms except Mac');
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
