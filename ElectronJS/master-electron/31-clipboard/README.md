# Shared API: Clipboard
https://electronjs.org/docs/api/clipboard#clipboard

Performing copy and paste operations on the system clipboard. Provides methods for reading 
and writing data of different formats as well as text (image, HTML, rich text, bookmarks, 
buffered data).

Examples see index.html