module.exports = [
    {
        label: 'My App',
        submenu: [
            {
                label: 'Item 1',
                click: () => {
                    console.log('=>> Menu item 1 clicked');
                },
                accelerator: 'Shift+Alt+G'
            },
            {
                label: 'Item 2',
                enabled: false
            },
            {
                label: 'Toggle Dev Tools',
                role: 'toggledevtools'
            }
        ]
    },{
        label: 'Actions',
        submenu: [
            { role: 'togglefullscreen' },
            { role: 'copy' },
            { role: 'paste' },
            { role: 'undo' },
            { role: 'redo' }
        ]
    }
];