# Shared API: Process
https://electronjs.org/docs/api/process#process

Electron's process object is extended from the Node.js process object and is 
accessible from both the main process as well as the renderer process. 

See examples in `main.js` and `index.html`