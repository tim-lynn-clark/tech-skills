const debug = require('debug');
const app   = 'electron-template';

// Generic Logs
const errors = debug(`${app}:errors`);
const values = debug(`${app}:values`);

// Component-specific Logs
const authProc  = debug(`${app}:processors:authorizationProcessor`);
const main     = debug(`${app}:main`);

// Wrappers
function errorLogger(message){
    if(errors.enabled) errors(message);
}

function valuesLogger(message){
    if(values.enabled) values(message);
}

function authorizationProcessorLogger(message){
    if(authProc.enabled) authProc(message);
}

function mainLogger(message){
    if(main.enabled) main(message);
}

module.exports = {
    generic: {
        errorLogger,
        valuesLogger
    },
    component: {
        mainLogger,
        authorizationProcessorLogger
    }
};
