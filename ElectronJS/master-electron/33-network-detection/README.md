# Network Detection (HTML 5 API not Electron)
https://developer.mozilla.org/en-US/docs/Web/API/NavigatorOnLine/Online_and_offline_events

Allows a developer to detect when the browser has an internet connection 
and when it does not, through the use of event listeners allowing the application 
to respond instantly to changes in connectivity. 

Examples see index.html
