# Off-screen Rendering
https://electronjs.org/docs/tutorial/offscreen-rendering#offscreen-rendering

Allows the developer to control how the content of a window is rendered, 
either using software (CPU) or hardware (GPU) rendering. 

For most use-cases software rendering is sufficient and will be used.

Examples see main.js