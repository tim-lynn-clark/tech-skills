// Electron Packages
const electron = require('electron');
const {app, BrowserWindow, Menu, MenuItem, session, globalShortcut, dialog, Tray, ipcMain} = electron;

// Node Packages
const path  = require('path');
const url   = require('url');
const fs    = require('fs');

//NOTE: to use software rendering, you must first turn hardware (GPU) acceleration off
app.disableHardwareAcceleration();

//NOTE: hold a reference to the window in which the background rendering is to take place
let bgWindow;

app.on('ready', () => {

    //NOTE: setting offscreen to true in the webPreferences object of the browser window initialization object
    bgWindow = new BrowserWindow({
        show: false,
        width: 1200,
        height: 800,
        webPreferences: {
            offscreen: true
        }
    });

    //NOTE: loading content in background (off screen)
    bgWindow.loadURL('http://www.neumont.edu');

    bgWindow.webContents.on('did-finish-load', () => {
        console.log(bgWindow.getTitle());
        // app.quit();
    });

    //NOTE: Capturing screenshots as window contents are changed by listening for the paint event
    let i =1;
    bgWindow.webContents.on('paint', (e, dirtyArea, nativeImage) => {
        let img = nativeImage.toPNG(100);
        fs.writeFile(`/Users/timothyclark/Downloads/screenshot_${i}.png`, img);
        i++;
    })

});