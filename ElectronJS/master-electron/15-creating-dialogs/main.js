const electron = require('electron');

//NOTE: adding 'dialog' to the Electron imports
const {app, BrowserWindow, Menu, session, globalShortcut, dialog} = electron;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

//NOTE: Create function to show a dialog
function showOpenFileDialog(){
    //NOTE: open a standard file dialog to the downloads folder
    let downloadFolder = app.getPath('downloads');
    //NOTE: defaultPath specifies where on the OS the dialog should open the dialog at
    //NOTE: buttonLabel specifies the text to put on the button in the dialog
    //NOTE: properties allows you to specify the functionality the native dialog should have
    // - openFile allows the user to open a file from the dialog
    // - multiSelections allows multiple files to be selected in the dialog (returns an array of file paths)
    // - createDirectory activates the 'new folder' button
    dialog.showOpenDialog({
        defaultPath: downloadFolder,
        buttonLabel: 'Select Spinner',
        properties: [
            'openFile',
            'multiSelections',
            'createDirectory'
        ]
    }, (openPath) => {
        console.log('=>> Selected File: ' + openPath);
    });
}

//NOTE: Create a file save dialog
function showSaveFileDialog(){

    let documentsFolder = app.getPath('documents');

    dialog.showSaveDialog({
        defaultPath: documentsFolder
    }, (filename) => {
        console.log(filename);
    });
}

//NOTE: Create a message dialog
function showMessageDialog(){

    //NOTE: message dialog uses an array of response buttons to obtain a user's response to the message displayed
    let buttons = ['yes', 'no', 'maybe'];

    dialog.showMessageBox({
        title: 'Question for you',
        message: 'Do you like bunnies?',
        detail: 'because I really really really want to know.',
        buttons: buttons
    }, (buttonIndex) => {
        //NOTE: message dialog will send back the index of the button that was selected by the user
        let selectedResponse = buttons[buttonIndex];
        console.log('=>> User Response: ' + selectedResponse);
    });
}

//NOTE: Create an error message dialog
function showErrorMessageDialog(){
    dialog.showErrorBox('Error', 'An error has happened, please restart the application.');
}



function createWindow () {

    //NOTE: just for show, use set timeout can call the show dialog function
    setTimeout(showOpenFileDialog, 1000);
    setTimeout(showSaveFileDialog, 2000);
    setTimeout(showMessageDialog, 3000);
    setTimeout(showErrorMessageDialog, 4000);

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
