const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
// <<-- MOD: Global Shortcut module added to Electron imports  -->>
const globalShortcut = electron.globalShortcut;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');
const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    //mainWindow.loadURL('https://www.google.com');

    //NOTE: Learing about web contents
    let mainContents = mainWindow.webContents;
    console.log(mainContents);
    mainContents.on('did-finish-load', () => {
        console.log('=>> web content loaded');
    });

    //NOTE: events for changing content within a web content object
    mainContents.on('will-navigate', (e, url) => {
        console.log('=>> will navigate to: ' + url);
    });
    mainContents.on('did-navigate', (e, url) => {
        console.log('=>> did navigate to: ' + url);
    });

    //NOTE: Electron automatically opens a new window when the user clicks a link with a target of _blank
    // mainContents.on('new-window', (e, url) => {
    //     console.log('=>> new window created for: ' + url);
    // });
    //NOTE: you can redirect a link with a _blank target and load it in a modal window
    mainContents.on('new-window', (e, url) => {
        console.log('=>> new window created for: ' + url);
        e.preventDefault();

        let modalWindow = new BrowserWindow({
            width: 600,
            height: 300,
            parent: mainWindow,
            modal: true
        });

        modalWindow.loadURL(url);

        modalWindow.on('closed', function () {
            console.log('=>> modal window closed...');
            modalWindow = null;
        });
    });

    //NOTE: responding to context menu events
    mainContents.on('context-menu', (e, params) => {
        console.log('Context menu opened on: ' + params.mediaType + ' at x: ' + params.x + ' and at y: ' + params.y);
        console.log('User selected text: ' + params.selectionText);
        console.log('Selection can be copied: ' + params.editFlags.canCopy);
    });


    // Open the DevTools.
    //FIXME: Comment statement out
    //mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
