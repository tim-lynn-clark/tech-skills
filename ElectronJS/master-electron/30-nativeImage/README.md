# Shared API: NativeImage
https://electronjs.org/docs/api/native-image#nativeimage

For creating tray, dock, and application icons using PNG or JPG files.

Examples see index.html