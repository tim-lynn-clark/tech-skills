# WebView Tag
https://electronjs.org/docs/api/webview-tag#webview-tag

Use the webview tag to embed 'guest' content (such as web pages) in your 
Electron app. The guest content is contained within the webview container. 
An embedded page within your app controls how the guest content is laid out 
and rendered.

Unlike an iframe, the webview runs in a separate process than your app. 
It doesn't have the same permissions as your web page and all interactions 
between your app and embedded content will be asynchronous. This keeps your 
app safe from the embedded content. Note: Most methods called on the webview 
from the host page require a synchronous call to the main process.

See `index.html` for example code.