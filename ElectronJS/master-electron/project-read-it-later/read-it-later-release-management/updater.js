
const {dialog, BrowserWindow, ipcMain}      = require('electron');
const {autoUpdater} = require('electron-updater');

autoUpdater.logger = require('electron-log');
autoUpdater.logger.transports.file.level = 'info';
// Logs to default paths based on OS
// on Linux: ~/.config/<app name>/log.log
// on OS X: ~/Library/Logs/<app name>/log.log
// on Windows: %USERPROFILE%\AppData\Roaming\<app name>\log.log

autoUpdater.autoDownload = false;

exports.check = () => {
    console.log('=>> Checking for app updates.');
    autoUpdater.checkForUpdates();

    //NOTE: Listen for update available event to be fired
    autoUpdater.on('update-available', () => {

        //track download progress percent
        let downloadProgress = 0;

        // Ask user if they would like the update
        dialog.showMessageBox({
            type: 'info',
            title: 'Update Available',
            message: 'A new version of ReadItLater is available. Do you want to update now?',
            buttons: ['Update', 'No']
        }, (buttonIndex) => {

            //if not 'update' return
            if(buttonIndex !== 0) return;

            //start download and show download progress in new window
            autoUpdater.downloadUpdate();

            //Create progress window
            let progressWin = new BrowserWindow({
                width: 350,
                height: 35,
                useContentSize: true,
                autoHideMenuBar: true,
                maximizable: false,
                fullscreen: false,
                fullscreenable: false,
                resizable: false
            });

            //Load progress HTML
            progressWin.loadURL(`file://${__dirname}/renderer/progress.html`);

            //Handle close
            progressWin.on('closed', () => {
                progressWin = null;
            });

            //Listen for progress update requests from renderer process
            ipcMain.on('download-progress-request', (e) => {
                // This is a synchronous channel based on the renderer request
                e.returnValue = downloadProgress;
            });

            //track download progress on autoUpdater
            autoUpdater.on('download-progress', (d) => {
                downloadProgress = d.percent;
                autoUpdater.logger.info(downloadProgress);
            });

            //Listen for download complete event
            autoUpdater.on('update-downloaded', () => {
                //Close progress window
                if(progressWin){
                    progressWin.close();
                }

                //Prompt for install
                dialog.showMessageBox({
                    type: 'info',
                    title: 'Update Ready',
                    message: 'A new version of ReadItLater is ready. Quit and install now?',
                    buttons: ['Yes', 'Later']
                }, (buttonIndex) => {
                        if(buttonIndex === 0) autoUpdater.quitAndInstall();
                });
            })

        })
    })
};