
// Modules
const {app, ipcMain} = require('electron');
const mainWindow = require('./mainWindow');
//NOTE: Using custom module to handle off-screen rendering
const readItem = require('./readItem');


// Enable Electron-Reload
require('electron-reload')(__dirname);

//NOTE: Listen for messages on the new-item channel coming from renderer process
ipcMain.on('new-item', (e, itemUrl) => {
    console.log('=>> IPC Message Received on new-item channel: ' + itemUrl);

    //Simulate long running offscreen rendering just to setup and test the IPC response message
    // setTimeout(() => {
    //     e.sender.send('new-item-success', {
    //         title: 'Google Loaded',
    //         screenshot: 'nativeImage screenshot'
    //     });
    // }, 5000)

    //NOTE: Using custom module to handle off-screen rendering
    readItem(itemUrl, (item) => {
        console.log('=>> Rendered URL data received');
        console.log(item);

        // Send back to renderer
        e.sender.send('new-item-success', item);
    })

});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', mainWindow.createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) mainWindow.createWindow()
});
