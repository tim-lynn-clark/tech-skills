// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const {ipcRenderer} = require('electron');
const items         = require('./items');
const menu          = require('./menu');


//NOTE: Allowing navigation using the arrow keys
$(document).keydown((e) => {
    switch(e.key){
        case 'ArrowUp':
            console.log('previous item');
            items.changeItem('up');
            break;
        case 'ArrowDown':
            console.log('next item');
            items.changeItem('down');
            break;
    }
});

//NOTE: Handle modal events (showing and hiding)
//Show add-modal
$('.open-add-modal').click(() => {
    $('#add-modal').addClass('is-active');
});
$('.close-add-modal').click(() => {
    $('#add-modal').removeClass('is-active');
});

//NOTE: Handle add-modal submission via button and enter key
$('#add-button').click(() => {
    let newItemUrl = $('#item-input').val();
    if(newItemUrl){
        console.log('=>> New Item URL: ' + newItemUrl);

        //Disable modal UI
        $('#item-input').prop('disabled', true);
        $('#add-button').addClass('is-loading');
        $('.close-add-modal').addClass('is-disabled');

        //NOTE: Send URL to main process over IPC messaging
        ipcRenderer.send('new-item', newItemUrl);
        //NOTE: See main.js to see the listener on the 'new-item' channel
    }
});

//NOTE: listen for messages coming from main process on new-item-success channel
ipcRenderer.on('new-item-success', (e, item) => {
    console.log('=>> Message received on new-item-success channel');
    console.log(item);

    // NOTE: Add new item to HTML collection in view
    items.addItem(item);

    //NOTE: save to model for saving to local storage
    items.itemsToRead.push(item);
    items.saveItems();

    //Close modal and reactivate all controls within it
    $('#add-modal').removeClass('is-active');
    $('#item-input').prop('disabled', false);
    $('#add-button').removeClass('is-loading');
    $('.close-add-modal').removeClass('is-disabled');

    //if first item to be added to the list, mark it as selected
    if(items.itemsToRead.length === 1){
        $('.read-item:first').addClass('is-active');
    }
});

//Simulate add URL on enter
$('#item-input').keyup((e) => {
    //NOTE: when using jQuery, calling an event handler function without providing a handler, will simply invoke the event
    if(e.key === 'Enter') $('#add-button').click();
});

//NOTE: Filter itemsToRead list to show only those that match the search term entered into the search input field
$('#search').keyup((e) => {
    //get search text
    let filter = $(e.currentTarget).val();
    console.log('=>> Filter value: ' + filter);

    //Loop through to read items in view
    $('.read-item').each((i, item) => {
        $(item).text().toLowerCase().includes(filter) ? $(item).show() : $(item).hide();
    });
});


//NOTE: load toReadItems when the application is loaded for the first time or refreshed
if(items.itemsToRead.length > 0){
    items.itemsToRead.forEach(items.addItem);
        $('.read-item:first').addClass('is-active');
}
