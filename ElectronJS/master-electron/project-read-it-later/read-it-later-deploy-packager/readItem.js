// Modules
const {BrowserWindow} = require('electron');

//NOTE: Browser Window for Off-Screen Rendering
let offScreenRenderer;

//NOTE: function to initiate off-screen rendering
module.exports = (url, callback) => {

    // Create new window
    offScreenRenderer = new BrowserWindow({
        width: 1000,
        height: 1000,
        show: false,
        webPreferences: {
            offscreen: true
        }
    });

    // Load item URL
    offScreenRenderer.loadURL(url);

    // Wait for contents to finish loading
    offScreenRenderer.webContents.on('did-finish-load', () => {
        // Get screenshot (thumbnail)
        offScreenRenderer.webContents.capturePage((image) => {
            // Convert image to dataURL for easy passing
            let screenshot = image.toDataURL();
            let title = offScreenRenderer.getTitle();

            // Return new item via callback
            callback({ title, screenshot, url });

            // Clean up the resources used
            offScreenRenderer.close();
            offScreenRenderer = null;
        })
    })
};