# Bookmarking Articles
Simple app that allows you to bookmark articles that you wish to 
read later. 

## Technologies Used
* Node JS
* NPM
* Electron
* BULMA: Modern CSS framework based on Flexbox
* Bower: for installing CSS libraries
* NPM: for installing JavaScript libraries

## Step 1: Project Setup
1. Use the basic Electron quick start project template
2. Install BULMA using Bower: `bower i bulma@0.3.1`
3. Install FontAwesome using Bower: `bower i font-awesome`
4. Create renderer folder, move `index.html` into it and rename to `main.html`, as this is the HTML for the application's main window
5. Move `renderer.js` into the renderer folder and rename to `app.js`
6. Create a `main.css` for storing main window styles
7. Update require statement in `main.html` from `require('./renderer.js')` to `require('./app.js')`
8. Add jQuery to app via `npm i --save jquery`
9. Load jQuery in `main.html` using `$ = require('jquery')`
10. Link all CSS frameworks in `main.html`
11. Separate out all of the main window creation code into its own module and import it into `main.js`

## Step 2: Submitting New Items
1. Create a modal window for adding new items using the [BULA Modal](https://bulma.io/documentation/components/modal/)
2. Add event listeners on the add and close buttons using jQuery selectors (see app.js)
3. Add event listeners for clicking the add button in the modal as well as simulating an add on enter attached to the input keyup event
4. Using the URL to get a page title and thumbnail of the page
    1. Send URL from renderer to main process using IPC messaging
    2. Use off-screen browser rendering to to load the URL, grab the page title, and take a screenshot
    3. Send notification back to the render over IPC messaging with the page title and screenshot
    
## Step 3: Custom Module for Rendering Off-Screen
See `readItem.js` module in app root in which all off-screen rendering logic was 
moved and then its use within main.js in the `ipcMain.on('new-item'` channel 
message listener. 

## Step 4: Displaying & Persisting Items
See `app.js` and new module `items.js` for implementation.

## Step 5: Selecting Items for Opening
See `app.js` and new module `items.js` for implementation

1. Filtering items using the search box (see app.js for implementation)
2. Selecting items using keyboard arrows `app.js` see `$(document).keydown`
3. Setting select state `items.js` see `exports.selectItem`
4. Selecting the next and previous elements `items.js` see `exports.changeItem`
5. Preparation for opening an item `item.js` see `exports.openItem`

## Step 6: Opening Items in Reader
See `reader.html` and `items.js` for implementation
    
## Step 7: Deleting Items
See `render.html` , `intems.js`, and `main.css` for implementation

## Step 8: Application Menu
See `menu.js` for implementation

## Step 9: Application Icons
##### Linux

set `icon` attribute of the main window, see `mainWindow.js`

`icon: '${__dirname}/icons/64x64.png'`

##### Mac OS X (Darwin) & Windows
Add the --icon=icon flag to the packager script, see below

## Step 9: Application Packaging using Electron Packager NPM
https://electronjs.org/docs/tutorial/application-distribution#application-distribution

https://github.com/electron-userland/electron-packager

1. Clean up package.json
2. Remove reference to reload package
3. Remove dev tools open statement
4. Install `electron-packager` using `sudo npm install -g electron-packager`
5. Run `electron-packager . --electron-version='2.0.2'` to build the app for the platform you are on

Obfuscating application files
1. Run `electron-packager . --electron-version='2.0.2' --asar=true` to build the app for the platform you are on

Setting icon file location (Mac & Windows)
1. Run `electron-packager . --electron-version='2.0.2' --asar=true --icon=icon` to build the app for the platform you are on

Allowing packager to overwrite previous builds
1. Run `electron-packager . --electron-version='2.0.2' --asar=true --icon=icon --overwrite` to build the app for the platform you are on