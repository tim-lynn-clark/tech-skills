const {app, ipcMain} = require('electron');
const mainWindow = require('./mainWindow');
const readItem = require('./readItem');

ipcMain.on('new-item', (e, itemUrl) => {

    readItem(itemUrl, (item) => {
        // Send back to renderer
        e.sender.send('new-item-success', item);
    })

});

app.on('ready', mainWindow.createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
});

app.on('activate', () => {
  if (mainWindow === null) mainWindow.createWindow()
});
