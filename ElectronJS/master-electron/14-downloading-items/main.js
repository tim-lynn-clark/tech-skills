const electron = require('electron');
const {app, BrowserWindow, Menu, session, globalShortcut} = electron;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    //NOTE: Event listening for download requests
    defaultSession.on('will-download', (event, item, webContents) => {

        //NOTE: building a download path for the local OS
        console.log(item);
        let downloadFolder = app.getPath('downloads');
        let downloadFilename = item.getFilename();
        let downloadPath = downloadFolder + '/' + downloadFilename;
        console.log(downloadPath);

        let fileSize = item.getTotalBytes();

        // Set the save path, making Electron not to prompt a save dialog.
        item.setSavePath(downloadPath);

        item.on('updated', (event, state) => {
            if (state === 'interrupted') {
                console.log('Download is interrupted but can be resumed')
            } else if (state === 'progressing') {
                if (item.isPaused()) {
                    console.log('Download is paused')
                } else {
                    //NOTE: Calculating the progress of the download
                    console.log(`Received bytes: ${item.getReceivedBytes()}`);
                    let progress = Math.round((item.getReceivedBytes() / fileSize) * 100);
                    console.log('Downloaded: ' + progress + '%');
                }
            }
        });
        item.once('done', (event, state) => {
            if (state === 'completed') {
                console.log('Download successfully')
            } else {
                console.log(`Download failed: ${state}`)
            }
        })
    });

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    //mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
