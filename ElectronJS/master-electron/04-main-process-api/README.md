# Electron's Main Process API

## Notes
https://electronjs.org/docs/api/app

`electron.app`: controls your application's event lifecycle

https://electronjs.org/docs/api/app#events

The `app` object has many events that are cross-platform but also 
has many that are platform specific (mac, win, linux).

See some commonly used events sending console messages in `main.js`

* ready: ln 25-28
* browser-window-blur(focus): ln 66-72

The app also has functions defined for manually invoking 
several of the events that can be listened for. These methods 
allow the application to control its own lifecycle based on 
its own logic or states.

There are several methods for accessing application metadata 
as well: `app.getVersion()`, `app.getName()`, `app.getLocale`, ...

https://electronjs.org/docs/api/app#methods

