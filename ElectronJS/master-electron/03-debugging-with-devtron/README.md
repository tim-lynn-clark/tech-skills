# Debugging with Devtron
[Devtron](https://electronjs.org/devtron) is an open source tool to help 
inspect, monitor, and debug electron applications and is built off of the 
Chrome Developer Tools in Chrome/Chromium.

Features it provides to developers on top of the regular Chrome dev tools
* Require Graph: visualize the apps internal library dependencies
* Event Listeners: explore currently registered event listeners
* IPC Monitor: track and inspect messages sent and received between process in the app
* Linter: Electron specific code linting

Steps
1. Install Devtron `npm install --save-dev devtron`
2. Manually run Devtron from the dev tools console in your application's window or add it to your renderer.js (remembering to remove it before publishing the app) `require('devtron').install()`