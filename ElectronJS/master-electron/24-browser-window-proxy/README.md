# Browser Window Proxy
https://electronjs.org/docs/api/browser-window-proxy#class-browserwindowproxy

Provides, to the main process, a proxy object representing a newly created 
browser window, allowing the main process to control the browser window 
using methods and properties exposed by the browser window proxy object. 
The proxy object is returned when `window.open` is called or bu not when a user 
clicks an Anchor tag that has its `target` set to `_blank`. To manage a window 
that is opened via an Anchor tag in your HTML UI pages, you must use a 
`onclick` method and call `window.open`.

## Specifying Features on a Window
https://developer.mozilla.org/en-US/docs/Web/API/Window/open