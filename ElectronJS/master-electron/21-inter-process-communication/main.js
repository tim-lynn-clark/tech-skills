// Electron Packages
const electron = require('electron');
const {app, BrowserWindow, Menu, MenuItem, session, globalShortcut, dialog, Tray, ipcMain} = electron;

// Node Packages
const path = require('path');
const url = require('url');

// Third-Party Packages
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

// Local Packages


//-------------------------------------------------------------------------------------------
//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);
//FIXME: Remove debug statement
console.log('=>> main.js executing...');
//-------------------------------------------------------------------------------------------

// Setup main app menu
let mainMenu = Menu.buildFromTemplate(require('./lib/mainMenu'));
// Setup a context menu
let contextMenu = Menu.buildFromTemplate(require('./lib/contextMenu'));
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

//NOTE: Adding an IPC listener on the main application process
//NOTE: using the on method of the ICP Main object, you specify the channel on which you will be listening for events to happen
//NOTE: the ability to create unique pub/sub channels allows a developer to create a different channel for each window to communicate with the main process on
//NOTE: or for creating channels that are specific to some piece of your application's logic or functionality
ipcMain.on('test-channel', (e, args) => {
    console.log('=>> SERVER - IPC ARGS: ' + args);

    //NOTE: responding to a message sent via this channel
    e.sender.send('test-channel', 'Message from IPCRenderer received on main process: ' + args);
});

//NOTE: Receiving and responsing to synchronous messages sent from a renderer process (not advisable, but might be necessary in some apps)
ipcMain.on('sync-channel', (e, args) => {
    console.log('=>> SERVER - sync message ARGS: ' + args);
    //NOTE: to demonstrate this, set a timeout and see that processing is stopped until the sync message has been responded to
    setTimeout(() => {
        e.returnValue = {
            message: 'A synchronous response from the main process', name: 'Tim'
        };
    }, 3000);
});

//NOTE: IPCMain sending messages to a specific renderer process (see below in main createWindow function)

// Function to create a new Tray so your application shows up in the OS tray
function createTray(){
    tray = new Tray('./assets/images/tray-icon24.png');
    tray.setToolTip('My Electron App');

    const trayMenu = Menu.buildFromTemplate([
        { label: 'Tray Menu Item' },
        { label: 'quit app', role: 'quit' }
    ]);
    tray.setContextMenu(trayMenu);
}

function createWindow () {
    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, '/views/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();

    //NOTE: IPCMain sending messages to a specific renderer process
    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.webContents.send('main-windows-channel', 'Message from main process to main window');
    });

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // Handle the call for a context menu
    mainWindow.webContents.on('context-menu', (e) => {
        e.preventDefault();
        contextMenu.popup({});
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    createWindow();
    createTray();
    // add main menu as app menu
    Menu.setApplicationMenu(mainMenu);

    // Access to the default app session, shared by all windows,
    // for managing state across the app. Key value pairs are stored
    // in the browser's local storage and persist across application restarts
    let defaultSession = session.defaultSession;

    // Monitoring computer power state changes
    electron.powerMonitor.on('on-ac', () => {
        console.log('=>> System is on AC power');
    });
    electron.powerMonitor.on('on-battery', () => {
        console.log('=>> System is running on batteries');
    });
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
