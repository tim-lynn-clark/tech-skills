# Inter-Process Communication (IPC)
On the main process: https://electronjs.org/docs/api/ipc-main

In the browser process: https://electronjs.org/docs/api/ipc-renderer

IPC is simply a built-in pub-sub system provided by Electron that makes 
communication between the main application process and the browser processes 
easy to setup and manage. 