// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

//NOTE: IPC event broadcasting to main process
const {ipcRenderer} = require('electron');

//NOTE: sending a synchronous message to the main process and expecting a response back before executing further
let syncResponse = ipcRenderer.sendSync('sync-channel', 'this is a synchronous request from the renderer processs to the main process.');
console.log('=>> Sync response from main process to sync message sent by browser: ' + JSON.stringify(syncResponse));

//NOTE: sending a message on the test-channel being listened to by the main process in main.js
ipcRenderer.send('test-channel', 'Hello from the renderer process in the browser.');

//NOTE: listening for messages send over the test-channel from the main process in main.js
ipcRenderer.on('test-channel', (e, args) => {
    console.log('=>> BROWSER - IPC ARGS: ' + args);
});

//NOTE: listening for message from main process to this specific window (see ln 92-95 in main.js)
ipcRenderer.on('main-windows-channel', (e, args) => {
    console.log('=>> From Main Process specifically to this renderer: ' + args);
});

//--------------------------------------------------------
//FIXME: Remove debug statement
console.log('=>> message from renderer.js...');
//FIXME: Remove before publication
require('devtron').install();
//--------------------------------------------------------