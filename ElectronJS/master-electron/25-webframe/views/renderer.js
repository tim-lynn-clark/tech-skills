// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

//IPC event broadcasting to main process
const {ipcRenderer} = require('electron');

//Sending a message on the test-channel being listened to by the main process in main.js
ipcRenderer.send('test-channel', 'Hello from the renderer process in the browser.');

//Listening for messages send over the test-channel from the main process in main.js
ipcRenderer.on('test-channel', (e, args) => {
    console.log('=>> BROWSER - IPC ARGS: ' + args);
});

//FIXME: Remove debug statement
console.log('=>> message from renderer.js...');
//FIXME: Remove before publication
require('devtron').install();