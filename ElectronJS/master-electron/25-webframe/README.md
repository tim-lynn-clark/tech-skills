# WebFrame
https://electronjs.org/docs/api/web-frame#webframe

The WebFrame controls the rendering of the contents within a browser window 
created by Electron. 