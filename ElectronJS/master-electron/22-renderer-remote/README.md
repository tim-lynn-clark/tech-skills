# Renderer Remote
Allows methods of the main process to be accessed by the renderer process 
without explicitly using the IPC pub-sub system. 