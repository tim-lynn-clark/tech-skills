
module.exports = function(app){

    // Function to create a new Tray so your application shows up in the OS tray
    function createTray(){
        tray = new Tray('./assets/images/tray-icon24.png');
        tray.setToolTip('My Electron App');

        const trayMenu = Menu.buildFromTemplate([
            { label: 'Tray Menu Item' },
            { label: 'quit app', role: 'quit' }
        ]);
        tray.setContextMenu(trayMenu);
    }

    function createWindow () {
        //FIXME: Remove debug statement
        console.log('=>> creating mainWindow...');

        // <<-- MOD: Setup a new windows state keeper -->>
        let winState = windowStateKeeper({
            defaultWidth: 1200,
            defaultHeight: 800,
        });

        // Create the browser window.
        // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
        mainWindow = new BrowserWindow({
            width: winState.width,
            height: winState.height,
            minWidth: 800,
            minHeight: 400,
            x: winState.x,
            y: winState.y});

        // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
        winState.manage(mainWindow);

        //FIXME: Remove debug statement
        console.log('=>> loading index.html into mainWindow...');

        // and load the index.html of the app.
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, '/views/index.html'),
            protocol: 'file:',
            slashes: true
        }));

        // Open the DevTools.
        //FIXME: Comment statement out
        mainWindow.webContents.openDevTools();

        // Emitted when the window is closed.
        mainWindow.on('closed', function () {
            //FIXME: Remove debug statement
            console.log('=>> mainWindow closed...');

            // Dereference the window object, usually you would store windows
            // in an array if your app supports multi windows, this is the time
            // when you should delete the corresponding element.
            mainWindow = null;
        });

        // Handle the call for a context menu
        mainWindow.webContents.on('context-menu', (e) => {
            e.preventDefault();
            contextMenu.popup({});
        });

        // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
        if (process.platform === 'darwin') {
            globalShortcut.register('Command+Q', () => {
                app.quit();
            })
        }
    }

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
    app.on('ready', () => {
        createWindow();
        createTray();
        // add main menu as app menu
        Menu.setApplicationMenu(mainMenu);

        // Monitoring computer power state changes
        electron.powerMonitor.on('on-ac', () => {
            console.log('=>> System is on AC power');
        });
        electron.powerMonitor.on('on-battery', () => {
            console.log('=>> System is running on batteries');
        });
    });

// Quit when all windows are closed.
    app.on('window-all-closed', function () {
        // On OS X it is common for applications and their menu bar
        // to stay active until the user quits explicitly with Cmd + Q
        if (process.platform !== 'darwin') {
            app.quit()
        }
    });

    app.on('activate', function () {
        // On OS X it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (mainWindow === null) {
            createWindow()
        }
    });

    return app;
};