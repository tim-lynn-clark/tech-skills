// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

//IPC event broadcasting to main process
const {ipcRenderer, remote} = require('electron');
//NOTE: to access a particular object/method on the main process you simply include it from the remote object
const {app, dialog, BrowserWindow} = remote;

//NOTE: The remote object provides a 'GET' api to the methods of the main process
//NOTE: Arrays and buffers from the main process object are copied over and are not referenced
//- NOTE: therefore changes to these data types in the renderer process will not be reflected in the main process
console.log(remote); //explore the 'remote' object in the browser console

//NOTE: the remote module communicates with the main process using synchronous ICP, this means that any call with block the rendering process
//NOTE: in the example below, the browser will not load its contents until this message box is dismissed
//NOTE: this holds true only when the call does not provide a callback function to make it async
dialog.showMessageBox({message: 'A message box invoked on the main process from within the renderer process.', buttons: ['OK']});

//NOTE: making the dialog async by providing a callback method
dialog.showMessageBox({message: 'Would you like to quite?', buttons: ['OK', 'CANCEL']}, (buttonIndex) => {
    if(buttonIndex === 0) app.quit();
});

//NOTE: example where a new window is created on the main process by being invoked by the renderer process
let win = new BrowserWindow({width: 400, height: 400});
win.loadURL('https://www.google.com');

//NOTE: accessing global node variables
console.log('=>> Node Global[my_version]: ' + remote.getGlobal('my_version'));


//Sending a message on the test-channel being listened to by the main process in main.js
ipcRenderer.send('test-channel', 'Hello from the renderer process in the browser.');

//Listening for messages send over the test-channel from the main process in main.js
ipcRenderer.on('test-channel', (e, args) => {
    console.log('=>> BROWSER - IPC ARGS: ' + args);
});

//FIXME: Remove debug statement
console.log('=>> message from renderer.js...');
//FIXME: Remove before publication
require('devtron').install();