const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
// <<-- MOD: Global Shortcut module added to Electron imports  -->>
const globalShortcut = electron.globalShortcut;

const path = require('path');
const url = require('url');

//TODO: Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let spinningModal;

function createWindow () {

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    //NOTE: minimum-width and minimum-height
    mainWindow = new BrowserWindow({width: 1200, height: 800, minWidth: 800, minHeight: 400});

    //NOTE: second window as spinner modal dialog to notify user of long running process
    spinningModal = new BrowserWindow({width: 400, height: 400, resizable: false, parent: mainWindow, modal: true, show: false});

    //NOTE: show spinner modal if main window becomes unresponsive
    mainWindow.on('unresponsive', () => {
        console.log('=>> main window has become unresponsive');
        console.log('=>> showing spinner modal');
        spinningModal.show();
    });

    //NOTE: hide spinner modal when main window becomes responsive
    mainWindow.on('responsive', () => {
        console.log('=>> main window has become responsive');
        console.log('=>> hiding spinner modal');
        spinningModal.hide();
    });

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    spinningModal.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    //mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }

    //NOTE: iterate over browser windows
    let windows = BrowserWindow.getAllWindows();
    console.log(windows);
    for(let i=0; i<windows.length; i++){
        let current = windows[i];
        console.log(current.id);
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
