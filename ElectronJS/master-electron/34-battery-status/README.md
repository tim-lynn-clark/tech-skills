# Battery Status
https://developer.mozilla.org/en-US/docs/Web/API/Battery_Status_API

Allows the application to detect whether or not the machine is running on 
battery power. 

Examples see index.html