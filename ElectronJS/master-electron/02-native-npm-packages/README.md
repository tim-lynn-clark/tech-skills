# Using Native NPM Packages
This project demonstrates how to 
setup electron to use NPM to compile native packages for the platform 
to which electron is being deployed.

The example used in this project is the bcrypt NPM package, which 
requires that the native bcrypt binary used in the package be built from 
source for the platform on which it is being installed. 

brcypt library usage => see top of main.js

Steps for Mac OS X:
1. Install Electron `npm i electron`
2. Create script file `electron-npm`
3. Paste npm script template from Electron doc 'Using Native Node Modules'
4. Remove last line containing `HOME=~/` with `npm install $1`
5. Update first line `npm_config_target` to the version of Electron you are using
6. Grant script execute permissions `chmod +x electron-npm`
7. Use new script to install native package, example bcrypt `./electron-npm bcrypt`

Steps for Windows:
1. Install Electron `npm i electron`
2. Install Windows build tools using CMD as Administrator `npm install --global --production windows-build-tools`
3. Add the python executable from the build tools directory in your user directory to your path `setx PYTHON "%USERPROFILE%\.windows-build-tools\python27\python.exe"`
4. Verify python has been set `set PYTHON`, should see `PYTHON=C:\Users\yourname\.windows-build-tools\python27\python.exe`
5. Switch back to a standard CMD prompt
6. Go to project directory
7. Install Electron Rebuild package `npm i -g electron-rebuild`
8. Use Electron Rebuild to rebuild the native package `electron-rebuild -w brcypt`
