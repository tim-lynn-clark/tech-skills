# Shared API: Shell
https://electronjs.org/docs/api/shell#shell

Allows Electron to interact with the underlying operating system such as opening 
files and URLs using the system's default application. 

 
