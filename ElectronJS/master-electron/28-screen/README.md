# Shared API: Screen
https://electronjs.org/docs/api/screen#screen

Allows the developer to access information about the application screen as 
well as the computer displays. Information such as screen sizes and layouts. 



