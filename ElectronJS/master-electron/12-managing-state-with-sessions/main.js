const electron = require('electron');
// Module to control application life.
//NOTE: the default session for app state management can be accessed through the session module as well as through the window web content (below)
const {app, BrowserWindow, Menu, session, globalShortcut} = electron;
// <<-- MOD: include electron-window-state for managing the state of your application -->
const windowStateKeeper = require('electron-window-state');

const path = require('path');
const url = require('url');

//FIXME: Remove before deployment - Electron-Reload included and passed the root of the application to watch for changes
require('electron-reload')(__dirname);

//FIXME: Remove debug statement
console.log('=>> main.js executing...');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, altWindow;

function createWindow () {

    // <<-- MOD: Setup a new windows state keeper -->>
    let winState = windowStateKeeper({
        defaultWidth: 1200,
        defaultHeight: 800,
    });

    //FIXME: Remove debug statement
    console.log('=>> creating mainWindow...');

    // Create the browser window.
    // <<-- MOD: with defaults set in the windowStateKeeper, use those defaults to initialize the window -->>
    mainWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 800,
        minHeight: 400,
        x: winState.x,
        y: winState.y});

    //NOTE: manually creating a custom session
    //NOTE: when using a custom session, any values added to local storage will NOT be persisted between application starts by default
    //NOTE: this session will be completely in-memory and will be wiped each time the application is restarted
    //let appSession = session.fromPartition('partition1');
    //NOTE: to add the ability for custom session to be persisted between app starts, you must preface the partition name with 'persist:'
    //NOTE: this session will be automatically persisted between app restarts
    //let appSession = session.fromPartition('persist:partition1');
    //NOTE: you are not required to manually create a custom session prior to the creation of the window (see altWindow initializer below below)



    //NOTE: custom session passed to this window as its session object using
    //NOTE: using a custom session, if you look in the dev tools, you will see that local storage entries are different for mainWindow and altWindow
    //NOTE: using manually created session above
    // altWindow = new BrowserWindow({
    //     width: winState.width,
    //     height: winState.height,
    //     minWidth: 400,
    //     minHeight: 400,
    //     x: winState.x,
    //     y: winState.y,
    //     webPreferences: {
    //         session: appSession
    //     }});
    //NOTE: using in-line custom session definition using partition property of web preferences
    altWindow = new BrowserWindow({
        width: winState.width,
        height: winState.height,
        minWidth: 400,
        minHeight: 400,
        x: winState.x,
        y: winState.y,
        webPreferences: {
            partition: 'persist:partition1'
        }});

    //NOTE: accessing default session using the Session module imported in the require statements at top of this file
    let defaultSession = session.defaultSession;
    //NOTE: accessing the default application session on the main window's web content
    //NOTE: session manages browser storage systems, like local storage
    let mainSession = mainWindow.webContents.session;
    let altSession = altWindow.webContents.session;
    console.log(mainSession); // should see 'Session {}' in the console
    console.log('=>> default session is session on mainWindow? ' + Object.is(defaultSession, mainSession));
    console.log('=>> default session is session on altWindow? ' + Object.is(defaultSession, altSession));
    console.log('=>> session on mainWindow is same as session on altWindow? ' + Object.is(mainSession, altSession));
    //NOTE: comparing the default session against the custom session created above
    //console.log('=>> custom session is global session? ' + Object.is(appSession, defaultSession));

    //NOTE: session (custom or default) can be cleared using the clearStorageData method of the session object you wish to clear
    mainSession.clearStorageData();



    // <<-- MOD: Passing the management of window state to the windowStateKeeper -->>
    winState.manage(mainWindow);

    //FIXME: Remove debug statement
    console.log('=>> loading index.html into mainWindow...');

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    altWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    //FIXME: Comment statement out
    mainWindow.webContents.openDevTools();
    altWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        //FIXME: Remove debug statement
        console.log('=>> mainWindow closed...');

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // <<-- MOD: Registering a keyboard shortcut to capture CMD+Q in OS X to close app -->>
    if (process.platform === 'darwin') {
        globalShortcut.register('Command+Q', () => {
            app.quit();
        })
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
