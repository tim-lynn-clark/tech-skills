# Managing Application State with Sessions
https://electronjs.org/docs/api/session#session

All Electron apps have a default session that is attached to all window 
objects. The session can be accessed as a property of the window's webContent 
object. 

The session is responsible for all storage types 
(cookies, local storage, web SQL, etc.) 