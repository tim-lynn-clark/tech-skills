
pub fn nth(n: u32) -> u32 {
    println!("Entering nth function");
    let primes = build_prime_list();
    primes[n as usize]
}

fn build_prime_list() -> Vec<u32> {
    println!("Entering build prime list function");
    let mut primes = vec![2];

    for n in 3..1_000_000 {
        if n %2 != 0 {
            if check_prime(n) {
                primes.push(n);
            }
        }
    }

    primes
}

fn check_prime(num:u32) -> bool {
    let stop = ((num as f64).sqrt() + 1.0) as u32;
    for i in 3..stop {
        if i % 2 != 0 {
            if num % i == 0 {
                return false;
            }
        }
    }
    return true;
}