use chrono::{Duration, NaiveTime, Timelike};

#[derive(Debug, Eq, PartialEq)]
pub struct Clock {
    hours: i32,
    minutes: i32,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let mut filtered_minutes = minutes;
        let mut filtered_hours = hours;

        while filtered_minutes < 0 {
            filtered_minutes += 60;
            filtered_hours -= 1;
        }
        while filtered_minutes > 59 {
            filtered_minutes -= 60;
            filtered_hours += 1;
        }
        while filtered_hours < 0 {
            filtered_hours += 24;
        }
        while filtered_hours > 23 {
            filtered_hours -= 24;
        }

        // Using NaiveTime to validate the final hours:minutes. Will panic if not valid.
        let time = NaiveTime::from_hms(filtered_hours as u32, filtered_minutes as u32, 0);
        Self {
            hours: time.hour() as i32,
            minutes: time.minute() as i32,
        }
    }

    pub fn add_minutes(&self, minutes: i32) -> Clock {
        let time = NaiveTime::from_hms(self.hours as u32, self.minutes as u32, 0);
        let duration = Duration::minutes(minutes as i64);
        let new_time = time + duration;
        Clock {
            hours: new_time.hour() as i32,
            minutes: new_time.minute() as i32,
        }
    }

    pub fn to_string(&self) -> String {
        format!("{:02}:{:02}", self.hours, self.minutes)
    }
}
