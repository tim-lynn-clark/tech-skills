pub fn reply(message: &str) -> &str {
    let trimmed_message = message.trim();
    let mut reply = "Whatever.";

    if trimmed_message.is_empty() {
        reply = "Fine. Be that way!"
    }
    else if is_upper_case(trimmed_message) && trimmed_message.ends_with('?'){
        reply = "Calm down, I know what I'm doing!";
    }
    else if trimmed_message.ends_with('?') {
        reply = "Sure.";
    }
    else if is_upper_case(trimmed_message) {
        reply = "Whoa, chill out!";
    }

    reply
}

fn is_upper_case(string: &str) -> bool {
     let mut result = false;
    if string.chars().any(char::is_alphabetic) {
        result = string == string.to_uppercase()
    }
    result
}
