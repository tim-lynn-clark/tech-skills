
static ENDING_VERSE: &str = "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n";

pub fn verse(n: u32) -> String {
    if n == 0 {
        return String::from(ENDING_VERSE);
    }

    if n == 1 {
        return String::from("1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n");
    }

    let reduced = n-1;
    if n == 2 {
        return String::from(
            format!(
                "{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} bottle of beer on the wall.\n", n, n, reduced
            )
        )
    }

    String::from(
        format!(
            "{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} bottles of beer on the wall.\n", n, n, reduced
        )
    )
}

pub fn sing(mut start: u32, mut end: u32) -> String {
    let mut compiled_verses = String::new();
    if end != 0 { end = end -1; }

    loop {
        compiled_verses.push_str(verse(start).as_str());
        start = start - 1;
        if start == end {
            if end == 0 {
                compiled_verses.push_str("\n");
                compiled_verses.push_str(ENDING_VERSE);
            }
            break;
        }
        compiled_verses.push_str("\n");
    }

    compiled_verses
}
