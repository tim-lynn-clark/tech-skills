use std::collections::HashMap;
use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let word_characters = word
        .to_lowercase()
        .chars()
        // https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.fold
        .fold(HashMap::new(), |mut set, character| {
            // https://doc.rust-lang.org/std/collections/struct.HashMap.html#method.entry
            *set.entry(character).or_insert(0) += 1;
            set
        });

    possible_anagrams
        .iter()
        // https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.filter
        .filter(|possibility| {
            possibility.len() == word.len() && !(possibility.to_lowercase() == word.to_lowercase())
        })
        .fold(HashSet::new(), |mut matches, possibility| {
            let characters =
                possibility
                    .to_lowercase()
                    .chars()
                    .fold(HashMap::new(), |mut set, character| {
                        *set.entry(character).or_insert(0) += 1;
                        set
                    });

            if word_characters == characters {
                matches.insert(possibility);
            }
            matches
        })
}
