pub fn is_armstrong_number(num: u32) -> bool {
    let digits: Vec<_> = num
        .to_string()
        .chars()
        .map(|d| d.to_digit(10).unwrap())
        .collect();

    let len: u32 = digits.len() as u32;
    let mut total: u32 = 0;
    for i in digits {
        total += i.pow(len);
    }

    num == total
}
