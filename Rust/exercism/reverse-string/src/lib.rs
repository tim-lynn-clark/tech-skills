use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    // See: https://docs.rs/unicode-segmentation/1.6.0/unicode_segmentation/struct.Graphemes.html
    let mut reversed = String::new();
    UnicodeSegmentation::graphemes(input, true)
        .collect::<Vec<&str>>()
        .iter()
        .rev()
        .for_each(|c| reversed.push_str(c));
    reversed
}
