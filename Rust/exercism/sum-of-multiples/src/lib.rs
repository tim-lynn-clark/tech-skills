pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    let mut multiples = Vec::new();
    factors.iter().for_each(|&i| {
        if i != 0 {
            let mut count = 1;
            loop {
                let multiple = i * count;
                if multiple >= limit {break;}
                if !multiples.contains(&multiple) {
                    multiples.push(multiple);
                }
                count = count + 1;
            }
        }
    });

    let mut sum = 0;
    multiples.iter().for_each(|i| { sum = sum + i });
    sum
}
