
pub fn factors(n: u64) -> Vec<u64> {
    // Make a mutable copy of the passed in number for which we are finding prime factors
    let mut num = n;

    // Create a vector to hold prime factors
    let mut factors:Vec<u64> = Vec::new();

    // Determine the number of times prime 2 will evenly divide into the number
    while num%2 == 0 {
        factors.push(2);
        num = num / 2;
    }

    // Begin looping over odd primes starting with 3
    let mut odd_prime = 3;
    let num_sqrt = (num as f64).sqrt() as u64;
    while odd_prime <= num_sqrt {
        // Determine the number of times odd prime will evenly divide into the number
        while num% odd_prime == 0 {
            factors.push(odd_prime);
            num = num / odd_prime;
        }

        // Increment by two to get to the next odd prime
        odd_prime = odd_prime + 2;
    }

    // Check to see if the number is a prime
    if num > 1 {
        factors.push(num);
    }

    factors
}
