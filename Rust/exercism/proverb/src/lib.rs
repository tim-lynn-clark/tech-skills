
pub fn build_proverb(list: &[&str]) -> String {
    let mut compiled_proverb = String::new();
    if list.len() == 0 { return compiled_proverb; }

    let mut previous_item = "";
    for index in 0..list.len()-1 {
        compiled_proverb.push_str(
            format!(
                "For want of a {} the {} was lost.\n",
                list[index],
                list[index+1]
            ).as_str()
        );
    }

    compiled_proverb.push_str(
        format!(
            "And all for the want of a {}.",
            list[0]
        ).as_str()
    );

    compiled_proverb
}
