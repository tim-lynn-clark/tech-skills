# Option (no null)

- https://doc.rust-lang.org/std/option/index.html

Rust does not have the concept of `null`, it simply does not allow you to have a variable that does not contain the data it claims to hold.

To handle cases where there may or may not be data within a variable, Rust provides the `Option` enum.

	// Standard Library Definition for the Option enum
	pub enum Option<T> {
		None,
		Some(T),
	}

As seen in the Option enum definition, it provides a `None` value, representing the *null case* and a `Some(T)` value that represents the *true case* and provides *data*. Utilizing generics, Rust allows the Option enum to be used with any Types within the language as well as those that are user-defined.

A practical example would be that of an HTTP request Struct that has a property for a query string. Not all HTTP requests will have query strings and therefore there exists the `null` case. We can use the `Option` enum to handle this case.

	struct Request {
	    path: String,
	    query_string: Option<String>,
	    method: Method,
	}

The `Option` enum is meant for handling the `null` case within Rust programs and is therefore automatically imported into all Rust code and can be used without an import from the standard library.
