# Match Expression

- https://doc.rust-lang.org/std/keyword.match.html

Match is often used in place of individual `if` statements when processing `Results` returned by functions that contain enums, commonly `Ok()` and `Err()`. It allows you to compare the contents of a result against patterns, when a match is located, the code for that match will be executed. It requires that all possible cases are handled, that is all enum values are accounted for as matches with accompanying code.

	match listener.accept() {
		Ok((stream, address)) =>  { //Handle success case here },
		Err(e) => println!("Failed to establish a connection: {}", e),
	}


The `_` can be used to tell the Rust compiler we are not interested in a returned value that we would normally be required to deal with.


	match listener.accept() {
		Ok((stream, _)) =>  { //Handle success case here },
		Err(e) => println!("Failed to establish a connection: {}", e),
	}


Can be used on other types as well, such as pattern matching again strings. Must provide a default case using the underscore.


	match "abcd" {
		"abcd" => println!("Found abcd"),
		"DCBA" => println!("Found DCBA"),
		_	=> println!("Nothing found...")
	}
