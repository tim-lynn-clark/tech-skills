# The Result Enum and Error Handling

- https://doc.rust-lang.org/std/result/

Rust does not provide exceptions nor an implementation of the commonly seen try/catch block. Instead, using the Results enum, it forces developers to acknowledge a potential error case so they can then handle it appropriately.

Errors can be recoverable (file access not allowed) or unrecoverable (database connection fails). Recoverable errors should be handled by the developer and should not crash the entire application. Unrecoverable errors should cause the application to terminate so that it is not left in an unusable state that may cause damage to the data it controls.

Standard library definition for the `Result` type:

	pub enum Result<T, E> {
		// Success case, holds a value of the specified type T
		Ok(T),
		// Error case, holds an error of the specified type E
		Err(E),
	}

Just like the `Options` type, the `Result` type is used widely within Rust programs and is therefore automatically imported into all Rust code and can be used without an import from the standard library.
