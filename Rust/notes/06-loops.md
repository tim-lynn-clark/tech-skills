# Loops

- https://doc.rust-lang.org/reference/expressions/loop-expr.html
- https://doc.rust-lang.org/std/keyword.loop.html
- https://doc.rust-lang.org/std/keyword.for.html
- https://doc.rust-lang.org/std/keyword.while.html

## Loop

Since infinite loops are common in system programming, Rust provides one for you.

	loop {
		// Code runs here
	}

Loops can be labeled (named)

	'outer: loop {
		'inner: loop {

		}
	}

## While Loop

	let mut i = 0;

	while i < 10 {
	    println!("hello");
	    i = i + 1;
	}

## While Let Loop

	let mut vals = vec![2, 3, 1, 2, 2];
	while let Some(v @ 1) | Some(v @ 2) = vals.pop() {
		// Prints 2, 2, then 1
		println!("{}", v);
	}

## For In Loop

	let v = &["apples", "cake", "coffee"];

	for text in v {
	    println!("I like {}.", text);
	}

	let mut sum = 0;
	for n in 1..11 {
	    sum += n;
	}
	assert_eq!(sum, 55);
