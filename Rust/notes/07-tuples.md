# Tuples

- Commonly used to return more than one value from a function
- Used to group values of different types together
- They have a fixed length, once declared they do not grow or shrink


	let myTuple = (5, "a", listener);


Tuples can be deconstructed into their component parts

	let (num, string, tcp) = myTuple;

