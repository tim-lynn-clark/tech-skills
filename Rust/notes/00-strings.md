# Strings

- https://doc.rust-lang.org/std/string/index.html
- https://doc.rust-lang.org/std/primitive.str.html
- https://doc.rust-lang.org/std/primitive.slice.html

**std::string::String** is not the same as **&str**

`&str` is a string slice not a string object, it is a view inside an existing string object.

*String literals* like `"127.0.0.1:8080"` equate to a string slice not a string object. This is because string literals are provided at compile time, since they are hard-coded into the source code, the compiler *bakes* those strings into the binary. This means that those strings are allocated in memory at runtime and are available for use.

Because they already exist and Rust is great at memory management, instead of allocating a new string when it reaches the string literal, it simply provides a slice of that existing string where it is called. This way, the contents of the string in memory can be used as many times as it needs to be without allocating additional memory.


	let address_and_port = String::from("127.0.0.1:8080");
	let just_port = &address_and_port[10..];
	dbg!(&address_and_port);
	dbg!(just_port); 			//NOTE: just_port is a slice


All strings in Rust are UTF8 encoded by default. In the example above, manually slicing a string works because all of the characters are UTF8.

**Warning:**

The example above in which a slice is created using `[10..]`, the assumption may be that you are extracting a substring that includes every character from the 10th to the nth. But that assumption is incorrect, the slice is actually capturing the 10th to the nth byte, not necessarily character.

In practice, when dealing with unicode characters and additional languages, you are not guaranteed that every character will take up a single byte as some languages characters take up several bytes. Using the string slice method above may end up only capturing half of a character.

When dealing with strings, it is better to use character boundaries when pulling substrings out of a string. The string struct provides utility methods for these purposes.
