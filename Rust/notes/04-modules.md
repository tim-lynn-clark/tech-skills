# Modules

- https://doc.rust-lang.org/reference/items/modules.html
- https://doc.rust-lang.org/std/keyword.mod.html

Used to separate and organize code within a Rust project. Also used to specify the visibility of code in order to protect that which should not be openly accessed outside of a module.

## Inline Modules

Everything inside of a `mod` is private by default. Anything that should be accessible externally has to be explicitly marked as public using the `pub` keyword.

	mod server {
		pub struct Server {
		    addr: String
		}

		impl Server {
		    pub fn new(addr: String) -> Self {
		        Self {
		            addr
		        }
		    }

		    pub fn run(self) {

		    }
		}
	}

Modules within modules are private by default, not just the "code". So any module within another module that should be accessible externally of the wrapping module must be marked public.

# Files as Modules

Inline modules are great for the small stuff, but a single code file with 3000 lines of code and 300 modules is not optimal for code maintenance. To solve this, Rust treats individual files as self-contained modules.

The code form above can be moved to a single file named `server.rs`, without the wrapping `mod` as it is not needed as each file is automatically treated as a module.


# Folders as Modules

Folders can be used as modules that can expose sub modules that exists within it. Within a folder a `mod.rs` file will need to be created that exposes the modules it wants to expose as public.


	// Exposing an entire sub module within this module
	pub mod request;

	// Exposing a struct from within a sub module of this module
	pub use request::Request;

`pub mod request;` this line within the `mod.rs` file exposes the entire request.rs module file as one of its public sub modules.

`pub use request::Request;` exposes just the Request struct from within the request module.
