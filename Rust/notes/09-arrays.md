# Arrays

A list of items of the same type.

Example: Array Literal


	let guesses = [1,3,2,4];


The array literal above ends up being of type [i32; 4], that is it contains only items that are of type i32 and it contains exactly 4 places to store items.

When passing arrays to function, by default, Rust must know the exact number of items in the array as the type of the array is the combination of the item type and the length. Therefore [i32; 4] is not the same type as [i32; 5] because the length of the array is different.

Example: causes a compile error as guesses only has 4 items defined but processArray requires an array with 5 items.

Error: mismatched types [E0308] expected `[i32; 5]`, found `[i32; 4]`

	fn processArray(a: [i32, 5]) {
		// do something here
	}

	processArray(guesses);


To fix type errors based on the number of items in an array, you pass a reference (slice of) to the array.

	fn processArray(a: &[i32]) {
		// do something here
	}

	processArray(&guesses);


Arrays can be pre-defined using the special syntax [initial_value, length] where the initial_value is the value that will be set at every index within the array and the length is how long the array will be. This is often used when creating data buffers for streams.

	let mut buffer = [0; 1024];
    stream.read(&mut buffer);
