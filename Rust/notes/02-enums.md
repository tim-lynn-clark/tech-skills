# Enumerations

- https://doc.rust-lang.org/std/keyword.enum.html

**Enums** in Rust are similar to those found in other languages, but have additional abilities that are usually not found in higher-level languages like C# and Java.

Each `value` in an enum is associated with an integer by default and are zero-index. In the example below, GET is equal to 1 and Delete is 2.

	enum Method {
	    GET,
	    DELETE,
	    POST,
	    PUT,
	    HEAD,
	    CONNECT,
	    OPTIONS,
	    TRACE,
	    PATCH,
	}

## Powerful Features

### Change value incrementation

In the example below, instead of beginning at zero, the enum will begin at three and increment from there for each additional value.

	enum Method {
	    GET = 3,
	    DELETE,
	    POST,
	    PUT,
	    HEAD,
	    CONNECT,
	    OPTIONS,
	    TRACE,
	    PATCH,
	}

### Mid-enum value incrementation change

The incrementation of the values can be changed in the middle of the enum. Here GET, DELETE and POST all hold 0, 1, and 2 respectively. But PUT jumps to 10 and those that follow will increment from that point.

	enum Method {
	    GET,
	    DELETE,
	    POST,
	    PUT = 10,
	    HEAD,
	    CONNECT,
	    OPTIONS,
	    TRACE,
	    PATCH,
	}

### Enum Variants can hold data and be of different data types

Here the GET, when used, will store a string as its value (perhaps the query string that came with the request). The DELETE will store an unsigned 64-bit number (perhaps the UUID of the entity to be deleted as a result of the request). The rest of the enums will

	enum Method {
	    GET(String),
	    DELETE(u64),
	    POST,
	    PUT,
	    HEAD,
	    CONNECT,
	    OPTIONS,
	    TRACE,
	    PATCH,
	}

	let myGet = Method::GET("page=5&page_count=50".to_string());
	let myDELETE = Method::DELETE(645);
