# Hash Maps

- Key value pairs. 

	use std::collections::HashMap;

	pub struct  QueryString<'buffer> {
	    data: HashMap<&'buffer str, Value<'buffer>>,
	}