# Structs

- https://doc.rust-lang.org/std/keyword.struct.html

A **Struct** is similar to a Class in other languages. It defines the `data` that will be contained within an instance of the type. It can also have an *implementation* that defines the `abilities` an instance of the Struct will have.

	struct Server {
		address: String
	}

	impl Server {
		fn new(address: String) -> Server {
			Server {
				address
			}
		}
	}

Within the implementation of a Struct, when adding abilities to the Struct, there exist two types of methods.

- *Associated Functions*: similar to class-level methods in other languages, deal with manipulating a Struct, such as adding a `new` function to the struct that handles creating an instance of the Struct. The `new` function is just a common practice, it is not a reserved word/function like it is in other languages. These functions are accessed using the `::` operator on the Struct type.

- *Methods*: these are instance methods that act on an instance of a struct, as you would expect in class-based object-oriented programming languages. These methods must have `self` passed to them so that they are associated with the instance of a struct. These functions are accessed using the `.` dot notation on an instance of the Struc.

*Self*: is an alias for the struct itself, when within the implementation. Instead of using the type name of the struct, we can use the reserved work `Self`. Altering the Server example above:

	impl Server {

		// Associated function
		fn new(address: String) -> Self {
			Self {
				address
			}
		}

		// Method
		fn run(self) {

		}
	}
