# if let
###### Match on only the variants we care about

`if let` allows you to match only on the variants you care about that would normally be found in a match and would be required to be handle.

	// NOTE: Must match for both some and none even if you do not care about the none case
    // match path.find('?') {
    //     Some(i_) => {
    //         query_string = Some(&path[i+1..]);
    //         path = &path[..i];
    //     }
    //     None => {}
    // }

    // NOTE: Getting to the some case without using match
    // let q = path.find('?');
    // if q.is_some() {
    //     let i =q.unwrap();
    //     query_string = Some(&path[i+1..]);
    //     path = &path[..i];
    // }

    // NOTE: using if/let to get at the some case without using a match
    if let Some(i) = path.find('?') {
        query_string = Some(&path[i+1..]);
        path = &path[..i];
    }
