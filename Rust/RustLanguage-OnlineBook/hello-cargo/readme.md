# Cargo for Rust
#### Package manager for third-party libraries

## Commands

Creating a new Rust project

    $ cargo new hello_cargo

Building for debugging

    $ cargo build

Building for release

    $ cargo build --release

Running Rust using Cargo

    $ cargo run

Checking to see if it will compile without creating an executable (faster than building)

    $ cargo check

 