fn main() {
    /*  NOTE: Variables are immutable by default in Rust. This forces developers to think about the
        data they are using within an application and whether or not that data should be changed
        as the application executes.

        Benefits are:
        - naturally thread-safe
        - manipulation of data must be intentional
        - forces developers to think about the data they are using

        Rules for defining variables:
        - must use the `let` keyword
        - do not have to have their type annotated (let age:i32 = 20)
        - can be set via a function call or runtime computation

        The rust compiler will throw errors if any part of your code attempts
        to mutate a variable that is assumed to be immutable. This allows the compiler
        to guarantee that your application state will not be mutated without
        you, the programmer, expressly indicating that mutation is allowed.
     */
    let x = 5;
    println!("The value of x is: {}", x);

    // This list, when uncommented, will throw an error when compiling as x is not mutable above
    // yet it is being changed here.
    x = 6;
    println!("The value of x is: {}", x);
}
