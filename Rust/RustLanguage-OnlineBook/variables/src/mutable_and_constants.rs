fn main() {
    /*  NOTE: The keyword `mut` can be used to change a variable from
        being immutable to mutable, Allowing the value to be changed in
        the following code.
     */
    let mut x = 5;
    println!("The value of x is: {}", x);

    x = 6;
    println!("The value of x is: {}", x);

    /*  NOTE: Rust also has constants, unlike variables declared with `let`
        and which do not have to have the their type defined, constants
        have the following rules surrounding them:
        - must be declared with the keyword `const`
        - must have the type annotated
        - must be set using a constant expression, cannot be set as the
            result of a function call or runtime computation
     */
    const MAX_POINTS: u32 = 100_000;

    /* NOTE: Variables can be shadowed when the identifier is reused with
        the `let` keyword. */
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);
}
