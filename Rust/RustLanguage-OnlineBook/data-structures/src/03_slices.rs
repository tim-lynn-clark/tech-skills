fn main() {
    /* NOTES: The Slice Type
       - A data type that does not have ownership.
       - Allows a reference to a contiguous sequence of elements in a collection
            rather than the entire collection. */
    let s = String::from("Hello world");

    /* Using the &s[starting_index, ending_index] notation, we get a reference to
       a portion of the string holding "Hello world". A note about the ending_index,
       it needs to be one more than the last position in the string you wish to capture,
       as it is and exclusive operation. */
    let hello = &s[0..5];
    let world = &s[6..11];


    /* This slice syntax has some shortcuts. */

    // Starting at the beginning of the string, you can omit the starting_index
    let start_slice_at_beginning = &s[..2]; // instead of &s[0..2]

    // Ending the slice at the end of the string, omit ending_index
    let end_slice_at_end = &s[3..]; // instead of &s[3..len];

    // Slice the entire string
    let entire_string_slice = &s[..]; // instead of &s[0..len]

    /* NOTE: Strings are complicated when you get into UTF-8. Slicing a string
       must happen at valid UTF-8 character boundaries. UTF-8 characters can be
       multibyte in size. */

    fn first_word(s: &String) -> &str {
        let bytes = s.as_bytes();

        for (i, &item) in bytes.iter().enumerate() {
            if item == b' ' {
                return &s[0..i];
            }
        }

        &s[..]
    }
}