fn main() {
    /* Note: Array type
       An array is a single chunk of memory allocated on the stack.
       All values within the array must be of the same type.
       Have a fixed length.
       Are useful when you want data allocated on the stack rather than the heap.
       Or when you want a fixed number of elements of a specific type. */
    let a = [1,2,3,4,5];

    /* Note: Arrays are zero indexed and indexed using bracket notation. */
    let first_value = a[0];

    /* Note: Typing an Array using the bracket notation [type, number of elements] */
    let b: [i32; 3] = [1,2,3];

    /* NOTE: Arrays can be initialized with the full number of values all set to a
     default value specified. This example creates an array with 8 values, with each
     value initialized with zero. */
    let byte = [0; 8];

    /* Note: Index out of bounds will cause a runtime error. */
    let does_not_exist = b[5];
}