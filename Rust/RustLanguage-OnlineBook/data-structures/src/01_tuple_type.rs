fn main() {
    /* Notes: Tuple Type
       a general way of grouping together some number of values with a
       variety of types into one compound type. */
    let tup: (i32, f64, u8) = (500, 6.4, 1);

    /* Notes: Accessing tuple values through indexing. Tuples are
        0 indexed like Arrays. */
    let first_item = tup.0;

    /* Notes: Destructuring a tuple into individual variables. */
    let (x, y, z) = tup;
}
