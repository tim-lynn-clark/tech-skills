#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

/* NOTES: the `impl` block is used to define functions within the context
   of a struct. That is, adding instance methods and associated functions
    (similar to class-level methods in OOP languages). */
impl Rectangle {
    /* the parameter `&self` defines this function as a method, as it takes in
       a reference to the instance of the struct on which the function is being
       called. In this case self is borrowed immutably. */
    fn area(&self) -> u32 {
        /* Within a method, the self keyword is used to access and potentially mutate
           the struct instance itself. */
        self.width * self.height
    }

    // Example of a method that takes in additional parameters.
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

/* NOTES: Structs are allowed to have multiple implementation blocks.
   not always necessary, as in this example there is really no reason to
   separate the associated functions from the methods. This practice is
   commonly found in generic types and traits. */
impl Rectangle {
    /* Example of an associated function. Notice it does not take in a
       reference to self, but it has functionality that deals with the
       rectangle struct type. */
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}