fn main() {
    // Struct definition
    #[derive(Debug)]
    struct User {
        first_name: String,
        last_name: String,
        email: String,
        active: bool,
    }

    // Immutable Instance of a Struct
    let bob = User {
        first_name: String::from("Bob"),
        last_name: String::from("Smith"),
        email: String::from("bob@bob.com"),
        active: true,
    };

    // Accessing struct values using dot notation
    println!("First Name: {}", bob.first_name);

    // As with everything in Rust, instances of structs are immutable by default
    // bob.first_name = "Freddy"; // Mutation Error

    // Mutable Instance of a Struct
    let mut sally = User {
        first_name: String::from("Sally"),
        last_name: String::from("Frederickson"),
        email: String::from("sally@sally.com"),
        active: true,
    };
    println!("{:?}", sally);
    sally.last_name = String::from("Fredericson");
    println!("{:?}", sally);

    /* Struct update syntax: struct instances can be updated using this
       special syntax. */
    let updated_bob = User {
        email: String::from("robert@bob.com"),
        ..bob
    };
    println!("{:?}", updated_bob);

    // Tuple Structs: a way of creating a specifically named Tuple type
    #[derive(Debug)]
    struct Color(i32, u64, String);
    let black = Color(0, 000000, String::from("Black"));
    println!("{:?}", black);

    #[derive(Debug)]
    struct Point(i32, i32, i32);
    let origin = Point(0,0,0);
    println!("{:?}", origin);

}
