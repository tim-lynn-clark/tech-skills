fn main() {
    let s1 = String::from("hello");

    //These ampersands are references, and they allow you to refer to some value without taking ownership of it.
    let len = calculate_length(&s1);

    // s1 can be used again as ownership to it was never given to `calculate_length`
    println!("The length of '{}' is {}.", s1, len);
    // Variables by nature are immutable, so s1, even when referenced, cannot be changed by the borrower
    change_bad(&s1);

    let mut s2 = String::from("Hello");
    change_good(&mut s2);
}

// These ampersands are references, and they allow you to refer to some value without taking ownership of it.
fn calculate_length(s: &String) -> usize {
    // `s` is a reference to the variable in main, and ownership is not taken by this function
    s.len()
}   // Even though `s` goes out of scope at the close of the function call, it does not get dropped
// because the function did not take ownership, due to the reference `&`,
// which is called `borrowing` in Rust

/* What borrowing looks like in memory:

    When borrowing a value as in the function above:
    - s1 is on the stack and holds a pointer to the string "hello" on the heap
    - When s1 is passed to calculate_length as a reference (using the & symbol),
        the pointer to "hello" is passed into the function
    - s is then placed on the stack and holds a pointer to the pointer that s1
        holds to "hello" on the heap
    - When calculate_length ends and s goes out of scope, s is invalidated.
        Because it was just a pointer to a pointer, the value is not discarded by
        Rust, just the pointer held on the stack. s1 still maintains a pointer to
        "hello" on the heap.

    s --> s1 --> value

 */


fn change_bad(some_string: &String) {
    // CODE => ERROR // some_string is an immutable referenced variable
    // some_string.push_str(", world");
    println!("{}", some_string);
}

fn change_good(some_string: &mut String) {
    some_string.push_str(", world");
}

/* NOTE: ONLY ONE MUTABLE REFERENCE ALLOWED
        But mutable references have one big restriction: you can have only one mutable reference
        to a particular piece of data in a particular scope.

        This restriction allows for mutation but in a very controlled fashion. It’s something
        that new Rustaceans struggle with, because most languages let you mutate whenever you’d like.

        The benefit of having this restriction is that Rust can prevent data races at compile time.
        A data race is similar to a race condition and happens when these three behaviors occur:

            - Two or more pointers access the same data at the same time.
            - At least one of the pointers is being used to write to the data.
            - There’s no mechanism being used to synchronize access to the data.

        Data races cause undefined behavior and can be difficult to diagnose and fix when you’re
        trying to track them down at runtime; Rust prevents this problem from happening because
        it won’t even compile code with data races!
    */
// CODE => ERROR
// let mut s = String::from("hello");
// let r1 = &mut s;
// let r2 = &mut s;
// println!("{}, {}", r1, r2);

// NOTE: A similar rule exists for combining mutable and immutable references.
// CODE => ERROR
// let mut s = String::from("hello");
// let r1 = &s; // no problem
// let r2 = &s; // no problem
// let r3 = &mut s; // BIG PROBLEM
// println!("{}, {}, and {}", r1, r2, r3);