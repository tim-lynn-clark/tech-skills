fn main() {
    let s = String::from("hello");    // s comes into scope

    println!("s can be used here: {}", s);      // s can be used here as it is still in scope

    takes_ownership(s);              // s's value moves into the function...

    // CODE => ERROR
    //println!("s can NOT be used here: {}", s);  // s can NOT be used here as it has transferred to the `take_ownership` function

    let x = 5;                             // x comes into scope

    makes_copy(x);                  // x would move into the function,

    println!("x can be used: {}", x);           // but i32 is a simple type and its value is Copied into `makes_copy` function, so it’s okay to still
    // use x afterward

}   // Here, x goes out of scope, then s. But because s's value was moved, nothing
// special happens.

fn takes_ownership(some_string: String) { // some_string comes into scope
    println!("{}", some_string);
}   // Here, some_string goes out of scope and `drop` is called. The backing
// memory is freed.

fn makes_copy(some_integer: i32) { // some_integer comes into scope
    println!("{}", some_integer);
}   // Here, some_integer goes out of scope. Nothing special happens.

fn gives_ownership() -> String {             // gives_ownership will move its
    // return value into the function
    // that calls it
    let some_string = String::from("hello"); // some_string comes into scope
    some_string                              // some_string is returned and
    // moves out to the calling
    // function
}

// takes_and_gives_back will take a String and return one
fn takes_and_gives_back(a_string: String) -> String { // a_string comes into scope
    a_string  // a_string is returned and moves out to the calling function
}