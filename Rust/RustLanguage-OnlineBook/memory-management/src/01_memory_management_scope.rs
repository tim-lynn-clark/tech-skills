
/*
    NOTES: Memory Management

    Traditional Methods:
        Garbage collection - ease of programming with significant performance hits
        Manual - difficulty in programming with significant performance increases

    Ownership: Rust uses a third approach; memory is managed through a system of ownership with a
    set of rules that the compiler checks at compile time. None of the ownership features slow down
    your program while it’s running

        Rules:
        - Each value in Rust has a variable that’s called its owner.
        - There can only be one owner at a time.
        - When the owner goes out of scope, the value will be dropped.

        Scope is important:
        Variables exist from the time they come into scope until that scope ends.

*/

fn main() {

    // Using a String object instead of a string literal, in order to demonstrate ownership
    // In relation to a complex type stored on the heap.
    {
        // `s` is created within the scope of the {} block
        let mut s = String::from("hello");
        // `s` can be accessed and used by code within the scope in which it lives
        s.push_str(", world!");
        println!("{}", s);
    } // Rust calls `drop` for us on all variables that were created within the scope of the {} code block
    // `s` no longer lives as the scope in which it was created has ended.
    // println!("{}", s); // This code, if uncommented will throw an error at compile-time


    { //New Scope: Is for fun
        /* Simple types are copy by value:
            - All the integer types, such as u32.
            - The Boolean type, bool, with values true and false.
            - All the floating point types, such as f64.
            - The character type, char.
            - Tuples, if they only contain types that are also Copy. For example, (i32, i32) is Copy, but (i32, String) is not.
        */

        let x = 5;
        let y = x;
        println!("x: {}, y: {}", x, y);
    }

    {//New Scope: Is for fun
        // Complex types are copy by reference, the pointer on the stack is copied,
        // but both variables point to the same heap location
        let s1 = String::from("Hey now!");
        let _s2 = s1; // Underscore added to stop build warnings on an unused variable

        /*  Problem here is that both s1 and s2 have separate entries on the stack but point to the
            same location on the heap. When they go out of scope at the closing curly brace, `drop`
            would normally be called on each variable and in turn the heap location would be cleaned twice
            This introduces a 'double free' error causing memory corruption and potential security vulnerabilities.
            Rust will not allow you to cause a 'double free' error, so this code will cause a compile-time error. */
        // CODE => ERROR
        // print!("s1: {}, s2: {}", s1, s2);

        /* Setting s2 to s1 is called a `move` in Rust, that is the value that s1 points to is
            `moved` to s2 and s2 becomes the owner of the value.
            s1 is then invalidated by the Rust compiler and only s2 is valid, when s2 goes out of scope,
            it alone is responsible for freeing the value from memory. */

        /*  CLONING/DEEP COPY: If the intent was to make a copy of the string, it would need to be cloned to avoid
            copying only the reference and avoiding the `move` error. */
        let s3 = String::from("Hey now!");
        let s4 = s3.clone();
        print!("s3: {}, s4: {}", s3, s4);
    }
}
