fn main() {
    /* NOTES:
        # Option Type (no null)
        - https://doc.rust-lang.org/std/option/index.html

        Rust does not have the concept of `null`, it simply does not allow you to have a variable
        that does not contain the data it claims to hold. To handle cases where there may or may
        not be data within a variable, Rust's standard library provides the `Option` enum. */


    // Standard Library Definition for the Option enum
    pub enum Option<T> {
        None,
        Some(T),
    }

    /* NOTES:
        As seen in the Option enum definition, it provides a `None` value, representing the
        *null case* and a `Some(T)` value that represents the *true case* and provides *data*.
        Utilizing generics, Rust allows the Option enum to be used with any Types within the
        language as well as those that are user-defined.

        A practical example would be that of an HTTP request Struct that has a property for a
        query string. Not all HTTP requests will have query strings and therefore there exists the
        `null` case. We can use the `Option` enum to handle this case. */

    struct Request {
        path: String,
        query_string: Option<String>,
        method: Method,
    }

    /* NOTES:
        The `Option` enum is meant for handling the `null` case within Rust programs and is
        therefore automatically imported into all Rust code and can be used without an import from the standard library.*/

    /* NOTES:
        Why is this better than having a null in the language. If there is no valid value, the None
        will provided. If there is a valid value it will be wrapped in a Some(T).

        By having the Option<T>, the actual value that will be used in logic and calculations is a
        different type than the Some(T) that is returned by as the Option. This forces the developer
        to unwrap the Some to get at the value before it can be used or deal with the None that is
         returned when there is no value. This is all checked at compile time, verifying that the
         both the true and false cases of a potentially missing value have been handled by the
         developer prior to the code being run.  */

    let x: i8 = 5;
    let y: Option<i8> = Some(5);

    let sum = x + y;
    /* NOTES:
        Failed compilation as the value of y could potentially be missing as it is declared as an
        Option type. There fore the math of x + y is invalid as they are of two different types. */

}