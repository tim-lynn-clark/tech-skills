fn main() {
    /* NOTES: Enums allow the defining of types that enumerate possible
       variants. Rust enumerations can encode meaning with data.

       - https://doc.rust-lang.org/std/keyword.enum.html
     */

    enum IpAddressKind {
        V4,
        V6
    }

    /* NOTES: enums can be used as a type for variables */
    let four = IpAddressKind::V4;
    let six = IpAddressKind::V6;

    /* NOTES: enums can be used as a type for parameters passed into functions  */
    fn route(ip_kind: IpAddressKind) {
        println!("{:?}", ip_address_kind);
    }
    route(four);
    route(six);

    /* NOTES:
        **Enums** in Rust are similar to those found in other languages, but have additional
        abilities that are usually not found in higher-level languages like C# and Java.
        Each `value` in an enum is associated with an integer by default and are zero-index.
        In the example below, GET is equal to 1 and Delete is 2. */

    enum MethodsVersionOne {
        GET,
        DELETE,
        POST,
        PUT,
        HEAD,
        CONNECT,
        OPTIONS,
        TRACE,
        PATCH,
    }

    /* NOTES:
       ## Powerful Features */

    /* NOTES:
        ### Change value incrementation
        In the example below, instead of beginning at zero, the enum will begin at three and
        increment from there for each additional value. */

    enum MethodsVersionTwo {
        GET = 3,
        DELETE,
        POST,
        PUT,
        HEAD,
        CONNECT,
        OPTIONS,
        TRACE,
        PATCH,
    }

    /* NOTES:
        ### Mid-enum value incrementation change
        The incrementation of the values can be changed in the middle of the enum. Here GET,
        DELETE and POST all hold 0, 1, and 2 respectively. But PUT jumps to 10 and those that
        follow will increment from that point. */

    enum MethodsVersionThree {
        GET,
        DELETE,
        POST,
        PUT = 10,
        HEAD,
        CONNECT,
        OPTIONS,
        TRACE,
        PATCH,
    }

    /* NOTES:
        ### Enum Variants can hold data and be of different data types
        Here the GET, when used, will store a string as its value (perhaps the query string that
        came with the request). The DELETE will store an unsigned 64-bit number (perhaps the UUID
        of the entity to be deleted as a result of the request). The rest of the enums will*/

    enum MethodsVersionFour {
        GET(String),
        DELETE(u64),
        POST,
        PUT,
        HEAD,
        CONNECT,
        OPTIONS,
        TRACE,
        PATCH,
    }

    let myGet = Method::GET("page=5&page_count=50".to_string());
    let myDELETE = Method::DELETE(645);

    /* NOTES: Enums with data holding variants is similar to defining individual structs, but
        the ability to reduce the complexity of function signatures as it combines all the structs
        into a single type. */

    // Individual structs example
    struct QuiteMessage; //unit struct
    struct MoveMessage {
        x: i32,
        y: i32,
    }
    struct WriteMessage(String);
    struct ChangeColorMessage(i32, i32, i32);


    // Enum to combine structs example
    enum Message {
        Quit,
        Move { x: i32, y: i32 },
        Write (string),
        ChangeColor (i32, i32, i32),
    }
    /* NOTES:
        - Quit has not data associated with it and therefore defaults to 0
        - Move includes an anonymous struct inside it.
        - Write includes a single String.
        - ChangeColor includes three i32 values that make up the color values. */

    impl Message {
        fn call(&self) {
            // Some method body would be defined here to work with the different enum variants
        }
    }
    let m = Message::Write(String::from("This is a message to you"));
    m.call();

}