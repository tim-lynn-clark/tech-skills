/* Notes: the Main function
   the `main` function is the entry point of many programs. */
fn main() {

    /* Notes: Functions are invoked using parentheses. */
    another_function();
    func_with_param(256);
    func_with_params(125, 364);

    /* NOTES: Function bodies are made up of a series of statements optionally ending in an expression.
    Because Rust is an expression-based language, there is an important distinction to understand.
    Other languages don’t have the same distinctions between statements and expressions.

    Statements do not return values. The `let y = 6` statement does not return a value, so with
    this imaginary example `let x = (let y = 6);` there isn’t anything for x to bind to.

    Expressions evaluate to something and make up most of the rest of the code that you’ll
    write in Rust. Consider a simple math operation, such as 5 + 6, which is an expression that
    evaluates to the value 11. Expressions can be part of statements: in Listing 3-1, the 6 in
    the statement let y = 6; is an expression that evaluates to the value 6. Calling a function
    is an expression. Calling a macro is an expression. The block that we use to create new
    scopes, {}, is an expression. */

    // These are statements with an expression
    let x = 5; // `5` is the expression

    let y = {
        let x = 3;
        x + 1 // Notice the lack of a semicolon, this means the result of this expression will be returned
    };
    // Everything wrapped in the curly braces is an expression since it is a new scope or block

    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);

    println!(
        "Function return: {}",
        func_with_explicit_return_statement()
    );

    println!(
        "Function return: {}",
        func_with_explicit_return_statement()
    );
}

/* Notes: Rust uses snake_case for function and variable names. All functions begin with the
   keyword `fn` */
fn another_function() {
    println!("This is another function.");
}

fn func_with_param(x: i32) {
    println!("The value of x is: {}", x);
}

// Parameters must be named and typed
fn func_with_params(x: i32, y: i32) {
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}

// Return values must be typed
fn func_with_explicit_return_statement() -> i32 {
    return 5; // An explicit return statement can be used to return a value from a function
}
fn func_with_implicit_return_expression() -> i32 {
    5 /* An implicit return can be done using the last expression (returns a value) of the function
    body. Notice the lack of a semicolon, this means the result of this expression will be the
    return value for this function. */
}

