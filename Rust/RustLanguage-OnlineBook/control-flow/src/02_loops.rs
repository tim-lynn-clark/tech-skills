fn main() {

    // Loops
    let mut counter = 0;
    let result = loop {
        counter += 1;
        println!("Looping to {}...", counter);

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("Count * 2 = {}", result);

    // While Loops
    let mut counter_two = 3;
    while counter_two != 0 {
        println!("{}!", number);
        counter_two -= 1;
    }

    // For Loops
    let a = [10, 20 , 30, 40, 50];
    for element in a.iter() {
        println!("The value is: {}", element);
    }

    // Ruby-like Loops
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("Lift off!!!");
}
