fn main() {

    /* Notes: Rust does not have truthy types like JavaScript,
       conditions must be an actual boolean value. */
    let condition = true;

    // If
    if condition {
        println!("Condition was true.");
    }

    // If/Else
    if condition {
        println!("Condition was true.");
    } else {
        println!("Condition was false.");
    }

    /* If and If/Else are expressions, therefore they can be used
        to return the results of another expression. This means
        they can be used on the right side of a let statement to provide
        a value from an expression in the if/else blocks. */
    let number = if condition {
        20 * 3
    } else {
        10 * 6
    };
    /* NOTE: The return values from the expressions within the if/else
       blocks must be the type required by the left-hand side of the
       let statement. */

    // If/Else if
    let another_condition = false;
    if condition {
        println!("Condition was true.");
    } else if another_condition {
        println!("Condition was false.");
    } else {
        println!("Something else happened here.");
    }

    println!("{}", number);
}
