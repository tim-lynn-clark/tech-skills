fn main() {
    /* NOTES:
        Rust has a powerful control flow operator called `match`. It allows the comparison
        of a value against a series of patterns and then execute code based on which pattern
        was matched.

        Patterns can be made up of literal values, variable names, wildcards, and many other things. */

    enum Coin {
        Penny = 1,
        Nickel = 5,
        Dime = 10,
        Quarter = 25,
    }

    fn value_in_cents(coin: Coin) -> u8 {
        match coin {
            Coin::Penny => 1,
            Coin::Nickel => 5,
            Coin::Dime => 10,
            Coin::Quarter => 25,
        }
    }
}