use std::io;

fn main() {
    print!("Enter your weight in (kg).");

    // NOTE: mut allows a variable to be reassigned (mutation allowed)
    let mut input = String::new();
    // NOTE: even if a variable is marked mutable, it still cannot be mutated
    // if it is passed to another function (ownership & scope).
    // Rust's Rules on Ownership
    // 1. Each value is owned by a variable.
    // 2. when the owner goes out of scope, the value will be deallocated.
    // 3. There can only be ONE owner at a time. (stops double-free errors commonly found in C and C++)

    /* NOTE: Borrowing
     * Is done through references, which are indicated by the `&`
     * By default event borrowed values are immutable and cannot be changed within the function
     * that is borrowing.
     * To make a value that is borrowed mutable, you have to use the key word `mut` after the `&`
     * like `&mut`.
    */

    /* NOTE: Result (wrapper around success and error). Results are key to Rust error handling as
     * the language does not support try/catch blocks.
     * the method `unwrap` on a result will return the success case of the result or will terminate the
     * application ungracefully. The `unwrap` method should be avoided in production code where possible.
     * */
    io::stdin().read_line(&mut input).unwrap();
    let earth_weight: f32 = input.trim().parse().unwrap();
    dbg!(earth_weight);

    let mars_weight = calculate_weight_on_mars(earth_weight);
    println!("Your weight on Mars: {}kg", mars_weight);
}

fn calculate_weight_on_mars(weight: f32) -> f32 {
    /*
     * Mars weight calculation
     * mars_weight = earth_weight / 9.81 m/s(2) x 3.711 m/s(2)
    */
    (weight / 9.81) * 3.711
}