use std::collections::HashMap;

// Sample Query String
// a=1&b=2&c&d=&e===&d=7&d=abc
// NOTE: deriving the ability for this struct to have the debug trait without manually implementing it.
#[derive(Debug)]
pub struct  QueryString<'buffer> {
    data: HashMap<&'buffer str, Value<'buffer>>,
}

// NOTE: deriving the ability for this struct to have the debug trait without manually implementing it.
#[derive(Debug)]
pub enum Value<'buffer> {
    Single(&'buffer str),
    Multiple(Vec<&'buffer str>),
}

impl<'buffer> QueryString<'buffer> {
    pub fn get(&self, key: &str) -> Option<&Value> {
        self.data.get(key)
    }
}

impl<'buffer> From<&'buffer str> for QueryString<'buffer> {
    fn from(s: &'buffer str) -> Self {
        let mut data = HashMap::new();

        for sub_str in s.split('&') {
            let mut key = sub_str;
            let mut val = "";

            if let Some(i) = sub_str.find('=') {
                key = &sub_str[..i];
                val = &sub_str[i+1..];
            }

            // If key already exists
            data.entry(key)
                .and_modify(|existing: &mut Value| match existing {
                    Value::Single(previous_value) => {
                        // NOTE: Manually constructing the Vector
                        // let mut new_value_list = Vec::new();
                        // new_value_list.push(val);
                        // new_value_list.push(previous_value);

                        // NOTE: Shorthand Vector literal syntax
                        //let mut new_value_list = vec![val, previous_value];

                        // NOTE: create and reassign the existing value with a vector literal
                        *existing = Value::Multiple(vec![val, previous_value])
                    }
                    Value::Multiple(value_list) => value_list.push(val)
                })
                .or_insert(Value::Single(val));
        }

        QueryString { data };

        unimplemented!()
    }
}