use std::convert::TryFrom;
use std::error::Error;
// NOTE: Multiple imports from specified package
use std::fmt::{Display, Debug, Formatter, Result as FmtResult};
use std::str;
use std::str::Utf8Error;
use std::str::FromStr;
use super::{QueryString, QueryStringValue};

// NOTE: deriving the ability for this struct to have the debug trait without manually implementing it.
#[derive(Debug)]
pub enum Method {
    GET,
    DELETE,
    POST,
    PUT,
    HEAD,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH,
}

impl FromStr for Method {
    type Err = MethodError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "GET" => Ok(Self::GET),
            "DELETE" => Ok(Self::DELETE),
            "POST" => Ok(Self::POST),
            "PUT" => Ok(Self::PUT),
            "HEAD" => Ok(Self::HEAD),
            "CONNECT" => Ok(Self::CONNECT),
            "OPTIONS" => Ok(Self::OPTIONS),
            "TRACE" => Ok(Self::TRACE),
            "PATCH" => Ok(Self::PATCH),
            _ => Err(MethodError),
        }
    }
}

pub struct MethodError;

/* HTTP GET Request Example
    GET /user?id=10 HTTP/1.1\r\n
    HEADERS \r\n
    BODY
 */

/* NOTE: Specifying a lifetime for this struct using the lifetime syntax which is similar to
 * generics. <'a> the 'a specifies the name of the lifetime to which variables can be assigned. */
// NOTE: deriving the ability for this struct to have the debug trait without manually implementing it.
#[derive(Debug)]
pub struct Request<'buffer> {
    path: &'buffer str,
    // Not all requests will have a query string, so we have to handle that case
    // We use the Rust option enum to handle the `null` cases as `null` is not allowed in the language
    query_string: Option<QueryString<'buffer>>,
    method: Method,
}

/* NOTE: In order to use the lifetime added to the struct, it must be added to any implementation
 * specific to that struct. */
impl<'buffer> Request<'buffer> {

}

/* NOTE: In order to use the lifetime added to the struct, it must be added to any implementation
 * specific to that struct. */
// NOTE: Implementing a Trait for type conversion from the standard library
impl<'buffer> TryFrom<&'buffer [u8]> for Request<'buffer> {
    type Error = ParseError;

    fn try_from(buffer: &'buffer [u8]) -> Result<Self, Self::Error> {
        // match str::from_utf8(buffer) {
        //     Ok(request) => {},
        //     Err(_) => return Err(ParseError::InvalidEncoding),
        // }

        // match str::from_utf8(buffer).or(Err(ParseError::InvalidEncoding)) {
        //     Ok(request) => {},
        //     Err(e) => return Err(e),
        // }

        // NOTE: Magic syntax provided by Rust and is equivalent to above
        let request = str::from_utf8(buffer)?;

        // match get_next_word(request) {
        //     Some((method, request)) => {},
        //     None => return Err(ParseError::InvalidRequest),
        // }

        // NOTE: Magic syntax provided by Rust and is equivalent to above
        let (method, request) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;
        let (mut path, request) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;
        let (protocol, _) = get_next_word(request).ok_or(ParseError::InvalidRequest)?;

        if protocol != "HTTP/1.1" {
            return Err(ParseError::InvalidProtocol);
        }

        let method: Method = method.parse()?;

        let mut query_string = None;

        // NOTE: Must match for both some and none even if you do not care about the none case
        // match path.find('?') {
        //     Some(i_) => {
        //         query_string = Some(&path[i+1..]);
        //         path = &path[..i];
        //     }
        //     None => {}
        // }

        // NOTE: Getting to the some case without using match
        // let q = path.find('?');
        // if q.is_some() {
        //     let i =q.unwrap();
        //     query_string = Some(&path[i+1..]);
        //     path = &path[..i];
        // }

        // NOTE: using if/let to get at the some case without using a match
        if let Some(i) = path.find('?') {
            query_string = Some(QueryString::from(&path[i+1..]));
            path = &path[..i];
        }

        Ok(Self {
            path,
            query_string,
            method
        })
    }
}

fn get_next_word(request: &str) -> Option<(&str, &str)> {
    // NOTE: using an infinite loop to iterate over string
    // let mut iter = request.chars();
    // loop {
    //     let item = iter.next();
    //     match item {
    //         Some(c) => {},
    //         None => break,
    //     }
    // }

    for (i, c) in request.chars().enumerate() {
        if c == ' ' || c == '\r' {
            /* NOTE: Normally it is unadvised when iterating over strings to increment the index
             * of the current iteration as not all characters are only one byte in size, when
             * incrementing the index by one on a u8 array, you are moving one byte forward, this
             * may end up only grabbing part of a character. In this case it is safe as we know
             * that the current index is pointing to a space character which is a single byte in
             * length and therefore we would not be moving into part of a character by advancing
             * one byte. */
            return Some((&request[..i], &request[i+i..]));
        }
    }

    None
}

/* NOTE: Custom errors can be created using enums and implementing the standard Error trait. */
pub enum ParseError {
    InvalidRequest,
    InvalidEncoding,
    InvalidProtocol,
    InvalidMethod,
}

impl ParseError {
    fn message(&self) -> &str {
        match self {
            Self::InvalidRequest => "Invalid Request",
            Self::InvalidEncoding => "Invalid Encoding",
            Self::InvalidProtocol => "Invalid Protocol",
            Self::InvalidMethod => "Invalid Method",
        }
    }
}

impl From<MethodError> for ParseError {
    fn from(_: MethodError) -> Self {
        Self::InvalidMethod
    }
}

impl From<Utf8Error> for ParseError {
    fn from(_: Utf8Error) -> Self {
        Self::InvalidEncoding
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for ParseError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Error for ParseError {}