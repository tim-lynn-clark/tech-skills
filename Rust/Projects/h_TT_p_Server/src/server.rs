use std::net::TcpListener;
use std::io::Read;
use std::convert::TryFrom;
use crate::http::Request;

// NOTE: Struct is like a class in other languages
pub struct Server {
    addr: String
}

// NOTE: Implementation is where the abilities of a struct live
impl Server {
    /* NOTE:
    *   Associated function, use :: to access them on the struct
    *   fn new(addr: String) -> Server {
    *     Server {
    *         addr
    *     }
    *   }
    *   Self is the alias for the name of the struct. */
    pub fn new(addr: String) -> Self {
        Self {
            addr
        }
    }

    /* NOTE:
    *   Methods use the . to access them on the instance of a struct
    *   Self must be passed to the function so that the instance of the struct becomes the owner
    *   of the method */
    pub fn run(self) {
        println!("Listening on {}", self.addr);
        // NOTE: Passing a reference (borrowing) to the address to the bind method, not moving ownership
        /* NOTE: `unwrap` is a function of the `Result` type, it is meant only for use on
         * unrecoverable error situations, when you want the application to crash rather than
         * handling the error and trying to recover. */
        let listener = TcpListener::bind(&self.addr).unwrap();

        // NOTE: Infinite loop to house code to check for incoming connections
        loop {
            // NOTE: Example processing a Result using If statements
            /* NOTE: TCP listener.accept returns a Result as it has the possibility of failing
             * so store it in a variable so we can handle the error and success cases. */
            // let res =  listener.accept();
            // NOTE: Checking to see if the result is the error case, if so, continue to the next iteration of the loop
            // if res.is_err() {
            //     continue;
            // }
            // NOTE: Since the error case was checked for and handled above, we can safely unwrap the Result and pull out the success case.
            // NOTE: Tuples can be deconstructed into their parts.
            //let (stream, address) = res.unwrap();

            // NOTE: Example of processing a Result using a Match Expression
            match listener.accept() {
                Ok((mut stream, address)) => {
                    // NOTE: create a buffer array to hold what is being read from the stream
                    // NOTE: Array syntax to define an array of a certain length and pre-populate the values using the same value specified
                    // [initial_value, length]
                    // NOTE: In a real-world server implementation, we would need to be smarter with the buffer so it does not overrun.
                    let mut buffer = [0; 1024];
                    match stream.read(&mut buffer) {
                        Ok(_) => {
                            println!("Received a request: {}", String::from_utf8_lossy(&buffer));

                            // NOTE: Methods for type conversion
                            // Request::try_from(&buffer as &[u8]);
                            match Request::try_from(&buffer[..]) {
                                Ok(request) => {
                                    dbg!(request);
                                },
                                Err(e) => println!("Failed to parse request: {}", e),
                            }
                        },
                        Err(e) => println!("Failed to read from stream: {}", e),
                    }
                },
                Err(e) => println!("Failed to establish a connection: {}", e),
            }
        }
    }
}