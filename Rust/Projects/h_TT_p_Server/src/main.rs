// Modules are imported into each other using `mod`
mod server;
mod http;

// If namespaced of modules get too long and repetitious to type, they can be
// shortened through the use of the `use` keyword.
use server::Server;
use http::Request;


fn main() {
    /* NOTE:
    *   std::string::String is not the same as &str
    *   &str is a string slice not a string object, it is a view inside a string object.
    *   String literals like "127.0.0.1:8080" equate to a string slice not a string object.
    *       Because string literals are provided at compile time, since they are hard-coded into
    *       the source code, the compiler `bakes`
    *
    *   Example:
    *       let address_and_port = String::from("127.0.0.1:8080");
    *       let just_port = &address_and_port[10..];
    *       dbg!(&address_and_port);
    *       dbg!(just_port) */
    // let server = server::Server::new("127.0.0.1:8080".to_string());
    let server = Server::new("127.0.0.1:8080".to_string());
    server.run();
}

// Modules can be used to encapsulate code and control its visibility
// mod server {
//     // NOTE: Struct is like a class in other languages
//     pub struct Server {
//         addr: String
//     }
//
//     // NOTE: Implementation is where the abilities of a struct live
//     impl Server {
//         /* NOTE:
//         *   Associated function, use :: to access them on the struct
//         *   fn new(addr: String) -> Server {
//         *     Server {
//         *         addr
//         *     }
//         *   }
//         *   Self is the alias for the name of the struct. */
//         pub fn new(addr: String) -> Self {
//             Self {
//                 addr
//             }
//         }
//
//         /* NOTE:
//         *   Methods use the . to access them on the instance of a struct
//         *   Self must be passed to the function so that the instance of the struct becomes the owner
//         *   of the method */
//         pub fn run(self) {
//
//         }
//     }
// }


// mod http {
//     pub mod request {
//         enum Method {
//             GET,
//             DELETE,
//             POST,
//             PUT,
//             HEAD,
//             CONNECT,
//             OPTIONS,
//             TRACE,
//             PATCH,
//         }
//
//         /* HTTP GET Request Example
//             GET /user?id=10 HTTP/1.1\r\n
//             HEADERS \r\n
//             BODY
//          */
//         pub struct Request {
//             path: String,
//             // Not all requests will have a query string, so we have to handle that case
//             // We use the Rust option enum to handle the `null` cases as `null` is not allowed in the language
//             query_string: Option<String>,
//             method: Method,
//         }
//     }
// }
