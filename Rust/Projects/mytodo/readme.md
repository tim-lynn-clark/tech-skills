
# Rust ToDo List Tutorial

###### Follows: https://erwabook.com/intro/index.html 

## Project Setup
    
### Rust programming language

Install using [rustup](https://rustup.rs/) an installer purpose-built for Rust

Verify the successful installation of Rust by typing the following in the terminal.

    $ rustc

Should see output like the following, only a lot more

    Usage: rustc [OPTIONS] INPUT

    Options:
    -h, --help          Display this message
    --cfg SPEC      Configure the compilation environment
    -L [KIND=]PATH      Add a directory to the library search path. The
    optional KIND can be one of dependency, crate, native,
    framework, or all (the default).

Verify `cargo`, Rust's package manager, is active with the following command.

    $ cargo

Should see output like the following, only a lot more

    Rust's package manager
    
    USAGE:
    cargo [+toolchain] [OPTIONS] [SUBCOMMAND]
    
    OPTIONS:
    -V, --version           Print version info and exit
    --list              List installed commands
    --explain <CODE>    Run `rustc --explain CODE`

### Create a new Rust project using Cargo

    $ cargo new mytodo
    $ cd mytodo
    $ cargo run

### Sqlite3 Lightweight Database

Utilizing [Homebrew](https://brew.sh/), install Sqlite3

    $ brew install sqlite

### Diesel ORM for Data Access

[Diesel](https://diesel.rs/) is an ORM for Rust that supports SQLite3, PostgreSQL, MySQL. 

Add Diesel as a dependency to the Rust project as seen below:

    [package]
    name = "myblog"
    version = "0.1.0"
    authors = ["Your Name <you@example.com>"]
    edition = "2018"
    
    [dependencies]
    diesel = { version = "1.0.0", features = ["sqlite"] }

Make sure everything compiles

    $ cargo run

Install the Diesel CLI

    $ cargo install diesel_cli --no-default-features --features sqlite

#### While still in the root folder of the Rust project do the following:

Set up the location of the SQLite database file for the project using a `.env` file.

    $ echo 'DATABASE_URL=./testdb.sqlite3' > .env

Get Diesel to set up the workspace

    $ diesel setup

Diesel supports [database migrations](https://diesel.rs/guides/getting-started/) similar to Ruby on Rails. Create your first migration.

    $ diesel migration generate task

The migration was created in a subfolder of the project titled `migrations`. Open up the `up.sql` file and add the following SQL to create the `task` table in the database.

    CREATE TABLE task (
        id INTEGER NOT NULL,
        title TEXT NOT NULL,
        PRIMARY KEY (id)
    );

Once the migration has been created, it can be run and the SQL is executed against the database.

    $ diesel migration run

Verify the table was created in the SQLite database.

    $ echo .dump | sqlite3 testdb.sqlite3

Migrations can be rolled back, once SQL has been added to the `down.sql` file of the same migration.

    DROP TABLE task;

Roll back

    $ diesel migration redo
    
Manually querying the SQLite database to see if data is making it into the DB.

    $ echo 'select * from task;' | sqlite3 testdb.sqlite3

