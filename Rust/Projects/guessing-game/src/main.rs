/* Note: use is used to import modules from external libraries included
    via Cargo as dependencies. */
use std::io;
use rand::Rng;
use std::cmp::Ordering;

/* Note: cargo doc --open, this command can be used to build documentation
    locally for crates included as dependencies in the cargo file as well as
    for the current project. Once built, the documentation is loaded in
    your browser. */

fn main() {
    println!("Guess a number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_number);

    loop {

        println!("Please input your guess.");

        /*  NOTE: Variables in Rust are immutable by default.
            let age = 20; once set, cannot be changed (constant in other languages).
            To make a variable mutable or changeable, it has to be annotated as mutable using the mut keyword.
        */
        let mut guess = String::new();

        /*  NOTE: Immutable vs. Mutable References Variables

            `&guess` is an example of a variable being passed as a reference to another scope (function or method).
            By default, referenced variables are immutable, even if they are mutable in the current scope. This stops the
            referenced variable from being changed unintentionally by code in another scope, or any scope that variable
            may be passed to up the call stack.

            `&mut guess` is an example of a variable being passed as a reference to another scope (function or method).
            The use of &mut indicates that the passed reference variable is mutable by the receiving scope.
            If the passed reference variable is meant to be changed within the new scope, it has to be marked as mutable.
        */
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");
        /* NOTE: expect is a method of the io::Result, if the result is an Err value, expect will
            cause the program to crash and display the provided error message. */

        /*  NOTE: Recreating the guess variable with new uInt32 type after parsing the int from the input string.
            In this instance, the second guess variable is shadowing the first guess.
         */
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }

        }
    }
}
