fn main() {
    for n in 0..10 {
    	if n % 2 == 0 {
    		continue; // If current iteration is positive, short circuit the look and move to the next iteration
    	}

    	println!("{} Hello, world!", n);
    }
}
