
/*
Rust’s match keyword is analogous to other languages' switch keyword. 
Unlike C’s switch however, match guarantees that all possible options 
for a type are explicitly handled. Additionally, a match does not“fall 
through”to the next option by default. Instead, match returns 
immediately when a match is found.
*/

fn main() {
	let item = 10;

	match item {
		0 			=> { println!("It is a Zero!"); }
		1 ..= 9 	=> { println!("It is between 1 and 9!"); }
		10 ..= 19 	=> { println!("It is between 10 and 19!"); }
		_ 			=> { println!("It is a Zero!"); } // in a match block, when _ is used, it will match all values. Use like a finally in a switch statement
	}
}
