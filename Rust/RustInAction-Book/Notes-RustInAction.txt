Notes - Rust in Action
----------------------

<<- Rust Toolchain ->>
rustc 			// Rust compiler
rustup			// Manages the Rust language installation (applows moving between compiler versions like RVM for Ruby or NVM for Node)

<<- Cargo -->>
cargo new		// Creates a new Rust project using the Cargo package manager
cargo build		// Builds the Rust project using rustc
cargo run 		// Runs the Rust project after compiling it with rustc
cargo doc		// Downloads the documentation for all crates being used in the Rust project (local access to docs if no internet connection)


<<- Basics -->>

Truthy/Falsey:
	Rust has no concept of "truthy" or "falsey" types. Other languages allow special values, such as 0 or an empty string, to stand for false and other values to represent true. Rust does not allow this. The only value that can be used for true is true and false is false.

Globals:
	Mutable global variables denoted with `static mut`
	Global variables use ALL CAPS

Const:
	Denotes variables where the value may never change once set

Keywords:
	unsafe: is a warning that the code being executed is at the same level of safety offered by C and Rust will not guarantee that it is safe
	pub: marks a function, struct, or enum as public code, accessible external to the module in which it was defined
		- enum variants are assumed to be public if the overall type is made public
		- even if a struct is made public, its implemented methods in `impl` must be explicitly marked as pub as well for them to be visible

Commenting Code:
	//! primarily used to annotate the current module
	/// generates documents that refer to the item that immediately follows it
	Use `rustdoc filename.rs` to generate documentation. Supports rich-text when documentation has been written using Markdown
	Use `cargo doc --open` to have Rust compile an HTML version of a crate's documentation

Primitive Types:
	- Implement the copy trait, this allows the values to be copied when otherwise it would be illegal. Pass-by-value rather than reference

Complex Types:
	- Do not implement the copy trait by default, pass-by-reference, which invokes all ownership and borrowing restrictions built into Rust
	- The copy trait can be implemented manually by implementing a drop method for the complex type.
		- Usually reserved for times when using unsafe code blocks where memory is allocated manually by the developer

<<- Numeric Types ->>

Numbers:
	The Rust standard library is slim when it comes to numeric types and excludes numeric types that are often available within other languages to handle:
	- rational numbers
	- complex numbers
	- arbitrary size integers and arbitrary precision floating point number for working with very large and very small numbers
	- fixed point decimal numbers for working with currencies
	Guidance:
	To handle currencies, complex numbers, or numbers with arbitrary precision, you need to use the num crate


<<- Memory Management ->>

Note:
	The ampersand (&) prefix to a variable acts as a unary operator and returns a reference to the memory location for the data that variable represents.
	- For any type T, the syntax &T returns a read-only reference to T


<<- Iteration ->>

Range Syntax:
	Exclusive: 0..5 	//will iterate 0-4, excluding 5
	Inclusive: 0..=5 	//will iterate 0-5, including 5

Iter():
	Some types provide iteration methods that understand how to iterate over a collection
	iter(): Returns a reference to each element in a collection
	iter_mut(): Allows the mutation of the values being provided in each iteration.
	into_iter(): Returns the value of each element in a collection (preventing other parts of the code from accessing those values)

<<- Functions ->>

Never Return:
	! is the never return type and is used when a function will never return after being called.


<<- Structs ->>

Structs:
	- see example: ch3-not-quite-file-1.rs
	- hold data
	- can have methods added to them through the use of an `impl` block
		- see example: ch3-file-doced.rs

Trait:
	Similar to interfaces, protocols, and contracts in other languages. They define the functions a struct will guarantee to have when it implements that trait.
		- see Example: Display Trait for printing to output streams ch3-implementing-display.rs


<<- Enums ->>

Enums:
	- Work with Rust's pattern matching capabilities
	- Support methods using the `impl` block just like structs
	- More powerful than a set of constants
	-


<<- Error Handling ->>

Result Type:
	An a type that represents both the standard and error case of execution.
	Has two states:
		- Ok
		- Err

<<- Barrow Checking, Ownership ->>

Three Concepts:
	- Ownership: relates to cleaning values up when they are no longer needed
	- Lifetime: the period when accessing a value is valid behavior. A function's local variables live until the function returns.
	- Borrow: has to do with accessing values. A value may have a single owner, but that owner can allow other parts of the program to access the value through borrowing (accessing it)

Shifting Ownership:
	- can be transfered through assignment
	- can be transfered by passing data through function barriers as an argument or as a return value

Strategies for dealing with ownership:
	- Use references where full ownership is not required
		- &T for read-only access
		- &mut T for read/write access
	- Duplicate the value
	- Refactoring code to reduce the number of long-lived objects
	- Wrap your data in a type designed to assist in movement issues
