fn main() {
    let items = vec!["This", "is", "a", "vector", "list", "of", "strings"];
    let mut samples = vec![];

    let mut counter = 0;

    while samples.len() < 7 {
    	samples.push(items[counter]);
    	counter += 1;
    }

    for i in 0..samples.len() {
    	println!("Sample #{}: {}", i+1, samples[i]);
    }
}
