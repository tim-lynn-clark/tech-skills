/* 1990's CHIP-8 Emulator */

/* CHIP-8 Variables
n - 4bits - low byte, low nibble - a number of bytes
x - 4bits - high byte, low nibble - a CPU register
y - 4bits - low byte, high nibble - a CPU register
c - 4bits - high byte, low nibble - an opcode group
kk - 8-bits - low byte, both nibbles - an integer
nnn - 12bits - high byte, low nibble and low byte, both nibbles - a memory address
*/

struct CPU {
	current_operation: u16,
	registers: [u8; 2];
}

impl CPU {

	/* CHIP-8 OpCodes
	Traditionally are u16 values made up of 4 nibbles (half a byte).
	Rust does not support nibbles (4-bit type), so we improvise and redefine them:
	OpCodes will be made up of two bytes, a high byte and a low byte.
	Each byte will be made up of two nibbles, the high and low nibbles.
	Example: 0x73EE
	Breakdown:
		0x denotes an operation
		73 high byte (u8)
			7 high nibble (u4)
			3 low nibble (u4)
		EE low byte (u8)
			E high nibble (u4)
			E low nibble (u4)
	*/
	fn read_opcode(&self) -> u16 {
		self.current_operation
	}

	fun run(&mut self) {
		loop {
			let opcode = self.read_opcode();

			let c = ((opcode & 0xF000) >> 12) as u8;
			let x = ((opcode & 0x0F00) >> 8) as u8;
			let y = ((opcode & 0x00F0) >> 4) as u8;
			let d = ((opcode & 0x000F) >> 0) as u8;

			match (c, x, y, d) {
				(0x8, _, _, 0x4) => self.add_xy(x, y),
				_ => todo!("opcode {:04x}", opcode),
			}
		}
	}

	fn add_xy(&mut self, x: u8, y: u8) {
		self.registers[x as usize] += self.registers[y as usize];
	}
}


fn main() {
	let mut cpu  = CPU {
		current_operation: 0,
		registers: [0; 2],
	};

	/* CHIP-8 OpCodes as defined by the chip's designers
	Operations involving two registers are grouped together with a left-hand 8. 
	0x8014 is the opcode for adding between register (0) and register (1). 
	The right-hand 4 indicates addition. 
	Code breakdown:
		0x  denotes an operation
		8   denotes operations involving two registers
		0	first register
		1	second register
		4	denotes the addition operation
	*/
	cpu.current_operation = 0x8014; 
	cpu.registers[0] = 5;
	cpu.registers[1] = 10;
}
