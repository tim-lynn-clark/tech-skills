struct CPU {
	current_operation: u16,
	registers: [u8; 2];
}


fn main() {
	let mut cpu  = CPU {
		current_operation: 0,
		registers: [0; 2],
	};

	/* Operations involving two registers are grouped together with a left-hand 8. 
	0x8014 is the opcode for adding between register (0) and register (1). 
	The right-hand 4 indicates addition. 
	Code breakdown:
		0x  denotes an operation
		8   denotes operations involving two registers
		0	first register
		1	second register
		4	denotes the addition operation
	*/
	cpu.current_operation = 0x8014; 
	cpu.registers[0] = 5;
	cpu.registers[1] = 10;
}
