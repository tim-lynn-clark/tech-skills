fn main() {
    let needle = 0xCB;
    let haystack = [1, 1, 2, 5, 15, 52, 203, 877, 4140, 21147];
  
    for item in haystack.into_iter() {
		// By using into_iter() instead of iter(), there is no need to use dereference (use *) to access the item
      if item == needle {
        println!("{}", item);
      }
    }
  }
  