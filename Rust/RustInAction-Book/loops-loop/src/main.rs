fn main() {
	let mut counter = 0;
    loop {
    	println!("Hello, world! {}", counter);
    	counter += 1;

    	if counter > 100 {
    		break; // discontinues iteration
    	}
    }
}
