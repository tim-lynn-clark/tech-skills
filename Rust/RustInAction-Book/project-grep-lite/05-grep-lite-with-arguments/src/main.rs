use regex::Regex;
use clap::{App, Arg};

fn main() {
	let args = App::new("grep-lite")
		.version("0.1")
		.about("Searches for patterns")
		.arg(Arg::with_name("pattern")
			.help("The pattern to search for")
			.takes_value(true)
			.required(true))
		.get_matches();

	//TODO: unwrap() "unwraps" a Result, crashing if an error occurs. Handling errors more robustly will be added later
	let pattern = args.value_of("pattern").unwrap();
    let reg = Regex::new(pattern).unwrap();

    let quote = "\
Every face, every shop, bedroom window, public-house, and
dark square is a picture feverishly turned--in search of what?
It is the same with books. What do we seek through millions of pages?";

	for (i, line) in quote.lines().enumerate() {
		let contains_substring = reg.find(line);
		match contains_substring {
			Some(_) => { // Some(T) is the postitive case of an Option
				let line_num = i + 1;
				println!("{}: {}", line_num, line);
			},
			None => (), // None is the negative case of an Option
		}
	}
}
