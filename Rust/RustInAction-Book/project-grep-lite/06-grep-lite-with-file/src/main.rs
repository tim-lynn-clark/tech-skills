use std::fs::File;
use std::io::BufReader; 
use std::io::prelude::*;
use regex::Regex;
use clap::{App, Arg};

fn main() {
	let args = App::new("grep-lite")
		.version("0.1")
		.about("Searches for patterns")
		.arg(Arg::with_name("pattern")
			.help("The pattern to search for")
			.takes_value(true)
			.required(true))
		.arg(Arg::with_name("input")
			.help("File to search")
			.takes_value(true)
			.required(true))
		.get_matches();

	//FIXME: unwrap() "unwraps" a Result, crashing if an error occurs. Handling errors more robustly will be added later
	let pattern = args.value_of("pattern").unwrap();
    let reg = Regex::new(pattern).unwrap();


    let input = args.value_of("input").unwrap();
    let f = File::open(input).unwrap();
    let reader = BufReader::new(f);

	for line_ in reader.lines() {
		let line = line_.unwrap();
		match reg.find(&line) {
			Some(_) => { // Some(T) is the postitive case of an Option
				println!("{}", line);
			},
			None => (), // None is the negative case of an Option
		}
	}
}
