use std::io;
use std::fs::File;
use std::io::BufReader; 
use std::io::prelude::*;
use regex::Regex;
use clap::{App, Arg};

fn main() {
	let args = App::new("grep-lite")
		.version("0.1")
		.about("Searches for patterns")
		.arg(Arg::with_name("pattern")
			.help("The pattern to search for")
			.takes_value(true)
			.required(true))
		.arg(Arg::with_name("input")
			.help("File to search")
			.takes_value(true)
			.required(true))
		.get_matches();

	//FIXME: unwrap() "unwraps" a Result, crashing if an error occurs. Handling errors more robustly will be added later
	let pattern = args.value_of("pattern").unwrap();
    let re = Regex::new(pattern).unwrap();


    let input = args.value_of("input").unwrap();


    if input == "_" {
    	let stdin = io::stdin();
    	let reader = stdin.lock();
    	process_lines(reader, re);
    } else {
		let f = File::open(input).unwrap();
    	let reader = BufReader::new(f);
    	process_lines(reader, re);
    }
}

fn process_lines<T: BufRead + Sized>(reader: T, re: Regex) {
	for line_ in reader.lines() {
		let line = line_.unwrap();
		match re.find(&line) {
			Some(_) => { // Some(T) is the postitive case of an Option
				println!("{}", line);
			},
			None => (), // None is the negative case of an Option
		}
	}
}