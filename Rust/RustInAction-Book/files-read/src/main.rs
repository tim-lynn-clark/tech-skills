use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

fn main() {
    let f = File::open("readme.md").unwrap(); // FIXME: App will crash if readme.md does not exist
    let mut reader = BufReader::new(f);

    let mut line = String::new();
    loop {
    	let len = reader.read_line(&mut line).unwrap(); // FIXME: App will crash if reading from disk fails
    	if len == 0 {
    		break;
    	}

    	println!("{} ({} bytes long)", line, len);
    	line.truncate(0); // clears the string so it can be resused on the next iteration without persisting the last stored string
    }
}
