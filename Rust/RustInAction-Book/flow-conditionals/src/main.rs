fn main() {
	// Boolean result
    let result = is_even(123456);
    println!("Result 1: {}", result);

    // Rust allows you to use the result of a conditional expression as a variable
    let result_two = if is_even(123456) {
    	"even"
    } else {
    	"odd"
    };
    println!("Result 2: {}", result_two);
}


fn is_even(a: i32) -> bool {
	if a % 2 == 0 {
		true
	} else {
		false
	}
}