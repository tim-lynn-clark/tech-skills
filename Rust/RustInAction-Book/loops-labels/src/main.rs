fn main() {
    
    'outer: for x in 0.. {
    	for y in 0.. {
    		for z in 0.. {

    			let sum = x + y + z;
    			println!("Sum: {}", sum);

    			if sum > 1000 {
    				break 'outer;
    			}
    		}
    	}
    }
}
