use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

fn main() {
    let f = File::open("readme.md").unwrap(); // FIXME: App will crash if readme.md does not exist
    let reader = BufReader::new(f);

    // NOTE: Using iterator provided by Buffered Reader
   	for line_ in reader.lines() {
   		let line = line_.unwrap(); // FIXME: App will crash if reading from disk fails
   		println!("{} ({} bytes long)", line, line.len());
   	}
}
