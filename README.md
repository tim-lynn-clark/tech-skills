# Tech Skills

Repository to hold code examples, demo apps, tutorial apps, code katas, and all other materials and assets created as I up-skill, retool, and learn new technologies.

Note: This repository contains code, apps, and assets imported from older private repositories the history of which was not imported with the code. 

Author: Tim Clark