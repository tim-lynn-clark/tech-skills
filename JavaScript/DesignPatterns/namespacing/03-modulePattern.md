# JavaScript Design Patterns
## Name-spacing

### Module Pattern
Solved the following problems in JavaScript

- Creating access control to data that should be private
- Providing a clear public API for custom libraries
- True encapsulation that was not built into JavaScript to start with

#### Utilizing IIFEs to Control access
Immediately Invoked Functional Expression

```JavaScript
  // IIFE will execute and return an object literal
  var myLibrary = (function(){

    // Private scope as everything within the anonymous function is hidden
    // Private variables
    let privateData = 'this is private stuff';
    // Private functions
    function doSomethingPrivate(){
      console.log('Just did something private.')
    }

    // The returned object contains the public API to the library
    return {
      doThis: function(){
        console.log('This was done.');
      },
      doThat: function(){
        console.log('That was done.');
        /*
        Accessible because this object was created within the scope of the
        anonymous function that was executed by the IIFE.
        */
        doSomethingPrivate();
      }
    }

  })();

  // Accessible because they are on the API object
  myLibrary.doThis();
  myLibrary.doThat();

  // NOT accessible because it is hidden within the scope of the anonymous function
  // doSomethingPrivate(); // Uncommenting this line will throw an error
```
