# JavaScript Design Patterns
## Name-spacing

### Module Reveal Pattern
Solves the following problems within the Module Pattern

- Ensuring private members cannot interact with public ones
- Reduce complexity introduced by the Module Pattern

Unlike the Module Pattern, the Module Reveal Pattern, does not place functions
and variables in the returned API object directly. Everything is created
within the private scope of the anonymous function.

Since both public and private members are within the same scope now, it is
important to differentiate between the two. This is commonly done by placing
an underscore in front of variable names that are private, to note them as such.
This of course requires that the developer be extra careful when using and
calling variables, making sure he is using the correct public or private one.

```JavaScript
  // IIFE will execute and return an object literal
  var myLibrary = (function(){

    // Private scope as everything within the anonymous function is hidden
    // Private variables
    let _privateData = 'this is private stuff';

    // Private functions
    function _doSomethingPrivate(){
      console.log('Just did something private.')
    }

    // Public API functions
    function doThis(){
      console.log('This was done.');
    }
    function doThat(){
      console.log('That was done.');
      _doSomethingPrivate();
    }

    // The returned object contains the public API to the library
    return {
      doThis,
      doThat
    }

  })();

  // Accessible because they are on the API object
  myLibrary.doThis();
  myLibrary.doThat();

  // NOT accessible because it is hidden within the scope of the anonymous function
  // doSomethingPrivate(); // Uncommenting this line will throw an error
```
