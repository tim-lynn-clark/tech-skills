# JavaScript Design Patterns
## Creational Patterns

### Builder Pattern
__Short Def:__ Focuses on constructing a complex object step by step.

__Long Def:__ The primary goal of this pattern is to simplify the creation of
complex objects and it provides the following advantages:

- Allows control over how and in what order the steps for building a complex
object occur
- Provides methods for varying the internal representation of a complex object
- Simplifies and adds organization to the construction of complex objects that
are built from other complex objects
- Often used to build aggregate/composite objects

#### Primary Components

__Component Parts:__ When using the builder pattern to create complex aggregate
objects, these are the tool used to create the component parts of the complex
object.

__Factories (optional):__ Even though factories are not necessarily a part of the
builder pattern and are optional when implementing it, they are still extremely
useful as the complexity of the building process increases.

__Builders:__ One or more builders may be used when implementing the builder
pattern, it all depends on just how complex the object being created is. If the
complex object being created depends on the creation of other complex objects,
the system may have builders being used within other builders.

__Director:__ This is the object responsible for gathering or receiving the
information that specifies or determines what complex object is to be built. Once
it has the information needed, it will kick of the appropriate builder and return
the needed complex object.


**Note*: components, builders, and directors can be plain old objects,
constructor functions, or classes depending on the system design and its specific
needs.

###### Script: Component parts used in example below
```JavaScript

// Design an example using cars

```
