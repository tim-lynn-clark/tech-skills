# JavaScript Design Patterns
## Creational Patterns

### Prototype Pattern

__Short Def:__ Instead of using traditional classes (templates) and the keyword
'new' to instantiate unique instances of a type, the prototype pattern uses existing
objects by cloning them and then enhancing them through adding properties or methods
to the object to meet the needs of the system.

__Long Def:__

This design pattern is probably the best supported pattern in JavaScript, This
is primarily due to the fact that JavaScript by nature is a prototypal language,
meaning that at its core, JavaScript uses the prototype pattern for all its built-in
objects creation mechanics.

The pattern solves the following system design issues:

- Reduces the cost, both memory & processor expense, inherent in the use of the
keyword 'new'
- Improves efficiency by removing duplication that is inherent in classical
inheritance hierarchies
- Allows objects that need to be created to be specified at run-time
- Provides the ability for object definitions to be dynamically loaded a
instantiated at run-time
