# JavaScript Design Patterns
## Structural Patterns

### Composite Pattern
In this pattern, the goal is to manipulate a group of objects that are of the same
or similar types as if they were one object. This is accomplished through the use
of a central orchestrating object that holds, within it, all of the objects that
need to be manipulated as one.

The components of this pattern are:

- Component:
  - The abstraction or interface used for all objects to be manipulated as one
  - Defines the properties and methods for accessing/manipulating a component
  - Defines the the methods for recursively accessing the component's parent and children components
- Leaf:
  - Represents leaf objects in the tree structure of the composition
  - Implements the component interface
- Composite (orchestrating object):
  - The root object in a composite structure, has one or more child components
  - Has methods allowing for the manipulation of child components (add, remove, etc.)
  - Implements the same interface as components
  - Delegates component interface calls on it to child components

##### Example Situation:
A group of files have been selected and need to be
renamed using a file name pattern provided by the user. In this situation:

- The files are represented by objects that implement a specific file component
interface
- The group of selected files is represented by a composite object that holds
references to all files to be manipulated.
- The selected file composite object implements the same file component interface

###### Example:
```JavaScript

```
