# JavaScript Design Patterns
## Structural Patterns

### Decorator Pattern
The intent of this pattern is to allow additional behaviors/abilities to be added
to a specific object instance without impacting other objects of the same type.
The pattern provides the following benefits:

- New functionality can be added to and removed from an object dynamically at
runtime
- Provides an alternative to subclassing when only minor behaviors need to be
added to one or more objects

This pattern is similar to the composite pattern with the exception being that
it performs the same wrapper functionality for a single object rather than a
collection of objects. The pattern prescribes:

- A wrapper object that encapsulates the object being modified and implements its
public interface is designed
- The wrapper object forwards all calls to the public interface (mimic of the
original object) to the original object
- The wrapper object overrides methods of the original object that need modification
- The wrapper implements the additional behaviors/abilities that are needed within
the system

##### Example Situation:


###### Example:
```JavaScript

```
