# JavaScript Design Patterns
## Structural Patterns

### Adapter Pattern

As its name implies, this pattern is all about adapting the public interface
of an object to meet the specifications of a different interface within the
system in which it is being used.

A situation in which the adapter pattern could be used is when utilizing a
third-party library. When pulling in objects designed by and implemented with
public interfaces that meet the needs of the author, it may be found that those
interfaces do not meet the needs of the system being developed. To handle this,
an adapter can be created to wrap those objects and expose their data and
functionality via public interfaces that better suits the needs of the system.

##### Example Situation:
An object is pulled in from a third-party library. That object must have a
specific interface to be used within the system but it is missing a required
method that is called frequently by the system. To resolve this, the third-party
object is wrapped in an adapter object that exposes what is necessary from that
object and adds the missing method to the object.

###### Example: Building an adapter for an object from an external library
```JavaScript

```
