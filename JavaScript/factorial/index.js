
function factorial(n){
    if(n < 0){
        throw new Error('n can not be negative');
    } else if(n === 0){
        return 1;
    }

    return n * factorial(n-1);
}

let result = factorial(5);
console.log(result);
