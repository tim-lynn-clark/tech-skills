"use strict";
// Advanced JavaScript Notes: Controlling Event Propagation
// ---------------------------------------------------------
//


// stopPropagation()
// ---------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/API/Event/stopPropagation
// Prevents further propagation of the current event in the capturing and
// bubbling phases.

// stopImmediatePropagation()
// ---------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/API/Event/stopImmediatePropagationhttps://developer.mozilla.org/en-US/docs/Web/API/Event/stopImmediatePropagation
// Prevents other listeners of the same event from being called.
// If several listeners are attached to the same element for the same event type,
// they are called in order in which they have been added. If during one such call,
// event.stopImmediatePropagation() is called, no remaining listeners will be called.


// preventDefault()
// ---------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault
// The Event interface's preventDefault() method tells the user agent that if
// the event does not get explicitly handled, its default action should not be
// taken as it normally would be. The event continues to propagate as usual,
// unless one of its event listeners calls stopPropagation() or
// stopImmediatePropagation(), either of which terminates propagation at once.


let nodes = Array.from(document.getElementsByClassName('node'));
nodes.forEach(function(node){
    node.addEventListener('click', function(event){
        // In this example, event execution will stop at the node with the class
        // stop-here because we are using stopPropagation and not preventDefault
        if(node.classList.contains('stop-here')) event.stopPropagation();
        console.log(node, event);
    }, true);
    // Changing which phase the event is connected to will change the order of
    // nodes until we hit the one with stop-here in its class list
});