/**/
processWeatherData(
    {
        "query":
        {
            "count": 1,
            "created": "2018-10-19T16:58:48Z",
            "lang": "en-US",
            "results":
            {
                "channel":
                {
                    "units":
                    {
                        "distance": "mi",
                        "pressure": "in",
                        "speed": "mph",
                        "temperature": "F"
                    },
                    "title": "Yahoo! Weather - Nome, AK, US",
                    "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/",
                    "description": "Yahoo! Weather for Nome, AK, US",
                    "language": "en-us",
                    "lastBuildDate": "Fri, 19 Oct 2018 08:58 AM AKDT",
                    "ttl": "60",
                    "location":
                    {
                        "city": "Nome",
                        "country": "United States",
                        "region": " AK"
                    },
                    "wind":
                    {
                        "chill": "32",
                        "direction": "45",
                        "speed": "16"
                    },
                    "atmosphere":
                    {
                        "humidity": "90",
                        "pressure": "1001.0",
                        "rising": "0",
                        "visibility": "15.8"
                    },
                    "astronomy":
                    {
                        "sunrise": "10:5 am",
                        "sunset": "7:26 pm"
                    },
                    "image":
                    {
                        "title": "Yahoo! Weather",
                        "width": "142",
                        "height": "18",
                        "link": "http://weather.yahoo.com",
                        "url": "http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif"
                    },
                    "item":
                    {
                        "title": "Conditions for Nome, AK, US at 08:00 AM AKDT",
                        "lat": "64.499474",
                        "long": "-165.405792",
                        "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/",
                        "pubDate": "Fri, 19 Oct 2018 08:00 AM AKDT",
                        "condition":
                        {
                            "code": "29",
                            "date": "Fri, 19 Oct 2018 08:00 AM AKDT",
                            "temp": "39",
                            "text": "Partly Cloudy"
                        },
                        "forecast":
                        [
                            {
                                "code": "30",
                                "date": "19 Oct 2018",
                                "day": "Fri",
                                "high": "44",
                                "low": "36",
                                "text": "Partly Cloudy"
                            },
                            {
                                "code": "34",
                                "date": "20 Oct 2018",
                                "day": "Sat",
                                "high": "39",
                                "low": "32",
                                "text": "Mostly Sunny"
                            },
                            {
                                "code": "34",
                                "date": "21 Oct 2018",
                                "day": "Sun",
                                "high": "37",
                                "low": "31",
                                "text": "Mostly Sunny"
                            },
                            {
                                "code": "30",
                                "date": "22 Oct 2018",
                                "day": "Mon",
                                "high": "37",
                                "low": "32",
                                "text": "Partly Cloudy"
                            },
                            {
                                "code": "30",
                                "date": "23 Oct 2018",
                                "day": "Tue",
                                "high": "39",
                                "low": "34",
                                "text": "Partly Cloudy"
                            },
                            {
                                "code": "28",
                                "date": "24 Oct 2018",
                                "day": "Wed",
                                "high": "40",
                                "low": "35",
                                "text": "Mostly Cloudy"
                            },                        {
                                "code": "28",
                                "date": "25 Oct 2018",
                                "day": "Thu",
                                "high": "39",
                                "low": "35",
                                "text": "Mostly Cloudy"
                            },
                            {
                                "code": "28",
                                "date": "26 Oct 2018",
                                "day": "Fri",
                                "high": "36",
                                "low": "34",
                                "text": "Mostly Cloudy"
                            },
                            {
                                "code": "30",
                                "date": "27 Oct 2018",
                                "day": "Sat",
                                "high": "35",
                                "low": "28",
                                "text": "Partly Cloudy"
                            },
                            {
                                "code": "30",
                                "date": "28 Oct 2018",
                                "day": "Sun",
                                "high": "29",
                                "low": "26",
                                "text": "Partly Cloudy"
                            }
                        ],
                        "description": "<![CDATA[<img src=\"http://l.yimg.com/a/i/us/we/52/29.gif\"/>\n<BR />\n<b>Current Conditions:</b>\n<BR />Partly Cloudy\n<BR />\n<BR />\n<b>Forecast:</b>\n<BR /> Fri - Partly Cloudy. High: 44Low: 36\n<BR /> Sat - Mostly Sunny. High: 39Low: 32\n<BR /> Sun - Mostly Sunny. High: 37Low: 31\n<BR /> Mon - Partly Cloudy. High: 37Low: 32\n<BR /> Tue - Partly Cloudy. High: 39Low: 34\n<BR />\n<BR />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-2460286/\">Full Forecast at Yahoo! Weather</a>\n<BR />\n<BR />\n<BR />\n]]>",
                        "guid":
                        {
                            "isPermaLink": "false"
                        }
                    }
                }
            }
        }
    }

);