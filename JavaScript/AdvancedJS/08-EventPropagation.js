"use strict";
// Advanced JavaScript Notes: Event Propagation
// ---------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events
// When an event is fired on an element that has parent elements (e.g. the <video>
// in our case), modern browsers run two different phases — the capturing phase
// and the bubbling phase.


// Event Capturing: Phase 1
// ---------------------------------------------------------
// 1. The browser checks to see if the element's outer-most ancestor (<html>) has
// an onclick event handler registered on it in the capturing phase, and runs
// it if so.
// 2. Then it moves on to the next element inside <html> and does the same thing,
// then the next one, and so on until it reaches the element that was actually
// clicked on.



// Event Bubbling: Phase 2
// ---------------------------------------------------------
// 1. The browser checks to see if the element that was actually clicked on has
// an onclick event handler registered on it in the bubbling phase, and runs it
// if so.
// 2. Then it moves on to the next immediate ancestor element and does the same
// thing, then the next one, and so on until it reaches the <html> element.
//
// In modern browsers, by default, all event handlers are registered in the
// bubbling phase.

// Event handlers in JavaScript can actually identify which phase of event handling
// they want to be handled.
let nodes = Array.from(document.getElementsByClassName('node'));
nodes.forEach(function(node){
    node.addEventListener('click', function(event){
        console.log(node, event);
    }, false);
    // the second argument to the addEventListener function is usually not included
    // when developers setup their event handlers. This second argument is used to
    // specify which event phase (capture or bubbling) is to be used for the event.
    // if not included or if included and set to 'false' it will be handled in the
    // bubbling phase, if set to true it will be handled in the capturing phase.

    // When false: you will see the events logged to the console starting with
    // the button node (bubbling phase)

    // When true: you will see the events logged to the console starting from the
    // body (capturing phase)

    // Because developers tend not to use the second argument to set the event phase
    // they want used, or because they are unaware of the second argument and how
    // event handling is done in JS, most developers think that events always
    // propagate from the node that was actually clicked, when in reality, it
    // always runs through both phases and handles events based on the phase
    // specified.

    // When understood, this can become a powerful tool in a JS developer's
    // toolbox when creating complex JS applications.
});