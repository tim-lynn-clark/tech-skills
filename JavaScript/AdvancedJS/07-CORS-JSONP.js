"use strict";
// Advanced JavaScript Notes: CORS and JSONP
// ---------------------------------------------------------
//

// -- CORS => Cross-Origin Resource Sharing
// By default, browsers do not all ow websites to request resources
// over HTTP across origins (domains).
// When CORS is active, this restriction is removed and requests can
// be made across origins (domains) and data can be shared.

// CORS handles this by sending a 'preflight request'
// this is done by sending an HTTP OPTIONS request with
// Access-Control-Request-Method header with the value set to the
// HTTP method you are inquiring as to whether or not the receiving
// domain allows CORS on

// Example: From moo.com to foo.com, wondering if foo.com will accept
// a PUT request from moo.com

// moo.com
// OPTIONS request to foo.com
// HTTP HEADERS => origin: moo.com, Access-Control-Request-Method: PUT

// foo.com
// HTTP response to moo.com
// HTTP HEADERS => Access-Control-Allow-Origin: moo.com, Access-Control-Allow-Methods: PUT

// in the response above, we see that foo.com will allow a PUT request from moo.com

// Example: using test-cors.org
//

// -- JSONP => Pre-dates the CORS standard
// Was the way to retrieve data across origins before CORS was implemented
// across browsers. Only works with GET requests (CORS works with all HTTP methods)
// JSONP wraps JSON data in a function
function myJSONPResponse(){
    return {
        'error': null,
        'data': [
            {
                'id': 1,
                'someValue': 'test'
            }
        ]
    }
};
// This works because browsers allow 'script' tags to be loaded across origins (domains).
// This feature is commonly used to pull in third-party libraries from remote
// content distribution networks for speed based on geolocation of the browser.
// Because of this, wrapping JSON data in a JS function, makes that data look like
// plain old JS and browsers will allow it to be pulled in across origins. This is
// also why JSONP is restricted to only HTTP GET requests.

// The process:
// When a script tag containing JS wrapped JSON is added to the HTML
// page:
// 1. the browser loads in the JS no matter the origin
// 2. then immediately executes that JS
// Because the JSON is wrapped in a JS function, that function can be executed
// in the receiving browser to retrieve the JSON data from the other domain.
// In a JSONP environment, it would be common practice for the data provider
// to provide API documentation that identifies the various function wrapper names
// they provide, so that the calling browser can provide the appropriate JS to
// execute those functions and pull out the data.

// Example:
// pretend that the myJSONPResponse JSONP response is on a different domain
// and was returned via an HTTP GET request as a .JS file
// the receiving browser would need add script to the HTML page and then
// execute the function to extract the JSONP data from the function in that script.
let returnedData = myJSONPResponse();
console.log(returnedData);
// *NOTE: unlike modern CORS requests that are usually done from calling
// browser as an AJAX requests, JSONP are done via standard HTTP GET's
// therefore the response JS containing the data does not show up in the XHR
// tab in browser developer tools. They show up in the regular HTTP request list
// as plain old responses to the HTTP GET that was sent.


// -- Real Example: Yahoo Weather API
//

function requestWeatherJSONP(url) {
    let script = document.createElement('script');
    script.src = url;

    let head = document.getElementsByTagName('head')[0];
    head.insertBefore(script, null);
}

function processWeatherData(data) {
    console.log(data);
}

// see file 07-YahooJSONPResponse.js to see what is sent back by Yahoo
let url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&format=jsonp&callback=processWeatherData';

requestWeatherJSONP(url);
