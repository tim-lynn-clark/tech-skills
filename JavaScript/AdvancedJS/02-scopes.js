"use strict"; 
// Advanced JavaScript Notes: Scopes
// ---------------------------------------------------------

// ---------------------------------------------------------
// Scope
// ---------------------------------------------------------
// The lifetime of a variable and where it can be accessed 
// used within a script. 

// -- Global Scope --
// Any variable that is declared in a script outside of all 
// code structures. When in the browser, all variables of 
// the window object are also available in the global scope.
// It does not go the other direction.

let thisIsGlobal = 1;
window.thisIsGlobalAsWell = 2;

console.log(thisIsGlobal); // 1
console.log(thisIsGlobalAsWell); // 2
console.log(window.thisIsGlobal); // undefined
console.log(window.thisIsGlobalAsWell); // 2

// -- Local Scope --
// Any variable declared inside a formal code structure
// such as a function.

function localScope() {
	let thisIsLocal = 3;
	console.log(thisIsLocal); // 3

	// All variables in enclosing scope are also available
	console.log(thisIsGlobal); // 1
	console.log(thisIsGlobalAsWell); // 2
}

// local scope variables are not available in containing scopes
//console.log(thisIsLocal); // Uncaught ReferenceError: thisIsLocal is not defined

localScope();


// -- Block-Level  Scope --
// Block-level scope exists only if you use let. If you use var to declare
// variables within your blocks, they will accessible outside the block scope.
// When using let to declare variables, they are only accessible inside the block scope.

for( var i=0; i<5; i++){
	var j = i;
	console.log('in block i:', i);
	console.log('in block j:', j);
}

console.log('outside block i:', i);
console.log('outside block j:', j);

for( let k=0; k<5; k++){
	let l = k;
	console.log('in block k:', k);
	console.log('in block l:', l);
}

// Uncaught ReferenceError: k is not defined
// console.log('outside block k:', k);
// Uncaught ReferenceError: l is not defined
//console.log('outside block l:', l);


// --  Scope Chaining --
// Variables are accessible to nested scopes

var availableToNested = 1;

funcion nestedFunctionScope() {
	var inNestedScope = 2;

	console.log(inNestedScope);
	console.log(availableToNested);
}