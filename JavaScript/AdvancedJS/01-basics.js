"use strict"; 
// Advanced JavaScript Notes: Basics
// ---------------------------------------------------------

// ---------------------------------------------------------
// "user strict"
// ---------------------------------------------------------
// - Allows you to place a function or script into a 'strict operating context', which means 
// 		code errors in your function or script that would normally be ignored or would fail 
// 		silently will now generate errors/warnings or throw terminal exceptions.
// - Provides errors when using reserved words or future reserved words
// - Throws errors when trying to delete used variables, functions, or function arguments
// - Makes the use of the keyword eval safer
// 		- will not let you accidentally override the function
//		- makes using the function to execute (evaluate then execute) JS in string format safer
//			this is done by encapsulating the string JS execution to it's own scope so variables do not
//			leak out into the surround code, which creates security issues. 
// * Simply put, it makes debugging easier.

// -- Usage --
// Placing the string at the top of your script will activate for the entire script
//"use strict"; 

// Placing the string at the top of a function activates it for just that function
// function strictFunction() {
// 	"use strict";
// }

// -- Example: Syntax Errors --
// <code>
// 		let theVal = 0;
// 		thVal = 1;
// </code>
// not so obvious syntax error that is not caught when out of strict mode
// without strict mode, no error or warning is thrown for this typo, making debugging difficult

// -- Example: eval function + variable leakage --
// Without strict mode, the variable 'a' in inside the string being executed
// within the eval function, will leak into the surrounding code and will override
// the 'a' variable declared in the regular code. The log would be 1 instead of the expected 2
// <code>
		let a = 2;
		console.log(`the value of a: ${a}`)
		eval("var a = 1");
		console.log(`the value of a has not changed: ${a}`);
// </code>
// In strict mode, the variable 'a' in the string being executed by the eval function
// is limited to the scope of that function. The log would be the expected 2 as no override
// occurred


// ---------------------------------------------------------
// Pass by Reference or Value?
// ---------------------------------------------------------
// Primitive types are passed by value
// Complex types (objects) are passed by reference

// -- Example: What is pass by value? --
// The copy of the value contained in the variable is passed
// <code>
		let b = 1;
		console.log(`value of a before passing: ${b}`);
		function foo(b){
			b = 2;
			console.log(`value of a inside foo after modification: ${b}`);
		}
		foo(b);
		console.log(`value of a after passing: ${b}`);
// </code>
// In the example above, the values printed to the console would be 1, 2, then 1.
// This is because numbers are primitive types and therefore when they are
// passed to another scope, a copy of their original value is passed, the original
// is not modified no matter what happens within the other scope.  
// This is why it is important that function taking in and manipulating primitive types
// always return the result of the manipulation and the caller catches that result and 
// assigns it to the original variable if it needs to be updated. 

// -- Example: what is pass by reference? --
// Complex objects sit on the heap and are referenced via their memory address (primitive type) 
// stored in the variable. When passing a complex type, the memory address (primitive value type)
// is copied and sent into the new scope and because the memory address references the 
// same complex object on the heap, any changes made to that object are made globally and
// every variable that holds a reference to that object, will see the changes to the internal
// structure of that object (change in data, added functions, etc.). 
// <code>
	 	let c = { test: 'test' };
	 	console.log(`the object c before passing:`, c);
	 	function boo(c){
	 		c.boo = "boooo";
	 	}
	 	boo(c);
	 	console.log(`the object c after passing:`, c);
// </code>
// In the example above, the external variable 'c' is passed to a new score, that of the function 'boo',
// a new property is added to the variable 'c' within the 'boo' scope, the external variable 'c' is then
// printed to the console and the new property exists.


// ---------------------------------------------------------
// Built-in Types
// ---------------------------------------------------------
// https://javascriptweblog.wordpress.com/2010/09/27/the-secret-life-of-javascript-primitives/ 
//
// -- Primitive Types --
// Boolean
// Number
// String
// Null
// Undefined
//
// -- Complete Types --
// Object
//
//
// -- Object Shadows and Coercion --
// The primitive types boolean, string, and number can be wrapped by their
// object counterparts by the interpreter and instantly move from being a
// primitive to becoming a complex type and then back again, all without the 
// express knowledge or permission of the developer. This called coercion.
// The corresponding objects they get wrapped by are commonly referred to as
// wrapper classes. These wrapper classes have methods and properties built into
// them and are used for manipulating the values they are wrapping.

console.log(typeof(1));
console.log(typeof('a'));
console.log(typeof(true));
console.log(typeof(undefined));

// null is one exception. 
// The typeof function has reported back null as an object for such a long time
// despite it being considered a primitive type, that it cannot be undone without
// breaking perhaps millions of scripts on the internet. 
console.log(typeof(null)); // One exception

console.log(typeof({}));


// ---------------------------------------------------------
// Differences between == and ===
// ---------------------------------------------------------
//

// Equality operator '=='
// This operator only checks the arguments on both side for 
// value equality. In other words, the value that each argument
// has at its core must be equal. The reason for the unexpected
// functionality, is due to coercion, JS tries to convert the values
// being compared into the same object type.

//false, this is expected as the two primitive strings are not equal as they do not have the same characters in them, in the same order
console.log('' == '0'); 

//true, even though one is a primitive number and the other is a primitive string with nothing in it
console.log(0 == ''); 	

//true, even though one is a primitive number and the other is a primitive string containing the number 0 character
console.log(0 == '0'); 	

//true, even though one is an empty primitive string and the other is a primitive string containing the number 0 character
console.log('' == '0'); 


// Strict equality operator '==='
// This operator checks the arguments on both side for both
// value and type equality. In other words, the arguments on 
// each side of the operator must contain the same value at
// their core and must be of the same JavaScript type 
// (primitive or complex)

// false, expected
console.log('' === '0'); 

// false, expected
console.log(0 === ''); 	

// false, expected
console.log(0 === '0'); 	

// false, expected
console.log('' === '0'); 


// ---------------------------------------------------------
// NaN and its uses
// ---------------------------------------------------------
// Stands for 'Not a Number'

console.log(NaN); // has a value of NaN

// 'number', which might be unexpected as it represents 
// something that is 'Not a Number'. It is commonly returned
// when a bad calculation happens involving number(s). 
console.log(typeof(NaN)); 

// -- NaN Gotchas --
// NaN compared to anything else is in JS evaluates to false
console.log(NaN == 1);
console.log(NaN == false);

// NaN compared to itself, is also false
// this is unexpected in that every other value in JS, when compared
// to itself will evaluate to true
console.log(NaN == NaN);
// This makes checking to see if a variable contains NaN does not work
let hasNaN = NaN;
console.log(hasNaN == NaN); // false
// To solve this, JS has a built-in function for check for NaN
console.log(isNaN(hasNaN));
// One problem with the 'isNaN' function is that it utilizes type coercion
// so it will give unexpected results such as
console.log(isNaN(1)); //false, expected
console.log(isNaN("1")); //false, unexpected as "1" is a string not a number
console.log(isNaN("A")); //true, expected as the string "A" is not a number and it cannot be coerced into one like "1"
// Because of this type coercion, the isNaN function is really not useful
// in that JS is a loosely typed language and you cannot be guaranteed that
// the variable passed to isNaN is actually number and not a string containing
// a number

// Best method for checking for NaN
// because NaN compared to itself is false and it is the only 
// JS object does this, we can reliably use the !== operator to check for
// NaN
console.log(hasNaN !== hasNaN);
hasNaN = 1;
console.log(hasNaN !== hasNaN);
hasNaN = 'a';
console.log(hasNaN !== hasNaN);


// ---------------------------------------------------------
// Recursion 
// ---------------------------------------------------------

// Factorial
console.log((function f(n){return ((n > 1) ? n * f(n-1) : n)})(4));