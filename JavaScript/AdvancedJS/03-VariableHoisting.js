"use strict"; 
// Advanced JavaScript Notes: Variable Hoisting
// ---------------------------------------------------------

//*NOTE: this is only applicable when using 'var' to declare variables
// it is better, safer, and more modern to use 'let' and 'const' to
// declare variables as they both solve a lot of these issues for you
// 'var' is antiquated and should rarely be used if at all. 


// ---------------------------------------------------------
// Hoisting
// ---------------------------------------------------------
// https://medium.freecodecamp.org/what-is-variable-hoisting-differentiating-between-var-let-and-const-in-es6-f1a70bb43d
// https://medium.freecodecamp.org/function-hoisting-hoisting-interview-questions-b6f91dbc2be8

// -- Example: Global Scope Hoisting --
// Especially in 'strict mode' you would expect that the following
// code would create an error when run due to the fact that the variable
// 'a' is not defined prior to it being used in the console.log statement. 
// However, due to variable hoisting,
// no error is thrown. Instead you get the following output.
console.log(a); // undefined
var a = 1;
console.log(a); // 1
// JavaScript implements what is called variable hoisting. 
// This is where the interpreter goes through the script it is
// about to execute, identifies all variables declarations
// splits them, places the initial variable declaration
// moves it to the top of the scope it lives in (script, function, block)
// and then leaves the variable assignment at the location it was found.

// The example above would look like this if rewritten by the interpreter
var b = undefined;
console.log(b); // undefined
b = 1;
console.log(b); // 1
// It is because of variable hoisting that the code does not error out
// as one might expect. 

// -- Example: 'let' fixes hoisting issues --
// *Note: this is not applicable when using 'let' to define your variables
// variables declared using let in strict mode, must first be declared prior to being used
//console.log(c); // Uncaught ReferenceError: c is not defined
let c = 1;
console.log(c); // 1


// -- Example: Function Scope Hoisting --
// With hoisting functions, the entire function definition is hoisted
// to the top of the script/scope
hoistIt();
function hoistIt() {
	console.log("hoistIt was hoisted...")
}

// -- Example: Function Expression Hoisting --
// When a function is stuffed into a variable via an expression
// the variable declaration is hoisted and can be referenced, but, 
// as in the variable example above, that variable is set to undefined
// and if an undefined variable is called as if it is an executable
// function you get an error.
// HoistAnotherOne(); //03-VariableHoisting.js:53 Uncaught TypeError: HoistAnotherOne is not a function
var HoistAnotherOne = function HoistAnotherOne() {
	console.log("HoistAnotherOne was not hoisted...");
};