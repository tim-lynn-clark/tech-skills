"use strict"; 
// Advanced JavaScript Notes: IIFE
// ---------------------------------------------------------
// Immediately Invoked Functional Expression

// Used to solve the issues of:

// 1. variable/function overriding 
// between scripts based on order in which they are loaded.
// -- script 1 -- var stuff = 1;
// -- script 2 -- var stuff = 2;
// script 1 loaded first script 2 second => stuff = 2
// script 2 loaded first script 1 second => stuff = 1

// 2. pollution of the global namespace, no encapsulation via
// namespacing is available as in other languages like C# and Java


// IIFEs are used to wrap custom scripts in a scope of their own
// creating a sudo-namespace for the custom code, protecting it from 
// being overridden or overriding other script's variables/functions.

// IIFE => ()()
// first set of parentheses create an executable scope, which must
// contain a function, commonly an anonymous function that wraps the
// script's code.
// second set of parentheses immediately execute that scope

// -- Example: Wrapped Script
let myLibrary = (function(){

	let test = 1;
	let something = {};

	return {
		test,
		something,
		printTest() { console.log('test:', test); },
		printSomething() { console.log('something:', something); }
	}

})();

let test = true;
let something = "this is not an object";

console.log(test);
console.log(something);
console.log(myLibrary.test);
console.log(myLibrary.something);
myLibrary.printTest();
myLibrary.printSomething();