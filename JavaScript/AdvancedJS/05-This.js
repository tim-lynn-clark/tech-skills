"use strict"; 
// Advanced JavaScript Notes: This keyword
// ---------------------------------------------------------
// 'this' is determined by context and the way in which items
// are called within a script. 


// this used at the root of a script always refers to the window 
// object (in browsers) or the global object (in Node.js).
console.log(this === window);
console.log(this);

this.myTest = 1; 
console.log(this.myTest);
console.log(window.myTest);
console.log(myTest);

var myOtherTest = 3;
console.log(this.myOtherTest);
console.log(window.myOtherTest);
console.log(myOtherTest);

// when not in 'strict mode' this used within the scope of a function 
// always references to the window object (in browsers) or the global 
// object (in Node.js).
// when in 'strict mode', this used within the scope of a function
// always returns undefined. 
function myFunction() {
	console.log(this);
}
myFunction();

// when a function is a method of an object, this refers to the 
// object to which the function belongs as a method, this is only true
// when the function is used in the context of a method of that object.
var myObject = {
	whatIsThis: function() {
		console.log(this);
	}
}
myObject.whatIsThis();

// when a function that is a method of an object is extracted for use 
// outside the context of the object, it reverts to behaving like a 
// regular function defined in the outerscope (see above: points to 
// window or is undefined depending on strict mode).
var borrowedFunction = myObject.whatIsThis;
borrowedFunction();


// functions embedded in object method function, do not use the context
// of the object, they default to regular function definition in the 
// outerscope (see above: points to window or is undefined depending on strict mode).

// Prior to fat arrow function, this stabilization was commonly used by 
// assigning this from the method to another variable for use within 
// nested function context

// Another stabilization option prior to fat arrow functions was to use
// the Function.prototype.bind() function to setup a function to use 
// a specified argument as its 'this' reference

// In conjunction with .bind(), Function.prototype.apply() can also be
// used to set the 'this' reference to a desired object. The difference 
// is that unlike .bind() which just takes an argument for 'this', .apply()
// also accepts an array of arguments to be passed to the function and then executes the function. 
// this removes the need to get a reference to the bound function and manually
// executing that function in different steps. Each has their uses. 

// Function.prototype.call() can also be used to set 'this' and bass in 
// arguments needed by the function. Instead of an array of arguments like .apply()
// .call() allows the individual arguments to the function to be provided individually

// Fat arrow function work differently than regular function definitions
// as they inherit the this reference from the context in which they are defined
// in this case it inherits the 'this' reference from the object method
// and there fore it refers back to the object. 
var myOtherObject = {
	callFunctioWithNestedFunction: function() {
		console.log("Method's this => ", this);

		// nested function
		function myNestedFunction() {
			console.log("Nested function's this => ", this);
		}
		myNestedFunction();

		// nested function using stabilized 'this' as 'self'
		var self = this;
		function myNestedFunctionStabilizedThis() {
			console.log("Nested function with stabilized this => ", self);	
		}
		myNestedFunctionStabilizedThis();

		// 'this' bound using .bind
		function myUnboundFunction(comment) {
			if(comment) console.log(comment);
			console.log("Bound nested function's this => ", this);
		}
		var myBoundFunction = myUnboundFunction.bind(this);
		myBoundFunction();

		// 'this' bound by apply and function immediately executed with arguments if provided
		myUnboundFunction.apply(this, ["Passing some arguments"]);

		// 'this' bound by call
		myUnboundFunction.call(this, "Oh yeah.");


		// nested fat arrow function
		var fatFunction = () => {
			console.log("Fat arrow function's this => ", this);	
		}
		fatFunction();
	}
}
myOtherObject.callFunctioWithNestedFunction();