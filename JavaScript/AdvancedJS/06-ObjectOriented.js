"use strict";
// Advanced JavaScript Notes: Object-Oriented Programing
// ---------------------------------------------------------
//

// Prototype Inheritance
// ---------------------------------------------------------
// Every object has a prototype object from which it was created (copied)
// and when looking for a property on an object, JavaScript will:
// 1. attempt to find the property on that object itself
// 2. if not found, will then search the object's prototype object
// 3. and so on and so on up the chain until it reaches the core JS object.

//

// -- ES6 Method of Assigning Object Prototype to Custom Parent Object --
// An object's prototype can be found through a __proto__ property on the object
let animal = {
	kind: 'generic animal object',
	moves: "I can't move yet, I do not know what I am.",
	says: function () {
		console.log(`I am a ${this.kind}.`);
	}
};

console.log(animal);
console.log(animal.__proto__);
// Being that animal is an object literal and does not inherit from any
// other object, it by default inherits from JavaScript's built-in Object,
// which comes with a lot of function and properties.

// Objects can have their __proto__ property directly set changing its
// original inheritance hierarchy at run time (can be very error prone).

let snail = {
	kind: 'snail',
	moves: "reallllllyyyyy sloooooowwwwly"
}

snail.__proto__ = animal;
console.log(snail);
console.log(snail.kind);
console.log(snail.__proto__.kind);
snail.says();
console.log(snail.__proto__.moves);
console.log(snail.moves);


// -- ES5 Method of Assigning Object Prototype to Custom Parent Object --
let blueBird = Object.create(animal);
// Manually set properties of the new object to be custom
blueBird.kind = 'blue bird';
blueBird.moves = 'fluttery';

console.log(blueBird);
console.log(blueBird.kind);
console.log(blueBird.moves);
blueBird.says();

// Passing object property values to the Object.create function so it can
// do it for you.
let crow = Object.create(animal, {
	kind: {value: "crow bird"},
	moves: {value: 'stealthy'}
});

console.log(crow);
console.log(crow.kind);
console.log(crow.moves);
crow.says();

// Pseudo-Classical Inheritance (the constructor pattern)
// ---------------------------------------------------------
// This is accomplished through the use of Constructor Functions in JavaScript (pre ES6)

function Lizard(kind, moves) {
	this.kind = kind;
	this.moves = moves;

	// methods can be added as properties
	this.says = function says() {
		console.log(`I am a ${this.kind}.`);
	}
}

// Methods can also be added through the prototype object
Lizard.prototype.bites = function() {
	console.log(`${this.kind} just bit me really hard as it ${this.moves} away.`);
};


let chameleon = new Lizard('chameleon', 'jerky');
console.log(chameleon);
console.log(chameleon.kind);
console.log(chameleon.moves);
chameleon.bites();
chameleon.says();

// Performing inheritance via a constructor and the new keyword
// Using new and a constructor function is kind of equivalent to:
var dragon = {
	dive: function() {
		console.log(`The ${this.kind} is diving directly at you.`);
	}
};
Lizard.call(dragon, 'dragon', 'flying');
console.log(dragon);
console.log(dragon.kind);
console.log(dragon.moves);
// The reason it is 'kind' of equivalent, is because the keyword 'new'
// actually does more that just make a call to a function
// it also manages setting up object prototypes and the inheritance of
// functions added to a constructor function's prototype object
// if the line below was uncommented, you would get an error because
// call does not setup prototype chaining like new does.
// dragon.bites(); // Uncaught TypeError: dragon.bites is not a function
dragon.says();
dragon.dive();


// Performing inheritance in the constructor function instead of externally
function SuperChameleon(name, kind, moves) {
	Lizard.call(this, kind, moves);
	this.name = name;
}

// The issue with the inheritance of the prototype object of the base object
// not being inherited properly (see lines 108-114 above and lines 133-135 below)
// is fixed by directly assigning the prototype
// of the parent constructor function to the the prototype of the child
// constructor function.
SuperChameleon.prototype = Object.create(Lizard.prototype);


let sparky = new SuperChameleon('sparky', 'super chameleon', 'smooth and rocky');
console.log(sparky);
console.log(sparky.kind);
console.log(sparky.moves);
// Same issue as using call externally, the inheritance is not setup properly and
// properties added to Lizard's prototype are not inherited (this was fixed by the addition of line 132 above)
sparky.bites(); // Uncaught TypeError: sparky.bites is not a function (this was fixed by the addition of line 132 above)
sparky.says();


// Pure Prototypal Inheritance
// ---------------------------------------------------------
// Without constructor functions

var Person = {
	init: function(firstName, lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		return this;
	},
	fullName: function() {
		return `${this.firstName} ${this.lastName}`;
	}
}

// -- using an init function to setup an object after creation --
var tim = Object.create(Person);
tim.init('Tim', 'Clark');
console.log(tim);
console.log(tim.fullName());

// -- using second argument to create function to setup object at creation --
var bree = Object.create(Person, {
	firstName: { value: 'Bree' },
	lastName: {value: 'Clark'}
});
console.log(bree);
console.log(bree.fullName());

// -- Using the factory pattern to create objects --
function PersonFactory(firstName, lastName) {
	var person = Object.create(Person);
	person.firstName = firstName;
	person.lastName = lastName;

	return person;
}

var fred = PersonFactory('Freddy', 'Krueger');
console.log(fred);
console.log(fred.fullName());
