const add = (x,y) => {
    return x+y;
};

const multiply = (x,y) => {
    return x*y;
};

//Exporting individual functions
export {add, multiply}

// Setting a default function to be exported
export default multiply