/**
 * Created by tclark on 3/30/17.
 */

//-----------------------------------------------------

// Using React + Promises
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
const weatherMapAPIKey = 'e11fa3bb95aeb528162a0fc169517287';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            city: 'Salt Lake City',
            description: ''
        }
    }

    componentDidMount(){
        this.grabWeather(this.state.city);
    }

    grabWeather(city){
        fetch(`http://api.openweathermap.org/data/2.5/weather?APPID=${weatherMapAPIKey}&q=${city}`)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    description: json.weather[0].description
                })
            });
    }

    render(){
        return (
            <div>
                <h1>Weather Report for {this.state.city}</h1>
                <h2>{this.state.description}</h2>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));


//-----------------------------------------------------

// import Entity from './entity';
//
// class Hobbit extends Entity {
//     constructor(name, height){
//         super(name, height);
//     }
//
//     //Override Greet
//     greet(){
//         super.greet();
//         console.log("I am from the shire, it's in middle earth.");
//     }
// }
//
// let Frodo = new Hobbit('Frodo Baggins', 4.5);
// console.log(Frodo);
// console.log(Frodo.name);
// Frodo.greet();


//-----------------------------------------------------

// // Modules: reusable encapsulated code
// import { students, total } from './students';
// console.log(students);
// console.log(total);

// // Importing specific functions
// import {add, multiply} from './calculator';
// console.log(add(2,3));
// console.log(multiply(2,3));

// // Importing default function
// import multiply from './calculator';
// console.log(multiply(2,3));

//-----------------------------------------------------

// // Filter method: returns new array based on initial array
//
// let isPassing = (grade) => {
//     return grade >= 70;
// };
//
// let scores = [90,85, 67, 71, 70, 55, 92];
// let passing = scores.filter(isPassing);
//
// console.log(passing);
//
// let failing = scores.filter((grade) => grade <= 69);
//
// console.log(failing);


//-----------------------------------------------------


// // Map Function
// let points = [10,20,30];
//
// let addOne = function(element){
//     return element + 1;
// };
//
// let increasedPoints = points.map(addOne);
//
// console.log('Traditional Map: ' + increasedPoints);
//
// let incPointsTwo = points.map(element => {
//     return element + 1;
// });
//
// console.log('Arrow Function Map: ' + incPointsTwo);
//
// let incPointsThree = points.map(element => element +1);
//
// console.log('Condensed Arrow Function Map: ' + incPointsThree);


//-----------------------------------------------------

// Arrow functions - anonymous functions

// Named function
// function blastoff(){
//     console.log("3.2.1.blastoff");
// }
// blastoff();

// //Anonymous function
// setTimeout(function(){
//     console.log("3.2.1.blastoff #1");
// }, 1000);
//
// //Arrow function
// setTimeout(() => {
//     console.log("3.2.1.blastoff #2");
// }, 1000);
//
// //Named Arrow function using a constant
// const blastoff = () => {
//     console.log("3.2.1.blastoff #3");
// };
// blastoff();
//
// //Named Arrow function using a let
// let blastoffTwo = () => {
//     console.log("3.2.1.blastoff #4");
// };
// blastoffTwo();

// Important Notes:
// Arrow functions do not bind their own this
// they bind the this of the surrounding scope
// this.a = 25;
//
// let print = function(){
//     this.a = 50;
//     console.log('this.a', this.a);
// };
//
// let arrowPrint = () => {
//     console.log('this.a in arrowPrint', this.a);
// };
//
// print();
// arrowPrint();

// let eye = "eye";
//
// const fire = (eye) => {
//     return `bulls-${eye}`;
// };
//
// console.log(fire(eye));


//-----------------------------------------------------

// // Destructuring Assignment with Objects
// let wizard  = {magical: true, power:10};
//
// //Old school
// // let magical = wizard.magical;
// // let power = wizard.power;
// // console.log(magical, power);
//
// //New school
// // let { magical, power} = wizard;
// // console.log(magical, power);
//
// // let ranger = {magical: false, power: 9};
// // let {magical, power} = ranger;
// // console.log(magical, power);
//
// //If variables are already declared
// let magical = true;
// let power = 2;
//
// let ranger = {magical: false, power: 9};
// // Wrapping in parens
// ({magical, power} = ranger);
// console.log(magical, power);

//-----------------------------------------------------

// // Destructuring Assignment with Arrays
// let c = [100, 200, 300];
// let a = c[0];
// let b = c[1];
//
// console.log(a,b);
//
// let [d, e] = c;
//
// console.log(d,e);
//
// let fellowship = ['Frodo', 'Fandalf', 'Aragorn'];
// let [hobbit, wizard, ranger] = fellowship;
//
// console.log(hobbit, wizard, ranger);
//
// let f = [100,200,300,400,500];
// let [g,...h] = f;
// console.log(g);
// console.log(h);


//-----------------------------------------------------

// //Spread operator
// let a = [7,8,9];
// let b = [6, ...a, 10];
//
// console.log(a);
// console.log(b);
//
// //Spread used in functions
// function print(a,b,c){
//     console.log(a,b,c);
// }
//
// let z = [1,2,3];
// //old school
// print(z[0], z[1], z[2]);
// //new school
// print(...z);
//
// function printModified(...z){
//     console.log(z);
// }
// printModified(1,2,3,4,5,6);


//-----------------------------------------------------

// //CONST: a constant variable
// const b = 2;
// //throws an error due to constants can only be assigned once
// //b = 3*4;
//
//
// //Template literals
// let c = 'hello';
// let d = 'world';
//
// //old school
// let e = c + ' ' + d;
// console.log(e);
//
// //new school
// let f = `${c} ${d}`;
// console.log(f);

//-----------------------------------------------------

// //let scoping
// let a = 'hello';
// console.log(a);
//
// {
//     let a = 'goodbye';
//     console.log('a inside scope ', a);
// }
//
// {
//     let salary = 90000;
// }
// //Will throw an error due to let scoping with block
// //console.log(salary);

//-----------------------------------------------------

