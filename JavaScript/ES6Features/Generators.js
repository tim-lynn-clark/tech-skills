// Generators
// ---------------------------------------
// What is a generator?
// A function that can be entered and exited multiple times (think yield blocks in Ruby)
// Through generators, we can execute code in the generator function, exit it, and then return back into it at the exact same place we left off

// Generator creation by placing a * directly after the function keyword or right before the function name with no spaces. 
// Yield is the keyword that, when used in a generator function, allows the function to pause and return to the caller 
// while saving a cursor as to where the execution of the function was paused so that when next() is called again on the 
// generator execution can pick back up at that spot.
function* numbers() {
	let counter = 0;
	counter++;
	console.log('counter: ' + counter);
	console.log('entering generator: numbers');
	yield;
	counter++;
	console.log('counter: ' + counter);
	console.log('middle of generator execution: numbers');
	yield;
	counter++;
	console.log('counter: ' + counter);
	console.log('exiting generator: numbers');
}

const gen = numbers();
console.log(gen.next()); //{ value: undefined, done: false } done with a false value indicates the function has not yet reached the end and is paused
console.log('=> external call #1 to gen.next complete...');
console.log(gen.next()); //{ value: undefined, done: false }
console.log('=> external call #2 to gen.next complete...');
console.log(gen.next()); //{ value: undefined, done: true } done with a true value indicates that the function has reached the end of its execution
console.log('=> external call #3 to gen.next complete...');

// Passing data back into a generator function at yield
let products = [
	{ id:1,  name: 'graphic t-shirt', 	type: 'clothing', 		quantity: 1, price: 15  },
	{ id:2,  name: 'rib-eye steak', 	type: 'meat', 			quantity: 3, price: 12  },
	{ id:3,  name: 'cucumber', 			type: 'vegetable', 		quantity: 2, price: 1   },
	{ id:4,  name: 'apple', 			type: 'fruit', 			quantity: 6, price: 1   },
	{ id:5,  name: 'whole wheat bread', type: 'baked goods', 	quantity: 4, price: 3   },
	{ id:6,  name: 'banana', 			type: 'fruit', 			quantity: 7, price: 1   },
	{ id:7,  name: 'butter lettuce',	type: 'vegetable', 		quantity: 3, price: 2   },
	{ id:8,  name: 'donuts', 			type: 'baked goods', 	quantity: 9, price: 6   },
	{ id:9,  name: 'tri-tip steak', 	type: 'meat', 			quantity: 4, price: 20  },
	{ id:10, name: 'white truffle', 	type: 'vegetable', 		quantity: 4, price: 120 },
];

function* shopingCart() {
	let selectedProducts = [];
	// get product one
	selectedProducts.push(yield 'select product 1');
	console.log(selectedProducts);

	// get product two
	selectedProducts.push(yield 'select product 2');
	console.log(selectedProducts);

	// get product three
	selectedProducts.push(yield 'select product 3');
	console.log(selectedProducts);

	// calculate shopping cart total
	let totalCost = selectedProducts.reduce((accumulator, product) => accumulator + product.price, 0);

	// get cash from user
	let cash = yield `please deposit $${totalCost}`;

	if(cash < totalCost)	cash += yield `You did not provide sufficient funds, please deposit $${totalCost-cash}.`;
	if(cash > totalCost)	return `Thank you. Your purchase was successful. $${cash-totalCost} is your change.`;
	if(cash === totalCost) 	return 'Thank you. Your purchase was successful.';
}

const cart = shopingCart();
console.log(cart.next());
console.log(cart.next(products[0]));
console.log(cart.next(products[3]));
console.log(cart.next(products[6]));
console.log(cart.next(10));
console.log(cart.next(8));


// What does a generator do?
// Really powerful when used in conjunction with for...of loops and iterating over widely different data structures

// Iteration with generators over complex objects

const testingTeam = {
	size: 3,
	lead: 'Jesus',
	testers: ['Sally', 'Jeremiah', 'Ternt']
}

const engineeringTeam = {
	testingTeam,
	size: 8,
	department: 'Engineering',
	projectManager: 'Laura',
	lead: 'Fred',
	engineers: ['Will', 'Sarah', 'Robert'],
	developers: ['Jack', 'Jill', 'Larry']
}

// Create a generator to use with a for...of loop to iterate over a team and pull only the attributes of concern
function* TestingTeamMemberIterator(team) {
	yield team.lead;

	for(let i=0; i<team.testers.length; i++){
		yield team.testers[i];
	}
}

function* EngineeringTeamMemberIterator(team) {
	yield team.projectManager;
	yield team.lead;

	for(let i=0; i<team.engineers.length; i++){
		yield team.engineers[i];
	}

	for(let i=0; i<team.developers.length; i++){
		yield team.developers[i];
	}

	// Generator delegation
	// using the yield* and an instance of another generator, you can have the parent generator work through the child generator's
	// yields until they are complete and gathering the results into an array
	const testingTeamMemberIterator = TestingTeamMemberIterator(team.testingTeam);
	yield* testingTeamMemberIterator;
}

const teamMemberNames = [];
for(let name of EngineeringTeamMemberIterator(engineeringTeam)) {
	teamMemberNames.push(name);
}

console.log(teamMemberNames);


