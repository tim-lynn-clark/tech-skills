// Promises
// ---------------------------------------
// 

let promise = new Promise((resolve, reject) => {
	 // can be resolved, meaning it was successfully executed
	 // resolve();

	 // or can be rejected, meaning an error occurred in execution
	 reject();
});

// Can attached event handles to handle either the resolving or rejecting of the promise
// promise.then(() => {
// 	// handle the resolving of the promise 
// 	console.log('Finally finished');
// });

// promise.catch(() => {
// 	// handle the errors that occurred
// 	console.log('An error occurred');
// });

// Promise event handlers are best when chained
promise
	.then(() => {
		console.log('Finally finished');
	})
	.then(() => {
		console.log('I also ran because the promise finished');
	})
	.catch(() => {
		console.log('Oh crap!');
	});