// Syntax Updates (expressive sugar)
// ---------------------------------------

// ---------------------------------------
// VAR
// ---------------------------------------
// Used to declare global-scope variables

// ---------------------------------------
// LET
// ---------------------------------------
// Used to declare variables whose value will change over time
// variables declared using LET live only within th scope in which they are declared
// Makes the code more readable, developers can instantly identify variables that should change over time

// ---------------------------------------
// CONST
// ---------------------------------------
// Used to declare variables whose value will never change over time (constant)
// Provides errors when code attempts to update the value of a CONST
// Makes the code more readable, developers can instantly identify variables that should not change over time


// ---------------------------------------
// ENHANCED OBJECT LITERALS
// ---------------------------------------
// Simply to remove extra keystrokes for the developer

const inventory = [
	{ title: 'Harry Potter', price: 10 },
	{ title: 'JavaScript the Good Parts', price: 20 },
	{ title: "Ender's Game", price: 25 },
];

// Traditional JS object factory function
function createBookShop(inventory) {
	return {
		inventory: inventory,
		inventoryValue: function() {
			return this.inventory.reduce((total, book) => total + book.price, 0);
		},
		priceForTitle: function(title) {
			return this.inventory.find(book => book.title === title).price;
		}
	}
}

const bookShopOne = createBookShop(inventory);
console.log(bookShopOne.inventoryValue());
console.log(bookShopOne.priceForTitle('Harry Potter'));

// Enhanced version
// - object properties that have the same name as the variable holding the value, you can now just specify the attribute name without doing the assignment
// - Object properties that are functions, you can now omit the property name and the keyword function and the name of the function will be used as the property name
function createBookShopAlt(inventory) {
	return {
		inventory,
		inventoryValue() {
			return this.inventory.reduce((total, book) => total + book.price, 0);
		},
		priceForTitle(title) {
			return this.inventory.find(book => book.title === title).price;
		}
	}
}

const bookShopTwo = createBookShopAlt(inventory);
console.log(bookShopTwo.inventoryValue());
console.log(bookShopTwo.priceForTitle('Harry Potter'));


// ---------------------------------------
// DEFAULT FUNCTION ARGUMENTS
// ---------------------------------------
// 

// Example: no default parameters
function makeAjaxRequest(url, method) {
	// Would need to check for argument values before proceeding
	if(!method) method = 'GET';

	// Do work here
}

makeAjaxRequest('https://www.google.com', 'GET');
makeAjaxRequest('https://www.google.com');

// Example: with default parameters
function makeAjaxRequest(url, method = 'GET') {
	// Do work here
}

makeAjaxRequest('https://www.google.com', 'GET');
makeAjaxRequest('https://www.google.com');

// Example: default user ID
function generateId() {
	return Math.random() * 99999999;
}

function User(id = generateId()) {
	this.id = id;
}

function createAdminUser(user = new User()) {
	user.admin = true;
	return user;
}

console.log(createAdminUser());

// ---------------------------------------
// REST AND SPREAD OPERATORS
// ---------------------------------------
// Introduced to reduce the amount of code written by the developer
let numbers = [1,2,3,4,5,10,20,30];


// Using reduce array helper on an array
function addNumbers(nums) {
	return nums.reduce((sum, num) => sum + num, 0);
}

console.log(addNumbers(numbers));

// What if the numbers passed in are not in an array
// REST operator (...) allows a function to take in an undefined number of arguments
// it will then wrap all of those arguments into a single array with the name provided
function addNumbersAlt(...nums) {
	return nums.reduce((sum, num) => sum + num, 0);
}

console.log(addNumbersAlt(1,2,3,4,5,10,20,30));

// SPREAD operator allows you to flatten out an array
// Example: taking two separate arrays and combining them without using an array helper method like concat
const colors = ['red', 'blue', 'yellow', 'green'];
const userColors = ['black', 'pink'];
const fallColors = ['fire red', 'crisp orange'];

const combinedColors = [...colors, ...userColors, ...fallColors, 'night blue'];
console.log(combinedColors);


// Example: using REST and SPREAD to verify something is in an array and adding it if it is not
function validateMilkIsInShoppingList(...items) {
	if(items.indexOf('milk') < 0) {
		return ['milk', ...items];
	}
	return items;
}

// No milk in list
let shoppingList = validateMilkIsInShoppingList('bananas', 'bread', 'eggs', 'steak');
console.log(shoppingList);

// Milk is there already
let shoppingList2 = validateMilkIsInShoppingList('bananas', 'bread', 'eggs', 'steak', 'milk');
console.log(shoppingList2);


// ---------------------------------------
// DESTRUCTURING
// ---------------------------------------
// 

let expense = {
	id: 1,
	type: 'business',
	amount: '$45 USD'
};

// ES5 Syntax
let typeAlt = expense.type;
let amountAlt = expense.amount;
console.log(`type: ${typeAlt}, amount: ${amountAlt}`);

// ES6 Syntax
let { type, amount } = expense;
console.log(`type: ${type}, amount: ${amount}`);

// ES6 Syntax: destructuring function parameters that are objects as they are passed to the function
function expenseSummary({id, type, amount}) {
	return `Expense #${id} is for ${type} and was in the amount of ${amount}`;
}

console.log(expenseSummary(expense));

// Destructuring arrays
const companies = ['Apple', 'Microsoft', 'IBM', 'Google', 'Facebook', 'Twitter', 'Uber', 'Lyft'];

let [name, ...rest] = companies;
console.log(name);
console.log(rest);

// Destructuring arrays of objects
const companyObjects = [
	{ name: 'Apple', location: 'Cupertino, CA'}, 
	{ name: 'Microsoft', location: 'Redmond, WA'}, 
	{ name: 'IBM', location: 'Armonk, NY'}
];

// [] states we are destructuring the array first
// {} states we get the first object in the array
// location states we look into that object and pull out the value of the property location of that object if it exists
let [{ location }] = companyObjects;
console.log(location);


// ---------------------------------------
// CLASSES
// ---------------------------------------
// Adds a traditional class inheritance layer on top of JavaScrupts built in prototype inheritance

class Car {
	constructor({ name, make, model }) {
		this.name  = name;
		this.make  = make;
		this.model = model;	
	}

	drive() {
		return `vrooom says ${this.name}`;
	}
}

let myCar = new Car({ name: 'Red Bullet', make: 'custom', model: 'custom' });
console.log(myCar);
console.log(myCar.drive());

class Toyota extends Car {

	constructor({ name, model, color }) {
		arguments[0].make  = 'Toyota';
		super(arguments[0]);

		this.color = color;
	}

	honk() {
		return `beep beep says ${this.name}`;
	}
}

let myToyota = new Toyota({ name: 'Blue Lightening', model: 'Tacoma', color: 'orange' });
console.log(myToyota);
console.log(myToyota.drive());
console.log(myToyota.honk());