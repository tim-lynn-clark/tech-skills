// Recursive Generators
// ---------------------------------------
// 

// Example: navigating a tree structure
class Comment {
	constructor(level, content, children = []) {
		this.level = level;
		this.content = content;
		this.children = children;
	}

	// adding [Symbol.iterator] to a class (built-in generator for a class)
	*[Symbol.iterator]() {
		yield this.level;
		yield this.content;
		for(let child of this.children){
			yield* child;
		}
	}
}

function buildChildNodes(level = 0) {
	return [
		new Comment(level, 'good comment'),
		new Comment(level, 'bad comment'),
		new Comment(level, 'meh')
	]
}

let levelOneChildren = buildChildNodes(1);
for(let node of levelOneChildren) {
	node.children = buildChildNodes(2);

	for(let innerNode of node.children) {
		innerNode.children = buildChildNodes(3);
	}
}

const tree = new Comment(0, 'Great post!', levelOneChildren);

for(let comment of tree) {
	console.log(comment);
}