// Array Helper Methods
// ---------------------------------------

// Utility arrays for examples
let colors = ['red', 'blue', 'yellow', 'green'];
let primaryColors = [
	{ color: 'red' }, 
	{ color: 'blue' }, 
	{ color: 'yellow' }
];
let numbers = [1,2,3,4,5,10,20,30];
let cars = [
	{ make: 'Jeep', price: 35000 },
	{ make: 'Mini', price: 25000 },
	{ make: 'Kia',  price: 10000 }
];
let posts = [
	{ id: 1, createdOn: '10/12/2018', title: 'This is my first post', text: 'Lorem ipsom something post 1'},
	{ id: 2, createdOn: '10/13/2018', title: 'This is my second post', text: 'Lorem ipsom something post 2'},
	{ id: 3, createdOn: '10/14/2018', title: 'This is my third post', text: 'Lorem ipsom something post 3'},
];
let comments = [
	{ id: 1, postId: 1, text: 'This is comment #1 on post #1'},
	{ id: 2, postId: 2, text: 'This is comment #1 on post #2'},
	{ id: 3, postId: 3, text: 'This is comment #1 on post #3'},
	{ id: 4, postId: 1, text: 'This is comment #2 on post #1'},
	{ id: 5, postId: 1, text: 'This is comment #3 on post #1'},
	{ id: 6, postId: 1, text: 'This is comment #4 on post #1'},
];
let products = [
	{ id:1,  name: 'graphic t-shirt', 	type: 'clothing', 		quantity: 1, price: 15  },
	{ id:2,  name: 'rib-eye steak', 	type: 'meat', 			quantity: 3, price: 12  },
	{ id:3,  name: 'cucumber', 			type: 'vegetable', 		quantity: 2, price: 1   },
	{ id:4,  name: 'apple', 			type: 'fruit', 			quantity: 6, price: 1   },
	{ id:5,  name: 'whole wheat bread', type: 'baked goods', 	quantity: 4, price: 3   },
	{ id:6,  name: 'banana', 			type: 'fruit', 			quantity: 7, price: 1   },
	{ id:7,  name: 'butter lettuce',	type: 'vegetable', 		quantity: 3, price: 2   },
	{ id:8,  name: 'donuts', 			type: 'baked goods', 	quantity: 9, price: 6   },
	{ id:9,  name: 'tri-tip steak', 	type: 'meat', 			quantity: 4, price: 20  },
	{ id:10, name: 'white truffle', 	type: 'vegetable', 		quantity: 4, price: 120 },
];
let computers = [
	{ id: 1, brand: 'Apple',  model: 'Macbook Pro', ram: 16, processors: 4, speed: 2.9 },
	{ id: 2, brand: 'Dell',   model: 'Inspirion',   ram: 8,  processors: 4, speed: 3.4 },
	{ id: 3, brand: 'HP',     model: '43',          ram: 8,  processors: 2, speed: 1.8 },
	{ id: 4, brand: 'Lenovo', model: 'R-Series ',   ram: 4,  processors: 1, speed: 0.9 },
];

// Utility functions for examples
const arrMin = arr => Math.min(...arr);
const arrMax = arr => Math.max(...arr);
const arrSum = arr => arr.reduce((a,b) => a + b, 0)
const arrAvg = arr => arr.reduce((a,b) => a + b, 0) / arr.length;


// ---------------------------------------
// CLASSIC ITERATION
// ---------------------------------------

// Classic array looping with 'for' loop
for(let i = 0; i < colors.length; i++) {
	console.log(colors[i]);
}


// ---------------------------------------
// FOREACH
// ---------------------------------------
// Takes an anonymous function and executes that function for each item in the array.

colors.forEach (function(color) {
	console.log(color);
});

colors.forEach ((color) => { console.log(color) });
colors.forEach (color => console.log(color));

let sum = 0;

numbers.forEach(number => sum += number);

console.log(sum);

// ---------------------------------------
// REDUCE
// ---------------------------------------
//

for(let color of colors) {
	console.log(color);
}


// ---------------------------------------
// MAP
// ---------------------------------------
// Most widely used helper - takes an anonymous function and executes that function for 
// each item in the array, the return value of the anonymous function is then placed in a new 
// array by the map function and when iteration is complete it returns that array to the caller.

let doubledNumbers = numbers.map(number => number * 2); 
// ES6 shortcut anonymous functions automatically return the result of number * 2
// in a more complex anonymous function, you need to make sure you use the return key word

console.log(doubledNumbers);
console.log(numbers);

// Commonly used to collect a list of properties off of a collection of objects
// in order to do something with just that data
// Example: pulling just the prices so you can calculate statistics (average, median, mean)
let prices = cars.map(car => car.price);
console.log(prices);
console.log(arrMin(prices));
console.log(arrMax(prices));
console.log(arrAvg(prices));

// Commonly used in front-end web development for rendering lists of data that need to be 
// transformed in some manner (wrapping attributes in HTML) before being placed in the view
// and without modifying the original array of data
let postViews = posts.map((post) => {
	return `<article id="${post.id}"><h3>${post.title}</h3><span>${post.text}</span></article>`;
});
console.log(postViews);


// ---------------------------------------
// FILTER
// ---------------------------------------
// Commonly used to filter lists of objects on a particular property
// Example: get only vegetables
let filteredProducts = products.filter(product => product.type === 'vegetable');
console.log(filteredProducts);

// Example: get only vegetables with a price lower than 100
let lowCostVegetables = products.filter(product => product.type === 'vegetable' && product.price < 100);
console.log(lowCostVegetables);

//Example: filtering through comments to find those that belong to a specific post
function commentsForPost(post, comments) {
	return comments.filter(comment => comment.postId === post.id);
}
console.log(commentsForPost(posts[0], comments));

//Challenge: This is a hard one!  Create a function called 'reject'.  Reject should work in the 
//opposite way of 'filter' - if a function returns 'true', the item should *not* be included 
//in the new array.  Hint: you can reuse filter.

function reject(array, iteratorFunction) {
	let toReject = array.filter(iteratorFunction);
	return array.filter(number => !toReject.includes(number));
}

let lessThanFifteen = reject(numbers, function(number){
   return number > 15;
});;
console.log(lessThanFifteen) // [ 1,2,3,4,5,10 ];

// ---------------------------------------
// FIND
// ---------------------------------------
// Search through an array and look for a specific item that matches the search criteria in the 
// array and return the first item that matches the moment it is found and short-circuiting the iteration
let car = cars.find(car => car.make === 'Jeep');
console.log(car);


// Generic find method
function findWhere(array, criteria) {
  return array.find((item) => {
  	let key = Object.keys(criteria)[0];
  	return item[key] === criteria[key];
  });
}
console.log(findWhere(cars, {price: 35000}));


// ---------------------------------------
// EVERY
// ---------------------------------------
// The every() method tests whether all elements in the array pass the test implemented by the provided function.

// Example: verify whether or not all computers in the list can run a program with specific requirements
// useful if running a computer lab and needing to deploy an update to Photoshop that has to be on all computers
let allComputersCompatible = computers.every(computer => computer.ram > 8);
console.log(allComputersCompatible); //[false] there is at least one computer that does not meet the criteria


// ---------------------------------------
// SOME
// ---------------------------------------
// The some() method tests whether at least one element in the array passes the test implemented by the provided function.

// Example: verify whether or not a computer in the list can run a program with specific requirements
// useful if only a single student needs a computer that can run a specific piece of software for a project
let oneComputersCompatible = computers.some(computer => computer.ram > 8);
console.log(oneComputersCompatible); //[true] there is at least one computer that does meet the criteria


// ---------------------------------------
// REDUCE
// ---------------------------------------
// The reduce() method executes a reducer function (that you provide) on each member of the array resulting in a single output value.

// Example: sum all number in the numbers array
let sumNum = numbers.reduce((sum, number) => sum + number, 0);
console.log(sumNum);

// Example: using reduce with an array to collect property values from an array of objects
let selectedColors = primaryColors.reduce(function(accumulator, primaryColor) {
	accumulator.push(primaryColor.color);
	return accumulator;
}, []);
console.log(selectedColors);

// Example: balanced parenthesis problem, done in interviews sometimes
let trueTest1 = "()()()()";
let trueTest2 = "(((())))";
let falseTest1 = ")))";
let falseTest2 = "())))";
let falseTest3 = ")(";
let falseTest4 = ")()(";
let falseTest5 = "()(";

function isParenBalanced(string) {
	// Positive or negative numbers are truthy, zero is falsy
	return !string.split("").reduce(function(counter, char) {
		if(counter < 0) { return counter; } // Handles the case were you have the correct number of opening and closing parens but they are in the wrong order
		if(char === '(') { return ++counter; }
		if(char === ')') { return --counter; }
		return counter;
	}, 0); 
	//the initial value of 0 will be the counter
	//every time we run into an opening paren we will increment the counter by 1
	//every time we run into a closing paren we will decrement the counter by 1
	//upon completion of array iteration
		// if counter is > 0, there were too many opening parens without corresponding closing parens
		// if counter is === 0, there were exactly as many opening parens as closing parens
		// if counter is < 0, there were too many closing parens without corresponding opening parens

}

console.log(isParenBalanced(trueTest1));
console.log(isParenBalanced(trueTest2));
console.log(isParenBalanced(falseTest1));
console.log(isParenBalanced(falseTest2));
console.log(isParenBalanced(falseTest3));
console.log(isParenBalanced(falseTest4));
console.log(isParenBalanced(falseTest5));


// Example: create a unique function that will remove duplicates from an array
function unique(array) {
  return array.reduce((accumulator, item) => {
     if(!accumulator.find(val => val === item)) accumulator.push(item);
     return accumulator;
  }, []);
}

let dupNumbers = [1,1,2,3,4,4];
console.log(unique(dupNumbers));