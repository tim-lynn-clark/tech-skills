// Fetch for Ajax with Promises
// ---------------------------------------
// Built into the browser
// Better to use a 3rd party library like Axios

let url = "https://jsonplaceholder.typicode.com/posts/";

let fetchPromise = fetch(url);

console.log(fetchPromise);

fetchPromise
	.then(response => response.json())
	.then(data => console.log(data))
	.catch(() => {

	})