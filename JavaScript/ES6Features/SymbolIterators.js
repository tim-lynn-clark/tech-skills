// Generators with Symbol.iterator
// ---------------------------------------
// Tool that teaches complex objects how to respond to a for...of loops on their own

const testingTeam = {
	size: 3,
	lead: 'Jesus',
	testers: ['Sally', 'Jeremiah', 'Trent'],
	[Symbol.iterator]: function* () {
		// Tool that teaches complex objects how to respond to a for...of loops on their own
		yield this.lead;

		for(let i=0; i<this.testers.length; i++){
			yield this.testers[i];
		}
	}
}

const engineeringTeam = {
	testingTeam,
	size: 8,
	department: 'Engineering',
	projectManager: 'Laura',
	lead: 'Fred',
	engineers: ['Will', 'Sarah', 'Robert'],
	developers: ['Jack', 'Jill', 'Larry'],
	[Symbol.iterator]: function* () {
		// Tool that teaches complex objects how to respond to a for...of loops on their own
		yield this.projectManager;
		yield this.lead;

		for(let i=0; i<this.engineers.length; i++){
			yield this.engineers[i];
		}

		for(let i=0; i<this.developers.length; i++){
			yield this.developers[i];
		}

		// Generator delegation 
		// Because the testing team object as a [Symbol.iterator] defined (built in generator)
		// there is no need to expressly identify a generator and execute it
		// you can now simply use the yield* on the object itself
		yield* this.testingTeam;
	}
}

const teamMemberNames = [];
for(let name of engineeringTeam) {
	teamMemberNames.push(name);
}

console.log(teamMemberNames);


