console.log("Hello from service-worker.js");

/* You can alter the default cache names by altering all or some of these pieces of information
 * These prefixes will be used within the Workbox libraries, but are not used in the caching strategies below
 * they will have the exact names you specify. So you will need to prefix them yourself.*/
const prefix = "first-service-worker";
const suffix = "v1";
import { setCacheNameDetails } from "workbox-core";
setCacheNameDetails({
  prefix,
  suffix,
});

const prefixCacheName = (name) => `${prefix}-${suffix}-${name}`;

// Precaching files
// import { precacheAndRoute } from "workbox-precaching";
// precacheAndRoute([{ url: "/TestCaching01.html", revision: null }]);

// Caching strategies
// https://developers.google.com/web/tools/workbox/guides/common-recipes
// https://developers.google.com/web/tools/workbox/guides/advanced-recipes
import { registerRoute } from "workbox-routing";
import {
  NetworkFirst,
  StaleWhileRevalidate,
  CacheFirst,
} from "workbox-strategies";

// Used for filtering matches based on status code, header, or both
import { CacheableResponsePlugin } from "workbox-cacheable-response";
// Used to limit entries in cache, remove entries after a certain period of time
import { ExpirationPlugin } from "workbox-expiration";

// Cache page navigations (html) with a Network First strategy
registerRoute(
  // Check to see if the request is a navigation to a new page
  ({ request }) => request.mode === "navigate",
  // Use a Network First caching strategy
  new NetworkFirst({
    // Put all cached files in a cache named 'pages'
    cacheName: prefixCacheName("pages"),
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
    ],
  })
);

// Cache CSS, JS, and Web Worker requests with a Stale While Revalidate strategy
registerRoute(
  // Check to see if the request's destination is style for stylesheets, script for JavaScript, or worker for web worker
  ({ request }) =>
    request.destination === "style" ||
    request.destination === "script" ||
    request.destination === "worker",
  // Use a Stale While Revalidate caching strategy
  new StaleWhileRevalidate({
    // Put all cached files in a cache named 'assets'
    cacheName: prefixCacheName("assets"),
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
    ],
  })
);

// Cache images with a Cache First strategy
registerRoute(
  // Check to see if the request's destination is style for an image
  ({ request }) => request.destination === "image",
  // Use a Cache First caching strategy
  new CacheFirst({
    // Put all cached files in a cache named 'images'
    cacheName: prefixCacheName("images"),
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new CacheableResponsePlugin({
        statuses: [200],
      }),
      // Don't cache more than 50 items, and expire them after 30 days
      new ExpirationPlugin({
        maxEntries: 50,
        maxAgeSeconds: 60 * 60 * 24 * 30, // 30 Days
      }),
    ],
  })
);
