const path = require("path");
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = {
  entry: "./src/imageProcessor.js",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new WorkboxPlugin.GenerateSW({
      runtimeCaching: [{
        urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
        handler: 'CacheFirst',
        options: {
          cacheName: 'ordered-image-loader-images',
          expiration: {
            maxEntries: 50,
            maxAgeSeconds: 60 * 60 * 24 * 30, // 30 Days
          },
        },
      }],
    })
  ]
};
