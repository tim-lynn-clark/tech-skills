const assetPath = "/assets/images/";
const imageStore = {};
let totalImages = 0;
let totalProcessed = 0;

// TODO: Remove after testing -> Simulates long network requests
function delay() {
    const wait = Math.random() * 1000;
    return new Promise(resolve => setTimeout(resolve, wait));
}

function extractImageMetaData(image) {
    let imageOrder = image.getAttribute('data-order');
    imageOrder = parseInt(imageOrder, 10);
    if(isNaN(imageOrder)) {
        return new Error("Invalid Format: Order could not be parsed.");
    }

    const imageName = image.getAttribute('data-src');
    const imagePath = `${assetPath}${imageName}`;
    const imageMetaData = {
        element: image,
        path: imagePath,
        data: null,
        loaded: null,
    };
    imageStore[imageOrder] = imageMetaData;
    return imageOrder;
}

function fileLoadedEnclosure(imageKey, reader) {
    return function fileLoaded() {
        imageStore[imageKey].data = reader.result;
        totalProcessed++;
        if(totalProcessed === totalImages) {
            loadImages();
        }
    }
}

async function fetchImageData(imageKey) {
    const response = await fetch(imageStore[imageKey].path);
    const imageData = await response.blob();
    const reader = new FileReader();
    reader.readAsDataURL(imageData);
    reader.onloadend = fileLoadedEnclosure(imageKey, reader);
}

function loadImages() {
    let keys = Object.keys(imageStore);
    keys = keys.sort()
    keys.map(key => {
        const image = imageStore[key];
        if(!image.loaded && image.data) {
            image.element.setAttribute(
                'src',
                image.data
            )
            totalProcessed++;
        }
    })
}

function imageProcessor() {
    const images = document.querySelectorAll('[data-src]');
    totalImages = images.length;
    images.forEach(async img => {

        // TODO: Remove after testing -> Simulates long network requests
        await delay();

        try{
            const imageKey = extractImageMetaData(img);
            await fetchImageData(imageKey);
        } catch (err) {
            console.log("<<-- FETCH ERROR -->>", err);
        }
    });
}

window.imageProcessor = imageProcessor;